<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use App\DonateOrder;
use App\SubPagesWithPdf;

class PaymentService extends Model
{
    public static function orderRegister($insert_id, $amount, $lang)
    {
        $payment_row = DonateOrder::find($insert_id);
        $project_id = $payment_row->project_id;
        $project = SubPagesWithPdf::find($project_id);
        $description = '';
        if ($lang == "am") {
            $language = "hy";
            $project_title = $project->title_am;
        } else {
            $language = $lang;
            $project_title = $project->title_{$lang};
        }
        $description = app('App\Http\Controllers\GlossaryController')->getTranslate("donate_description", $lang) . " " . $project_title;

        $responce = self::callIpayApi($insert_id, $amount, $language, $description);
        $array_resp = json_decode($responce, true);
        if (isset($array_resp['errorCode']) && $array_resp['errorCode'] == '0') {
            $payment_row->paymentid = $array_resp['orderId'];
            $payment_row->save();
            $formUrl = $array_resp['formUrl'];
        } else {
            $payment_row->status = $array_resp['errorCode'];
            $payment_row->save();
            if ($lang == "am") {
                $formUrl = url('/') . '/fail/';
            } else {
                $formUrl = url('/') . "/" . $lang . '/fail/';
            }
        }
        return redirect($formUrl);
    }

    public static function getOrderStatusExtended($orderId, $lang)
    {
        $responce = self::callOrderStatusExtendedApi($orderId);
       // return $responce;
        $array_result = json_decode($responce);
        $payment_row = DonateOrder::where('paymentid', $orderId)->first();
        //return $lang;
        if ($lang == "am") {
            $base_url = url('/');
        } else {
            $base_url = url('/') . "/" . $lang;
        }

        if (isset($payment_row) && !empty($payment_row)) {
            $all_cost = $payment_row->amount;
            if ($array_result->errorCode == '0' && $array_result->orderStatus == '2' && ($all_cost * 100) == $array_result->paymentAmountInfo->depositedAmount) {
                $payment_row->status = 55;
                $payment_row->save();
                $formUrl = $base_url.'/success';
            } else {
                $payment_row->status = $array_result->errorCode;
                $payment_row->save();
                $formUrl = $base_url . '/fail';
            }
        } else {
            $formUrl = $base_url . '/fail';
        }
        //return $formUrl;
        return redirect($formUrl);
    }

    public static function callIpayApi($insert_id, $amount, $language, $description)
    {
        $endpoint_url = env('PAYMENT_REGISTER_URL');
        $login = env('PAYMENT_LOGIN');
        $password = env('PAYMENT_PASSWORD');
        if ($language == "hy") {
            $returnUrl = url('/') . "/" . env('RETURN_URL');
        } else {
            $returnUrl = url('/') . "/" . $language . "/" . env('RETURN_URL');
        }
        $pay_amount = 100 * $amount;
        $request = new Client();
        $param_array = array('userName' => $login, 'password' => $password, 'language' => $language, 'orderNumber' => $insert_id, 'amount' => $pay_amount, 'currency' => '840', 'description' => $description, 'returnUrl' => $returnUrl);
        try {
            $results = $request->request('GET', $endpoint_url . http_build_query($param_array), [])->getBody()->getContents();
        } catch (ClientException $e) {
            $results = $e;
        }
        return $results;
    }

    public static function callOrderStatusExtendedApi($orderId)
    {
        $endpoint_url = env('PAYMENT_ORDER_STATUS_EXTENDED_URL');
        $login = env('PAYMENT_LOGIN');
        $password = env('PAYMENT_PASSWORD');
        $request = new Client();
        $param_array = array('userName' => $login, 'password' => $password, 'orderId' => $orderId);
        try {
            $results = $request->request('GET', $endpoint_url . http_build_query($param_array), [])->getBody()->getContents();
        } catch (ClientException $e) {
            $results = $e;
        }
        return $results;
    }
}