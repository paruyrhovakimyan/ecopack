<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Coach;

class CoachCategory extends Model
{
    public function chaches($id)
    {
        $result = array();
        $alls = Coach::orderby("coach_order", "asc")->get();
        foreach ($alls as $all){
            if(in_array($id, json_decode($all->coach_category_id))){
                $result[] = $all;
            }
        }
        return $result;
    }
}
