<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Product;

class ArticleController extends Controller
{

    public function index($cat, $slug){

        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $lang = $first_lang->short;
        $news = DB::table('news')->select(
            "short_desc_$lang as short_desc",
            "description_$lang as description",
            "title_$lang as title",
            "short_desc_$lang as short_desc",
            "meta_title_$lang as meta_title",
            "meta_desc_$lang as meta_desc",
            "meta_key_$lang as meta_key",
            "id",
            "cat_id",
            "created_at",
            "image",
            "date1",
            "video",
            "tag",
            "slug"
        )
            ->where('slug', $slug)
            ->first();
        $category_info = DB::table('categories')
            ->select(
                "title_$lang as title"
            )
            ->where("id", $news->cat_id)->first();
        if(isset($news->id) && is_numeric($news->id)){
            if(isset($news->video) && $news->video !=''){
                $video_link = $this->YoutubeID($news->video);
                $news->video_link = "https://www.youtube.com/embed/".$video_link;
            }else{
                $news->video_link = '';
            }


            $tag_block = '';
            if(!empty($news->tag)){
                $tag_array = explode(",",$news->tag);

                foreach ($tag_array as $tag){
                    $tag_block .="<a href='#'>$tag</a>";
                }

            }
            $news->tag_block = $tag_block;

            $bradcramp = app('App\Http\Controllers\BredcrampController')->getBredcramp($lang,"post",$slug);


            $category_info = DB::table('categories')
                ->select(
                    "title_$lang as title",
                    "description_$lang as description",
                    "id",
                    "slug",
                    "page_parent"
                )
                ->where("id", $news->cat_id)->first();

            $page_parent = $category_info->page_parent;

            $cat_slug = $category_info->slug;


            $other_posts = DB::table('news')->select(
                "title_$lang as title",
                "image",
                "slug"
            )
                ->where('cat_id', '=', $news->cat_id)
                ->where('slug', '!=', $slug)
                ->where('image', '!=', '')
                ->limit(3)
                ->get();


            $news->news_galleries = '';
            $news->news_pdfs= '';
            $page_news_pdfs = DB::table('news_pdfs')->select("pdf_text_$lang as pdf_desc","pdf_file_$lang as pdf_file","pdf_name_$lang as title", "id")
                ->where('news_id',$news->id)
                ->where('hidden',0)->get();
            if(!empty($page_news_pdfs)){
                $news->news_pdfs = $page_news_pdfs;
            }
            $page_gallery = DB::table('news_gallery_infos')->where('parent_id',$news->id)->where('hidden',0)->get();
            foreach ($page_gallery as $gallery){
                $gallery_images = DB::table('news_gallery')->where('parent_id',$gallery->id)->get();
                $gallery->images = $gallery_images;
                $news->news_galleries = $page_gallery;
            }


            $news->news_sub_page_pdf= '';

            $news_sub_page_pdfs = DB::table('news_sub_page')
                ->where('news_id',$news->id)
                ->where('hidden',0)
                ->get();


            if(!empty($news_sub_page_pdfs)){
                foreach ($news_sub_page_pdfs as $news_sub_page_pdf) {
                    $news_sub_page_pdf->pdfs = DB::table('news_sub_page_pdf')
                        ->where('sub_page_id', $news_sub_page_pdf->id)
                        ->get();
                }
                $news->news_sub_page_pdf = $news_sub_page_pdfs;
            }



            if($news->meta_title !=''){
                $meta_data['meta_title'] = $news->meta_title;
            }else{
                $meta_data['meta_title'] = $news->title;
            }
            $meta_data['meta_desc'] = $news->meta_desc;
            $meta_data['meta_key'] = $news->meta_key;
            $meta_data['meta_image'] = url('/')."/uploads/news/".$news->image;
            $meta_data['meta_url'] = url()->current();
            $sidebar_pages = DB::table('categories')
                ->select("id","title_$lang as title","slug")
                ->where('page_parent',$page_parent)
                ->get();
            $sidebar_pag_posts = DB::table('news')->select("title_$lang as title", "image", "slug")
                ->where('cat_id', '=', 23)
                ->limit(3)
                ->get();
            $the_bests = Product::where("the_best", '=', 1)->limit(4)->get();
            return view('frontend.category.single', compact('the_bests','sidebar_pages','sidebar_pag_posts','category_info','meta_data',"cat_slug",'news','other_posts','bradcramp', 'lang'));
        }else{
            return view('frontend.errors.404', compact( 'lang'));
        }



    }
    public function indexLang($local_lang, $cat, $slug){
        $lang = $local_lang;
        $news = DB::table('news')->select(
            "short_desc_$lang as short_desc",
            "description_$lang as description",
            "title_$lang as title",
            "short_desc_$lang as short_desc",
            "meta_title_$lang as meta_title",
            "meta_desc_$lang as meta_desc",
            "meta_key_$lang as meta_key",
            "id",
            "cat_id",
            "created_at",
            "image",
            "date1",
            "video",
            "tag",
            "slug"
        )
            ->where('slug', $slug)
            ->first();



        if(isset($news->id) && is_numeric($news->id)){

            if(isset($news->video) && $news->video !=''){
                $video_link = $this->YoutubeID($news->video);
                $news->video_link = "https://www.youtube.com/embed/".$video_link;
            }else{
                $news->video_link = '';
            }


            $tag_block = '';
            if(!empty($news->tag)){
                $tag_array = explode(",",$news->tag);

                foreach ($tag_array as $tag){
                    $tag_block .="<a href='#'>$tag</a>";
                }

            }
            $news->tag_block = $tag_block;

            $bradcramp = app('App\Http\Controllers\BredcrampController')->getBredcramp($lang,"post",$slug);


            $category_info = DB::table('categories')
                ->select(
                    "title_$lang as title",
                    "description_$lang as description",
                    "id",
                    "slug",
                    "page_parent"
                )
                ->where("id", $news->cat_id)->first();

            $page_parent = $category_info->page_parent;

            $cat_slug = $category_info->slug;


            $other_posts = DB::table('news')->select(
                "title_$lang as title",
                "image",
                "slug"
            )
                ->where('cat_id', '=', $news->cat_id)
                ->where('slug', '!=', $slug)
                ->limit(5)
                ->get();


            $news->news_galleries = '';
            $news->news_pdfs= '';
            $page_news_pdfs = DB::table('news_pdfs')->select("pdf_text_$lang as pdf_desc","pdf_file_$lang as pdf_file","pdf_name_$lang as title", "id")
                ->where('news_id',$news->id)
                ->where('hidden',0)->get();
            if(!empty($page_news_pdfs)){
                $news->news_pdfs = $page_news_pdfs;
            }
            $page_gallery = DB::table('news_gallery_infos')->where('parent_id',$news->id)->where('hidden',0)->get();
            foreach ($page_gallery as $gallery){
                $gallery_images = DB::table('news_gallery')->where('parent_id',$gallery->id)->get();
                $gallery->images = $gallery_images;
                $news->news_galleries = $page_gallery;
            }


            $news->news_sub_page_pdf= '';

            $news_sub_page_pdfs = DB::table('news_sub_page')
                ->where('news_id',$news->id)
                ->where('hidden',0)
                ->get();


            if(!empty($news_sub_page_pdfs)){
                foreach ($news_sub_page_pdfs as $news_sub_page_pdf) {
                    $news_sub_page_pdf->pdfs = DB::table('news_sub_page_pdf')
                        ->where('sub_page_id', $news_sub_page_pdf->id)
                        ->get();
                }
                $news->news_sub_page_pdf = $news_sub_page_pdfs;
            }




            if($news->meta_title !=''){
                $meta_data['meta_title'] = $news->meta_title;
            }else{
                $meta_data['meta_title'] = $news->title;
            }
            $meta_data['meta_desc'] = $news->meta_desc;
            $meta_data['meta_key'] = $news->meta_key;
            $meta_data['meta_image'] = url('/')."/uploads/news/".$news->image;
            $meta_data['meta_url'] = url()->current();
            return view('frontend.category.single', compact('meta_data',"cat_slug",'news','other_posts','bradcramp', 'lang'));

        }else{
            return view('frontend.errors.404', compact( 'lang'));
        }


    }



    private function YoutubeID($url)
    {
        if(strlen($url) > 11)
        {
            if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match))
            {
                return $match[1];
            }
            else
                return false;
        }

        return $url;
    }
}
