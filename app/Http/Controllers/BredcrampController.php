<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BredcrampController extends Controller
{
    public function getBredcramp($lang,$type,$slug){
        $bredcramp = '<a class="link_href" href="'.url('/').'">'.app('App\Http\Controllers\GlossaryController')->getTranslate("home",$lang).'</a> <small> > </small>';
        switch ($type){
            case "search":{

                $bredcramp .= '<span>'.app('App\Http\Controllers\GlossaryController')->getTranslate("search_placeholder",$lang).'</span><small> > </small><span>'.$slug.'</span>';
                break;
            }
            case "category":{
                $current_title = DB::table('categories')
                    ->select("title_$lang as title","id","page_parent")
                    ->where("slug", $slug)->first();

                if($current_title->page_parent !=0){
                    $page_parent_info = DB::table('pages')
                        ->select("title_$lang as title","id","parent","slug",'hidden')
                        ->where("id", $current_title->page_parent)->first();
                    if($page_parent_info->hidden == 1){
                        $bredcramp .= '<span>'.app('App\Http\Controllers\GlossaryController')->getTranslate("haytararum_enq",$lang).'</span><small> > </small>';
                    }else{
                        $bredcramp .= '<a class="link_href" href="'.url('/').'/category/news">'.$page_parent_info->title.'</a><small> > </small>';
                    }

                }

                $bredcramp .= '<span>'.$current_title->title.'</span>';

                break;
            }
            case "productCat":{
                $current_title = DB::table('product_cats')
                    ->select("title_$lang as title","id")
                    ->where("id", $slug)->first();
                $bredcramp .= '<a class="link_href" href="'.app('App\Http\Controllers\GlossaryController')->getTranslate("home_url",$lang).'page/khanout">Խանութ</a><small> > </small>';
                $bredcramp .= '<span>'.$current_title->title.'</span>';

                break;
            }
            case "product":{

                $current_product = DB::table('pr_cat_ids')
                    ->join('products', function ($join) use ($slug) {
                        $join->on('products.id', '=', 'pr_cat_ids.pr_id')
                            ->where('pr_cat_ids.pr_id','=', $slug);
                    })
                    ->first();


                $current_title = DB::table('products')
                    ->select("title_$lang as title","id")
                    ->where("id", $slug)->first();
                $current_cat_title = DB::table('product_cats')
                    ->select("title_$lang as title","id")
                    ->where("id", $current_product->cat_id)->first();
                $bredcramp .= '<a class="link_href" href="'.app('App\Http\Controllers\GlossaryController')->getTranslate("home_url",$lang).'products/'.$current_cat_title->id.'">'.$current_cat_title->title.'</a><small> > </small>';
                $bredcramp .= '<span>'.$current_title->title.'</span>';

                break;
            }
            case "projects":{
                $current_title = DB::table('project_categories')
                    ->select("title_$lang as title","id","page_parent")
                    ->where("slug", $slug)->first();

                if($current_title->page_parent !=0){
                    $page_parent_info = DB::table('pages')
                        ->select("title_$lang as title","id","parent","slug",'hidden')
                        ->where("id", $current_title->page_parent)->first();
                    if($page_parent_info->hidden == 1){
                        $bredcramp .= '<span>'.$page_parent_info->title.'</span><small> > </small>';
                    }else{
                        $bredcramp .= '<a class="link_href" href="'.url('/').'/project_category/news">'.$page_parent_info->title.'</a><small> > </small>';
                    }

                }

                $bredcramp .= '<span>'.$current_title->title.'</span>';

                break;
            }
            case "post":{
                $post_info = DB::table('news')
                    ->select("title_$lang as title","id","cat_id")
                    ->where("slug", $slug)->first();
                $cat_title = DB::table('categories')
                    ->select("title_$lang as title","id","slug")
                    ->where("id", $post_info->cat_id)->first();
                if($cat_title->id != 4){

                    $bredcramp .= '<a class="link_href" href="'.url('/').'/category/'.$cat_title->slug.'">'.$cat_title->title.'</a> <small> > </small>';

                    $bredcramp .= '<span>'.$post_info->title.'</span>';
                }
                break;
            }
            case "page":{
                $page_info = DB::table('pages')
                    ->select("title_$lang as title","id","parent")
                    ->where("slug", $slug)->first();
                if($page_info->parent !=0){
                    $page_parent_info = DB::table('pages')
                        ->select("title_$lang as title","id","parent","slug","hidden")
                        ->where("id", $page_info->parent)->first();
                    if($page_parent_info->hidden == 1){
                        $bredcramp .= '<span>'.$page_parent_info->title.'</span><small> > </small>';
                    }else{
                        $bredcramp .= '<span>'.$page_parent_info->title.'</span><small> > </small>';
                    }

                }
                $bredcramp .= '<span>'.$page_info->title.'</span>';


                break;
            }
            case "gallery_infos":{
                $page_info = DB::table('gallery_infos')
                    ->select("title_$lang as title","id","parent_id","gallery_parent")
                    ->where("slug", $slug)->first();
                $parent_page = Page::find($page_info->parent_id);
                $bredcramp .= '<span>'.$parent_page->{"title_$lang"}.'</span><small> > </small>';
                if($page_info->gallery_parent !=0){
                    $page_parent_info = DB::table('gallery_infos')
                        ->select("title_$lang as title","id","slug","hidden")
                        ->where("id", $page_info->gallery_parent)->first();
                    $bredcramp .= '<span>'.$page_parent_info->title.'</span><small> > </small>';

                }
                $bredcramp .= '<span>'.$page_info->title.'</span>';

                break;
            }
            case "page_pdf":{
                $page_info = DB::table('sub_pages_with_pdfs')
                    ->select("title_$lang as title","id","parent_id")
                    ->where("slug", $slug)->first();
                if($page_info->parent_id !=0){
                    $page_parent_info = DB::table('pages')
                        ->select("title_$lang as title","id","parent","slug","hidden")
                        ->where("id", $page_info->parent_id)->first();
                    if($page_parent_info->hidden == 1){
                        $bredcramp .= '<span>'.$page_parent_info->title.'</span><small> > </small>';
                    }else{
                        $bredcramp .= '<a class="link_href" href="'.url('/').'/page/'.$page_parent_info->slug.'">'.$page_parent_info->title.'</a><small> > </small>';
                    }

                }
                $bredcramp .= '<span>'.$page_info->title.'</span>';


                break;
            }
        }

        return $bredcramp;


    }
}
