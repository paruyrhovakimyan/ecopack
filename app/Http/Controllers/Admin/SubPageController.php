<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class SubPageController extends Controller
{

    private function mb_str_split( $string ) {
        # Split at all position not after the start: ^
        # and not before the end: $
        return preg_split('/(?<!^)(?!$)/u', $string );
    }
    private function create_english($title){
        $charlist = $this->mb_str_split( $title );
        $array_hayeren = array('.','ա','բ','գ','դ','ե','զ','է','ը','թ','ժ','ի','լ','խ','ծ','կ','հ','ձ','ղ','ճ','մ','յ','ն','շ','ո','չ','պ','ջ','ռ','ս','վ','տ','ր','ց','ւ','փ','ք','և','օ','ֆ',' ','  ','/','\\','&','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','1','2','3','4','5','6','7','8','9','0');
        $array_english = array('-','a','b','g','d','e','z','e','y','t','zh','i','l','kh','ts','k','h','dz','gh','ch','m','y','n','sh','o','ch','p','j','r','s','v','t','r','c','u','p','q','ev','o','f','-','-','-','-','-','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','1','2','3','4','5','6','7','8','9','0');
        $new ='';
        if(count($charlist)<41){
            $counts = count($charlist);
        }
        else{
            $counts = 40;
        }
        for($i=0;$i<$counts;$i++){
            $key = array_search(mb_strtolower($charlist[$i],'UTF-8'),$array_hayeren);
            $new .= $array_english[$key];

        }
        return $new;
    }
    private function create_slug($title,$id_obj){

        $slug = $this->create_english($title);
        $article_isset_slug = DB::table('pages')->where('slug', '=', $slug)->orderBy('id', 'desc')->count();
        if($article_isset_slug > 0){
            $new_slug = $slug."-".$id_obj;
        }else{
            $new_slug = $slug;
        }

        return $new_slug;
    }

    public function index($parent_id){
        $default_lang = config('app.locale');
        $pages = DB::table('pages')->select("title_$default_lang as title", "id")->where('parent',$parent_id)->get();
        return view("admin.sub_pages.all", compact('pages','default_lang','parent_id'));
    }

    public function create($parent_id)
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $page_settings = DB::table('pages')->select("config_logo","config_bg_image","id","title_$default_lang as title","config_data1","config_data2","config_cat","config_tag","config_image")->where('id',$parent_id)->first();

        $page_bredcrump = '<h4><a href="'.url('/').'/admin/pages/'.$page_settings->id.'">'.$page_settings->title.'</a> | ADD PAGE </h4>';

        if($page_settings->config_cat == 1){
            $categories = DB::table('categories')->select("id","hidden","title_$default_lang as title")->where( 'page_parent', $parent_id)->get();
        }else{
            $categories = '';
        }
        $languages = DB::table('languages')->where('hidden','=', 0)->get();
        $settings = DB::table('settings')->where('id', 1)->first();
        return view('admin.pages.create',compact('languages','page_bredcrump','settings','parent_id',"page_settings",'categories'));
    }

    public function store($parent_id,Request $request){
        if (isset($request->add_page) && $request->add_page == "ok") {
            //Armenian
            $title_am = $request->title;
            $desc_am = $request->desc;
            $meta_key_am = $request->meta_key;
            if ($request->meta_title == '') {
                $meta_title_am = $title_am;
            } else {
                $meta_title_am = $request->meta_title;
            }
            if ($request->meta_desc == '') {
                $meta_desc_am = substr(strip_tags($desc_am), 0, 255);
            } else {
                $meta_desc_am = $request->meta_desc;
            }


            //Russian
            $title_ru = $request->title_ru;
            $desc_ru = $request->desc_ru;
            $meta_key_ru = $request->meta_key_ru;
            if ($request->meta_title_ru == '') {
                $meta_title_ru = $title_ru;
            } else {
                $meta_title_ru = $request->meta_title_ru;
            }
            if ($request->meta_desc_ru == '') {
                $meta_desc_ru = substr(strip_tags($desc_ru), 0, 255);
            } else {
                $meta_desc_ru = $request->meta_desc_ru;
            }


            //English
            $title_en = $request->title_en;
            $desc_en = $request->desc_en;
            $meta_key_en = $request->meta_key_en;
            if ($request->meta_title_en == '') {
                $meta_title_en = $title_en;
            } else {
                $meta_title_en = $request->meta_title_en;
            }
            if ($request->meta_desc_en == '') {
                $meta_desc_en = substr(strip_tags($desc_en), 0, 255);
            } else {
                $meta_desc_en = $request->meta_desc_en;
            }


            if (isset($request->sub_page) && $request->sub_page == '1') {
                $sub_page = $request->sub_page;
            } else {
                $sub_page = 0;
            }
            if (isset($request->gallery) && $request->gallery == '1') {
                $gallery = $request->gallery;
            } else {
                $gallery = 0;
            }
            if (isset($request->pdf_uploader) && $request->pdf_uploader == '1') {
                $pdf_uploader = $request->pdf_uploader;
            } else {
                $pdf_uploader = 0;
            }

            if(isset($request->parent_id) && $request->parent_id !=''){
                $parent_id = $request->parent_id;
            }else{
                $parent_id = 0;
            }

            if ($request->file("image_en")) {
                $image = $request->file("image_en");
                $generatedString = time();
                $extension = $request->file("image_en")->getClientOriginalExtension();
                $newFile = "public/uploads/page/" . $generatedString . "." . $extension;

                $filepath = $generatedString . "." . $extension;

                Image::make($image->getRealPath())->orientate()->save($newFile);
            } else {
                $filepath = "";
            }

            $id = DB::table('pages')->insertGetId(
                [
                    'title_am' => $title_am,
                    'description_am' => $desc_am,
                    'title_ru' => $title_ru,
                    'description_ru' => $desc_ru,
                    'title_en' => $title_en,
                    'description_en' => $desc_en,
                    'meta_title_am' => $meta_title_am,
                    'meta_desc_am' => $meta_desc_am,
                    'meta_key_am' => $meta_key_am,
                    'meta_title_en' => $meta_title_en,
                    'meta_desc_en' => $meta_desc_en,
                    'meta_key_en' => $meta_key_en,
                    'meta_title_ru' => $meta_title_ru,
                    'meta_desc_ru' => $meta_desc_ru,
                    'meta_key_ru' => $meta_key_ru,
                    'sub_pages' => $sub_page,
                    'pdf_uploder' => $pdf_uploader,
                    'gallery' => $gallery,
                    'slug' => 'slug',
                    'image' => $filepath

                ]
            );

            if (config('app.locale') == 'en') {
                $slug = $this->create_slug($title_en, $id);
            } elseif (config('app.locale') == 'ru') {
                $slug = $this->create_slug($title_ru, $id);
            } else {
                $slug = $this->create_slug($title_am, $id);
            }

            DB::table('pages')
                ->where('id', $id)
                ->update(
                    [
                        'slug' => $slug
                    ]
                );
            return redirect('admin/pages');


        }

    }

    public function edit($parent_id,$id)
    {
        $settings = DB::table('settings')->where('id', 1)->first();
        $page = DB::table('pages')->where('id', $id)->where('parent',$parent_id)->first();
        return view('admin.sub_pages.edit', compact('page','settings','parent_id'));
    }


    public function update(Request $request, $id)
    {
        if (isset($request->edit_page) && $request->edit_page == "ok"){

            $parent_id = $request->parent_id;
            //Armenian
            $title_am = $request->title;
            $desc_am = $request->desc;
            $meta_key_am = $request->meta_key;
            if($request->meta_title == ''){
                $meta_title_am = $title_am;
            }else{
                $meta_title_am = $request->meta_title;
            }
            if($request->meta_desc == ''){
                $meta_desc_am = substr(strip_tags($desc_am),0,255);
            }else{
                $meta_desc_am = $request->meta_desc;
            }




            //Russian
            $title_ru = $request->title_ru;
            $desc_ru = $request->desc_ru;
            $meta_key_ru = $request->meta_key_ru;
            if($request->meta_title_ru == ''){
                $meta_title_ru = $title_ru;
            }else{
                $meta_title_ru = $request->meta_title_ru;
            }
            if($request->meta_desc_ru == ''){
                $meta_desc_ru = substr(strip_tags($desc_ru),0,255);
            }else{
                $meta_desc_ru = $request->meta_desc_ru;
            }




            //English
            $title_en = $request->title_en;
            $desc_en = $request->desc_en;
            $meta_key_en = $request->meta_key_en;
            if($request->meta_title_en == ''){
                $meta_title_en = $title_en;
            }else{
                $meta_title_en = $request->meta_title_en;
            }
            if($request->meta_desc_en == ''){
                $meta_desc_en = substr(strip_tags($desc_en),0,255);
            }else{
                $meta_desc_en = $request->meta_desc_en;
            }


            $slug = $request->slug;
            if($slug ==''){
                if(config('app.locale') == 'en'){
                    $new_slug = $this->create_slug($title_en,$id);
                }elseif (config('app.locale') == 'ru'){
                    $new_slug = $this->create_slug($title_ru,$id);
                }else{
                    $new_slug = $this->create_slug($title_am,$id);
                }

            }else {
                $article_isset_slug = DB::table('pages')->where('slug', '=', $slug)->where('id', '!=', $id)->orderBy('id', 'desc')->count();
                if ($article_isset_slug > 0) {
                    $new_slug = $slug . "-" . $id;
                } else {
                    $new_slug = $slug;
                }
            }
            $short_desc_am = $request->short_desc;
            $short_desc_ru = $request->short_desc_ru;
            $short_desc_en = $request->short_desc_en;

            DB::table('pages')
                ->where('id', $id)
                ->where('parent', $parent_id)
                ->update(
                    [
                        'title_am' => $title_am,
                        'description_am' => $desc_am,
                        'title_ru' => $title_ru,
                        'description_ru' => $desc_ru,
                        'title_en' => $title_en,
                        'description_en' => $desc_en,
                        'meta_title_am' => $meta_title_am,
                        'meta_desc_am' => $meta_desc_am,
                        'meta_key_am' => $meta_key_am,
                        'meta_title_en' => $meta_title_en,
                        'meta_desc_en' => $meta_desc_en,
                        'meta_key_en' => $meta_key_en,
                        'meta_title_ru' => $meta_title_ru,
                        'meta_desc_ru' => $meta_desc_ru,
                        'meta_key_ru' => $meta_key_ru,
                        'slug' => $new_slug,
                        'short_desc_am' => $short_desc_am,
                        'short_desc_ru' => $short_desc_ru,
                        'short_desc_en' => $short_desc_en

                    ]
                );
            $data['edit_successful'] = "yes";
            $settings = DB::table('settings')->where('id', 1)->first();
            $page = DB::table('pages')->where('id', $id)->where('parent',$parent_id)->first();
            return view('admin.sub_pages.edit', compact('page','data', 'settings','parent_id'));


        }
    }
}
