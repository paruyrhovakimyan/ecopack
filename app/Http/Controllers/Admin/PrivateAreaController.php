<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PrivateAreaController extends Controller
{
    public function index(){
        $private_areas = DB::table('private_area_config')->get();
        if(!empty($private_areas)){
            foreach ($private_areas as $private_area){
                $private_area_files = DB::table('private_area_pdf')->where('private_area_id', $private_area->id)->get();
                $private_area->files = $private_area_files;

            }
        }

        return view("admin.private_area.all", compact('private_areas'));
    }

    public function create(){
        return view("admin.private_area.create");
    }

    public function store(Request $request){
        if(isset($request->add_private_area) && $request->add_private_area){
            $title = $request->title;
            $desc = $request->description;
            $login = $request->login;
            $password = $request->password;

            DB::table('private_area_config')->insert(
                [
                    'title' => $title,
                    'description' => $desc,
                    'login' => $login,
                    'password' => $password,
                ]
            );
        }
        return redirect('admin/private_area');
    }

    public function edit($id){
        $private_area = DB::table('private_area_config')->where('id',$id)->first();
        return view("admin.private_area.edit", compact('private_area'));
    }

    public function update(Request $request, $id){
        if(isset($request->edit_private_area) && $request->edit_private_area == "ok"){
            $title = $request->title;
            $desc = $request->description;
            $login = $request->login;
            $password = $request->password;

            DB::table('private_area_config')
                ->where('id', $id)
                ->update(
                [
                    'title' => $title,
                    'description' => $desc,
                    'login' => $login,
                    'password' => $password,
                ]
            );
        }
        return redirect('admin/private_area');
    }


    public function add_pdf($id){
        $private_area = DB::table('private_area_config')->where('id',$id)->first();
        $title = $private_area->title;
        return view("admin.private_area_pdf.create", compact('id','title'));
    }

    public function save_pdf(Request $request){
        if(isset($request->add_pdf) && $request->add_pdf == "ok"){
            $parent_id = $request->parent_id;
            $title = $request->title;
            $desc = $request->description;

            if ($request->pdf_en) {
                $pdf_en = $request->pdf_en->getClientOriginalName();
                $request->pdf_en->move(
                    base_path() . '/public/uploads/private_area_pdf', $pdf_en
                );
            } else {
                $pdf_en = "";
            }
            DB::table('private_area_pdf')->insert(
                [
                    'private_area_id' => $parent_id,
                    'title' => $title,
                    'short_desc' => $desc,
                    'file' => $pdf_en
                ]
            );


        }
        return redirect('admin/private_area');
    }

    public function edit_pdf($id){
        $private_area_pdf = DB::table('private_area_pdf')->where('id', $id)->first();
        $private_area_parent = DB::table('private_area_config')->where('id',$private_area_pdf->private_area_id)->first();
       $parent_title = $private_area_parent->title;
        return view('admin.private_area_pdf.edit',compact('private_area_pdf','parent_title'));
    }

    public function update_pdf(Request $request, $id){
        if(isset($request->edit_pdf) && $request->edit_pdf == "ok"){
            $title = $request->title;
            $desc = $request->description;

            if ($request->pdf_en) {
                $pdf_en = $request->pdf_en->getClientOriginalName();
                $request->pdf_en->move(
                    base_path() . '/public/uploads/private_area_pdf', $pdf_en
                );
            } else {
                $pdf_en = $request->old_pdf_title;
            }
            DB::table('private_area_pdf')
                ->where('id', $id)
                ->update(
                [
                    'title' => $title,
                    'short_desc' => $desc,
                    'file' => $pdf_en
                ]
            );


        }
        return redirect('admin/private_area');
    }



    public function delete_area($id){
        DB::table('private_area_config')->where('id', '=', $id)->delete();
    }
    public function delete_private_area_pdf($id){
        DB::table('private_area_pdf')->where('id', '=', $id)->delete();
    }

}
