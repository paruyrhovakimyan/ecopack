<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use App\GalleryInfo as GalleryInfo;

class PageGalleryController extends Controller
{

    public function add_gallery($parent_id){

        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $page_bredcrump = '';
        $page_parent = DB::table('pages')->select("title_$default_lang as title", "parent")->where('id', $parent_id)->first();
        $page_super_parent = DB::table('pages')->select("title_$default_lang as title")->where('id', $page_parent->parent)->first();
        $page_bredcrump = '<h4><a href="'.url('/').'/admin/pages/'.$page_parent->parent.'">'.$page_super_parent->title .'</a> | '.$page_parent->title.'</h4>';
        $languages = DB::table('languages')->where('hidden','=', 0)->get();
        return view("admin.page_gallery.create", compact('languages','page_bredcrump','parent_id'));
    }


    public function storeGallery(Request $request,$parent_id){

        if(isset($request->add_gallery) && $request->add_gallery == "ok"){
            $title = $request->title;
            $desc = $request->description;
            $galleryInfo = new GalleryInfo();
            $galleryInfo->parent_id = $parent_id;
            foreach($request->all() as $key => $val){
                if($key !='_token' && $key !='add_gallery' && $key !='filename'){
                    $galleryInfo->$key = $val;
                }
            }
            $galleryInfo->save();
            if (isset($request->filename) && $request->filename != '') {
                foreach ($request->filename as $filename) {

                    DB::table('gallery')->insert(
                        [
                            'image' => $filename,
                            'parent_id' => $galleryInfo->id
                        ]
                    );
                }
            }
        }


        $page_parent = DB::table('pages')->select("parent")->where('id', $parent_id)->first();

        return redirect('admin/pages/'.$page_parent->parent);

    }


    public function edit_gallery($id){

        $gallery = DB::table('gallery_infos')->where('id',$id)->first();

        $gallery_parent_page = $gallery->parent_id;

        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $page_parent = DB::table('pages')->select("title_$default_lang as title", "parent")->where('id', $gallery_parent_page)->first();
        $page_super_parent = DB::table('pages')->select("title_$default_lang as title")->where('id', $page_parent->parent)->first();
        $page_bredcrump = '<h4><a href="'.url('/').'/admin/pages/'.$page_parent->parent.'">'.$page_super_parent->title.' </a> | '.$page_parent->title.'</h4>';

        $projectImages = DB::table('gallery')->where('parent_id',$id)->get();
        $dropzoneDefaultHidden = '';
        if (count($projectImages) != 0) {
            $dropzoneDefaultHidden = 'dz-default-hidden';
        }
        $languages = DB::table('languages')->where('hidden','=', 0)->get();

        return view("admin.page_gallery.edit", compact('languages','gallery','projectImages','dropzoneDefaultHidden','default_lang','page_bredcrump'));
    }


    public function updateGallery(Request $request,$id){

        if(isset($request->edit_gallery) && $request->edit_gallery == "ok"){
            $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
            $default_lang = $first_lang->short;

            $page_gallery = GalleryInfo::find($id);
            foreach($request->all() as $key => $val){
                if($key !='_token' && $key !='edit_gallery' && $key !='filename'){
                    $page_gallery->$key = $val;
                }
            }
            $page_gallery->save();
            DB::table('gallery')->where('parent_id', '=', $id)->delete();
            if (isset($request->filename) && $request->filename != '') {
                foreach ($request->filename as $filename) {

                    DB::table('gallery')->insert(
                        [
                            'image' => $filename,
                            'parent_id' => $id
                        ]
                    );
                }
            }
           }


        $gallery_parent = DB::table('gallery_infos')->select("parent_id")->where('id', $id)->first();
        $page_parent = DB::table('pages')->select("parent")->where('id', $gallery_parent->parent_id)->first();

        return redirect('admin/pages/'.$page_parent->parent);
    }

    public function upload(Request $request)
    {
        $imageName = substr(md5(microtime()),rand(0,26),10);
        $image = Input::file('file');
        $generatedString = time();
        $extension = $request->file("file")->getClientOriginalExtension();
        $newFile = public_path('uploads/gallery/' . $imageName.'_'. $generatedString . "." . $extension);
        $newFileThumb = public_path('uploads/gallery/thumbs/' . $imageName.'_'. $generatedString . "." . $extension);
        $newFileThumb_150 = public_path('uploads/gallery/150/' . $imageName.'_'. $generatedString . "." . $extension);
        $filepath = $imageName.'_'.$generatedString . "." . $extension;

        Image::make($image->getRealPath())->orientate()->save($newFile);
        Image::make($image->getRealPath())->resize(260, 350)->orientate()->save($newFileThumb);
        Image::make($image->getRealPath())->resize(null, 150, function ($constraint) {
            $constraint->aspectRatio();
        })->orientate()->save($newFileThumb_150);
        echo $filepath;
    }

    public function gallery_delete(Request $request){
        $back_url = app('Illuminate\Routing\UrlGenerator')->previous();
        $id = $request->id;
        DB::table('gallery_infos')->where('id', '=', $id)->delete();
        return redirect($back_url);
    }
}
