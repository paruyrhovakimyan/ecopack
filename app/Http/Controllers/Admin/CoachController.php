<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Coach;
use App\CoachCategory;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;

class CoachController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;

        $categories = Coach::all();
        return view("admin.coach.all", compact('categories','default_lang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();

        $categories = CoachCategory::all();
        return view('admin.coach.create', compact('default_lang','categories','languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($request->add_coach) && $request->add_coach == "ok"){

            $coach = new Coach();


            if ($request->file("image")) {

                $image = Input::file('image');
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();

                $newFile = public_path('uploads/coach/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->orientate()->save($newFile);


                $newFileThumb = public_path('uploads/coach/thumbs/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(350, 280)->save($newFileThumb);

                $newFileThumb_150 = public_path('uploads/coach/150/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(null, 150, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb_150);
                $filepath = $generatedString . "." . $extension;
            } else {
                $filepath = "";
            }

            foreach ($request->all() as $key => $val) {
                if ($key != '_token' && $key != 'add_coach') {
                    $coach->$key = $val;
                }
            }
            $coach->image = $filepath;
            $coach->coach_category_id = json_encode($request->coach_category_id);
            $coach->save();

            return redirect('admin/coaches');


        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $coach = Coach::find($id);
        $categories = CoachCategory::all();
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();
        return view('admin.coach.edit', compact('default_lang','languages','coach','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (isset($request->edit_category) && $request->edit_category == "ok"){
            $coach = Coach::find($id);

            if ($request->file("image")) {

                $image = Input::file('image');
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();

                $newFile = public_path('uploads/coach/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->orientate()->save($newFile);


                $newFileThumb = public_path('uploads/coach/thumbs/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(350, 280)->save($newFileThumb);

                $newFileThumb_150 = public_path('uploads/coach/150/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(null, 150, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb_150);
                $filepath = $generatedString . "." . $extension;
            }else{
                $filepath= $request->old_image;
            }


            foreach ($request->all() as $key => $val) {
                if ($key != 'old_image' && $key != '_method' && $key != '_token' && $key != 'edit_category') {
                    $coach->$key = $val;
                }
            }
            $coach->image = $filepath;
            $coach->coach_category_id = json_encode($request->coach_category_id);
            $coach->save();

            return redirect('admin/coaches/');


        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Coach::find($id);
        $user->delete();
    }
}
