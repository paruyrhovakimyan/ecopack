<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = DB::table('menu')->get();
        return view("admin.menu.all", compact('menus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pages = DB::table('pages')->get();
        $categories = DB::table('categories')->get();
        $menus = DB::table('menu')->get();
        return view("admin.menu.create", compact('menus','pages','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(isset($request->create_menu) && $request->create_menu == "ok"){
            $title = $request->menu_title;
            $id = DB::table('menu')->insertGetId(
                [
                    'title' => $title
                ]
            );
            return redirect('admin/menu/'.$id.'/edit');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $default_lang = config('app.locale');
        $pages = DB::table('pages')->select("title_$default_lang as title","id")->get();
        $categories = DB::table('categories')->select("title_$default_lang as title","id")->get();
        $menus = DB::table('menu')->get();
        $current_menu  = DB::table('menu')->where('id', $id)->first();

        $menu_parent_items = DB::table('menu_items')
            ->where('menu_id', $id)
            ->where('item_parent', 1)
            ->orderBy('item_order', 'asc')
            ->get();



        foreach ($menu_parent_items as $row_parent_item){
            $row_parent_item->child[] = array();
            $row_parent_item->i=1;
            $resourse_id = $row_parent_item->resourse_id;
            switch($row_parent_item->item_type){
                case 'page':{
                    $row_page = DB::table('pages')
                        ->select("title_$default_lang as title")
                        ->where('id', $resourse_id)
                        ->first();
                    if($row_page){

                        $menu_item_name = $row_page->title;
                        $row_parent_item->name = $menu_item_name;
                    }else{
                        $row_parent_item->name = '';
                    }

                    break;
                }

                case 'category':{
                    $row_page = DB::table('categories')
                        ->select("title_$default_lang as title")
                        ->where('id', $resourse_id)
                        ->first();
                    $menu_item_name = $row_page->title;
                    $row_parent_item->name = $menu_item_name;
                    break;
                }

                case 'link':{
                    $row_page = DB::table('menu_links')
                        ->select("link_name_$default_lang as link_name")
                        ->where('link_id', $resourse_id)
                        ->first();
                    $menu_item_name = $row_page->link_name;
                    $row_parent_item->name = $menu_item_name;
                    break;
                }
            }


            $id_item_p = $row_parent_item->item_id;


            $result_items1 = DB::table('menu_items')
                ->where('menu_id', $id)
                ->where('item_parent', $id_item_p)
                ->orderBy('item_order', 'asc')
                ->get();
            $j = 0;
            foreach ($result_items1 as $row_item1) {
                $resourse_id1 = $row_item1->resourse_id;
                switch($row_item1->item_type){
                    case 'page':{
                        $row_page = DB::table('pages')
                            ->select("title_$default_lang as title")
                            ->where('id', $resourse_id1)
                            ->first();
                        $menu_item_name = $row_page->title;
                        $row_parent_item->child[$j]['name'] = $menu_item_name;
                        $row_parent_item->child[$j]['id'] = $row_item1->item_id;
                        $row_parent_item->child[$j]['order'] = $row_item1->item_order;
                        break;
                    }
                    case 'category':{
                        $row_page = DB::table('categories')
                            ->select("title_$default_lang as title")
                            ->where('id', $resourse_id1)
                            ->first();
                        if($row_page){
                            $menu_item_name = $row_page->title;
                            $row_parent_item->child[$j]['name'] = $menu_item_name;
                            $row_parent_item->child[$j]['id'] = $row_item1->item_id;
                            $row_parent_item->child[$j]['order'] = $row_item1->item_order;
                        }

                        break;
                    }
                    case 'link':{
                        $row_page = DB::table('menu_links')
                            ->select("link_name_$default_lang as link_name")
                            ->where('link_id', $resourse_id1)
                            ->first();
                        $menu_item_name = $row_page->link_name;
                        $row_parent_item->child[$j]['name'] = $menu_item_name;
                        $row_parent_item->child[$j]['id'] = $row_item1->item_id;
                        $row_parent_item->child[$j]['order'] = $row_item1->item_order;
                        break;
                    }
                }




                //errord

                $id_item_p2 = $row_item1->item_id;


                $result_items2 = DB::table('menu_items')
                    ->where('menu_id', $id)
                    ->where('item_parent', $id_item_p2)
                    ->orderBy('item_order', 'asc')
                    ->get();
                $j2 = 0;
                foreach ($result_items2 as $row_item2) {
                    $resourse_id2 = $row_item2->resourse_id;
                    switch($row_item2->item_type){
                        case 'page':{
                            $row_page2 = DB::table('pages')
                                ->select("title_$default_lang as title")
                                ->where('id', $resourse_id2)
                                ->first();
                            $menu_item_name2 = $row_page2->title;
                            $row_parent_item->child[$j]['child2'][$j2]['name'] = $menu_item_name2;
                            $row_parent_item->child[$j]['child2'][$j2]['id'] = $row_item2->item_id;
                            $row_parent_item->child[$j]['child2'][$j2]['order'] = $row_item2->item_order;
                            break;
                        }
                        case 'category':{
                            $row_page2 = DB::table('categories')
                                ->select("title_$default_lang as title")
                                ->where('id', $resourse_id2)
                                ->first();
                            $menu_item_name2 = $row_page2->title;
                            $row_parent_item->child[$j]['child2'][$j2]['name'] = $menu_item_name2;
                            $row_parent_item->child[$j]['child2'][$j2]['id'] = $row_item2->item_id;
                            $row_parent_item->child[$j]['child2'][$j2]['order'] = $row_item2->item_order;
                            break;
                        }
                        case 'link':{
                            $row_page2 = DB::table('menu_links')
                                ->select("link_name_$default_lang as link_name")
                                ->where('link_id', $resourse_id2)
                                ->first();
                            $menu_item_name2 = $row_page2->link_name;
                            $row_parent_item->child[$j]['child2'][$j2]['name'] = $menu_item_name2;
                            $row_parent_item->child[$j]['child2'][$j2]['id'] = $row_item2->item_id;
                            $row_parent_item->child[$j]['child2'][$j2]['order'] = $row_item2->item_order;
                            break;
                        }
                    }




                    //errord

                    $id_item_p3 = $row_item2->item_id;


                    $result_items3 = DB::table('menu_items')
                        ->where('menu_id', $id)
                        ->where('item_parent', $id_item_p3)
                        ->orderBy('item_order', 'asc')
                        ->get();
                    $j3 = 0;
                    foreach ($result_items3 as $row_item3) {
                        $resourse_id3 = $row_item3->resourse_id;
                        switch($row_item3->item_type){
                            case 'page':{
                                $row_page3 = DB::table('pages')
                                    ->select("title_$default_lang as title")
                                    ->where('id', $resourse_id3)
                                    ->first();
                                $menu_item_name3 = $row_page3->title;
                                $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['name'] = $menu_item_name3;
                                $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['id'] = $row_item3->item_id;
                                $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['order'] = $row_item3->item_order;
                                break;
                            }
                            case 'category':{
                                $row_page3 = DB::table('categories')
                                    ->select("title_$default_lang as title")
                                    ->where('id', $resourse_id3)
                                    ->first();
                                $menu_item_name3 = $row_page3->title;
                                $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['name'] = $menu_item_name3;
                                $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['id'] = $row_item3->item_id;
                                $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['order'] = $row_item3->item_order;
                                break;
                            }
                            case 'link':{
                                $row_page3 = DB::table('menu_links')
                                    ->select("link_name_$default_lang as link_name")
                                    ->where('link_id', $resourse_id3)
                                    ->first();
                                $menu_item_name3 = $row_page3->link_name;
                                $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['name'] = $menu_item_name3;
                                $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['id'] = $row_item3->item_id;
                                $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['order'] = $row_item3->item_order;
                                break;
                            }
                        }



                        //errord

                        $id_item_p4 = $row_item3->item_id;


                        $result_items4 = DB::table('menu_items')
                            ->where('menu_id', $id)
                            ->where('item_parent', $id_item_p4)
                            ->orderBy('item_order', 'asc')
                            ->get();
                        $j4 = 0;
                        foreach ($result_items4 as $row_item4) {
                            $resourse_id4 = $row_item4->resourse_id;
                            switch($row_item4->item_type){
                                case 'page':{
                                    $row_page4 = DB::table('pages')
                                        ->select("title_$default_lang as title")
                                        ->where('id', $resourse_id4)
                                        ->first();
                                    $menu_item_name4 = $row_page4->title;
                                    $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['name'] = $menu_item_name4;
                                    $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['id'] = $row_item4->item_id;
                                    $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['order'] = $row_item4->item_order;
                                    break;
                                }
                                case 'category':{
                                    $row_page4 = DB::table('categories')
                                        ->select("title_$default_lang as title")
                                        ->where('id', $resourse_id4)
                                        ->first();
                                    $menu_item_name4 = $row_page4->title;
                                    $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['name'] = $menu_item_name4;
                                    $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['id'] = $row_item4->item_id;
                                    $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['order'] = $row_item4->item_order;
                                    break;
                                }
                                case 'link':{
                                    $row_page4 = DB::table('menu_links')
                                        ->select("link_name_$default_lang as link_name")
                                        ->where('link_id', $resourse_id4)
                                        ->first();
                                    $menu_item_name4 = $row_page4->link_name;
                                    $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['name'] = $menu_item_name4;
                                    $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['id'] = $row_item4->item_id;
                                    $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['order'] = $row_item4->item_order;
                                    break;
                                }
                            }


                            //errord

                            $id_item_p5 = $row_item4->item_id;


                            $result_items5 = DB::table('menu_items')
                                ->where('menu_id', $id)
                                ->where('item_parent', $id_item_p5)
                                ->orderBy('item_order', 'asc')
                                ->get();
                            $j5 = 0;
                            foreach ($result_items5 as $row_item5) {
                                $resourse_id5 = $row_item5->resourse_id;
                                switch($row_item5->item_type){
                                    case 'page':{
                                        $row_page5 = DB::table('pages')
                                            ->select("title_$default_lang as title")
                                            ->where('id', $resourse_id5)
                                            ->first();
                                        $menu_item_name5 = $row_page5->title;
                                        $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['name'] = $menu_item_name5;
                                        $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['id'] = $row_item5->item_id;
                                        $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['order'] = $row_item5->item_order;
                                        break;
                                    }
                                    case 'category':{
                                        $row_page5 = DB::table('categories')
                                            ->select("title_$default_lang as title")
                                            ->where('id', $resourse_id5)
                                            ->first();
                                        $menu_item_name5 = $row_page5->title;
                                        $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['name'] = $menu_item_name5;
                                        $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['id'] = $row_item5->item_id;
                                        $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['order'] = $row_item5->item_order;
                                        break;
                                    }
                                    case 'link':{
                                        $row_page5 = DB::table('menu_links')
                                            ->select("link_name_$default_lang as link_name")
                                            ->where('link_id', $resourse_id5)
                                            ->first();
                                        $menu_item_name5 = $row_page5->link_name;
                                        $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['name'] = $menu_item_name5;
                                        $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['id'] = $row_item5->item_id;
                                        $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['order'] = $row_item5->item_order;
                                        break;
                                    }
                                }



                                //errord

                                $id_item_p6 = $row_item5->item_id;


                                $result_items6 = DB::table('menu_items')
                                    ->where('menu_id', $id)
                                    ->where('item_parent', $id_item_p6)
                                    ->orderBy('item_order', 'asc')
                                    ->get();
                                $j6 = 0;
                                foreach ($result_items6 as $row_item6) {
                                    $resourse_id6 = $row_item6->resourse_id;
                                    switch($row_item6->item_type){
                                        case 'page':{
                                            $row_page6 = DB::table('pages')
                                                ->select("title_$default_lang as title")
                                                ->where('id', $resourse_id6)
                                                ->first();
                                            $menu_item_name6 = $row_page6->title;
                                            $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['child6'][$j6]['name'] = $menu_item_name6;
                                            $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['child6'][$j6]['id'] = $row_item6->item_id;
                                            $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['child6'][$j6]['order'] = $row_item6->item_order;
                                            break;
                                        }
                                        case 'category':{
                                            $row_page6 = DB::table('categories')
                                                ->select("title_$default_lang as title")
                                                ->where('id', $resourse_id6)
                                                ->first();
                                            $menu_item_name6 = $row_page6->title;
                                            $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['child6'][$j6]['name'] = $menu_item_name6;
                                            $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['child6'][$j6]['id'] = $row_item6->item_id;
                                            $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['child6'][$j6]['order'] = $row_item6->item_order;
                                            break;
                                        }
                                        case 'link':{
                                            $row_page6 = DB::table('menu_links')
                                                ->select("link_name_$default_lang as link_name")
                                                ->where('link_id', $resourse_id6)
                                                ->first();
                                            $menu_item_name6 = $row_page6->link_name;
                                            $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['child6'][$j6]['name'] = $menu_item_name6;
                                            $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['child6'][$j6]['id'] = $row_item6->item_id;
                                            $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['child6'][$j6]['order'] = $row_item6->item_order;
                                            break;
                                        }
                                    }

                                    $j5++;
                                    $row_parent_item->i++;
                                }
                                //errord


                                $j5++;
                                $row_parent_item->i++;
                            }
                            //errord


                            $j4++;
                            $row_parent_item->i++;
                        }
                        //errord







                        $j3++;
                        $row_parent_item->i++;
                    }
                    //errord




                    $j2++;
                    $row_parent_item->i++;
                }
                //errord






                $j++;
                $row_parent_item->i++;
            }

            $row_parent_item->i++;
        }
        $settings = DB::table('settings')->where('id', 1)->first();
        return view("admin.menu.edit", compact('menus','current_menu','settings','menu_parent_items','pages','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('menu_items')->where('menu_id', '=', $id)->delete();
        DB::table('menu')->where('id', '=', $id)->delete();
    }


    public function save_menu(Request $request){
        $json = $request->json;
        $menu_id = (int)$request->menu_id;
        $menu_title = $request->menu_title;
        DB::table('menu')
            ->where('id', $menu_id)
            ->update(
                [
                    'title' => $menu_title
                ]
            );
        $menu_structure = json_decode($json);
        $i = 0;
        $array_for_db = array();
        foreach($menu_structure as $item){
            switch($item->type){
                case 'old':{
                    $id_menu_item = $item->id;
                    $result_this_old = DB::table('menu_items')->where('item_id', $id_menu_item)->get();
                    foreach ($result_this_old as $row_this_id) {
                        $array_for_db[$i]['type'] = $row_this_id->item_type;
                        $array_for_db[$i]['resource_id'] = $row_this_id->resourse_id;
                    }
                    break;
                }
                case 'page':{
                    $array_for_db[$i]['type'] = 'page';
                    $array_for_db[$i]['resource_id'] = $item->id;
                    break;
                }
                case 'category':{
                    $array_for_db[$i]['type'] = 'category';
                    $array_for_db[$i]['resource_id'] = $item->id;
                    break;
                }
                case 'link':{
                    $array_for_db[$i]['type'] = 'link';
                    $array_for_db[$i]['resource_id'] = $item->id;
                    break;
                }
            }
            $array_for_db[$i]['parent'] = $item->col;
            $array_for_db[$i]['order'] = $item->row;
            $i++;

        }

        DB::table('menu_items')->where('menu_id', '=', $menu_id)->delete();

        for($i=0;$i<count($array_for_db);$i++){
            $type = $array_for_db[$i]['type'];
            $resource_id = $array_for_db[$i]['resource_id'];
            $order = $array_for_db[$i]['order'];
            $parent = $array_for_db[$i]['parent'];


            DB::table('menu_items')
                ->insert(
                [
                    'menu_id' => $menu_id,
                    'item_type' => $type,
                    'resourse_id' => $resource_id,
                    'item_order' => $order,
                    'item_parent' => $parent,
                ]
            );
        }
        for($k=7;$k>1;$k--){
            $result_childes = DB::table('menu_items')->where('item_parent', $k)->where('menu_id', $menu_id)->get();
            foreach ($result_childes as $row_childe){
                $id_ch_item = $row_childe->item_id;
                $order_ch_item = $row_childe->item_order;
                $row_parent = DB::table('menu_items')
                    ->where('item_parent', $k-1)
                    ->where('menu_id', $menu_id)
                    ->where('item_order', '<', $order_ch_item )
                    ->orderBy('item_order', 'DESC')
                    ->first();
                $parent_id = $row_parent->item_id;
                DB::table('menu_items')
                    ->where('item_id', $id_ch_item)
                    ->update(
                        [
                            'item_parent' => $parent_id
                        ]
                    );
            }
        }


    }

    public function add_link_to_menu(Request $request){

        $link_name = (string)$request->link_name;
        $link_href = (string)$request->link_href;

        $link_name_ru = (string)$request->link_name_ru;
        $link_href_ru = (string)$request->link_href_ru;

        $link_name_en = (string)$request->link_name_en;
        $link_href_en = (string)$request->link_href_en;

        $id = DB::table('menu_links')->insertGetId(
            [
                'link_name_am' => $link_name,
                'link_href_am' => $link_href,
                'link_name_ru' => $link_name_ru,
                'link_href_ru' => $link_href_ru,
                'link_name_en' => $link_name_en,
                'link_href_en' => $link_href_en,
            ]
        );
        echo $id;

    }
}
