<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ContactController extends Controller
{
    public function index(){
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = DB::table('languages')->where('hidden','=', 0)->get();
        $contact_info_array = array();
        foreach ($languages as $language){
            $table = "contact_page_".$language->short;
            $contact_info = DB::table($table)->where('id', '=', 1)->first();
            $contact_info_array[] = $contact_info;
        }
        return view("admin.contact", compact('languages','contact_info_array'));
    }

    public function save(Request $request){
        if(isset($request->save) && $request->save == "ok"){

            $languages = DB::table('languages')->where('hidden','=', 0)->get();
            foreach ($languages as $key => $language){
                $table = "contact_page_".$language->short;
                $title = $request->title;
                $subtitle = $request->subtitle;
                $firstname = $request->firstname;
                $name_contact = $request->name_contact;
                $lastname = $request->lastname;
                $lastname_contact = $request->lastname_contact;
                $mail = $request->mail;
                $email_contact = $request->email_contact;
                $phone = $request->phone;
                $phone_contact = $request->phone_contact;
                $message = $request->message;
                $message_contact = $request->message_contact;
                $captcha = $request->captcha;
                $send_label = $request->send_label;
                $address_label = $request->address_label;
                $address = $request->address;
                $help_label = $request->help_label;
                $mail_right = $request->mail_right;
                $need_right = $request->need_right;
                $need_help_right = $request->need_help_right;
                $help_phone_right = $request->help_phone_right;
                $help_work_right = $request->help_work_right;

                DB::table($table)
                    ->where('id', 1)
                    ->update(
                        [

                            'title' => $title[$key],
                            'subtitle' => $subtitle[$key],
                            'firstname' => $firstname[$key],
                            'name_contact' => $name_contact[$key],
                            'lastname' => $lastname[$key],
                            'lastname_contact' => $lastname_contact[$key],
                            'mail' => $mail[$key],
                            'email_contact' => $email_contact[$key],
                            'phone' => $phone[$key],
                            'phone_contact' => $phone_contact[$key],
                            'message' => $message[$key],
                            'message_contact' => $message_contact[$key],
                            'captcha' => $captcha[$key],
                            'send_label' => $send_label[$key],
                            'address_label' => $address_label[$key],
                            'address' => $address[$key],
                            'help_label' => $help_label[$key],
                            'mail_right' => $mail_right[$key],
                            'need_right' => $need_right[$key],
                            'need_help_right' => $need_help_right[$key],
                            'help_phone_right' => $help_phone_right[$key],
                            'help_work_right' => $help_work_right[$key],
                        ]
                    );

            }
            $contact_info_array = array();
            foreach ($languages as $language){
                $table = "contact_page_".$language->short;
                $contact_info = DB::table($table)->where('id', '=', 1)->first();
                $contact_info_array[] = $contact_info;
            }

            $settings = DB::table('settings')->where('id', 1)->first();
            return view("admin.contact", compact('languages','contact_info_array'));
        }
    }
}
