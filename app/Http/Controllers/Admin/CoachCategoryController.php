<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CoachCategory;
use Illuminate\Support\Facades\DB;

class CoachCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;

        $categories = CoachCategory::all();
        return view("admin.coachcat.all", compact('categories','default_lang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();

        return view('admin.coachcat.create', compact('default_lang','languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($request->add_category) && $request->add_category == "ok"){

            $category = new CoachCategory();

            foreach ($request->all() as $key => $val) {
                if ($key != '_token' && $key != 'add_category') {
                    $category->$key = $val;
                }
            }
            $category->save();

            return redirect('admin/coachcat');


        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $category = CoachCategory::find($id);
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();
        return view('admin.coachcat.edit', compact('default_lang','languages','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (isset($request->edit_category) && $request->edit_category == "ok"){
            $category = CoachCategory::find($id);


            foreach ($request->all() as $key => $val) {
                if ($key != '_method' && $key != '_token' && $key != 'edit_category') {
                    $category->$key = $val;
                }
            }
            $category->save();

            return redirect('admin/coachcat/');


        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = CoachCategory::find($id);
        $user->delete();
    }
}
