<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class ServicesController extends Controller
{


    private function mb_str_split( $string ) {
        # Split at all position not after the start: ^
        # and not before the end: $
        return preg_split('/(?<!^)(?!$)/u', $string );
    }
    private function create_english($title){
        $charlist = $this->mb_str_split( $title );
        $array_hayeren = array('.','ա','բ','գ','դ','ե','զ','է','ը','թ','ժ','ի','լ','խ','ծ','կ','հ','ձ','ղ','ճ','մ','յ','ն','շ','ո','չ','պ','ջ','ռ','ս','վ','տ','ր','ց','ւ','փ','ք','և','օ','ֆ',' ','  ','/','\\','&','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','1','2','3','4','5','6','7','8','9','0');
        $array_english = array('-','a','b','g','d','e','z','e','y','t','zh','i','l','kh','ts','k','h','dz','gh','ch','m','y','n','sh','o','ch','p','j','r','s','v','t','r','c','u','p','q','ev','o','f','-','-','-','-','-','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','1','2','3','4','5','6','7','8','9','0');
        $new ='';
        if(count($charlist)<41){
            $counts = count($charlist);
        }
        else{
            $counts = 40;
        }
        for($i=0;$i<$counts;$i++){
            $key = array_search(mb_strtolower($charlist[$i],'UTF-8'),$array_hayeren);
            $new .= $array_english[$key];

        }
        return $new;
    }
    private function create_slug($title,$id_obj){

        $slug = $this->create_english($title);
        $article_isset_slug = DB::table('services')->where('slug', '=', $slug)->orderBy('id', 'desc')->count();
        if($article_isset_slug > 0){
            $new_slug = $slug."-".$id_obj;
        }else{
            $new_slug = $slug;
        }

        return $new_slug;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $default_lang = config('app.locale');
        $services = DB::table('services')->select("id","hidden","service_order","title_$default_lang as title")->get();
        return view("admin.services.all", compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $settings = DB::table('settings')->where('id', 1)->first();
        return view('admin.services.create',compact('settings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($request->add_service) && $request->add_service == "ok") {
            $title = $request->title;
            $short_desc = $request->short_desc;
            $desc = $request->desc;

            if ($request->meta_title == '') {
                $meta_title = $title;
            } else {

                $meta_title = $request->meta_title;
            }
            if ($request->meta_desc == '') {
                $meta_desc = substr(strip_tags($desc), 0, 255);
            } else {
                $meta_desc = strip_tags($request->meta_desc);
            }
            $meta_key = $request->meta_key;


            $title_ru = $request->title_ru;
            $short_desc_ru = $request->short_desc_ru;
            $desc_ru = $request->desc_ru;


            if ($request->meta_title_ru == '') {
                $meta_title_ru = $title_ru;
            } else {

                $meta_title_ru = $request->meta_title_ru;
            }
            if ($request->meta_desc_ru == '') {
                $meta_desc_ru = substr(strip_tags($desc_ru), 0, 255);
            } else {
                $meta_desc_ru = strip_tags($request->meta_desc_ru);
            }
            $meta_key_ru = $request->meta_key_ru;


            $title_en = $request->title_en;
            $short_desc_en = $request->short_desc_en;
            $desc_en = $request->desc_en;

            if ($request->meta_title_en == '') {
                $meta_title_en = $title_en;
            } else {

                $meta_title_en = $request->meta_title_en;
            }
            if ($request->meta_desc_en == '') {
                $meta_desc_en = substr(strip_tags($desc_en), 0, 255);
            } else {
                $meta_desc_en = strip_tags($request->meta_desc_en);
            }
            $meta_key_en = $request->meta_key_en;


            $service_order = $request->service_order;
            $service_date = $request->service_date;



            if ($request->file("image")) {
                $image = $request->file("image");
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();
                $newFile = "uploads/services/" . $generatedString . "." . $extension;
                $newFileThumb = "uploads/services/thumbs/" . $generatedString . "." . $extension;
                $filepath = $generatedString . "." . $extension;

                Image::make($image->getRealPath())->resize(null, 470, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFile);
                Image::make($image->getRealPath())->resize(null, 250, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb);
            } else {
                $filepath = "";
            }



            $id = DB::table('services')->insertGetId(
                [
                    'title_am' => $title,
                    'description_am' => $desc,
                    'short_desc_am' => $short_desc,
                    'meta_title_am' => $meta_title,
                    'meta_key_am' => $meta_key,
                    'meta_desc_am' => $meta_desc,
                    'title_ru' => $title_ru,
                    'description_ru' => $desc_ru,
                    'short_desc_ru' => $short_desc_ru,
                    'meta_title_ru' => $meta_title_ru,
                    'meta_key_ru' => $meta_key_ru,
                    'meta_desc_ru' => $meta_desc_ru,
                    'title_en' => $title_en,
                    'description_en' => $desc_en,
                    'short_desc_en' => $short_desc_en,
                    'meta_title_en' => $meta_title_en,
                    'meta_key_en' => $meta_key_en,
                    'meta_desc_en' => $meta_desc_en,
                    'service_order' => $service_order,
                    'banner' => $filepath,
                    'date' => $service_date,
                    'slug' => 'slug',
                ]
            );
            if(config('app.locale') == 'en'){
                $slug = $this->create_slug($title_en,$id);
            }elseif (config('app.locale') == 'ru'){
                $slug = $this->create_slug($title_ru,$id);
            }else{
                $slug = $this->create_slug($title,$id);
            }
            DB::table('services')
                ->where('id', $id)
                ->update(
                    [
                        'slug'=> $slug
                    ]
                );



            if(isset($request->sub_page_title)){
                $sub_page_titles = $request->sub_page_title;
                $sub_page_titles_ru = $request->sub_page_title_ru;
                $sub_page_titles_en = $request->sub_page_title_en;
                $sub_page_desc_all = $request->sub_page_desc;
                $sub_page_desc_ru_all = $request->sub_page_desc_ru;
                $sub_page_desc_en_all = $request->sub_page_desc_en;

                foreach ($sub_page_titles as $index => $sub_page_title){
                    DB::table('service_sub_page')->insert(
                        [
                            'service_id' => $id,
                            'title_am' => $sub_page_title,
                            'description_am' => $sub_page_desc_all[$index],
                            'title_ru' => $sub_page_titles_ru[$index],
                            'description_ru' => $sub_page_titles_en[$index],
                            'title_en' => $sub_page_desc_ru_all[$index],
                            'description_en' => $sub_page_desc_en_all[$index]
                        ]
                    );

                }
            }


        }
        return redirect('admin/services');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $settings = DB::table('settings')->where('id', 1)->first();
        $service = DB::table('services')->where('id', $id)->first();
        $service_sub_pages = DB::table('service_sub_page')->where('service_id', $id)->get();
        return view('admin.services.edit', compact('service','service_sub_pages','settings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if (isset($request->edit_service) && $request->edit_service == "ok") {
            $title = $request->title;
            $short_desc = $request->short_desc;
            $desc = $request->desc;
            $service_order = $request->service_order;

            if ($request->meta_title == '') {
                $meta_title = $title;
            } else {

                $meta_title = $request->meta_title;
            }
            if ($request->meta_desc == '') {
                $meta_desc = substr(strip_tags($desc), 0, 255);
            } else {
                $meta_desc = strip_tags($request->meta_desc);
            }
            $meta_key = $request->meta_key;


            $title_ru = $request->title_ru;
            $short_desc_ru = $request->short_desc_ru;
            $desc_ru = $request->desc_ru;


            if ($request->meta_title_ru == '') {
                $meta_title_ru = $title_ru;
            } else {

                $meta_title_ru = $request->meta_title_ru;
            }
            if ($request->meta_desc_ru == '') {
                $meta_desc_ru = substr(strip_tags($desc_ru), 0, 255);
            } else {
                $meta_desc_ru = strip_tags($request->meta_desc_ru);
            }
            $meta_key_ru = $request->meta_key_ru;


            $title_en = $request->title_en;
            $short_desc_en = $request->short_desc_en;
            $desc_en = $request->desc_en;

            if ($request->meta_title_en == '') {
                $meta_title_en = $title_en;
            } else {

                $meta_title_en = $request->meta_title_en;
            }
            if ($request->meta_desc_en == '') {
                $meta_desc_en = substr(strip_tags($desc_en), 0, 255);
            } else {
                $meta_desc_en = strip_tags($request->meta_desc_en);
            }
            $meta_key_en = $request->meta_key_en;


            $service_date = $request->service_date;
            if ($request->file("image")) {
                $image = $request->file("image");
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();
                $newFile = "uploads/services/" . $generatedString . "." . $extension;
                $newFileThumb = "uploads/services/thumbs/" . $generatedString . "." . $extension;
                $filepath = $generatedString . "." . $extension;

                Image::make($image->getRealPath())->resize(null, 470, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFile);
                Image::make($image->getRealPath())->resize(null, 250, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb);
            }else{
                $filepath= $request->old_image;
            }


            $slug = $request->slug;
            if($slug ==''){
                if(config('app.locale') == 'en'){
                    $new_slug = $this->create_slug($title_en,$id);
                }elseif (config('app.locale') == 'ru'){
                    $new_slug = $this->create_slug($title_ru,$id);
                }else{
                    $new_slug = $this->create_slug($title,$id);
                }
            }else{
                $service_isset_slug = DB::table('services')->where('slug', '=', $slug)->where('id', '!=', $id)->orderBy('id', 'desc')->count();
                if($service_isset_slug > 0){
                    $new_slug = $slug."-".$id;
                }else{
                    $new_slug = $slug;
                }
            }



            DB::table('services')
                ->where('id', $id)
                ->update(
                    [
                        'title_am' => $title,
                        'description_am' => $desc,
                        'short_desc_am' => $short_desc,
                        'service_order' => $service_order,
                        'meta_title_am' => $meta_title,
                        'meta_key_am' => $meta_key,
                        'meta_desc_am' => $meta_desc,
                        'title_ru' => $title_ru,
                        'description_ru' => $desc_ru,
                        'short_desc_ru' => $short_desc_ru,
                        'meta_title_ru' => $meta_title_ru,
                        'meta_key_ru' => $meta_key_ru,
                        'meta_desc_ru' => $meta_desc_ru,
                        'title_en' => $title_en,
                        'description_en' => $desc_en,
                        'short_desc_en' => $short_desc_en,
                        'meta_title_en' => $meta_title_en,
                        'meta_key_en' => $meta_key_en,
                        'meta_desc_en' => $meta_desc_en,
                        'banner' => $filepath,
                        'date' => $service_date,
                        'slug' => $new_slug
                    ]
                );


            DB::table('service_sub_page')->where('service_id', '=', $id)->delete();
            if(isset($request->sub_page_title)){
                $sub_page_titles = $request->sub_page_title;
                $sub_page_titles_ru = $request->sub_page_title_ru;
                $sub_page_titles_en = $request->sub_page_title_en;
                $sub_page_desc_all = $request->sub_page_desc;
                $sub_page_desc_ru_all = $request->sub_page_desc_ru;
                $sub_page_desc_en_all = $request->sub_page_desc_en;

                foreach ($sub_page_titles as $index => $sub_page_title){
                    DB::table('service_sub_page')->insert(
                        [
                            'service_id' => $id,
                            'title_am' => $sub_page_title,
                            'description_am' => $sub_page_desc_all[$index],
                            'title_ru' => $sub_page_titles_ru[$index],
                            'description_ru' => $sub_page_titles_en[$index],
                            'title_en' => $sub_page_desc_ru_all[$index],
                            'description_en' => $sub_page_desc_en_all[$index]
                        ]
                    );

                }
            }


            $data['edit_successful'] = "yes";
            $service = DB::table('services')->where('id', $id)->first();
            $service_sub_pages = DB::table('service_sub_page')->where('service_id', $id)->get();
            $settings = DB::table('settings')->where('id', 1)->first();
            return view('admin.services.edit', compact('service', 'data', 'service_sub_pages','settings'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('services')->where('id', '=', $id)->delete();
        DB::table('service_sub_page')->where('service_id', '=', $id)->delete();
    }

    public function changeHidden(Request $request)
    {
        if (isset($_REQUEST)) {
            $id = $request->id;
            $hidden = $request->update_hidden;
            DB::table('services')
                ->where('id', $id)
                ->update(['hidden' => $hidden]);
            if ($hidden == 1) {
                echo "0";
            } else {
                echo "1";
            }
        }


    }

    public function add_sub_pages()
    {
        ?>
        <div class="services_sub_page">
            <div onclick="remove_from_authors(this)" class="remove_sub_page"><i class="fa fa-trash-o"></i></div>
            <div style="<?=(config('app.locale') != 'am') ? "display: none" : ""; ?>" class="form-group arm_block">
                <label for="title">Armenian Title</label>
                <input id="about_title" type="text" class="form-control" name="sub_page_title[]" value="" required autofocus>
            </div>
            <div style="<?=(config('app.locale') != 'ru') ? "display: none" : ""; ?>" class="form-group rus_block">
                <label for="title">Russian Title</label>
                <input id="about_title" type="text" class="form-control" name="sub_page_title_ru[]" value="" autofocus>
            </div>
            <div style="<?=(config('app.locale') != 'en') ? "display: none" : ""; ?>" class="form-group eng_block">
                <label for="title">English Title</label>
                <input id="about_title" type="text" class="form-control" name="sub_page_title_en[]" value="" autofocus>
            </div>
            <div style="<?=(config('app.locale') != 'am') ? "display: none" : ""; ?>" class="form-group arm_block">
                <label for="Description">Armenian Description</label>
                <textarea class="form-control description" name="sub_page_desc[]"></textarea>
            </div>
            <div style="<?=(config('app.locale') != 'ru') ? "display: none" : ""; ?>" class="form-group rus_block ">
                <label for="Description">Russian Description</label>
                <textarea class="form-control description" name="sub_page_desc_ru[]"></textarea>
            </div>
            <div style="<?=(config('app.locale') != 'en') ? "display: none" : ""; ?>" class="form-group eng_block">
                <label for="Description">English Description</label>
                <textarea class="form-control description" name="sub_page_desc_en[]"></textarea>
            </div>
        </div>
        <script>
            tinymce.init({
                selector: 'textarea.description',
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste jbimages",
                    "textcolor"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link jbimages",
                images_upload_base_path: '/images',
                relative_urls: false,
                remove_script_host : false,
                convert_urls : false,
                height: 400
            });
        </script>
        <?php
    }
}
