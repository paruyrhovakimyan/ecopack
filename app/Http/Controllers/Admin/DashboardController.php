<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use App\SubPagesWithPdf;

class DashboardController extends Controller
{
    public function index(){

        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        return redirect('admin/homepage');
    }

    public function upload(Request $request){
        if($request->file("files")) {
            $file = $request->file("files");

            $update_pdf = $file->getClientOriginalName();
            $file->move(
                base_path() . '/public/uploads/content/', $update_pdf
            );
        }else {
            $pdf = "";
        }
    }

    public function adminMenu(){

        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $admin_menu = '';
        $admin_url = url('/');
        $pages = DB::table("pages")->select("slug","title_$default_lang as title", "id","parent")->where('id', '!=', '2')->where('parent', 0)->orderBy("page_order", "asc")->get();
        $url = $_SERVER['REQUEST_URI'];
        $url_array = explode("/",$url);
        $active_page = 0;

        if(isset($_GET['parent']) && $_GET['parent'] !=0){
            $page_parent = DB::table("pages")->select('parent')->where('id', $_GET['parent'])->first();
            if($page_parent->parent !=0){
                $active_page = $page_parent->parent;
            }else{
                $active_page = $_GET['parent'];
            }
        }else{
            if(isset($url_array['3']) && $url_array['3'] != "create" && ($url_array['2']== "sub_pdf" || $url_array['2'] == "sub_pages" || $url_array['2'] == "pages")){
                $page_parent = DB::table("pages")->select('parent')->where('id', $url_array['3'])->first();
                if($page_parent->parent != 0){
                    $active_page = $page_parent->parent;
                }elseif($url_array['3'] == "create"){
                    $active_page = '';
                }else{
                    $active_page = $url_array['3'];
                }
            }elseif($url_array['2']== "sub_pages_with_pdf"){
                $id = $url_array['3'];
                $page_parent = DB::table("pages")->select('parent')->where('id', $id)->first();
                if($page_parent->parent != 0){
                    $active_page = $page_parent->parent;
                }else{
                    $active_page = $id;
                }
            }elseif ($url_array['2']== "page_gallery"){
                if( $url_array['3']== "edit"){
                    $gallery_parent = DB::table("gallery_infos")->select('parent_id')->where('id', $url_array['4'])->first();
                    $page_parent = DB::table("pages")->select('parent')->where('id', $gallery_parent->parent_id)->first();
                }else{
                    $page_parent = DB::table("pages")->select('parent')->where('id', $url_array['4'])->first();
                }


                if($page_parent->parent != 0){
                    $active_page = $page_parent->parent;
                }else{

                    $page_parent1 = DB::table("pages")->select('parent')->where('id',  $page_parent->parent)->first();
                    $active_page = $page_parent1->parent;
                }
            }elseif ($url_array['2'] == "news" || $url_array['2']== "news_pdf"|| $url_array['2']== "news_sub_page_pdf"){
                $news_id = $url_array['3'];
                $page_parent1 = DB::table("news")->select('parent_id')->where('id',  $news_id)->first();
                $active_page = $page_parent1->parent_id;
            }elseif ($url_array['2'] == "categories"){
                $news_id = $url_array['3'];
                $page_parent1 = DB::table("categories")->select('page_parent')->where('id',  $news_id)->first();
                $active_page = $page_parent1->page_parent;
            }elseif ($url_array['2'] == "news_gallery"){

                if($url_array['3'] == "edit"){
                    $news_gal_id = $url_array['4'];
                    $gallery = DB::table("news_gallery_infos")->where('id',$news_gal_id)->first();
                    $parent_news = $gallery->parent_id;
                    $page_parent1 = DB::table("news")->select('parent_id')->where('id',  $parent_news)->first();
                    $active_page = $page_parent1->parent_id;
                }else{
                    $news_id = $url_array['4'];
                    $page_parent1 = DB::table("news")->select('parent_id')->where('id',  $news_id)->first();
                    $active_page = $page_parent1->parent_id;
                }


            }

        }


        foreach ($pages as $page) {
            if($active_page == $page->id){
                $active_item = "active_menu_tab";
            }else{
                $active_item = "";
            }
            $admin_menu .= '<li  class="'.$active_item.' submenu sub_menu'.$page->id.'">
                        <a href="'.$admin_url.'/admin/pages/'.$page->id.'">'.$page->title.'</a>
                    </li>';
        }
        return $admin_menu;

    }

    public function hidden_check(Request $request){
        $id = $request->id;
        $type = $request->type;
        $hidden = $request->hidden;

        switch ($type){
            case "product":{
                DB::table('products')
                    ->where('id', $id)
                    ->update(
                        [
                            'hidden' => $hidden,
                        ]
                    );
                break;
            }
            case "language":{
                DB::table('languages')
                    ->where('id', $id)
                    ->update(
                        [
                            'hidden' => $hidden,
                        ]
                    );
                break;
            }

            case "page":{
                DB::table('pages')
                    ->where('id', $id)
                    ->update(
                        [
                            'hidden' => $hidden,
                        ]
                    );
                break;
            }
            case "coach_cat":{
                DB::table('coach_categories')
                    ->where('id', $id)
                    ->update(
                        [
                            'hidden' => $hidden,
                        ]
                    );
                break;
            }
            case "prcat":{
                DB::table('product_cats')
                    ->where('id', $id)
                    ->update(
                        [
                            'hidden' => $hidden,
                        ]
                    );
                break;
            }
            case "teams":{
                DB::table('teams')
                    ->where('id', $id)
                    ->update(
                        [
                            'hidden' => $hidden,
                        ]
                    );
                break;
            }
            case "sub_page_with_pdf":{
                DB::table('sub_pages_with_pdf')
                    ->where('id', $id)
                    ->update(
                        [
                            'hidden' => $hidden,
                        ]
                    );
                break;
            }
            case "page_pdf":{
                DB::table('page_pdf')
                    ->where('id', $id)
                    ->update(
                        [
                            'hidden' => $hidden,
                        ]
                    );
                break;
            }
            case "news_sub_page":{
                DB::table('news_sub_page')
                    ->where('id', $id)
                    ->update(
                        [
                            'hidden' => $hidden,
                        ]
                    );
                break;
            }

            case "page_gallery":{
                DB::table('gallery_infos')
                    ->where('id', $id)
                    ->update(
                        [
                            'hidden' => $hidden,
                        ]
                    );
                break;
            }
            case "sub_pages_with_galleries":{
                DB::table('gallery_infos')
                    ->where('id', $id)
                    ->update(
                        [
                            'hidden' => $hidden,
                        ]
                    );
                break;
            }
            case "news_pdf":{
                DB::table('news_pdf')
                    ->where('id', $id)
                    ->update(
                        [
                            'hidden' => $hidden,
                        ]
                    );
                break;
            }
            case "news_gallery":{
                DB::table('news_gallery_infos')
                    ->where('id', $id)
                    ->update(
                        [
                            'hidden' => $hidden,
                        ]
                    );
                break;
            }
            case "news":{
                DB::table('news')
                    ->where('id', $id)
                    ->update(
                        [
                            'hidden' => $hidden,
                        ]
                    );
                break;
            }
            case "category":{
                DB::table('categories')
                    ->where('id', $id)
                    ->update(
                        [
                            'hidden' => $hidden,
                        ]
                    );
                break;
            }
            case "project_categories":{
                DB::table('project_categories')
                    ->where('id', $id)
                    ->update(
                        [
                            'hidden' => $hidden,
                        ]
                    );
                break;
            }
            case "slide":{
                DB::table('slider_items')
                    ->where('id', $id)
                    ->update(
                        [
                            'hidden' => $hidden,
                        ]
                    );;
                break;
            }
            case "partner":{
                DB::table('partners')
                    ->where('id', $id)
                    ->update(
                        [
                            'hidden' => $hidden,
                        ]
                    );;
                break;
            }
            case "private_area_pdf":{
                DB::table('private_area_pdf')
                    ->where('id', $id)
                    ->update(
                        [
                            'hidden' => $hidden,
                        ]
                    );;
                break;
            }
            case "private_area":{
                DB::table('private_area_config')
                    ->where('id', $id)
                    ->update(
                        [
                            'hidden' => $hidden,
                        ]
                    );;
                break;
            }
            case "careers":{
                DB::table('careers')
                    ->where('id', $id)
                    ->update(
                        [
                            'hidden' => $hidden,
                        ]
                    );;
                break;
            }
            case "coaches":{
                DB::table('coaches')
                    ->where('id', $id)
                    ->update(
                        [
                            'hidden' => $hidden,
                        ]
                    );;
                break;
            }


        }
    }

    public function checked_delete(Request $request){


        $back_url = app('Illuminate\Routing\UrlGenerator')->previous();

        var_dump($request->checked_one_input);
        if(!empty($request->checked_one_input)){
            foreach ($request->checked_one_input as $key => $val) {
                $type = $request->check_type[$key];
                switch ($type){
                    case "careers":{
                        DB::table('careers')->where('id', '=', $val)->delete();
                        break;
                    }
                    case "page":{
                        DB::table('pages')->where('id', '=', $val)->delete();
                        break;
                    }
                    case "teams":{
                        DB::table('teams')->where('id', '=', $val)->delete();
                        break;
                    }
                    case "news_sub_page":{
                        $images_for_delete = DB::table('news_sub_page_pdf')->where('sub_page_id', $val)->get();
                        foreach ($images_for_delete as $image_for_delete){
                            $newFile = "uploads/news/pdfs/" .$image_for_delete->file;
                            unlink($newFile);
                        }
                        DB::table('news_sub_page')->where('id', '=', $val)->delete();
                        break;
                    }
                    case "pdf":{
                        DB::table('page_pdf')->where('id', '=', $val)->delete();
                        break;
                    }
                    case "gallery":{
                        $images_for_delete = DB::table('news_gallery')->where('parent_id', '=', $val)->get();

                        foreach ($images_for_delete as $image_for_delete){
                            $newFile = "uploads/gallery/" .$image_for_delete->image;
                            $newFileThumb = "uploads/gallery/thumbs/" .$image_for_delete->image;
                            $newFileThumb_150 = "uploads/gallery/150/".$image_for_delete->image;
                            if(file_exists($newFile)){
                                unlink($newFile);
                            }
                            if(file_exists($newFile)){
                                unlink($newFileThumb);
                            }
                            if(file_exists($newFile)){
                                unlink($newFileThumb_150);
                            }
                        }
                        DB::table('gallery_infos')->where('id', '=', $val)->delete();
                        break;
                    }
                    case "sub_page_with_pdf":{
                        $images_for_delete = DB::table('sub_pages_with_pdf_files')->where('sub_page_id', '=', $val)->get();

                        foreach ($images_for_delete as $image_for_delete){
                            $newFile = "uploads/page/pdfs/" .$image_for_delete->file;
                            if(file_exists($newFile)){
                                unlink($newFile);
                            }
                        }
                        DB::table('sub_pages_with_pdf')->where('id', '=', $val)->delete();
                        break;
                    }
                    case "news":{
                        DB::table('news')->where('id', '=', $val)->delete();
                        break;
                    }
                    case "category":{
                        DB::table('categories')
                            ->where('id', '=', $val)->delete();
                        break;
                    }
                    case "slide":{
                        DB::table('slider_items')
                            ->where('id', '=', $val)->delete();
                        break;
                    }
                    case "partner":{
                        DB::table('partners')
                            ->where('id', '=', $val)->delete();
                        break;
                    }
                    case "users":{
                        DB::table('users')
                            ->where('id', '=', $val)->delete();
                        break;
                    }
                    case "admins":{
                        DB::table('admins')
                            ->where('id', '=', $val)->delete();
                        break;
                    }
                    case "private_area":{
                        DB::table('private_area_config')
                            ->where('id', '=', $val)->delete();
                        break;
                    }
                    case "private_area_pdf":{
                        DB::table('private_area_pdf')
                            ->where('id', '=', $val)->delete();
                        break;
                    }
                    case "project_categories":{
                        DB::table('project_categories')
                            ->where('id', '=', $val)->delete();
                        break;
                    }

                }
            }
        }

        return redirect($back_url);

    }


    public function upload_content(Request $request){
        $imageName = substr(md5(microtime()),rand(0,26),10);
        $image = request()->file('file');
        $generatedString = time();
        $extension = $image->getClientOriginalExtension();
        $newFile = public_path('uploads/content_img/');
        $filepath = $imageName.'_'.$generatedString . "." . $extension;

        $image->move($newFile, $filepath);
        $url = url('/');
        $img_file = "$url/public/uploads/content_img/$filepath";
        $location = '/public/uploads/content_img/'.$filepath;

        $array = ['location' => $location];
        $json = json_encode($array);
        return $json;
    }

    public function donateIrders(){
        $orders = DB::table('donate_orders')->where('status', '=', 55)->orderBy('id', 'desc')->get();
        foreach ($orders as $order){
            $project_id = $order->project_id;
            $project = SubPagesWithPdf::find($project_id);
            $order->project_title = $project->title_am;
        }
        return view("admin.orders.orders", compact('orders'));
    }
}
