<?php

namespace App\Http\Controllers\Admin;

use App\GalleryInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class SubPageWithGalleryController extends Controller
{

    public function add_gallery($parent_id){

        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $page_bredcrump = '';
        $page_parent = DB::table('pages')->select("title_$default_lang as title", "parent")->where('id', $parent_id)->first();

        if($page_parent->parent != 0){
            $page_super_parent = DB::table('pages')->select("title_$default_lang as title")->where('id', $page_parent->parent)->first();
            $page_bredcrump = '<h4><a href="'.url('/').'/admin/pages/'.$page_parent->parent.'">'.$page_super_parent->title .'</a> | '.$page_parent->title.'</h4>';
        }else{
            $page_bredcrump = '<h4>'.$page_parent->title.' | Add New</h4>';
        }

        $languages = DB::table('languages')->where('hidden','=', 0)->get();
        return view("admin.sub_page_gallery.create", compact('languages','page_bredcrump','parent_id'));
    }


    public function storeGallery(Request $request,$parent_id){
        if(isset($request->add_gallery) && $request->add_gallery == "ok"){
            $title = $request->title;
            $desc = $request->description;
            $galleryInfo = new GalleryInfo();
            $galleryInfo->slug = '-';
            $galleryInfo->parent_id = $parent_id;
            foreach($request->all() as $key => $val){
                if($key !='_token' && $key !='add_gallery' && $key !='filename'){
                    $galleryInfo->$key = $val;
                }
            }
            $galleryInfo->save();
            if (isset($request->filename) && $request->filename != '') {
                foreach ($request->filename as $filename) {

                    DB::table('gallery')->insert(
                        [
                            'image' => $filename,
                            'parent_id' => $galleryInfo->id
                        ]
                    );
                }
            }
        }
        $slug = $this->create_slug($galleryInfo->title_am, $galleryInfo->id);
        DB::table('gallery_infos')
            ->where('id', $galleryInfo->id)
            ->update(
                [
                    'slug' => $slug
                ]
            );


        return redirect('admin/pages/'.$parent_id);

    }
    private function mb_str_split($string)
    {
        # Split at all position not after the start: ^
        # and not before the end: $
        return preg_split('/(?<!^)(?!$)/u', $string);
    }

    private function create_english($title)
    {
        $charlist = $this->mb_str_split($title);
        $array_hayeren = array('.','ա','բ','գ','դ','ե','զ','է','ը','թ','ժ','ի','լ','խ','ծ','կ','հ','ձ','ղ','ճ','մ','յ','ն','շ','ո','չ','պ','ջ','ռ','ս','վ','տ','ր','ց','ւ','փ','ք','և','օ','ֆ',' ','  ','/','\\','&','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','1','2','3','4','5','6','7','8','9','0');
        $array_english = array('-','a','b','g','d','e','z','e','y','t','zh','i','l','kh','ts','k','h','dz','gh','ch','m','y','n','sh','o','ch','p','j','r','s','v','t','r','c','u','p','q','ev','o','f','-','-','-','-','-','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','1','2','3','4','5','6','7','8','9','0');
        $new = '';
        if (count($charlist) < 41) {
            $counts = count($charlist);
        } else {
            $counts = 40;
        }
        for ($i = 0; $i < $counts; $i++) {
            $key = array_search(mb_strtolower($charlist[$i], 'UTF-8'), $array_hayeren);
            $new .= $array_english[$key];

        }
        return $new;
    }

    private function create_slug($title, $id_obj)
    {

        $slug = $this->create_english($title);
        $article_isset_slug = DB::table('gallery_infos')->where('slug', '=', $slug)->orderBy('id', 'desc')->count();
        if ($article_isset_slug > 0) {
            $new_slug = $slug . "-" . $id_obj;
        } else {
            $new_slug = $slug;
        }

        return $new_slug;
    }

    public function edit_gallery($id){


        $gallery = DB::table('gallery_infos')->where('id',$id)->first();

        $gallery_parent_page = $gallery->parent_id;

        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $page_parent = DB::table('pages')->select("title_$default_lang as title", "parent")->where('id', $gallery_parent_page)->first();

        if($page_parent->parent != 0){
            $page_super_parent = DB::table('pages')->select("title_$default_lang as title")->where('id', $page_parent->parent)->first();
            $page_bredcrump = '<h4><a href="'.url('/').'/admin/pages/'.$page_parent->parent.'">'.$page_super_parent->title.' </a> | '.$gallery->title.'</h4>';
        }else{
            $page_bredcrump = '<h4>'.$page_parent->title.' | '.$gallery->{"title_$default_lang"}.'</h4>';
        }




        $projectImages = DB::table('gallery')->where('parent_id',$id)->get();
        $dropzoneDefaultHidden = '';
        if (count($projectImages) != 0) {
            $dropzoneDefaultHidden = 'dz-default-hidden';
        }
        $languages = DB::table('languages')->where('hidden','=', 0)->get();

        return view("admin.sub_page_gallery.edit", compact('languages','gallery','projectImages','dropzoneDefaultHidden','default_lang','page_bredcrump'));
    }


    public function updateGallery(Request $request,$id){


        if(isset($request->edit_gallery) && $request->edit_gallery == "ok"){
            $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
            $default_lang = $first_lang->short;

            $page_gallery = GalleryInfo::find($id);
            foreach($request->all() as $key => $val){
                if($key !='_token' && $key !='edit_gallery' && $key !='filename'){
                    $page_gallery->$key = $val;
                }
            }
            $page_gallery->save();
            DB::table('gallery')->where('parent_id', '=', $id)->delete();
            if (isset($request->filename) && $request->filename != '') {
                foreach ($request->filename as $filename) {

                    DB::table('gallery')->insert(
                        [
                            'image' => $filename,
                            'parent_id' => $id
                        ]
                    );
                }
            }
        }


        $gallery_parent = DB::table('gallery_infos')->select("parent_id")->where('id', $id)->first();

        return redirect('admin/pages/'.$gallery_parent->parent_id);
    }

    public function upload(Request $request)
    {
        $imageName = substr(md5(microtime()),rand(0,26),10);
        $image = Input::file('file');
        $generatedString = time();
        $extension = $request->file("file")->getClientOriginalExtension();
        $newFile = public_path('uploads/gallery/' . $imageName.'_'. $generatedString . "." . $extension);
        $newFileThumb = public_path('uploads/gallery/thumbs/' . $imageName.'_'. $generatedString . "." . $extension);
        $newFileThumb_150 = public_path('uploads/gallery/150/' . $imageName.'_'. $generatedString . "." . $extension);
        $filepath = $imageName.'_'.$generatedString . "." . $extension;

        Image::make($image->getRealPath())->orientate()->save($newFile);
        Image::make($image->getRealPath())->resize(260, 350)->orientate()->save($newFileThumb);
        Image::make($image->getRealPath())->resize(null, 150, function ($constraint) {
            $constraint->aspectRatio();
        })->orientate()->save($newFileThumb_150);
        echo $filepath;
    }

    public function gallery_delete(Request $request){
        $back_url = app('Illuminate\Routing\UrlGenerator')->previous();
        $id = $request->id;
        DB::table('gallery_infos')->where('id', '=', $id)->delete();
        return redirect($back_url);
    }
}
