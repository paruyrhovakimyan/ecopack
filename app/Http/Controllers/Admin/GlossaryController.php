<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Glossary as Glossary;


class GlossaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {



        if(isset($request->id) && $request->id !=''){
            $id = $request->id;
            $edit_glssary = DB::table('glossaries')->where('id', $id)->first();
        }else{
            $edit_glssary = '';
        }
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();
        $all_glossary = DB::table('glossaries')->orderBy('id', 'desc')->get();
        return view("admin.glossary.all", compact('languages','all_glossary','edit_glssary'));
    }





    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(isset($request->save_glossary) && $request->save_glossary == 'ok'){
            $short_code = $request->short_code;
            $glossary_am = $request->glossary_am;
            $glossary_ru = $request->glossary_ru;
            $glossary_en = $request->glossary_en;

            if($request->glossary_id != 0){

                $glossarty = Glossary::find($request->glossary_id);
            }else{
                $glossarty = new Glossary();
            }
            foreach ($request->all() as $key => $val) {
                if ($key != 'add_glossary_submit' && $key != '_token' && $key != 'save_glossary' && $key != 'glossary_id' && $key != 'glossary_id') {
                    $glossarty->$key = $val;
                }
            }
            $glossarty->save();

        }
        return redirect('admin/glossary');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
