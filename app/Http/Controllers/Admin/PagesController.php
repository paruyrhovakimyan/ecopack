<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use App\Page as Page;
use App\CoachCategory;
use App\Coach;

class PagesController extends Controller
{


    private function mb_str_split($string)
    {
        # Split at all position not after the start: ^
        # and not before the end: $
        return preg_split('/(?<!^)(?!$)/u', $string);
    }

    private function create_english($title)
    {
        $charlist = $this->mb_str_split($title);
        $array_hayeren = array('.', 'ա', 'բ', 'գ', 'դ', 'ե', 'զ', 'է', 'ը', 'թ', 'ժ', 'ի', 'լ', 'խ', 'ծ', 'կ', 'հ', 'ձ', 'ղ', 'ճ', 'մ', 'յ', 'ն', 'շ', 'ո', 'չ', 'պ', 'ջ', 'ռ', 'ս', 'վ', 'տ', 'ր', 'ց', 'ւ', 'փ', 'ք', 'և', 'օ', 'ֆ', ' ', '  ', '/', '\\', '&', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0');
        $array_english = array('-', 'a', 'b', 'g', 'd', 'e', 'z', 'e', 'y', 't', 'zh', 'i', 'l', 'kh', 'ts', 'k', 'h', 'dz', 'gh', 'ch', 'm', 'y', 'n', 'sh', 'o', 'ch', 'p', 'j', 'r', 's', 'v', 't', 'r', 'c', 'u', 'p', 'q', 'ev', 'o', 'f', '-', '-', '-', '-', '-', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0');
        $new = '';
        if (count($charlist) < 41) {
            $counts = count($charlist);
        } else {
            $counts = 40;
        }
        for ($i = 0; $i < $counts; $i++) {
            $key = array_search(mb_strtolower($charlist[$i], 'UTF-8'), $array_hayeren);
            $new .= $array_english[$key];

        }
        return $new;
    }

    private function create_slug($title, $first_code)
    {

        $slug = $this->create_english($title);
        $article_isset_slug = DB::table("pages")->where('slug', '=', $slug)->orderBy('id', 'desc')->count();
        if ($article_isset_slug > 0) {
            $new_slug = $slug . "-" . $article_isset_slug;
        } else {
            $new_slug = $slug;
        }

        return $new_slug;
    }
    private function create_slug_id($title, $first_code, $id)
    {

        $slug = $this->create_english($title);
        $article_isset_slug = DB::table("pages")->where('id', '!=', $id)->where('slug', '=', $slug)->orderBy('id', 'desc')->count();
        if ($article_isset_slug > 0) {
            $new_slug = $slug . "-" . $article_isset_slug;
        } else {
            $new_slug = $slug;
        }

        return $new_slug;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $pages = DB::table('pages')->select("gallery", "pdf_uploder", "sub_pages", "title_$default_lang as title", "id")->where('id', '!=', '2')->where('parent', 0)->get();
        return view("admin.pages.all", compact('pages', 'default_lang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages = DB::table('languages')->where('hidden','=', 0)->get();
        return view('admin.pages.create', compact('languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if (isset($request->add_page) && $request->add_page == "ok") {

            $languages = DB::table('languages')->where('hidden','=', 0)->get();

            $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
            $default_lang = $first_lang->short;
            $first_title = $request->input("title_".$default_lang);

            //Page Configs

            if (isset($request->sub_page) && $request->sub_page == '1') {
                $sub_page = $request->sub_page;
            } else {
                $sub_page = 0;
            }
            if (isset($request->gallery) && $request->gallery == '1') {
                $gallery = $request->gallery;
            } else {
                $gallery = 0;
            }
            if (isset($request->pdf_uploader) && $request->pdf_uploader == '1') {
                $pdf_uploader = $request->pdf_uploader;
            } else {
                $pdf_uploader = 0;
            }

            if (isset($request->parent) && $request->parent != '') {
                $parent_id = $request->parent;
            } else {
                $parent_id = 0;
            }
            if (isset($request->page_with_pdf) && $request->page_with_pdf != '') {
                $page_with_pdf = $request->page_with_pdf;
            } else {
                $page_with_pdf  = 0;
            }

            if (isset($request->config_data1) && $request->config_data1 != '') {
                $config_data1 = $request->config_data1;
            } else {
                $config_data1 = 0;
            }
            if (isset($request->config_data2) && $request->config_data2 != '') {
                $config_data2 = $request->config_data2;
            } else {
                $config_data2 = 0;
            }
            if (isset($request->config_cat) && $request->config_cat != '') {
                $config_cat = $request->config_cat;
            } else {
                $config_cat = 0;
            }
            if (isset($request->config_tag) && $request->config_tag != '') {
                $config_tag = $request->config_tag;
            } else {
                $config_tag = 0;
            }
            if (isset($request->config_image) && $request->config_image != '') {
                $config_image = $request->config_image;
            } else {
                $config_image = 0;
            }
            if (isset($request->config_bg_image) && $request->config_bg_image != '') {
                $config_bg_image = $request->config_bg_image;
            } else {
                $config_bg_image = 0;
            }
            if (isset($request->config_logo) && $request->config_logo != '') {
                $config_logo = $request->config_logo;
            } else {
                $config_logo = 0;
            }


            if (isset($request->config_project) && $request->config_project != '') {
                $config_project = $request->config_project;
            } else {
                $config_project = 0;
            }

            if ($request->file("image")) {

                $image = Input::file('image');
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();
                $newFile = public_path('uploads/page/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->orientate()->save($newFile);
                $newFileThumb = public_path('uploads/page/thumbs/' . $generatedString . "." . $extension);
                $filepath = $generatedString . "." . $extension;
                Image::make($image->getRealPath())->resize(520, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb);

            } else {
                $filepath = "";
            }

            if ($request->file("image_bg")) {
                $image = Input::file('image_bg');
                $generatedString = "bg_".time();
                $extension = $request->file("image_bg")->getClientOriginalExtension();
                $newFile =  public_path('uploads/page/' . $generatedString . "." . $extension);
                $image_bg = $generatedString . "." . $extension;

                Image::make($image->getRealPath())->orientate()->save($newFile);
            } else {
                $image_bg = "";
            }
            if ($request->file("image_logo")) {
                $image = Input::file('image_logo');
                $generatedString = "logo_".time();
                $extension = $request->file("image_logo")->getClientOriginalExtension();
                $newFile =  public_path('uploads/page/thumbs/' . $generatedString . "." . $extension);

                $image_logo = $generatedString . "." . $extension;

                Image::make($image->getRealPath())->orientate()->save($newFile);
            } else {
                $image_logo = "";
            }

            $back_color = $request->bg_color;
            $page = new Page();
            $new_slug = $this->create_slug($first_title, $default_lang);
            $page->slug = $new_slug;
            $page->sub_pages = $sub_page;
            $page->gallery = $gallery;
            $page->pdf_uploder = $pdf_uploader;
            $page->page_with_pdf = $page_with_pdf;
            $page->config_data1 = $config_data1;
            $page->config_data2 = $config_data2;
            $page->config_cat = $config_cat;
            $page->config_tag = $config_tag;
            $page->config_image = $config_image;
            $page->config_bg_image = $config_bg_image;
            $page->config_logo = $config_logo;
            $page->config_project = $config_project;

            foreach($request->all() as $key => $val){
                if($key !='add_page'){
                    $page->$key = $val;
                }
            }

            if (isset($request->show_in_home) && $request->show_in_home != '') {
                $show_in_home = $request->show_in_home;
            } else {
                $show_in_home = 0;
            }
            $page->show_in_home = $show_in_home;
            $page->image = $filepath;
            $page->image_bg = $image_bg;
            $page->image_logo = $image_logo;
            $page->bg_color = $back_color;
            $page->save();
            if ($parent_id != 0) {
                return redirect('admin/pages/' . $parent_id);
            } else {
                return redirect('admin/pages');
            }


        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($id == 21){
            return redirect('admin/pages/21/edit');
        }else{

            $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
            $default_lang = $first_lang->short;
            $page = DB::table("pages")
                ->select(
                    "title_$default_lang as title",
                    "description_$default_lang as description",
                    "short_desc_$default_lang as short_desc",
                    "meta_title_$default_lang as meta_title",
                    "meta_desc_$default_lang as meta_desc",
                    "meta_key_$default_lang as meta_key",
                    "gallery",
                    "sub_pages",
                    "pdf_uploder",
                    "parent",
                    "hidden",
                    "slug",
                    "id",
                    "config_data1",
                    "config_data2",
                    "config_cat",
                    "config_tag",
                    "config_image",
                    "sub_pages_with_gallery",
                    "config_project",
                    "page_with_pdf"
                )->where('id', $id)->first();
            $page->child_pages = '';
            if ($page->sub_pages == '1') {
                $child_pages = DB::table("pages")->select(
                    "title_$default_lang as title",
                    "description_$default_lang as description",
                    "short_desc_$default_lang as short_desc",
                    "meta_title_$default_lang as meta_title",
                    "meta_desc_$default_lang as meta_desc",
                    "meta_key_$default_lang as meta_key",
                    "gallery",
                    "sub_pages",
                    "pdf_uploder",
                    "parent",
                    "hidden",
                    "slug",
                    "id",
                    "config_data1",
                    "config_data2",
                    "config_cat",
                    "config_tag",
                    "config_image",
                    "page_with_pdf"
                )
                    ->where('parent', $id)
                    ->orderBy('id', 'desc')->get();
                if (!empty($child_pages)) {
                    foreach ($child_pages as $child_page) {
                        $child_page->child_page_galleries = '';
                        $child_page->child_page_pdfs = '';
                        $child_page->page_gallery_count = 0;
                        if($child_page->pdf_uploder == 1){
                            $child_page_pdfs = DB::table("page_pdfs")->select("pdf_name_$default_lang as title", "id", "hidden")
                                ->where('page_id',$child_page->id)->orderBy('id', 'desc')->get();
                            $child_page->child_page_pdfs = $child_page_pdfs;
                        }
                        if($child_page->gallery == 1){
                            $page_gallery = DB::table("gallery_infos")->select("hidden","id","title_$default_lang as title")->where('parent_id',$child_page->id)->orderBy('id', 'desc')->get();
                            $page_gal_count = DB::table("gallery_infos")->where('parent_id',$child_page->id)->orderBy('id', 'desc')->count();
                           if($page_gal_count > 0){
                               foreach ($page_gallery as $page_gallery_one) {

                                   $page_gallery_count = DB::table('gallery')->where('parent_id',$page_gallery_one->id)->count();
                                   $child_page->child_page_galleries = $page_gallery;
                               }
                               $child_page->page_gallery_count = $page_gallery_count;
                           }

                        }
                        $child_page->child_sub_pages_with_pdf = '';
                        if ($child_page->page_with_pdf == 1){
                            $child_sub_pages_with_pdfs = DB::table("sub_pages_with_pdfs")
                                ->select("title_$default_lang as title","id","hidden")
                                ->where('parent_id',$child_page->id)
                                ->where('folder_parent',0)
                                ->get();
                            if (!empty($child_sub_pages_with_pdfs)){
                                $child_page->child_sub_pages_with_pdf = $child_sub_pages_with_pdfs;
                            }
                        }
                        if(isset($child_sub_pages_with_pdfs) && !empty($child_sub_pages_with_pdfs)){
                            foreach ($child_sub_pages_with_pdfs as $child_sub_pages_with_pdf) {
                                $child_sub_pages_with_pdf->pdf_folders = '';
                                $child_sub_pdfs_folder = DB::table("sub_pages_with_pdfs")
                                    ->select("title_$default_lang as title","description_$default_lang as description","id","hidden")
                                    ->where('folder_parent',$child_sub_pages_with_pdf->id)
                                    ->get();
                                if (!empty($child_sub_pdfs_folder)){
                                    $child_sub_pages_with_pdf->pdf_folders = $child_sub_pdfs_folder;
                                }


                                $child_sub_pages_with_pdf->gallery_folders = '';

                                $page_gallery = DB::table("gallery_infos")->select("hidden","id","title_$default_lang as title")->where('sub_parent_id',$child_sub_pages_with_pdf->id)->orderBy('id', 'desc')->get();
                                $page_gal_count = DB::table("gallery_infos")->where('sub_parent_id',$child_sub_pages_with_pdf->id)->orderBy('id', 'desc')->count();
                                if($page_gal_count > 0){
                                    foreach ($page_gallery as $page_gallery_one) {

                                        $page_gallery_count = DB::table('gallery')->where('sub_parent_id',$page_gallery_one->id)->count();
                                        $child_sub_pages_with_pdf->gallery_folders = $page_gallery;
                                    }
                                    $child_sub_pages_with_pdf->page_gallery_count = $page_gallery_count;
                                }

                            }
                        }


                    }
                    $page->child_pages = $child_pages;
                }
            }


            $page->sub_pages_with_galleries = '';

            if($page->sub_pages_with_gallery == 1){
                $sub_pages_with_gallery = DB::table("gallery_infos")->select("hidden","id","title_$default_lang as title","parent_id")->where('gallery_parent',0)->where('parent_id',$page->id)->orderBy('id', 'desc')->get();
                if(count($sub_pages_with_gallery) > 0){
                    foreach ($sub_pages_with_gallery as $page_gallery_one) {
                        $page_gallery_one->gallery_sub_galeries = '';
                        $page_sub_gallery = DB::table("gallery_infos")->select("hidden","id","title_$default_lang as title")->where('gallery_parent',$page_gallery_one->id)->orderBy('id', 'desc')->get();

                        if(count($page_sub_gallery) > 0){
                            $page_gallery_one->gallery_sub_galeries = $page_sub_gallery;
                        }



                    }
                    $page->sub_pages_with_galleries = $sub_pages_with_gallery;
                    $page->sub_pages_with_galleries_count = count($sub_pages_with_gallery);
                }

            }


           /* echo "<pre>";
            var_dump($page->sub_pages_with_galleries);
            echo "</pre>";
            return 1;*/


            $page->sub_pages_with_pdf = '';
            if ($page->page_with_pdf == '1'){
                $sub_pages_with_pdfs = DB::table("sub_pages_with_pdfs")->select("title_$default_lang as title","id","hidden")->where('parent_id',$id)->get();
                if (!empty($sub_pages_with_pdfs)){
                    $page->sub_pages_with_pdf = $sub_pages_with_pdfs;
                }
            }

            if($page->pdf_uploder == 1){
                $parebt_page_pdfs = DB::table("page_pdfs")->select("pdf_name_$default_lang as title", "id")
                    ->where('page_id',$id)
                    ->orderBy('id', 'desc')->get();
                $page->parent_page_pdfs = $parebt_page_pdfs;
            }
            $page->sub_pages_with_projects = '';
            if($page->config_project == 1){
                $sub_pages_with_projects = DB::table("sub_pages_with_pdfs")->select("title_$default_lang as title","id","hidden")->where('parent_id',$id)->get();
                if (!empty($sub_pages_with_projects)){
                    $page->sub_pages_with_projects = $sub_pages_with_projects;
                }
            }



            $page->newses = '';
            $page->news_pdf = '';
            $page->news_sub_page = '';
            if($page->config_cat == 1){

                $page_news = DB::table("news")->select("cat_id","page_pdf","gallery_config","pdf_config","title_$default_lang as title","id","created_at","image","hidden")
                    ->where("parent_id", $id)->orderby('id', "DESC")
                    ->orderBy('id', 'desc')->get();


                if(!empty($page_news)){
                    foreach ($page_news as $page_new) {
                        $page_new->count_news_sub_page_pdf = 0;

                        $page_new->news_gallery_count = 0;
                        $page_news_pdfs = DB::table("news_pdfs")->select("pdf_name_$default_lang as title", "id","hidden")
                            ->where('news_id',$page_new->id)
                            ->orderBy('id', 'desc')->get();
                        if(!empty($page_news_pdfs)){
                            $page_new->pdf = $page_news_pdfs;
                        }


                        $news_galleries = DB::table("news_gallery_infos")->where('parent_id',$page_new->id)
                            ->orderBy('id', 'desc')->get();
                        if(!empty($news_galleries)){
                            $page_new->galleries =  $news_galleries;
                            foreach ($news_galleries as $news_gallery){
                                $news_gallery_count = DB::table('news_gallery')->where('parent_id',$news_gallery->id)->count();
                                $news_gallery->count_news_sub_page_pdf = $news_gallery_count;
                            }
                        }

                        $page_news_sub_page = DB::table("news_sub_page")->select("title_$default_lang as title","hidden", "id") ->where('news_id',$page_new->id)
                            ->orderBy('id', 'desc')->get();
                        //return $page_new->id;
                        foreach ($page_news_sub_page as $page_news_sub_page_one){
                            $count_news_sub_page_pdf = DB::table("news_sub_page_pdf")->where('sub_page_id',$page_news_sub_page_one->id)
                                ->count();
                            $page_news_sub_page_one->count_news_sub_page_pdf = $count_news_sub_page_pdf;
                        }

                        if(!empty($page_news_sub_page)){
                            $page_new->news_sub_page = $page_news_sub_page;
                        }
                        //return $page_new->count_news_sub_page_pdf;
                    }

                    $page->newses = $page_news;

                }

            }

            return view('admin.pages.show', compact('default_lang','page'));
        }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;

        $languages = DB::table('languages')->where('hidden','=', 0)->get();
        $page = DB::table('pages')->where('id', $id)->first();
        $page_settings = DB::table('pages')->select("calendar_config","config_project","config_logo","config_bg_image","id","config_data1","config_data2","config_cat","config_tag","config_image")->where('id',$page->parent)->first();

        $settings = DB::table('settings')->where('id', 1)->first();

        $coachcategories = CoachCategory::all();
        $coaches = Coach::all();

        $page_calendars = DB::table('calendars')->where('parent_id', '=', $id)->get();
        $page->calendar_times = array();
        $page->erku_coach_cats = array();
        $page->ereq_coach_cats = array();
        $page->choreq_coach_cats = array();
        $page->hing_coach_cats = array();
        $page->urbat_coach_cats = array();
        $page->shabat_coach_cats = array();
        $page->erku_coaches = array();
        $page->ereq_coaches = array();
        $page->choreq_coaches = array();
        $page->hing_coaches = array();
        $page->urbat_coaches = array();
        $page->shabat_coaches = array();
        $page->kiraki_coach_cats = array();
        $page->kiraki_coaches = array();

        if(isset($page_calendars)){

            foreach ($page_calendars as $page_calendar){
                $page->calendar_times[] = json_decode($page_calendar->calendar_time);

               $page->erku_coach_cats[] = json_decode($page_calendar->erku_coach_cat);
                 $page->ereq_coach_cats[] = json_decode($page_calendar->ereq_coach_cat);
                $page->choreq_coach_cats[] = json_decode($page_calendar->choreq_coach_cat);
                $page->hing_coach_cats[] = json_decode($page_calendar->hing_coach_cat);
                $page->urbat_coach_cats[] = json_decode($page_calendar->urbat_coach_cat);
                $page->shabat_coach_cats[] = json_decode($page_calendar->shabat_coach_cat);
                $page->kiraki_coach_cats[] = json_decode($page_calendar->kiraki_coach_cat);
                $page->erku_coaches[] = json_decode($page_calendar->erku_coachs);
                $page->ereq_coaches[] = json_decode($page_calendar->ereq_coachs);
                $page->choreq_coaches[] = json_decode($page_calendar->choreq_coachs);
                $page->hing_coaches[] = json_decode($page_calendar->hing_coachs);
                $page->urbat_coaches[] = json_decode($page_calendar->urbat_coachs);
                $page->shabat_coaches[] = json_decode($page_calendar->shabat_coachs);
                $page->kiraki_coaches[] = json_decode($page_calendar->kiraki_coachs);
            }

        }
        return view('admin.pages.edit', compact('page_calendars','coaches','coachcategories','default_lang','languages','page_settings','page', 'settings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (isset($request->edit_page) && $request->edit_page == "ok") {





            $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
            $default_lang = $first_lang->short;
            $first_title = $request->input("title_".$default_lang);

            $new_slug = $this->create_slug_id($first_title, $default_lang, $id);

            if (isset($request->sub_page) && $request->sub_page == '1') {
                $sub_page = $request->sub_page;
            } else {
                $sub_page = 0;
            }
            if (isset($request->gallery) && $request->gallery == '1') {
                $gallery = $request->gallery;
            } else {
                $gallery = 0;
            }
            if (isset($request->pdf_uploader) && $request->pdf_uploader == '1') {
                $pdf_uploader = $request->pdf_uploader;
            } else {
                $pdf_uploader = 0;
            }
            if (isset($request->config_data1) && $request->config_data1 != '') {
                $config_data1 = $request->config_data1;
            } else {
                $config_data1 = 0;
            }
            if (isset($request->config_data2) && $request->config_data2 != '') {
                $config_data2 = $request->config_data2;
            } else {
                $config_data2 = 0;
            }
            if (isset($request->config_cat) && $request->config_cat != '') {
                $config_cat = $request->config_cat;
            } else {
                $config_cat = 0;
            }
            if (isset($request->config_tag) && $request->config_tag != '') {
                $config_tag = $request->config_tag;
            } else {
                $config_tag = 0;
            }
            if (isset($request->config_image) && $request->config_image != '') {
                $config_image = $request->config_image;
            } else {
                $config_image = 0;
            }
            if (isset($request->page_with_pdf) && $request->page_with_pdf != '') {
                $page_with_pdf = $request->page_with_pdf;
            } else {
                $page_with_pdf  = 0;
            }

            if(isset($request->additional_for_sub) && $request->additional_for_sub !=''){
                if($request->additional_for_sub == 1){
                    $pdf_uploader = 1;
                }elseif($request->additional_for_sub == 2){
                    $gallery = 1;
                }
            }


            if ($request->menu_item_hidden == '') {
                $hidden = 1;
            } else {
                $hidden = 0;
            }

            if (isset($request->config_bg_image) && $request->config_bg_image != '') {
                $config_bg_image = $request->config_bg_image;
            } else {
                $config_bg_image = 0;
            }
            if (isset($request->config_logo) && $request->config_logo != '') {
                $config_logo = $request->config_logo;
            } else {
                $config_logo = 0;
            }

            if (isset($request->config_project) && $request->config_project != '') {
                $config_project = $request->config_project;
            } else {
                $config_project = 0;
            }

            if ($request->file("image")) {

                $image = Input::file('image');
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();
                $newFile = public_path('uploads/page/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->orientate()->save($newFile);
                $newFileThumb = public_path('uploads/page/thumbs/' . $generatedString . "." . $extension);
                $filepath = $generatedString . "." . $extension;
                Image::make($image->getRealPath())->resize(520, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb);

            } else {
                $filepath = $request->old_image;
            }

            if ($request->file("image_bg")) {
                $image = Input::file('image_bg');
                $generatedString = "bg_".time();
                $extension = $request->file("image_bg")->getClientOriginalExtension();
                $newFile = public_path('uploads/page/' . $generatedString . "." . $extension);

                $image_bg = $generatedString . "." . $extension;

                Image::make($image->getRealPath())->orientate()->save($newFile);
            } else {
                $image_bg = $request->old_image_bg;
            }
            if ($request->file("image_logo")) {
                $image = Input::file('image_logo');

                $generatedString = "logo_".time();
                $extension = $request->file("image_logo")->getClientOriginalExtension();

                $newFile = public_path('uploads/page/' . $generatedString . "." . $extension);

                $image_logo = $generatedString . "." . $extension;

                Image::make($image->getRealPath())->orientate()->save($newFile);
            } else {
                $image_logo = $request->old_image_logo;
            }
            $back_color = $request->bg_color;
            $page = Page::find($id);


            $page->slug = $new_slug;
            $page->sub_pages = $sub_page;
            $page->gallery = $gallery;
            $page->pdf_uploder = $pdf_uploader;
            $page->page_with_pdf = $page_with_pdf;
            $page->config_data1 = $config_data1;
            $page->config_data2 = $config_data2;
            $page->config_cat = $config_cat;
            $page->config_tag = $config_tag;
            $page->config_image = $config_image;
            $page->config_bg_image = $config_bg_image;
            $page->config_logo = $config_logo;
            $page->config_bg_image = $config_bg_image;
            $page->config_logo = $config_logo;
            $page->config_project = $config_project;
            $calendar_table_counts = $request->calendar_table_counts;
            $page->hidden = $hidden;
            foreach($request->all() as $key => $val){
                
                if(
                    strpos($key, 'jamer_') === false && 
                    strpos($key, 'shabat_caoch_') === false &&
                    strpos($key, 'shabat_caochcat_') === false &&
                    strpos($key, 'kiraki_caoch_') === false &&
                    strpos($key, 'kiraki_caochcat_') === false &&
                    strpos($key, 'urbat_caoch_') === false && 
                    strpos($key, 'urbat_caochcat_') === false && 
                    strpos($key, 'hing_caoch_') === false && 
                    strpos($key, 'hing_caochcat_') === false && 
                    strpos($key, 'choreq_caoch_') === false && 
                    strpos($key, 'choreq_caochcat_') === false && 
                    strpos($key, 'ereq_caoch_') === false && 
                    strpos($key, 'ereq_caochcat_') === false && 
                    strpos($key, 'erku_caoch_') === false && 
                    strpos($key, 'erku_caochcat_') === false && 
                    strpos($key, 'jamer_') === false && 
                    $key !='calendar_title_en' && $key !='calendar_title_ru' && $key !='calendar_title_am' && $key !='calendar_table_counts' && $key !='shabat_caoch' && $key !='urbat_caoch' && $key !='hing_caoch' && $key !='choreq_caoch' && $key !='ereq_caoch' && $key !='erku_caoch' && $key !='shabat_caochcat' && $key !='urbat_caochcat' && $key !='hing_caochcat' && $key !='choreq_caochcat' && $key !='ereq_caochcat' && $key !='erku_caochcat' && $key !='jamer' && $key !='old_image_logo' && $key !='old_image_bg' && $key !='edit_page' && $key!='_method' && $key!='menu_item_hidden'&& $key!='old_image'){
                    $page->$key = $val;
                }

            }
            if (isset($request->show_in_home) && $request->show_in_home != '') {
                $show_in_home = $request->show_in_home;
            } else {
                $show_in_home = 0;
            }
            $page->show_in_home = $show_in_home;
            $page->image = $filepath;
            $page->image_bg = $image_bg;
            $page->image_logo = $image_logo;
            $page->bg_color = $back_color;
            $page->save();
        }


        DB::table('calendars')->where('parent_id', '=', $id)->delete();
        for ($i=0; $i<$calendar_table_counts;$i++){
            $new_jam_array = array();
            $erku_caochcat_array = array();
            $erku_caoch_array = array();
            $ereq_caochcat_array = array();
            $ereq_caoch_array = array();
            $choreq_caochcat_array = array();
            $choreq_caoch_array = array();
            $hing_caochcat_array = array();
            $hing_caoch_array = array();
            $urbat_caochcat_array = array();
            $urbat_caoch_array = array();
            $shabat_caochcat_array = array();
            $shabat_caoch_array = array();
            $kiraki_caochcat_array = array();
            $kiraki_caoch_array = array();
            if(isset($request->{"jamer_$i"})){
                foreach ($request->{"jamer_$i"} as $key_jam => $jam){
                    if($jam !=''){
                        $new_jam_array[] = $jam;
                        $erku_caochcat_array[] = $request->{"erku_caochcat_$i"}[$key_jam];
                        $erku_caoch_array[] = $request->{"erku_caoch_$i"}[$key_jam];

                        $ereq_caochcat_array[] = $request->{"ereq_caochcat_$i"}[$key_jam];
                        $ereq_caoch_array[] = $request->{"ereq_caoch_$i"}[$key_jam];

                        $choreq_caochcat_array[] = $request->{"choreq_caochcat_$i"}[$key_jam];
                        $choreq_caoch_array[] = $request->{"choreq_caoch_$i"}[$key_jam];

                        $hing_caochcat_array[] = $request->{"hing_caochcat_$i"}[$key_jam];
                        $hing_caoch_array[] = $request->{"hing_caoch_$i"}[$key_jam];

                        $urbat_caochcat_array[] = $request->{"urbat_caochcat_$i"}[$key_jam];
                        $urbat_caoch_array[] = $request->{"urbat_caoch_$i"}[$key_jam];

                        $shabat_caochcat_array[] = $request->{"shabat_caochcat_$i"}[$key_jam];
                        $shabat_caoch_array[] = $request->{"shabat_caoch_$i"}[$key_jam];

                        $kiraki_caochcat_array[] = $request->{"kiraki_caochcat_$i"}[$key_jam];
                        $kiraki_caoch_array[] = $request->{"kiraki_caoch_$i"}[$key_jam];
                    }
                }
                $jamer = json_encode($new_jam_array);
                $erku_caochcat= json_encode($erku_caochcat_array);
                $erku_caoch= json_encode($erku_caoch_array);
                $ereq_caochcat= json_encode($ereq_caochcat_array);
                $ereq_caoch= json_encode($ereq_caoch_array);
                $choreq_caochcat= json_encode($choreq_caochcat_array);
                $choreq_caoch= json_encode($choreq_caoch_array);

                $hing_caochcat= json_encode($hing_caochcat_array);
                $hing_caoch= json_encode($hing_caoch_array);
                $urbat_caochcat= json_encode($urbat_caochcat_array);
                $urbat_caoch= json_encode($urbat_caoch_array);
                $shabat_caochcat= json_encode($shabat_caochcat_array);
                $shabat_caoch= json_encode($shabat_caoch_array);
                $kiraki_caochcat= json_encode($kiraki_caochcat_array);
                $kiraki_caoch= json_encode($kiraki_caoch_array);
                $calendar_title_am = $request->calendar_title_am[$i];
                $calendar_title_ru = $request->calendar_title_ru[$i];
                $calendar_title_en = $request->calendar_title_en[$i];
                DB::table('calendars')->insert(
                    [
                        'parent_id' => $id,
                        'calendar_title_am' => $calendar_title_am,
                        'calendar_title_ru' => $calendar_title_ru,
                        'calendar_title_en' => $calendar_title_en,
                        'calendar_time' => $jamer,
                        'erku_coach_cat' => $erku_caochcat,
                        'ereq_coach_cat' => $ereq_caochcat,
                        'choreq_coach_cat' => $choreq_caochcat,
                        'hing_coach_cat' => $hing_caochcat,
                        'urbat_coach_cat' => $urbat_caochcat,
                        'shabat_coach_cat' => $shabat_caochcat,
                        'kiraki_coach_cat' => $kiraki_caochcat,
                        'erku_coachs' => $erku_caoch,
                        'ereq_coachs' => $ereq_caoch,
                        'choreq_coachs' => $choreq_caoch,
                        'hing_coachs' => $hing_caoch,
                        'urbat_coachs' => $urbat_caoch,
                        'shabat_coachs' => $shabat_caoch,
                        'kiraki_coachs' => $kiraki_caoch
                    ]
                );
            }

        }








        $page = DB::table('pages')->select("id", "parent")->where('id', $id)->first();
        if ($page->parent != 0) {
            return redirect('/admin/pages/' . $page->parent);
        } else {
            return redirect('/admin/pages/' . $id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        DB::table('menu_items')->where('resourse_id', '=', $id)->delete();
        DB::table('pages')->where('id', '=', $id)->delete();
    }

    public function addCalendarRow(){
        $rowcount = $_POST["rowcount"];
        $table = $_POST["table"];
        $responce = '<tr>
                                                    <td class="jamer_td"><input type="text" name="jamer_'.$table.'[]" class="form-control"></td>
                                                    <td  data-day="erku" data-index="'.$rowcount.'" data-table="'.$table.'" class="add_column_info add_column_info_erku_'.$rowcount.'_'.$table.'">
                                                        <small class="calendar_small erku_caochcat_info_'.$rowcount.'_'.$table.'"></small>
                                                        <small class="calendar_small erku_caoch_info_'.$rowcount.'_'.$table.'"></small>
                                                        <input type="hidden" name="erku_caochcat_'.$table.'[]" class="erku_caochcat_'.$rowcount.'_'.$table.'">
                                                        <input type="hidden" name="erku_caoch_'.$table.'[]" class="erku_caoch_'.$rowcount.'_'.$table.'">
                                                       
                                                    </td>
                                                    <td data-day="ereq" data-index="'.$rowcount.'" data-table="'.$table.'" class="add_column_info add_column_info_ereq_'.$rowcount.'_'.$table.'">

                                                        <small class="calendar_small ereq_caochcat_info_'.$rowcount.'_'.$table.'"></small>
                                                        <small class="calendar_small ereq_caoch_info_'.$rowcount.'_'.$table.'"></small>
                                                        <input type="hidden" name="ereq_caochcat_'.$table.'[]" class="ereq_caochcat_'.$rowcount.'_'.$table.'">
                                                        <input type="hidden" name="ereq_caoch_'.$table.'[]" class="ereq_caoch_'.$rowcount.'_'.$table.'">
                                                        

                                                    </td>
                                                    <td  data-day="choreq" data-index="'.$rowcount.'" data-table="'.$table.'" class="add_column_info add_column_info_choreq_'.$rowcount.'_'.$table.'">
                                                        <small class="calendar_small choreq_caochcat_info_'.$rowcount.'_'.$table.'"></small>
                                                        <small class="calendar_small choreq_caoch_info_'.$rowcount.'_'.$table.'"></small>
                                                        <input type="hidden" name="choreq_caochcat_'.$table.'[]" class="choreq_caochcat_'.$rowcount.'_'.$table.'">
                                                        <input type="hidden" name="choreq_caoch_'.$table.'[]" class="choreq_caoch_'.$rowcount.'_'.$table.'">
                                                        
                                                    </td>
                                                    <td  data-day="hing" data-index="'.$rowcount.'" data-table="'.$table.'" class="add_column_info add_column_info_hing_'.$rowcount.'_'.$table.'">
                                                        <small class="calendar_small hing_caochcat_info_'.$rowcount.'_'.$table.'"></small>
                                                        <small class="calendar_small hing_caoch_info_'.$rowcount.'_'.$table.'"></small>
                                                        <input type="hidden" name="hing_caochcat_'.$table.'[]" class="hing_caochcat_'.$rowcount.'_'.$table.'">
                                                        <input type="hidden" name="hing_caoch_'.$table.'[]" class="hing_caoch_'.$rowcount.'_'.$table.'">
                                                        
                                                    </td>
                                                    <td  data-day="urbat" data-index="'.$rowcount.'" data-table="'.$table.'" class="add_column_info add_column_info_urbat_'.$rowcount.'_'.$table.'">
                                                        <small class="calendar_small urbat_caochcat_info_'.$rowcount.'_'.$table.'"></small>
                                                        <small class="calendar_small urbat_caoch_info_'.$rowcount.'_'.$table.'"></small>
                                                        <input type="hidden" name="urbat_caochcat_'.$table.'[]" class="urbat_caochcat_'.$rowcount.'_'.$table.'">
                                                        <input type="hidden" name="urbat_caoch_'.$table.'[]" class="urbat_caoch_'.$rowcount.'_'.$table.'">
                                                       
                                                    </td>
                                                    <td  data-day="shabat" data-index="'.$rowcount.'" data-table="'.$table.'" class="add_column_info add_column_info_shabat_'.$rowcount.'_'.$table.'">
                                                        <small class="calendar_small shabat_caochcat_info_'.$rowcount.'_'.$table.'"></small>
                                                        <small class="calendar_small shabat_caoch_info_'.$rowcount.'_'.$table.'"></small>
                                                        <input type="hidden" name="shabat_caochcat_'.$table.'[]" class="shabat_caochcat_'.$rowcount.'_'.$table.'">
                                                        <input type="hidden" name="shabat_caoch_'.$table.'[]" class="shabat_caoch_'.$rowcount.'_'.$table.'">
                                                    </td>
                                                    <td  data-day="kiraki" data-index="'.$rowcount.'" data-table="'.$table.'" class="add_column_info add_column_info_kiraki_'.$rowcount.'_'.$table.'">
                                                        <small class="calendar_small kiraki_caochcat_info_'.$rowcount.'_'.$table.'"></small>
                                                        <small class="calendar_small kiraki_caoch_info_'.$rowcount.'_'.$table.'"></small>
                                                        <input type="hidden" name="kiraki_caochcat_'.$table.'[]" class="kiraki_caochcat_'.$rowcount.'_'.$table.'">
                                                        <input type="hidden" name="kiraki_caoch_'.$table.'[]" class="kiraki_caoch_'.$rowcount.'_'.$table.'">
                                                    </td>
                                                    <td class="delete_calendar_tr_td"><a class="delete_calendar_tr"><i class="fa fa-times" aria-hidden="true"></i></a></td>
                                                </tr>';

        return $responce;

    }


    public function getCoachCat($cat, $lang){

        if($cat !=0){
            $cat_row = CoachCategory::find($cat);
            if($cat_row){
                return $cat_row->{"title_$lang"};
            }else{
                return "";
            }

        }


    }
    public function getCoach($id, $lang){
        if($id !=0){
            $cat_row = Coach::find($id);
            if($cat_row){
                return $cat_row->{"title_$lang"};
            }else{
                return "";
            }
        }


    }

}
