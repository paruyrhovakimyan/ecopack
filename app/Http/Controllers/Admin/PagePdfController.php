<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\UrlGenerator;
use App\PagePdf as PagePdf;

class PagePdfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($parent_id)
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $pages = DB::table('page_pdfs')->select("pdf_name_$default_lang as title", "id")->where('page_id', $parent_id)->get();
        return view("admin.page_pdf.all", compact('pages', 'default_lang', 'parent_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($parent_id)
    {


        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();

        $page_child = DB::table('pages')->select("title_$default_lang as title", "parent")->where('id', $parent_id)->first();
        $page_bredcrump = "<h4>";
        if ($page_child->parent != 0) {
            $page_parent = DB::table('pages')->select("title_$default_lang as title")->where('id', $page_child->parent)->first();
            $page_bredcrump .= '<a href="' . url('/') . '/admin/pages/' . $page_child->parent . '">' . $page_parent->title . '</a> | ';
        }
        $page_bredcrump .= $page_child->title . "</h4>";
        $settings = DB::table('settings')->where('id', 1)->first();

        return view('admin.page_pdf.create', compact('languages', 'settings', 'parent_id', 'page_bredcrump'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($parent_id, Request $request)
    {
        if (isset($request->add_pdf) && $request->add_pdf == "ok") {

            $page_pdfs = new PagePdf();


            $languages = DB::table('languages')->where('hidden', '=', 0)->get();
            $page_pdfs->page_id = $parent_id;
            foreach ($languages as $language) {
                $field_name = "pdf_file_".$language->short;
                $db_field = "pdf_file_".$language->short;
                if ($request->hasFile($field_name)) {
                    $destinationPath =  base_path() . '/public/uploads/page/pdf/';
                    $file = $request->file($field_name);
                    $file_name = $file->getClientOriginalName();
                    $file->move($destinationPath , $file_name);
                    $page_pdfs->$db_field= $file_name;
                }
            }

            foreach ($request->all() as $key => $val) {
                if ($key != '_token' && $key != 'add_pdf' && $key != 'old_pdf_title') {
                    $page_pdfs->$key = $val;
                }
            }
            $page_pdfs->save();
            $parent_page = DB::table('pages')
                ->select(
                    "parent"
                )->where('id', $parent_id)->first();
            if ($parent_page->parent != 0) {
                $super_parent = $parent_page->parent;
            } else {
                $super_parent = $parent_id;
            }

            return redirect('admin/pages/' . $super_parent);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($parent_id, $id)
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();

        $page_child = DB::table('pages')->select("title_$default_lang as title", "parent")->where('id', $parent_id)->first();
        $page_bredcrump = "<h4>";
        if ($page_child->parent != 0) {
            $page_parent = DB::table('pages')->select("title_$default_lang as title")->where('id', $page_child->parent)->first();
            $page_bredcrump .= '<a href="' . url('/') . '/admin/pages/' . $page_child->parent . '">' . $page_parent->title . '</a> | ';
        }
        $page_bredcrump .= $page_child->title . "</h4>";
        $settings = DB::table('settings')->where('id', 1)->first();

        $page_pdf = DB::table('page_pdfs')->select("pdf_file_$default_lang as pdf_file", "pdf_name_$default_lang as title", "pdf_text_$default_lang as pdf_text", "id")->where("id", $id)->where('page_id', $parent_id)->first();


        return view('admin.page_pdf.edit', compact('default_lang','languages','settings', 'parent_id', 'page_bredcrump', 'page_pdf'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $parent_id, $id)
    {
        if (isset($request->edit_pdf) && $request->edit_pdf == "ok") {



            $page_pdfs = PagePdf::find($id);


            $languages = DB::table('languages')->where('hidden', '=', 0)->get();
            $page_pdfs->page_id = $parent_id;


            foreach ($request->all() as $key => $val) {
                if ($key != '_token' && $key != 'edit_pdf' && $key != 'old_pdf_title') {
                    $page_pdfs->$key = $val;
                }
            }
            foreach ($languages as $language) {
                $field_name = "pdf_file_".$language->short;
                $db_field = "pdf_file_".$language->short;
                if ($request->hasFile($field_name)) {
                    $destinationPath =  base_path() . '/public/uploads/page/pdf/';
                    $file = $request->file($field_name);
                    $file_name = $file->getClientOriginalName();
                    $file->move($destinationPath , $file_name);
                    $page_pdfs->$db_field= $file_name;
                }
            }
            $page_pdfs->save();
        }
        $page_parent = DB::table('pages')->select("parent")->where('id', $parent_id)->first();
        if (isset($page_parent->parent) && $page_parent->parent != 0) {
            return redirect('/admin/pages/' . $page_parent->parent);
        } else {
            return redirect('/admin/pages/' . $parent_id);
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete_pdf(Request $request)
    {
        $back_url = app('Illuminate\Routing\UrlGenerator')->previous();
        $id = $request->id;
        DB::table('page_pdf')->where('id', '=', $id)->delete();
        return redirect($back_url);
    }
}
