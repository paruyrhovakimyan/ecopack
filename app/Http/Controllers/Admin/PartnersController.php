<?php

namespace App\Http\Controllers\Admin;

use App\Partner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class PartnersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $partners = DB::table('partners')->select("id","partner_order","image","created_at","hidden","title_$default_lang as title")->get();
        return view("admin.partners.all", compact('partners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $settings = DB::table('settings')->where('id', 1)->first();
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();

        return view('admin.partners.create',compact('languages','default_lang','settings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(isset($request->add_partner) && $request->add_partner == "ok"){


            $partner = new Partner();


            if ($request->file("image")) {

                $image = Input::file('image');
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();

                $newFile = public_path('uploads/partners/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(null, 546, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFile);


                $newFileThumb = public_path('uploads/partners/thumbs/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(180, 94)->orientate()->save($newFileThumb);

                $newFileThumb_150 = public_path('uploads/partners/150/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(null, 150, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb_150);
                $filepath = $generatedString . "." . $extension;
            } else {
                $filepath = "";
            }
            foreach ($request->all() as $key => $val) {
                if ($key != '_token' && $key != 'add_partner') {
                    $partner->$key = $val;
                }
            }
            $partner->image = $filepath;
            $partner->save();
        }
        return redirect('admin/partners');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dropzoneDefaultHidden = '';
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();


        $partner = DB::table('partners')->where('id', $id)->first();
        $settings = DB::table('settings')->where('id', 1)->first();
        return view('admin.partners.edit', compact('default_lang','languages','partner', 'dropzoneDefaultHidden','settings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(isset($request->edit_partner) && $request->edit_partner =="ok"){
            $partner = Partner::find($id);


            if ($request->file("image")) {

                $image = Input::file('image');
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();

                $newFile = public_path('uploads/partners/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(null, 546, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFile);


                $newFileThumb = public_path('uploads/partners/thumbs/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(180, 94)->orientate()->save($newFileThumb);

                $newFileThumb_150 = public_path('uploads/partners/150/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(null, 150, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb_150);
                $filepath = $generatedString . "." . $extension;
            } else {
                $filepath = $request->old_image;
            }
            foreach ($request->all() as $key => $val) {
                if ($key != 'old_image' &&  $key != '_token'  &&  $key!='_method' && $key != 'edit_partner') {
                    $partner->$key = $val;
                }
            }
            $partner->image = $filepath;
            $partner->save();

        }
        return redirect('admin/partners');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('partners')->where('id', '=', $id)->delete();
    }

    public function upload(Request $request)
    {
        $imageName = substr(md5(microtime()),rand(0,26),10);
        $image = $request->file('file');
        $generatedString = time();
        $extension = $request->file("file")->getClientOriginalExtension();
        $newFile = "uploads/partners/" .$imageName.'_'. $generatedString . "." . $extension;
        $newFileThumb = "uploads/partners/thumbs/" .$imageName.'_'. $generatedString . "." . $extension;
        $newFileThumb_150 = "uploads/partners/150/" .$imageName.'_'. $generatedString . "." . $extension;
        $filepath = $imageName.'_'.$generatedString . "." . $extension;

        Image::make($image->getRealPath())->resize(null, 546, function ($constraint) {
            $constraint->aspectRatio();
        })->orientate()->save($newFile);
        Image::make($image->getRealPath())->resize(180, 94)->orientate()->save($newFileThumb);
        Image::make($image->getRealPath())->resize(null, 150, function ($constraint) {
            $constraint->aspectRatio();
        })->orientate()->save($newFileThumb_150);
        echo $filepath;
    }

    public function changeHidden( Request $request){
        if(isset($_REQUEST)){
            $id = $request->id;
            $hidden = $request->update_hidden;
            DB::table('partners')
                ->where('id', $id)
                ->update(['hidden' => $hidden]);
            if($hidden == 1){
                echo "0";
            }else{
                echo "1";
            }
        }


    }
}
