<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Category as Category;

class CategoriesController extends Controller
{


    private function mb_str_split( $string ) {
        # Split at all position not after the start: ^
        # and not before the end: $
        return preg_split('/(?<!^)(?!$)/u', $string );
    }
    private function create_english($title){
        $charlist = $this->mb_str_split( $title );
        $array_hayeren = array('.','ա','բ','գ','դ','ե','զ','է','ը','թ','ժ','ի','լ','խ','ծ','կ','հ','ձ','ղ','ճ','մ','յ','ն','շ','ո','չ','պ','ջ','ռ','ս','վ','տ','ր','ց','ւ','փ','ք','և','օ','ֆ',' ','  ','/','\\','&','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','1','2','3','4','5','6','7','8','9','0');
        $array_english = array('-','a','b','g','d','e','z','e','y','t','zh','i','l','kh','ts','k','h','dz','gh','ch','m','y','n','sh','o','ch','p','j','r','s','v','t','r','c','u','p','q','ev','o','f','-','-','-','-','-','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','1','2','3','4','5','6','7','8','9','0');
        $new ='';
        if(count($charlist)<41){
            $counts = count($charlist);
        }
        else{
            $counts = 40;
        }
        for($i=0;$i<$counts;$i++){
            $key = array_search(mb_strtolower($charlist[$i],'UTF-8'),$array_hayeren);
            $new .= $array_english[$key];

        }
        return $new;
    }
    private function create_slug($title,$id_obj){

        $slug = $this->create_english($title);
        $article_isset_slug = DB::table('categories')->where('slug', '=', $slug)->orderBy('id', 'desc')->count();
        if($article_isset_slug > 0){
            $new_slug = $slug."-".$article_isset_slug;
        }else{
            $new_slug = $slug;
        }

        return $new_slug;
    }
    private function create_slug_id($title, $first_code, $id)
    {

        $slug = $this->create_english($title);
        $article_isset_slug = DB::table("categories")->where('id', '!=', $id)->where('slug', '=', $slug)->orderBy('id', 'desc')->count();
        if ($article_isset_slug > 0) {
            $new_slug = $slug . "-" . $article_isset_slug;
        } else {
            $new_slug = $slug;
        }

        return $new_slug;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;

        $parent_page = $_REQUEST['parent'];

        $refferer_url = app('Illuminate\Routing\UrlGenerator')->previous();

        if($parent_page == '' || $parent_page == 0){
            return redirect($refferer_url);
        }

        $categories = DB::table('categories')->select("id","hidden","title_$default_lang as title")->where( 'page_parent', $parent_page)->get();
        return view("admin.categories.all", compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();
        $settings = DB::table('settings')->where('id', 1)->first();

        return view('admin.categories.create', compact('default_lang','languages','settings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($request->add_category) && $request->add_category == "ok"){

            $category = new Category();
            $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
            $default_lang = $first_lang->short;
            $first_title = $request->input("title_".$default_lang);
            $new_slug = $this->create_slug($first_title, $default_lang);
            $category->slug = $new_slug;
            foreach ($request->all() as $key => $val) {
                if ($key != '_token' && $key != 'add_category') {
                    $category->$key = $val;
                }
            }
            $category->save();
            $parent_page = $request->input("parent_page");

            return redirect('admin/categories?parent='.$parent_page);


        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $settings = DB::table('settings')->where('id', 1)->first();
        $category = Category::find($id);
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();
        return view('admin.categories.edit', compact('default_lang','languages','category', 'settings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (isset($request->edit_category) && $request->edit_category == "ok"){
            $category = Category::find($id);

            $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
            $default_lang = $first_lang->short;
            $first_title = $request->input("title_".$default_lang);
            $new_slug = $this->create_slug_id($first_title, $default_lang, $id);

            $category->slug = $new_slug;
            foreach ($request->all() as $key => $val) {
                if ($key != '_method' && $key != '_token' && $key != 'edit_category') {
                    $category->$key = $val;
                }
            }
            $category->save();
            $category_parent = DB::table('categories')->select('page_parent')->where('id', $id)->first();

            return redirect('admin/categories?parent='.$category_parent->page_parent);


        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('categories')->where('id', '=', $id)->delete();
    }

    public function changeHidden( Request $request){
        if(isset($_REQUEST)){
            $id = $request->id;
            $hidden = $request->update_hidden;
            DB::table('categories')
                ->where('id', $id)
                ->update(['hidden' => $hidden]);
            if($hidden == 1){
                echo "0";
            }else{
                echo "1";
            }
        }


    }
}
