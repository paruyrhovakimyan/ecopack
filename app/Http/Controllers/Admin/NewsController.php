<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use App\News as News;

class NewsController extends Controller
{

    private function mb_str_split( $string ) {
        # Split at all position not after the start: ^
        # and not before the end: $
        return preg_split('/(?<!^)(?!$)/u', $string );
    }
    private function create_english($title){
        $charlist = $this->mb_str_split( $title );
        $array_hayeren = array('.','ա','բ','գ','դ','ե','զ','է','ը','թ','ժ','ի','լ','խ','ծ','կ','հ','ձ','ղ','ճ','մ','յ','ն','շ','ո','չ','պ','ջ','ռ','ս','վ','տ','ր','ց','ւ','փ','ք','և','օ','ֆ',' ','  ','/','\\','&','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','1','2','3','4','5','6','7','8','9','0');
        $array_english = array('-','a','b','g','d','e','z','e','y','t','zh','i','l','kh','ts','k','h','dz','gh','ch','m','y','n','sh','o','ch','p','j','r','s','v','t','r','c','u','p','q','ev','o','f','-','-','-','-','-','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','1','2','3','4','5','6','7','8','9','0');
        $new ='';
        if(count($charlist)<41){
            $counts = count($charlist);
        }
        else{
            $counts = 40;
        }
        for($i=0;$i<$counts;$i++){
            $key = array_search(mb_strtolower($charlist[$i],'UTF-8'),$array_hayeren);
            $new .= $array_english[$key];

        }
        return $new;
    }
    private function create_slug($title,$id_obj){

        $slug = $this->create_english($title);
        $article_isset_slug = DB::table('news')->where('slug', '=', $slug)->orderBy('id', 'desc')->count();
        if($article_isset_slug > 0){
            $new_slug = $slug."-".$id_obj;
        }else{
            $new_slug = $slug;
        }

        return $new_slug;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $default_lang = config('app.locale');
        $newses = DB::table('news')->select("title_$default_lang as title","id","created_at","image")->get();
        return view("admin.news.all", compact('newses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();
        $parent_id = $_REQUEST['parent'];
        $page_settings = DB::table('pages')->select("config_data1","config_data2","config_cat","config_tag","config_image")->where('id',$parent_id)->first();
        $categories = DB::table('categories')->select("id","hidden","title_$default_lang as title")->where( 'page_parent', $parent_id)->get();
        $settings = DB::table('settings')->where('id', 1)->first();
        return view('admin.news.create', compact('languages','settings',"page_settings",'categories','parent_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($request->add_news) && $request->add_news == "ok"){

            if ($request->file("image")) {

                $image = Input::file('image');
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();

                $newFile = public_path('uploads/news/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->orientate()->save($newFile);


                $newFileThumb = public_path('uploads/news/thumbs/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(360, 250)->save($newFileThumb);

                $newFileThumb_150 = public_path('uploads/news/150/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(null, 150, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb_150);
                $filepath = $generatedString . "." . $extension;
            } else {
                $filepath = "";
            }


            if($request->date1 !=''){
                $data1 = date('Y-m-d H:i:s', strtotime($request->date1));
            }else{
                $data1 = date('Y-m-d H:i:s');
            }
            if($request->date2 !=''){
                $data2 = date('Y-m-d H:i:s', strtotime($request->date2));
            }else{
                $data2 = NULL;
            }
            $cat_id = $request->cat_id;
            $parent_id = $request->parent_id;


            if (isset($request->pdf_config) && $request->pdf_config != '') {
                $pdf_config = $request->pdf_config;
            } else {
                $pdf_config = 0;
            }

            if (isset($request->gallery_config) && $request->gallery_config != '') {
                $gallery_config = $request->gallery_config;
            } else {
                $gallery_config = 0;
            }
            if (isset($request->page_pdf) && $request->page_pdf != '') {
                $page_pdf = $request->page_pdf;
            } else {
                $page_pdf = 0;
            }

            $news = new News();
            $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
            $default_lang = $first_lang->short;
            $first_title = $request->input("title_".$default_lang);
            $new_slug = $this->create_slug($first_title, $default_lang);
            $news->slug = $new_slug;
            $news->pdf_config = $pdf_config;
            $news->gallery_config = $gallery_config;
            $news->page_pdf = $page_pdf;


            foreach ($request->all() as $key => $val) {
                if ($key != '_token' && $key != 'add_news') {
                    if (strpos($key, 'description_') !== false) {
                        $news->$key = str_replace("../../", url('/')."/", $val);
                    }else{
                        $news->$key = $val;
                    }

                }
            }
            if (isset($request->stick_home) && $request->stick_home != '') {
                $stick_home = $request->stick_home;
            } else {
                $stick_home = 0;
            }

            $news->stick_home = $stick_home;
            $news->date1 = $data1;
            $news->date2 = $data2;
            $news->cat_id = $cat_id;
            $news->image = $filepath;
            $news->save();
            $id = $news->id;


            if (isset($request->filename) && $request->filename != '') {
                foreach ($request->filename as $filename) {

                    DB::table('news_gallery')->insert(
                        [
                            'image' => $filename,
                            'parent_id' => $id
                        ]
                    );
                }
            }
            return redirect('admin/pages/' . $parent_id);


        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();



        $settings = DB::table('settings')->where('id', 1)->first();
        $news = DB::table('news')
            ->where('id', $id)
            ->first();

        $parent_id = $news->parent_id;

        $news->date1 = date('d-m-Y', strtotime($news->date1));
        $news->date2 = date('d-m-Y', strtotime($news->date2));

        $page_settings = DB::table('pages')->select("title_$default_lang as title","id","config_data1","config_data2","config_cat","config_tag","config_image")->where('id',$parent_id)->first();

        $page_settings->parent_title = '<a href="'.url("/").'/admin/pages/'.$page_settings->id.'">'.$page_settings->title.'</a>';

        $categories = DB::table('categories')->select("id","hidden","title_$default_lang as title")->where( 'page_parent', $parent_id)->get();

        $projectImages = DB::table('news_gallery')->where('parent_id',$id)->get();
        $dropzoneDefaultHidden = '';
        if (count($projectImages) != 0) {
            $dropzoneDefaultHidden = 'dz-default-hidden';
        }

        return view('admin.news.edit', compact('default_lang','languages','gallery','projectImages','dropzoneDefaultHidden','news','settings','page_settings','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (isset($request->edit_news) && $request->edit_news == "ok"){



            if ($request->file("image")) {

                $image = Input::file('image');
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();

                $newFile = public_path('uploads/news/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->orientate()->save($newFile);


                $newFileThumb = public_path('uploads/news/thumbs/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(360, 250)->save($newFileThumb);

                $newFileThumb_150 = public_path('uploads/news/150/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(null, 150, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb_150);
                $filepath = $generatedString . "." . $extension;
            }else{
                $filepath= $request->old_image;
            }

            $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
            $default_lang = $first_lang->short;
            $first_title = $request->input("title_".$default_lang);
            $new_slug = $request->slug;

            //$new_slug = $this->create_slug($first_title, $default_lang);
            
if($request->date1){
    $data1 = date('Y-m-d H:i:s', strtotime($request->date1));
    //$data1 = Carbon::createFromFormat('Y-m-d H:i:s', $request->date1);
}else{
    $data1 = NULL;
}


            if($request->date2 !=''){
                $data2 = date('Y-m-d H:i:s', strtotime($request->date2));
            }else{
                $data2 = NULL;
            }

            $cat_id = $request->cat_id;



            if (isset($request->pdf_config) && $request->pdf_config != '') {
                $pdf_config = $request->pdf_config;
            } else {
                $pdf_config = 0;
            }

            if (isset($request->gallery_config) && $request->gallery_config != '') {
                $gallery_config = $request->gallery_config;
            } else {
                $gallery_config = 0;
            }
            if (isset($request->page_pdf) && $request->page_pdf != '') {
                $page_pdf = $request->page_pdf;
            } else {
                $page_pdf = 0;
            }
            $news = News::find($id);



            $news->pdf_config = $pdf_config;
            $news->gallery_config = $gallery_config;
            $news->page_pdf = $page_pdf;


            foreach ($request->all() as $key => $val) {
                if ($key != 'old_image' && $key != '_token' && $key!='_method' && $key != 'edit_news') {
                    $news->$key = $val;
                }
            }
            if (isset($request->stick_home) && $request->stick_home != '') {
                $stick_home = $request->stick_home;
            } else {
                $stick_home = 0;
            }
            $news->stick_home = $stick_home;
            $news->slug = $new_slug;
            $news->date1 = $data1;
            $news->date2 = $data2;
            $news->cat_id = $cat_id;
            $news->image = $filepath;
            $news->save();
        }
        $news_parent = DB::table('news')->select("parent_id")->where('id', $id)->first();
        $parent_id = $news_parent->parent_id;
        return redirect('admin/pages/' . $parent_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('news')->where('id', '=', $id)->delete();
    }
}
