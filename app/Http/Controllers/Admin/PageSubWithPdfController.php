<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\UrlGenerator;
use App\SubPagesWithPdf as SubPagesWithPdf;
use App\SubPageWithPdfFiles as SubPageWithPdfFiles;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Carbon\Carbon;


class PageSubWithPdfController extends Controller
{

    private function mb_str_split($string)
    {
        # Split at all position not after the start: ^
        # and not before the end: $
        return preg_split('/(?<!^)(?!$)/u', $string);
    }

    private function create_english($title)
    {
        $charlist = $this->mb_str_split($title);
        $array_hayeren = array('.', 'ա', 'բ', 'գ', 'դ', 'ե', 'զ', 'է', 'ը', 'թ', 'ժ', 'ի', 'լ', 'խ', 'ծ', 'կ', 'հ', 'ձ', 'ղ', 'ճ', 'մ', 'յ', 'ն', 'շ', 'ո', 'չ', 'պ', 'ջ', 'ռ', 'ս', 'վ', 'տ', 'ր', 'ց', 'ւ', 'փ', 'ք', 'և', 'օ', 'ֆ', ' ', '  ', '/', '\\', '&', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0');
        $array_english = array('-', 'a', 'b', 'g', 'd', 'e', 'z', 'e', 'y', 't', 'zh', 'i', 'l', 'kh', 'ts', 'k', 'h', 'dz', 'gh', 'ch', 'm', 'y', 'n', 'sh', 'o', 'ch', 'p', 'j', 'r', 's', 'v', 't', 'r', 'c', 'u', 'p', 'q', 'ev', 'o', 'f', '-', '-', '-', '-', '-', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0');
        $new = '';
        if (count($charlist) < 41) {
            $counts = count($charlist);
        } else {
            $counts = 40;
        }
        for ($i = 0; $i < $counts; $i++) {
            $key = array_search(mb_strtolower($charlist[$i], 'UTF-8'), $array_hayeren);
            $new .= $array_english[$key];

        }
        return $new;
    }

    private function create_slug($title, $id_obj)
    {

        $slug = $this->create_english($title);
        $article_isset_slug = DB::table('sub_pages_with_pdfs')->where('slug', '=', $slug)->orderBy('id', 'desc')->count();
        if ($article_isset_slug > 0) {
            $new_slug = $slug . "-" . $id_obj;
        } else {
            $new_slug = $slug;
        }

        return $new_slug;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($parent_id)
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $pages = DB::table('news_pdf')->select("pdf_name_$default_lang as title", "id")->where('news_id',$parent_id)->get();
        return view("admin.page_pdf.all", compact('pages','default_lang','parent_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($parent_id)
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $page_child = DB::table('pages')->select("title_$default_lang as title","parent","config_project")->where('id', $parent_id)->first();
        $config_project = $page_child->config_project;
        $page_bredcrump = "<h4>";
        if($page_child->parent != 0){
            $page_parent = DB::table('pages')->select("title_$default_lang as title","parent")->where('id', $page_child->parent)->first();
            $page_bredcrump .= '<a href="'.url('/').'/admin/pages/'.$page_child->parent.'">'.$page_parent->title.'</a> | ';
        }
        if(isset($_GET['sub_id']) && is_numeric($_GET['sub_id'])){
            $sub_name = $news_sub_page = DB::table('sub_pages_with_pdfs')->select("title_$default_lang as title_sub")->where("id", $_GET['sub_id'])->first();
            $sub_name_bredcrump = " | ".$sub_name->title_sub;
        }else{
            $sub_name_bredcrump = "";
        }
        $page_bredcrump .= $page_child->title.$sub_name_bredcrump."</h4>";



        $settings = DB::table('settings')->where('id', 1)->first();
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();

        $project_categories = DB::table('project_categories')->select("title_$default_lang as title","id")->where('hidden', '=', 0)->where('page_parent', '=', $parent_id)->get();
        return view('admin.sub_pages_with_pdf.create_sub',compact('config_project','project_categories','languages','settings','parent_id','page_bredcrump'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($parent_id,Request $request)
    {
        if(isset($request->add_news_sub_page) && $request->add_news_sub_page == "ok"){


            $sub_pages_with_pdf = new SubPagesWithPdf();

            if ($request->file("image_logo")) {
                $image = Input::file('image_logo');
                $generatedString = "logo_".time();
                $extension = $request->file("image_logo")->getClientOriginalExtension();
                $newFile =  public_path('uploads/page/thumbs/' . $generatedString . "." . $extension);

                $image_logo = $generatedString . "." . $extension;

                Image::make($image->getRealPath())->orientate()->save($newFile);
            } else {
                $image_logo = "";
            }


            if ($request->file("image")) {

                $image = Input::file('image');
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();
                $newFile = public_path('uploads/page/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->orientate()->save($newFile);
                $newFileThumb = public_path('uploads/page/thumbs/' . $generatedString . "." . $extension);
                $filepath = $generatedString . "." . $extension;
                Image::make($image->getRealPath())->resize(520, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb);

            } else {
                $filepath = "";
            }


            $sub_pages_with_pdf->parent_id = $parent_id;
            foreach ($request->all() as $key => $val) {
                if ($key != 'image' && $key != 'filename' && $key != 'image_logo' && $key != 'sub_pdfs_ids' && $key != '_token' && $key != 'add_news_sub_page' && $key != 'old_pdf_title') {
                    if (strpos($key, 'description_') !== false) {
                        $sub_pages_with_pdf->$key = str_replace("../../../", url('/')."/", $val);
                    }else{
                        $sub_pages_with_pdf->$key = $val;
                    }


                }
            }

            if ($request->file("image_logo2")) {
                $image2 = Input::file('image_logo2');
                $generatedString2 = "logo2_".time();
                $extension2 = $request->file("image_logo2")->getClientOriginalExtension();
                $newFile2 =  public_path('uploads/page/thumbs/' . $generatedString2 . "." . $extension2);

                $image_logo2 = $generatedString2 . "." . $extension2;

                Image::make($image2->getRealPath())->orientate()->save($newFile2);

            }else{
                $image_logo2 = "";
            }
            $sub_pages_with_pdf->logo_hover = $image_logo2;
            $sub_pages_with_pdf->image = $image_logo;
            $sub_pages_with_pdf->image_main = $filepath;

            if($request->date1 !=''){
                $data1 = Carbon::parse($request->date1)->format('d-m-Y');
            }else{
                $data1 =  Carbon::now()->format('d-m-Y');
            }
            if($request->date2 !=''){
                $data2 = Carbon::parse($request->date2)->format('d-m-Y');
            }else{
                $data2 =  Carbon::now()->format('d-m-Y');
            }
            $sub_pages_with_pdf->date1 = $data1;
            $sub_pages_with_pdf->date2 = $data2;

            if (isset($request->show_in_home) && $request->show_in_home != '') {
                $show_in_home = $request->show_in_home;
            } else {
                $show_in_home = 0;
            }
            $sub_pages_with_pdf->show_in_home = $show_in_home;




            if (isset($request->need_donate) && $request->need_donate != '') {
                $need_donate = $request->need_donate;
            } else {
                $need_donate = 0;
            }
            $sub_pages_with_pdf->need_donate = $need_donate;


            $sub_pages_with_pdf->save();
            $news_sub_page = $sub_pages_with_pdf->id;

            $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
            $default_lang = $first_lang->short;
            $first_title = $request->input("title_".$default_lang);


            $slug = $this->create_slug($first_title, $news_sub_page);

            DB::table('sub_pages_with_pdfs')
                ->where('id', $news_sub_page)
                ->update(
                    [
                        'slug' => $slug
                    ]
                );

            $sub_pdfs_ids = $request->sub_pdfs_ids;
            if (isset($sub_pdfs_ids) && !empty($sub_pdfs_ids)) {
                foreach ($sub_pdfs_ids as $sub_pdfs_id) {

                    DB::table('sub_page_with_pdf_files')
                        ->where('id', $sub_pdfs_id)
                        ->update(
                            [
                                'sub_page_id' => $news_sub_page,
                            ]
                        );
                }
            }
            if (isset($request->filename) && $request->filename != '') {
                foreach ($request->filename as $filename) {

                    DB::table('sub_pages_with_pdfs_gallery')->insert(
                        [
                            'image' => $filename,
                            'parent_id' => $news_sub_page
                        ]
                    );
                }
            }

            $parent_page = DB::table('pages')
                ->select(
                    "parent"
                )->where('id', $parent_id)->first();
            if($parent_page->parent !=0){
                $super_parent = $parent_page->parent;
            }else{
                $super_parent =$parent_id;
            }

            return redirect('admin/pages/'.$super_parent);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($parent_id, $id)
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $page_child = DB::table('pages')->select("title_$default_lang as title","parent",'config_project')->where('id', $parent_id)->first();
        $config_project = $page_child->config_project;
        $page_bredcrump = "<h4>";
        if($page_child->parent != 0){
            $page_parent = DB::table('pages')->select("title_$default_lang as title")->where('id', $page_child->parent)->first();
            $page_bredcrump .= '<a href="'.url('/').'/admin/pages/'.$page_child->parent.'">'.$page_parent->title.'</a> | ';
        }
        $page_bredcrump .= $page_child->title."</h4>";
        $settings = DB::table('settings')->where('id', 1)->first();

        $news_sub_page = DB::table('sub_pages_with_pdfs')->where("id", $id)->where('parent_id',$parent_id)->first();

        $news_sub_page_pdfs = DB::table('sub_page_with_pdf_files')->where("sub_page_id", $id)->orderBy('id', 'desc')->get();



        $projectImages = DB::table('sub_pages_with_pdfs_gallery')->where('parent_id',$id)->get();
        $dropzoneDefaultHidden = '';
        if (count($projectImages) != 0) {
            $dropzoneDefaultHidden = 'dz-default-hidden';
        }
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();
        $project_categories = DB::table('project_categories')->select("title_$default_lang as title","id")->where('hidden', '=', 0)->where('page_parent', '=', $parent_id)->get();
        return view('admin.sub_pages_with_pdf.edit_sub',compact('config_project','project_categories','languages','projectImages','dropzoneDefaultHidden','default_lang','news_sub_page_pdfs','settings','parent_id','page_bredcrump','news_sub_page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $parent_id, $id)
    {
        if(isset($request->edit_news_sub_page) && $request->edit_news_sub_page == "ok"){

            $sub_pages_with_pdf = SubPagesWithPdf::find($id);
            $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
            $default_lang = $first_lang->short;
            $first_title = $request->input("title_".$default_lang);

            $slug = $request->slug;
            if ($slug == '') {
                $new_slug = $this->create_slug($first_title, $id);
            } else {
                $article_isset_slug = DB::table('sub_pages_with_pdfs')->where('slug', '=', $slug)->where('id', '!=', $id)->orderBy('id', 'desc')->count();
                if ($article_isset_slug > 0) {
                    $new_slug = $slug . "-" . $id;
                } else {
                    $new_slug = $slug;
                }
            }
            foreach ($request->all() as $key => $val) {
                if ($key != 'image_logo2' && $key != 'old_image_logo2' && $key != 'old_image' && $key != 'filename' && $key != 'old_image_logo' && $key != 'image_logo' && $key != 'sub_pdfs_ids' && $key != '_token' && $key != 'edit_news_sub_page' && $key != 'old_pdf_title') {
                    $sub_pages_with_pdf->$key = $val;
                }
            }
            if ($request->file("image_logo")) {
                $image = Input::file('image_logo');
                $generatedString = "logo_".time();
                $extension = $request->file("image_logo")->getClientOriginalExtension();
                $newFile =  public_path('uploads/page/thumbs/' . $generatedString . "." . $extension);

                $image_logo = $generatedString . "." . $extension;

                Image::make($image->getRealPath())->orientate()->save($newFile);

            }else{
                $image_logo = $request->old_image_logo;
            }
            if ($request->file("image_logo2")) {
                $image2 = Input::file('image_logo2');
                $generatedString2 = "logo2_".time();
                $extension2 = $request->file("image_logo2")->getClientOriginalExtension();
                $newFile2 =  public_path('uploads/page/thumbs/' . $generatedString2 . "." . $extension2);

                $image_logo2 = $generatedString2 . "." . $extension2;

                Image::make($image2->getRealPath())->orientate()->save($newFile2);

            }else{

                $image_logo2 = $request->old_image_logo2;
            }
            if ($request->file("image")) {

                $image = Input::file('image');
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();
                $newFile = public_path('uploads/page/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->orientate()->save($newFile);
                $newFileThumb = public_path('uploads/page/thumbs/' . $generatedString . "." . $extension);
                $filepath = $generatedString . "." . $extension;
                Image::make($image->getRealPath())->resize(520, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb);

            }else{
                $filepath = $request->old_image;
            }
            $sub_pages_with_pdf->image = $image_logo;
            $sub_pages_with_pdf->image_main = $filepath;
            $sub_pages_with_pdf->logo_hover = $image_logo2;


            if($request->date1 !=''){
                $data1 = Carbon::parse($request->date1)->format('d-m-Y');
            }else{
                $data1 =  Carbon::now()->format('d-m-Y');
            }
            if($request->date2 !=''){
                $data2 = Carbon::parse($request->date2)->format('d-m-Y');
            }else{
                $data2 =  Carbon::now()->format('d-m-Y');
            }
            $sub_pages_with_pdf->date1 = $data1;
            $sub_pages_with_pdf->date2 = $data2;


            if (isset($request->show_in_home) && $request->show_in_home != '') {
                $show_in_home = $request->show_in_home;
            } else {
                $show_in_home = 0;
            }

            if (isset($request->need_donate) && $request->need_donate != '') {
                $need_donate = $request->need_donate;
            } else {
                $need_donate = 0;
            }
            $sub_pages_with_pdf->need_donate = $need_donate;
            $sub_pages_with_pdf->show_in_home = $show_in_home;
            $sub_pages_with_pdf->save();

            DB::table('sub_pages_with_pdfs_gallery')->where('parent_id', '=', $id)->delete();
            if (isset($request->filename) && $request->filename != '') {
                foreach ($request->filename as $filename) {

                    DB::table('sub_pages_with_pdfs_gallery')->insert(
                        [
                            'image' => $filename,
                            'parent_id' => $id
                        ]
                    );
                }
            }


        }
        $parent_page = DB::table('pages')
            ->select(
                "parent"
            )->where('id', $parent_id)->first();
        if($parent_page->parent !=0){
            $super_parent = $parent_page->parent;
        }else{
            $super_parent =$parent_id;
        }

        return redirect('admin/pages/'.$super_parent);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete_sub_pdf(Request $request){

        $id = $request->id;
        DB::table('sub_page_with_pdf_files')->where('id', $id)->delete();
        return "ok";

    }
    public function news_sub_page_pdf_delete(Request $request){

        $id = $request->id;

        $images_for_delete = DB::table('sub_page_with_pdf_files')->where('sub_page_id', $id)->get();

        DB::table('sub_pages_with_pdfs')->where('id', $id)->delete();
        return "ok";

    }

    public function edit_sub_pdf_block(Request $request){
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();
        $id = $request->id;
       $pdf_result = DB::table('sub_page_with_pdf_files')->where('id', $id)->first();
        $result = '<div class="sub_pdf_append_block show_block">
            <div id="sub_pdf_append_head" class="sub_pdf_append_head">
                <div class="sub_pdf_append_title">Upload pdf</div>
                <div class="sub_pdf_append_dragh"></div>
                <button type="button" class="sub_with_pdf_append_close_edit" aria-hidden="true"><i class="mce-ico mce-i-remove"></i></button>
            </div>';

        if( count($languages) > 1){

            $result .= '<ul class="languages_tabs">';
        foreach($languages as $language){
            if($language->first == 1)
            {
                $active_tab = "active_tab";
            }else{
                $active_tab = '';
            }
            $result .= '<li data-tab="'.$language->short.'"
                        class="lang_tab '.$language->short."_tab".' '.$active_tab.'">
                        <p><span>'.$language->title.'</span></p>
                    </li>';
        }


            $result .= '</ul>';
        }

            foreach ($languages as $language){
                if($language->first == 1){
                    $active_class = "active_field";
                }else {
                    $active_class = "hidden_field" ;
                }

                $result .= '
<div class="form-group lang_field lang_field_'. $language->short .' '.$active_class.'">
                <label for="title">'. $language->title .' Title</label>
                <input id="about_title" type="text" class="append__with_file_title_edit_'.$language->short.' form-control" name="pdf_title_'.$language->short.'" value="'.$pdf_result->{"title_".$language->short}.'"
                       autofocus>
            </div>
             <div class="form-group lang_field lang_field_'.$language->short.' '.$active_class.'">
                <label for="Description">'.$language->title.' Short Description</label>
                <textarea class="form-control append_with_file_desc_edit_'.$language->short.' short_desc" name="pdf_desc_'.$language->short.'">'.$pdf_result->{"short_desc_".$language->short}.'</textarea>
            </div>
            
            <div class="sub_pdf_append_box form-group lang_field lang_field_'.$language->short.' '.$active_class.'">
                <div class="">
                    <label>Upload '.$language->title.' PDF</label>
                </div>
                <div class="pdf_file_title">
                    <input type="text" name="old_pdf_title_'.$language->short.'" readonly="" class="append_pdf_change_edit old_pdf_title old_pdf_title_'.$language->short.'" value="'.$pdf_result->{"file_".$language->short}.'">
                </div>
                <input type="hidden" class="current_with_pdf_file_name_'.$language->short.'" name="current_pdf_file_name" value="'.$pdf_result->{"file_".$language->short}.'" >
                <div class="file-upload btn btn_1 green" style="float:left;">
                    <span>Choose file</span>
                    <input data-lang="'.$language->short.'" type="file" name="pdf_file_'.$language->short.'" id="editpage_with_pdf_'.$language->short.'" class="image upload page_with_pdf_file page_with_pdf_file_'.$language->short.'">
                </div>
            </div>
            
            ';
            }

        $result .= '
            
            <input type="hidden" class="news_with_pdf_parent" name="parent_id" value="">
            <input type="hidden" class="sub_with_pdf_id" name="pdf_id" value="'.$pdf_result->id.'">
            <div class="sub_pdf_append_footer">
                <button type="button" class="sub_with_pdf_append_update">Save</button>
            </div>
        </div>';
        return $result;
    }

    public function delete_pdf(Request $request)
    {
        $back_url = app('Illuminate\Routing\UrlGenerator')->previous();
        $id = $request->id;
        DB::table('news_pdf')->where('id', '=', $id)->delete();
        return redirect($back_url);
    }

    public function plus_pdf(Request $request){
        $pdf_block = '<div class="sub_pdf_append_block show_block"><div class="form-group ">
                                    <label for="title">Title</label>
                                    <input id="about_title" type="text" class="form-control" name="pdf_title[]" value=""
                                           autofocus>
                                </div>
                                <div class="form-group">
                                    <label for="Description">Short Description</label>
                                    <textarea class="form-control short_desc" name="pdf_desc[]"></textarea>
                                </div>
                                <div class="not_media_box form-group">
                                    <div class="">
                                        <label>Upload PDF</label>
                                    </div>
                                   
                                    <div class="file-upload btn btn_1 green" style="float:left;">
                                        <span>Choose file</span>
                                        <input type="file" name="pdf_file[]" class="image upload  news_sub_pdf_file" >
                                    </div>
<div class="pdf_append_block_delete">
                                                    <i class="icon-trash"></i>
                                                </div>
                                </div></div>';
        return $pdf_block;
    }

    public function upload_pdf(Request $request){


        $languages = DB::table('languages')->where('hidden','=', 0)->get();
        $sub_page_with_pdf_files = new SubPageWithPdfFiles();
        if(isset($request->parent_id) && $request->parent_id !='undefined'){
            $parent_id =  $request->parent_id;
        }else{
            $parent_id =  0;
        }
        $sub_page_with_pdf_files->sub_page_id = $parent_id;

        $return = '<div class="open_sub_pages_with_block">';
        $file_count = 0;
        foreach($languages as $language){
            $db_title_field = "title_".$language->short;
            $db_desc_field = "short_desc_".$language->short;
            $db_file_field = "file_".$language->short;
            $sub_page_with_pdf_files->$db_title_field= $request->$db_title_field;
            $sub_page_with_pdf_files->$db_desc_field= $request->$db_desc_field;


            if(request()->file($db_file_field)){
                $filename = request()->file($db_file_field);
                $pdf_name = $filename->getClientOriginalName();
                $filename->move(
                    base_path() . '/public/uploads/page/pdfs', $pdf_name
                );
                $sub_page_with_pdf_files->$db_file_field= $pdf_name;
                $file_count++;
            }
            if($file_count == 0){
                return 0;
            }
            if($language->first == 1){
                $active_class = "active_field";
            }else {
                $active_class = "hidden_field" ;
            }
            $return .= ' <p class="lang_field '. $active_class .' lang_field_'.$language->short.' open_sub_pdf_block_title" >'.$request->$db_title_field.'</p>';
        }
        $sub_page_with_pdf_files->save();
        $insertId = $sub_page_with_pdf_files->id;


        $return .= '
        <input type="hidden" name="sub_pdfs_ids[]" value="'.$insertId.'">
        <div data-id="'.$insertId.'" class="pdf_append_block_delete">
                    <i class="icon-trash"></i>
                </div></div>';

        return $return;

    }

    public function update_sub_pdf(Request $request){


        $news_sub_pdf_id =  $request->news_sub_pdf_id;

        $languages = DB::table('languages')->where('hidden','=', 0)->get();
        $sub_page_with_pdf_files = SubPageWithPdfFiles::find($news_sub_pdf_id);

        $result_titles = array();
        foreach($languages as $language){
            $db_title_field = "title_".$language->short;
            $db_desc_field = "short_desc_".$language->short;
            $db_file_field = "file_".$language->short;
            $db_old_file_field = "old_file_".$language->short;
            $sub_page_with_pdf_files->$db_title_field= $request->$db_title_field;
            $sub_page_with_pdf_files->$db_desc_field= $request->$db_desc_field;

            if(request()->file($db_file_field)){
                $filename = request()->file($db_file_field);
                $pdf_name = $filename->getClientOriginalName();
                $filename->move(
                    base_path() . '/public/uploads/page/pdfs', $pdf_name
                );
                $sub_page_with_pdf_files->$db_file_field= $pdf_name;
            }else{
                $sub_page_with_pdf_files->$db_file_field= $request->$db_old_file_field;
            }
            $result_titles[$language->short] = $request->$db_title_field;

        }
        $sub_page_with_pdf_files->save();
        return $result_titles;
    }
}
