<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use App\HomePage as HomePage;

class HomePageController extends Controller
{
    public function index(){
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;

        $home_settings = DB::table('home_pages')
            ->where('id', 1)->first();
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();
        return view('admin.home_page', compact('languages','home_settings'));
    }
    public function save(Request $request){

        $home_page = HomePage::find(1);

        if ($request->file("image")) {

            $image = Input::file('image');
            $generatedString = time();
            $extension = $request->file("image")->getClientOriginalExtension();

            $newFile = public_path('uploads/home_page/' . $generatedString . "." . $extension);
            Image::make($image->getRealPath())->orientate()->save($newFile);

            $filepath = $generatedString . "." . $extension;
        } else {
            $filepath = $request->old_image;
        }

        if ($request->file("block_2_img")) {

            $image2 = Input::file('block_2_img');
            $generatedString2 = time();
            $extension2 = $request->file("block_2_img")->getClientOriginalExtension();

            $newFile2 = public_path('uploads/home_page/' . $generatedString2 . "." . $extension2);
            Image::make($image2->getRealPath())->orientate()->save($newFile2);

            $filepath2 = $generatedString2 . "." . $extension2;
        } else {
            $filepath2 = $request->old_image2;
        }

        if ($request->file("block_3_img")) {

            $image3 = Input::file('block_3_img');
            $generatedString3 = time();
            $extension3 = $request->file("block_3_img")->getClientOriginalExtension();

            $newFile3 = public_path('uploads/home_page/' . $generatedString3 . "." . $extension3);
            Image::make($image3->getRealPath())->orientate()->save($newFile3);

            $filepath3 = $generatedString3 . "." . $extension3;
        } else {
            $filepath3 = $request->old_image3;
        }


       if(isset($request->save_home_meta) && $request->save_home_meta == "ok"){
           foreach ($request->all() as $key => $val) {
               if ($key != 'old_image2'&& $key != 'old_image3' && $key != 'old_image' && $key != '_token' && $key != 'save_home_meta') {
                   $home_page->$key = $val;
               }
           }

           $home_page->image = $filepath;
           $home_page->block_2_img = $filepath2;
           $home_page->block_3_img = $filepath3;
           $home_page->save();
           return redirect('/admin/homepage');
       }
    }
}
