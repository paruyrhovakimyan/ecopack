<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class MediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $default_lang = config('app.locale');
        $all_media = DB::table('media')->select("title_$default_lang as title","id")->get();
        return view("admin.media.all", compact('all_media'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.media.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($request->add_media) && $request->add_media == "ok"){
            //Armenian
            $title_am = $request->title;
            $desc_am = $request->desc;
            $meta_key_am = $request->meta_key;
            if($request->meta_title == ''){
                $meta_title_am = $title_am;
            }else{
                $meta_title_am = $request->meta_title;
            }
            if($request->meta_desc == ''){
                $meta_desc_am = substr(strip_tags($desc_am),0,255);
            }else{
                $meta_desc_am = $request->meta_desc;
            }




            //Russian
            $title_ru = $request->title_ru;
            $desc_ru = $request->desc_ru;
            $meta_key_ru = $request->meta_key_ru;
            if($request->meta_title_ru == ''){
                $meta_title_ru = $title_ru;
            }else{
                $meta_title_ru = $request->meta_title_ru;
            }
            if($request->meta_desc_ru == ''){
                $meta_desc_ru = substr(strip_tags($desc_ru),0,255);
            }else{
                $meta_desc_ru = $request->meta_desc_ru;
            }




            //English
            $title_en = $request->title_en;
            $desc_en = $request->desc_en;
            $meta_key_en = $request->meta_key_en;
            if($request->meta_title_en == ''){
                $meta_title_en = $title_en;
            }else{
                $meta_title_en = $request->meta_title_en;
            }
            if($request->meta_desc_en == ''){
                $meta_desc_en = substr(strip_tags($desc_en),0,255);
            }else{
                $meta_desc_en = $request->meta_desc_en;
            }

            $video_link = $request->video_link;

            if($request->date_day == '' && $request->date_month=='' && $request->date_year==''){
                $media_date = date("d/m/Y");
            }else{
                $media_date = $request->date_day."/".$request->date_month."/".$request->date_year;
            }


            DB::table('media')->insert(
                [
                    'title_am' => $title_am,
                    'description_am' => $desc_am,
                    'title_ru' => $title_ru,
                    'description_ru' => $desc_ru,
                    'title_en' => $title_en,
                    'description_en' => $desc_en,
                    'meta_title_am' => $meta_title_am,
                    'meta_desc_am' => $meta_desc_am,
                    'meta_key_am' => $meta_key_am,
                    'meta_title_en' => $meta_title_en,
                    'meta_desc_en' => $meta_desc_en,
                    'meta_key_en' => $meta_key_en,
                    'meta_title_ru' => $meta_title_ru,
                    'meta_desc_ru' => $meta_desc_ru,
                    'meta_key_ru' => $meta_key_ru,
                    'video' => $video_link,
                    'created_at' => $media_date

                ]
            );
            return redirect('admin/media');


        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $settings = DB::table('settings')->where('id', 1)->first();
        $media = DB::table('media')->where('id', $id)->first();
        $media_date = explode("/", $media->created_at);
        $media->day = $media_date['0'];
        $media->month = $media_date['1'];
        $media->year = $media_date['2'];
        return view('admin.media.edit', compact('media','settings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (isset($request->edit_media) && $request->edit_media == "ok"){
            //Armenian
            $title_am = $request->title;
            $desc_am = $request->desc;
            $meta_key_am = $request->meta_key;
            if($request->meta_title == ''){
                $meta_title_am = $title_am;
            }else{
                $meta_title_am = $request->meta_title;
            }
            if($request->meta_desc == ''){
                $meta_desc_am = substr(strip_tags($desc_am),0,255);
            }else{
                $meta_desc_am = $request->meta_desc;
            }




            //Russian
            $title_ru = $request->title_ru;
            $desc_ru = $request->desc_ru;
            $meta_key_ru = $request->meta_key_ru;
            if($request->meta_title_ru == ''){
                $meta_title_ru = $title_ru;
            }else{
                $meta_title_ru = $request->meta_title_ru;
            }
            if($request->meta_desc_ru == ''){
                $meta_desc_ru = substr(strip_tags($desc_ru),0,255);
            }else{
                $meta_desc_ru = $request->meta_desc_ru;
            }




            //English
            $title_en = $request->title_en;
            $desc_en = $request->desc_en;
            $meta_key_en = $request->meta_key_en;
            if($request->meta_title_en == ''){
                $meta_title_en = $title_en;
            }else{
                $meta_title_en = $request->meta_title_en;
            }
            if($request->meta_desc_en == ''){
                $meta_desc_en = substr(strip_tags($desc_en),0,255);
            }else{
                $meta_desc_en = $request->meta_desc_en;
            }

            $video_link = $request->video_link;

            if($request->date_day == '' && $request->date_month=='' && $request->date_year==''){
                $media_date = date("d/m/Y");
            }else{
                $media_date = $request->date_day."/".$request->date_month."/".$request->date_year;
            }


            DB::table('media')
                ->where('id', $id)
                ->update(
                [
                    'title_am' => $title_am,
                    'description_am' => $desc_am,
                    'title_ru' => $title_ru,
                    'description_ru' => $desc_ru,
                    'title_en' => $title_en,
                    'description_en' => $desc_en,
                    'meta_title_am' => $meta_title_am,
                    'meta_desc_am' => $meta_desc_am,
                    'meta_key_am' => $meta_key_am,
                    'meta_title_en' => $meta_title_en,
                    'meta_desc_en' => $meta_desc_en,
                    'meta_key_en' => $meta_key_en,
                    'meta_title_ru' => $meta_title_ru,
                    'meta_desc_ru' => $meta_desc_ru,
                    'meta_key_ru' => $meta_key_ru,
                    'video' => $video_link,
                    'created_at' => $media_date

                ]
            );
            return redirect('admin/media/'.$id.'/edit');


        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('media')->where('id', '=', $id)->delete();
    }
}
