<?php

namespace App\Http\Controllers\Admin;

use App\NewsPdf;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\UrlGenerator;

class NewsPdfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($parent_id)
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $pages = DB::table('news_pdf')->select("pdf_name_$default_lang as title", "id")->where('news_id',$parent_id)->get();
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();
        return view("admin.page_pdf.all", compact('languages','pages','default_lang','parent_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($parent_id)
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $page_child = DB::table('news')->select("title_$default_lang as title","parent_id")->where('id', $parent_id)->first();
        $page_bredcrump = "<h4>";
        if($page_child->parent_id != 0){
            $page_parent = DB::table('pages')->select("title_$default_lang as title")->where('id', $page_child->parent_id)->first();
            $page_bredcrump .= '<a href="'.url('/').'/admin/pages/'.$page_child->parent_id.'">'.$page_parent->title.'</a> | ';
        }
        $page_bredcrump .= $page_child->title."</h4>";
        $settings = DB::table('settings')->where('id', 1)->first();
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();

        return view('admin.news_pdf.create',compact('languages','settings','parent_id','page_bredcrump'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($parent_id,Request $request)
    {
        if(isset($request->add_pdf) && $request->add_pdf == "ok"){
            $page_pdfs = new NewsPdf();


            $languages = DB::table('languages')->where('hidden', '=', 0)->get();
            $page_pdfs->news_id = $parent_id;
            foreach ($languages as $language) {
                $field_name = "pdf_file_".$language->short;
                $db_field = "pdf_file_".$language->short;
                if ($request->hasFile($field_name)) {
                    $destinationPath =  base_path() . '/public/uploads/news/pdf/';
                    $file = $request->file($field_name);
                    $file_name = $file->getClientOriginalName();
                    $file->move($destinationPath , $file_name);
                    $page_pdfs->$db_field= $file_name;
                }
            }

            foreach ($request->all() as $key => $val) {
                if ($key != '_token' && $key != 'add_pdf' && $key != 'old_pdf_title') {
                    $page_pdfs->$key = $val;
                }
            }
            $page_pdfs->save();



            $parent_page = DB::table('news')
                ->select(
                    "parent_id"
                )->where('id', $parent_id)->first();
            if($parent_page->parent_id !=0){
                $super_parent = $parent_page->parent_id;
            }else{
                $super_parent =$parent_id;
            }

            return redirect('admin/pages/'.$super_parent);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($parent_id, $id)
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;

        $page_child = DB::table('news')->select("title_$default_lang as title","parent_id")->where('id', $parent_id)->first();
        $page_bredcrump = "<h4>";
        if($page_child->parent_id != 0){
            $page_parent = DB::table('pages')->select("title_$default_lang as title")->where('id', $page_child->parent_id)->first();
            $page_bredcrump .= '<a href="'.url('/').'/admin/pages/'.$page_child->parent_id.'">'.$page_parent->title.'</a> | ';
        }
        $page_bredcrump .= $page_child->title."</h4>";
        $settings = DB::table('settings')->where('id', 1)->first();

        $page_pdf = DB::table('news_pdfs')->where('news_id',$parent_id)->first();

        $languages = DB::table('languages')->where('hidden', '=', 0)->get();

        return view('admin.news_pdf.edit',compact('default_lang','languages','settings','parent_id','page_bredcrump','page_pdf'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $parent_id, $id)
    {
        if(isset($request->edit_pdf) && $request->edit_pdf == "ok"){
            $page_pdfs = NewsPdf::find($id);


            $languages = DB::table('languages')->where('hidden', '=', 0)->get();
            $page_pdfs->news_id = $parent_id;


            foreach ($request->all() as $key => $val) {
                if ($key != '_token' && $key != 'edit_pdf' && $key != 'old_pdf_title') {
                    $page_pdfs->$key = $val;
                }
            }
            foreach ($languages as $language) {
                $field_name = "pdf_file_".$language->short;
                $db_field = "pdf_file_".$language->short;
                if ($request->hasFile($field_name)) {
                    $destinationPath =  base_path() . '/public/uploads/news/pdf/';
                    $file = $request->file($field_name);
                    $file_name = $file->getClientOriginalName();
                    $file->move($destinationPath , $file_name);
                    $page_pdfs->$db_field= $file_name;
                }
            }
            $page_pdfs->save();


        }
        $parent_page = DB::table('news')
            ->select(
                "parent_id"
            )->where('id', $parent_id)->first();
        if($parent_page->parent_id !=0){
            $super_parent = $parent_page->parent_id;
        }else{
            $super_parent =$parent_id;
        }

        return redirect('admin/pages/'.$super_parent);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function delete_pdf(Request $request)
    {
        $back_url = app('Illuminate\Routing\UrlGenerator')->previous();
        $id = $request->id;
        DB::table('news_pdf')->where('id', '=', $id)->delete();
        return redirect($back_url);
    }
}
