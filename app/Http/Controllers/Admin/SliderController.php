<?php

namespace App\Http\Controllers\Admin;

use App\SliderItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $sliders = DB::table('slider')->get();

        foreach ($sliders as $slider) {
            $slider->slides_count = DB::table('slider_items')->where('slider_id', $slider->id)->get()->count();
        }

        return view("admin.slider.all", compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($slider_id)
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();

        return view('admin.slider.create', compact('default_lang','languages','slider_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(isset($request->add_slider) && $request->add_slider == "ok"){
            $title = $request->title;
            DB::table('slider')->insert(
                [
                    'title' => $title,
                ]
            );
        }
        return redirect('admin/slider');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();


        $slider = DB::table('slider')->where('id', $id)->first();
        $slides = DB::table('slider_items')->where('slider_id', $id)->orderBy('item_order', 'asc')->get();
        return view('admin.slider.edit', compact('default_lang','languages','slider','slides'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(isset($request->edit_slider) && $request->edit_slider == "ok"){

            $slider_item = new SliderItem();



            if ($request->file("image_logo")) {

                $image = Input::file('image_logo');
                $generatedString1 = time();
                $extension1 = $request->file("image_logo")->getClientOriginalExtension();

                $newFile1 = public_path('uploads/slider/' . $generatedString1 . "." . $extension1);
                Image::make($image->getRealPath())->resize(74, 74)->orientate()->save($newFile1);

                $filepath_icon = $generatedString1 . "." . $extension1;
            } else {
                $filepath_icon = '';
            }



            if ($request->file("image")) {

                $image = Input::file('image');
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();

                $newFile = public_path('uploads/slider/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(1600, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFile);


                $newFileThumb = public_path('uploads/slider/thumbs/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(180, 94)->orientate()->save($newFileThumb);

                $newFileThumb_150 = public_path('uploads/slider/150/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(null, 150, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb_150);
                $filepath = $generatedString . "." . $extension;
            } else {
                $filepath = '';
            }
            $slide_title = $request->slide_title;
            $effect = $request->slide_effect;
            $slide_link = $request->slide_link;

            if(isset($request->new_tab)){
                $new_tab = $request->new_tab;
            }else{
                $new_tab = 0;
            }
            $item_order = $request->item_order;


            $change_speed = $request->change_speed;

            if(isset($request->title_paralax)){
                $title_paralax = $request->title_paralax;
            }else{
                $title_paralax = 0;
            }
$title_effect_id = $request->title_effect;
            if($request->title_effect == '1'){
                $title_effect = '[{"delay":1000,"split":"chars","split_direction":"forward","splitdelay":0.1,"speed":2000,"frame":"0","from":"x:[-105%];z:0;rX:0deg;rY:0deg;rZ:-90deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]';
            }elseif ($request->title_effect == '2'){
                $title_effect = '[{"delay":1000,"split":"chars","split_direction":"forward","splitdelay":0.05,"speed":2000,"frame":"0","from":"x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]';
            }elseif($request->title_effect == '3'){
                $title_effect = '[{"delay":1000,"split":"chars","split_direction":"forward","splitdelay":0.05,"speed":2000,"frame":"0","from":"y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]';
            }elseif($request->title_effect == '4'){
                $title_effect = '[{"delay":1000,"split":"chars","split_direction":"forward","splitdelay":0.05,"speed":2000,"frame":"0","from":"y:[-100%];z:0;rZ:35deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]';
            }elseif($request->title_effect == '5'){
                $title_effect = '[{"delay":1000,"split":"chars","split_direction":"forward","splitdelay":0.05,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]';
            }else{
                $title_effect = '[{"delay":1000,"split":"chars","split_direction":"forward","splitdelay":0.1,"speed":2000,"frame":"0","from":"x:[-105%];z:0;rX:0deg;rY:0deg;rZ:-90deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]';
            }

            foreach ($request->all() as $key => $val) {
                if ($key != 'image_logo'  &&  $key != '_token'  &&  $key!='_method' && $key != 'edit_slider') {
                    $slider_item->$key = $val;
                }
            }
            $slider_item->title_effect_id = $title_effect_id;
            $slider_item->title_effect = $title_effect;
            $slider_item->title_paralax = $title_paralax;
            $slider_item->image = $filepath;
            $slider_item->slider_id = $id;
            $slider_item->slider_icon = $filepath_icon;
            $slider_item->save();
        }
        return redirect('admin/slider/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function changeHidden( Request $request){
        if(isset($_REQUEST)){
            $id = $request->id;
            $hidden = $request->update_hidden;
            DB::table('slider_items')
                ->where('id', $id)
                ->update(['hidden' => $hidden]);
            if($hidden == 1){
                echo "0";
            }else{
                echo "1";
            }
        }


    }
    public function slide_delete(Request $request){
        $slide_delete = $request->id;
        DB::table('slider_items')->where('id', '=', $slide_delete)->delete();
    }


    public function edit_slide($id){
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();

        $slide = DB::table('slider_items')->where('id', $id)->first();
        return view('admin.slider.slide_edit', compact('default_lang','languages','slide'));
    }

    public function update_slide(Request $request){
        if(isset($request->edit_slide) && $request->edit_slide == "ok"){
            $slide_id = $request->slide_id;
            $slider_item = SliderItem::find($slide_id);


            if ($request->file("image_logo")) {

                $image = Input::file('image_logo');
                $generatedString1 = time();
                $extension1 = $request->file("image_logo")->getClientOriginalExtension();

                $newFile1 = public_path('uploads/slider/' . $generatedString1 . "." . $extension1);
                Image::make($image->getRealPath())->resize(74, 74)->orientate()->save($newFile1);

                $filepath_icon = $generatedString1 . "." . $extension1;
            } else {
                $filepath_icon = $request->old_image_logo;
            }

            if ($request->file("image")) {

                $image = Input::file('image');
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();

                $newFile = public_path('uploads/slider/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(1600, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFile);


                $newFileThumb = public_path('uploads/slider/thumbs/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(180, 94)->orientate()->save($newFileThumb);

                $newFileThumb_150 = public_path('uploads/slider/150/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(null, 150, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb_150);
                $filepath = $generatedString . "." . $extension;
            } else {
                $filepath = $request->old_image;
            }
            $slide_title = $request->slide_title;
            $effect = $request->slide_effect;
            $slide_link = $request->slide_link;

            if(isset($request->new_tab)){
                $new_tab = $request->new_tab;
            }else{
                $new_tab = 0;
            }
            $item_order = $request->item_order;


            $change_speed = $request->change_speed;

            if(isset($request->title_paralax)){
                $title_paralax = $request->title_paralax;
            }else{
                $title_paralax = 0;
            }
            $title_effect_id = $request->title_effect;
            if($request->title_effect == '1'){
                $title_effect = '[{"delay":1000,"split":"chars","split_direction":"forward","splitdelay":0.1,"speed":2000,"frame":"0","from":"x:[-105%];z:0;rX:0deg;rY:0deg;rZ:-90deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]';
            }elseif ($request->title_effect == '2'){
                $title_effect = '[{"delay":1000,"split":"chars","split_direction":"forward","splitdelay":0.05,"speed":2000,"frame":"0","from":"x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]';
            }elseif($request->title_effect == '3'){
                $title_effect = '[{"delay":1000,"split":"chars","split_direction":"forward","splitdelay":0.05,"speed":2000,"frame":"0","from":"y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]';
            }elseif($request->title_effect == '4'){
                $title_effect = '[{"delay":1000,"split":"chars","split_direction":"forward","splitdelay":0.05,"speed":2000,"frame":"0","from":"y:[-100%];z:0;rZ:35deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]';
            }elseif($request->title_effect == '5'){
                $title_effect = '[{"delay":1000,"split":"chars","split_direction":"forward","splitdelay":0.05,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]';
            }else{
                $title_effect = '[{"delay":1000,"split":"chars","split_direction":"forward","splitdelay":0.1,"speed":2000,"frame":"0","from":"x:[-105%];z:0;rX:0deg;rY:0deg;rZ:-90deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]';
            }

            foreach ($request->all() as $key => $val) {
                if ($key != 'image_logo'  &&  $key != 'old_image_logo'  &&  $key != 'slide_id'  &&  $key != 'old_image'  &&  $key != '_token'  &&  $key!='_method' && $key != 'edit_slide') {
                    $slider_item->$key = $val;
                }
            }
            $slider_item->title_effect_id = $title_effect_id;
            $slider_item->title_effect = $title_effect;
            $slider_item->title_paralax = $title_paralax;
            $slider_item->image = $filepath;
            $slider_item->slider_icon = $filepath_icon;
            $slider_item->save();
            return redirect('admin/slider/1/edit');
        }

    }
}
