<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AboutController extends Controller
{

    public function __construct()
    {
        if(!session('admin')){
            return view('admin.login');
        }
    }

    public function index()
    {
        $about_info = DB::table('pages')->where('id', '=', 1)->first();
        return view("admin.about", compact('about_info'));
    }

    /**
     *
     */
    public function save(Request $request)
    {
        if (isset($request->save_about) && $request->save_about == "ok") {
            $page_title = $request->about_title;
            $page_desc = $request->desc;
            if ($request->meta_title == '') {
                $meta_title = $page_title;
            } else {

                $meta_title = $request->meta_title;
            }
            if ($request->meta_desc == '') {
                $meta_desc = substr(strip_tags($page_desc), 0, 350);
            } else {
                $meta_desc = strip_tags($request->meta_desc);
            }
            $meta_key = $request->meta_key;


            $page_title_ru = $request->title_ru;
            $page_desc_ru = $request->desc_ru;
            if ($request->meta_title_ru == '') {
                $meta_title_ru = $page_title_ru;
            } else {

                $meta_title_ru = $request->meta_title_ru;
            }
            if ($request->meta_desc_ru == '') {
                $meta_desc_ru = substr(strip_tags($page_desc_ru), 0, 350);
            } else {
                $meta_desc_ru = strip_tags($request->meta_desc_ru);
            }
            $meta_key_ru = $request->meta_key_ru;


            $page_title_en = $request->title_en;
            $page_desc_en = $request->desc_en;
            if ($request->meta_title_en == '') {
                $meta_title_en = $page_title_en;
            } else {

                $meta_title_en = $request->meta_title_en;
            }
            if ($request->meta_desc_en == '') {
                $meta_desc_en = substr(strip_tags($page_desc_en), 0, 350);
            } else {
                $meta_desc_en = strip_tags($request->meta_desc_en);
            }
            $meta_key_en = $request->meta_key_en;


            DB::table('pages')
                ->where('id', 1)
                ->update(
                    [
                        'title' => $page_title,
                        'description' => $page_desc,
                        'meta_title' => $meta_title,
                        'meta_key' => $meta_key,
                        'meta_desc' => $meta_desc,
                        'title_ru' => $page_title_ru,
                        'description_ru' => $page_desc_ru,
                        'meta_title_ru' => $meta_title_ru,
                        'meta_key_ru' => $meta_key_ru,
                        'meta_desc_ru' => $meta_desc_ru,
                        'title_en' => $page_title_en,
                        'description_en' => $page_desc_en,
                        'meta_title_en' => $meta_title_en,
                        'meta_key_en' => $meta_key_en,
                        'meta_desc_en' => $meta_desc_en
                    ]);
            $about_info = DB::table('pages')->where('id', '=', 1)->first();
            return view("admin.about", compact('about_info'));
        }
    }
}
