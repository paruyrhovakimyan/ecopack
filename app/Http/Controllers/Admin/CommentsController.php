<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CommentsController extends Controller
{
    public function index(){
        $comments = DB::table('comments')->get();
        foreach ($comments as $comment){
            $user_id = $comment->user_id;
            $user_result = DB::table('users')->where('id', '=',$user_id )->first();
            $comment->user_name = $user_result->name;
            $article_id = $comment->article_id;
            $article_result = DB::table('articles')->where('id', '=',$article_id )->first();
            if(isset($article_result->title) && $article_result->title != ''){
                $comment->article_name = $article_result->title;
            }else{
                $comment->article_name = '';
            }
        }
        return view("admin.comments.all", compact('comments'));
    }

    public function changeHidden( Request $request){
        if(isset($_REQUEST)){
            $id = $request->id;
            $hidden = $request->update_hidden;
            DB::table('comments')
                ->where('id', $id)
                ->update(['approve' => $hidden]);
            if($hidden == 1){
                echo "0";
            }else{
                echo "1";
            }
        }


    }

    public function destroy($id)
    {
        DB::table('comments')->where('id', '=', $id)->delete();
    }
}
