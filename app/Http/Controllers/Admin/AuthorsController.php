<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class AuthorsController extends Controller
{



    private function mb_str_split( $string ) {
        # Split at all position not after the start: ^
        # and not before the end: $
        return preg_split('/(?<!^)(?!$)/u', $string );
    }
    private function create_english($title){
        $charlist = $this->mb_str_split( $title );
        $array_hayeren = array('.','ա','բ','գ','դ','ե','զ','է','ը','թ','ժ','ի','լ','խ','ծ','կ','հ','ձ','ղ','ճ','մ','յ','ն','շ','ո','չ','պ','ջ','ռ','ս','վ','տ','ր','ց','ւ','փ','ք','և','օ','ֆ',' ','  ','/','\\','&','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','1','2','3','4','5','6','7','8','9','0');
        $array_english = array('-','a','b','g','d','e','z','e','y','t','zh','i','l','kh','ts','k','h','dz','gh','ch','m','y','n','sh','o','ch','p','j','r','s','v','t','r','c','u','p','q','ev','o','f','-','-','-','-','-','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','1','2','3','4','5','6','7','8','9','0');
        $new ='';
        if(count($charlist)<41){
            $counts = count($charlist);
        }
        else{
            $counts = 40;
        }
        for($i=0;$i<$counts;$i++){
            $key = array_search(mb_strtolower($charlist[$i],'UTF-8'),$array_hayeren);
            $new .= $array_english[$key];

        }
        return $new;
    }
    private function create_slug($title,$id_obj){

        $slug = $this->create_english($title);
        $article_isset_slug = DB::table('authors')->where('slug', '=', $slug)->orderBy('id', 'desc')->count();
        if($article_isset_slug > 0){
            $new_slug = $slug."-".$id_obj;
        }else{
            $new_slug = $slug;
        }

        return $new_slug;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $default_lang = config('app.locale');
        $authors = DB::table('authors')->select("image","id", "name_$default_lang as name")->get();
        return view("admin.authors.all", compact('authors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $settings = DB::table('settings')->where('id', 1)->first();
        return view('admin.authors.create', compact('settings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(isset($request->add_author) && $request->add_author == "ok"){
            $title_am = $request->title;
            $desc_am = $request->desc;
            $meta_key_am = $request->meta_key;
            if($request->meta_title == ''){
                $meta_title_am = $title_am;
            }else{
                $meta_title_am = $request->meta_title;
            }
            if($request->meta_desc == ''){
                $meta_desc_am = substr(strip_tags($desc_am),0,255);
            }else{
                $meta_desc_am = $request->meta_desc;
            }




            //Russian
            $title_ru = $request->title_ru;
            $desc_ru = $request->desc_ru;
            $meta_key_ru = $request->meta_key_ru;
            if($request->meta_title_ru == ''){
                $meta_title_ru = $title_ru;
            }else{
                $meta_title_ru = $request->meta_title_ru;
            }
            if($request->meta_desc_ru == ''){
                $meta_desc_ru = substr(strip_tags($desc_ru),0,255);
            }else{
                $meta_desc_ru = $request->meta_desc_ru;
            }




            //English
            $title_en = $request->title_en;
            $desc_en = $request->desc_en;
            $meta_key_en = $request->meta_key_en;
            if($request->meta_title_en == ''){
                $meta_title_en = $title_en;
            }else{
                $meta_title_en = $request->meta_title_en;
            }
            if($request->meta_desc_en == ''){
                $meta_desc_en = substr(strip_tags($desc_en),0,255);
            }else{
                $meta_desc_en = $request->meta_desc_en;
            }


            $email = $request->mail;
            $gender = $request->gender;
            $birthday = $request->birth_day.'/'.$request->birth_month.'/'.$request->birth_year;

            if ($request->file("image")) {
                $image = $request->file("image");
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();
                $newFile = "uploads/authors/" . $generatedString . "." . $extension;
                $filepath = $generatedString . "." . $extension;

                Image::make($image->getRealPath())->resize(250, 250)->orientate()->save($newFile);
            } else {
                $filepath = "";
            }
            $id = DB::table('authors')->insertGetId(
                [
                    'name_am' => $title_am,
                    'description_am' => $desc_am,
                    'name_ru' => $title_ru,
                    'description_ru' => $desc_ru,
                    'name_en' => $title_en,
                    'description_en' => $desc_en,
                    'meta_title_am' => $meta_title_am,
                    'meta_desc_am' => $meta_desc_am,
                    'meta_key_am' => $meta_key_am,
                    'meta_title_en' => $meta_title_en,
                    'meta_desc_en' => $meta_desc_en,
                    'meta_key_en' => $meta_key_en,
                    'meta_title_ru' => $meta_title_ru,
                    'meta_desc_ru' => $meta_desc_ru,
                    'meta_key_ru' => $meta_key_ru,
                    'image'=> $filepath,
                    'email'=> $email,
                    'gender'=> $gender,
                    'birthday'=> $birthday,
                    'slug'=> "slug",

                ]
            );

            if(config('app.locale') == 'en'){
                $slug = $this->create_slug($title_en,$id);
            }elseif (config('app.locale') == 'ru'){
                $slug = $this->create_slug($title_ru,$id);
            }else{
                $slug = $this->create_slug($title_am,$id);
            }
            DB::table('authors')
                ->where('id', $id)
                ->update(
                    [
                        'slug'=> $slug
                    ]
                );

            return redirect('admin/authors');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $author = DB::table('authors')->where('id', $id)->first();
        if($author->birthday !=''){
            $birthday_array = explode("/", $author->birthday);
            $author->day = $birthday_array['0'];
            $author->month = $birthday_array['1'];
            $author->year = $birthday_array['2'];
        }
        $settings = DB::table('settings')->where('id', 1)->first();
        return view('admin.authors.edit', compact('author', 'settings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(isset($request->edit_author) && $request->edit_author = "ok"){
            $title_am = $request->title;
            $desc_am = $request->desc;
            $meta_key_am = $request->meta_key;
            if($request->meta_title == ''){
                $meta_title_am = $title_am;
            }else{
                $meta_title_am = $request->meta_title;
            }
            if($request->meta_desc == ''){
                $meta_desc_am = substr(strip_tags($desc_am),0,255);
            }else{
                $meta_desc_am = $request->meta_desc;
            }




            //Russian
            $title_ru = $request->title_ru;
            $desc_ru = $request->desc_ru;
            $meta_key_ru = $request->meta_key_ru;
            if($request->meta_title_ru == ''){
                $meta_title_ru = $title_ru;
            }else{
                $meta_title_ru = $request->meta_title_ru;
            }
            if($request->meta_desc_ru == ''){
                $meta_desc_ru = substr(strip_tags($desc_ru),0,255);
            }else{
                $meta_desc_ru = $request->meta_desc_ru;
            }




            //English
            $title_en = $request->title_en;
            $desc_en = $request->desc_en;
            $meta_key_en = $request->meta_key_en;
            if($request->meta_title_en == ''){
                $meta_title_en = $title_en;
            }else{
                $meta_title_en = $request->meta_title_en;
            }
            if($request->meta_desc_en == ''){
                $meta_desc_en = substr(strip_tags($desc_en),0,255);
            }else{
                $meta_desc_en = $request->meta_desc_en;
            }


            $email = $request->mail;
            $gender = $request->gender;
            $birthday = $request->birth_day.'/'.$request->birth_month.'/'.$request->birth_year;

            if ($request->file("image")) {
                $image = $request->file("image");
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();
                $newFile = "uploads/authors/" . $generatedString . "." . $extension;
                $filepath = $generatedString . "." . $extension;

                Image::make($image->getRealPath())->resize(250, 250)->orientate()->save($newFile);
            }else{
                $filepath= $request->old_image;
            }

            $slug = $request->slug;

            if($slug ==''){
                if(config('app.locale') == 'en'){
                    $new_slug = $this->create_slug($title_en,$id);
                }elseif (config('app.locale') == 'ru'){
                    $new_slug = $this->create_slug($title_ru,$id);
                }else{
                    $new_slug = $this->create_slug($title_am,$id);
                }
            }else {
                $author_isset_slug = DB::table('articles')->where('slug', '=', $slug)->where('id', '!=', $id)->orderBy('id', 'desc')->count();
                if ($author_isset_slug > 0) {
                    $new_slug = $slug . "-" . $id;
                } else {
                    $new_slug = $slug;
                }
            }

            DB::table('authors')
                ->where('id', $id)
                ->update(
                    [
                        'name_am' => $title_am,
                        'description_am' => $desc_am,
                        'name_ru' => $title_ru,
                        'description_ru' => $desc_ru,
                        'name_en' => $title_en,
                        'description_en' => $desc_en,
                        'meta_title_am' => $meta_title_am,
                        'meta_desc_am' => $meta_desc_am,
                        'meta_key_am' => $meta_key_am,
                        'meta_title_en' => $meta_title_en,
                        'meta_desc_en' => $meta_desc_en,
                        'meta_key_en' => $meta_key_en,
                        'meta_title_ru' => $meta_title_ru,
                        'meta_desc_ru' => $meta_desc_ru,
                        'meta_key_ru' => $meta_key_ru,
                        'image'=> $filepath,
                        'email'=> $email,
                        'gender'=> $gender,
                        'birthday'=> $birthday,
                        'slug'=> $new_slug,
                    ]
                );
            $data['edit_successful'] = "yes";
            $author = DB::table('authors')->where('id', $id)->first();
            if($author->birthday !=''){
                $birthday_array = explode("/", $author->birthday);
                $author->day = $birthday_array['0'];
                $author->month = $birthday_array['1'];
                $author->year = $birthday_array['2'];
            }
            $settings = DB::table('settings')->where('id', 1)->first();
            return view('admin.authors.edit', compact('author','data','settings'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('authors')->where('id', '=', $id)->delete();
    }

    public function add_author(){
        ?>

        <div class="new_author_block author_fields">

            <input style="<?=(config('app.locale') != 'am') ? "display: none" : ""; ?>" type="text" placeholder="Arminian author name" class="arm_block author_nik_name form-control" name="author_nik_name[]" value="">
            <input style="<?=(config('app.locale') != 'ru') ? "display: none" : ""; ?>" type="text" placeholder="Russian author name" class="rus_block author_nik_name form-control" name="author_nik_name_ru[]" value="">
            <input style="<?=(config('app.locale') != 'en') ? "display: none" : ""; ?>"  type="text" placeholder="English author name" class="eng_block author_nik_name form-control" name="author_nik_name_en[]" value="">
            <select name="author_name[]" class="author-combobox-wrap form-control article_author_name">
                <option value="0" >--Select author--</option>
                <?php
                $authors = DB::table('authors')->get();
                foreach ($authors as $author){
                    ?>
                    <option value="<?=$author->id?>" ><?=$author->name_am?></option>
                    <?php
                }
                ?>
            </select>
            <div onclick="remove_from_authors(this)" class="del_author_block"><i class="fa fa-trash-o"></i></div>
        </div>
<?php
    }
}
