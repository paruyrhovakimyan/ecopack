<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    private function mb_str_split( $string ) {
        # Split at all position not after the start: ^
        # and not before the end: $
        return preg_split('/(?<!^)(?!$)/u', $string );
    }
    private function create_english($title){
        $charlist = $this->mb_str_split( $title );
        $array_hayeren = array('.','ա','բ','գ','դ','ե','զ','է','ը','թ','ժ','ի','լ','խ','ծ','կ','հ','ձ','ղ','ճ','մ','յ','ն','շ','ո','չ','պ','ջ','ռ','ս','վ','տ','ր','ց','ւ','փ','ք','և','օ','ֆ',' ','  ','/','\\','&','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','1','2','3','4','5','6','7','8','9','0');
        $array_english = array('-','a','b','g','d','e','z','e','y','t','zh','i','l','kh','ts','k','h','dz','gh','ch','m','y','n','sh','o','ch','p','j','r','s','v','t','r','c','u','p','q','ev','o','f','-','-','-','-','-','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','1','2','3','4','5','6','7','8','9','0');
        $new ='';
        if(count($charlist)<41){
            $counts = count($charlist);
        }
        else{
            $counts = 40;
        }
        for($i=0;$i<$counts;$i++){
            $key = array_search(mb_strtolower($charlist[$i],'UTF-8'),$array_hayeren);
            $new .= $array_english[$key];

        }
        return $new;
    }
    private function create_slug($title,$id_obj){

            $slug = $this->create_english($title);
        $article_isset_slug = DB::table('articles')->where('slug', '=', $slug)->orderBy('id', 'desc')->count();
            if($article_isset_slug > 0){
                $new_slug = $slug."-".$id_obj;
            }else{
                $new_slug = $slug;
            }

        return $new_slug;
    }



    public function index()
    {
        $default_lang = config('app.locale');
        $articles = DB::table('articles')->select("title_$default_lang as title","created_at","image_$default_lang as image", "id")->orderBy('id', 'desc')->get();
        return view("admin.articles.all", compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$default_lang = config('app.locale');

        $settings = DB::table('settings')->where('id', 1)->first();
        $categories = DB::table('categories')->where( 'id', '!=', '4')->get();
        $all_authors = DB::table('authors')->get();
        return view('admin.articles.create', compact('categories','all_authors','settings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($request->add_article) && $request->add_article == "ok"){
            //Armenian
            $title_am = $request->title;
            $short_desc_am = $request->short_desc;
            $desc_am = $request->desc;
            $meta_key_am = $request->meta_key;
            if($request->meta_title == ''){
                $meta_title_am = $title_am;
            }else{
                $meta_title_am = $request->meta_title;
            }
            if($request->meta_desc == ''){
                $meta_desc_am = substr(strip_tags($short_desc_am),0,255);
            }else{
                $meta_desc_am = $request->meta_desc;
            }

            //Russian
            $title_ru = $request->title_ru;
            $short_desc_ru = $request->short_desc_ru;
            $desc_ru = $request->desc_ru;
            $meta_key_ru = $request->meta_key_ru;
            if($request->meta_title_ru == ''){
                $meta_title_ru = $title_ru;
            }else{
                $meta_title_ru = $request->meta_title_ru;
            }
            if($request->meta_desc_ru == ''){
                $meta_desc_ru = substr(strip_tags($short_desc_ru),0,255);
            }else{
                $meta_desc_ru = $request->meta_desc_ru;
            }




            //English
            $title_en = $request->title_en;
            $short_desc_en = $request->short_desc_en;
            $desc_en = $request->desc_en;
            $meta_key_en = $request->meta_key_en;
            if($request->meta_title_en == ''){
                $meta_title_en = $title_en;
            }else{
                $meta_title_en = $request->meta_title_en;
            }
            if($request->meta_desc_en == ''){
                $meta_desc_en = substr(strip_tags($short_desc_en),0,255);
            }else{
                $meta_desc_en = $request->meta_desc_en;
            }

            if ($request->file("image")) {
                $image = $request->file("image");
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();
                $newFile = "uploads/articles/" . $generatedString . "." . $extension;
                $newFileThumb = "uploads/articles/thumbs/" . $generatedString . "." . $extension;
                $newFileThumb_150 = "uploads/articles/150/" . $generatedString . "." . $extension;
                $filepath = $generatedString . "." . $extension;

                Image::make($image->getRealPath())->resize(null, 546, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFile);
                Image::make($image->getRealPath())->resize(260, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb);
                Image::make($image->getRealPath())->resize(150, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb_150);
            } else {
                $filepath = "";
            }


            if ($request->file("image_en")) {
                $image_en = $request->file("image_en");
                $generatedString_en = time();
                $extension_en = $request->file("image_en")->getClientOriginalExtension();
                $newFile_en = "uploads/articles/eng/" . $generatedString_en . "." . $extension_en;
                $newFileThumb_en = "uploads/articles/eng/thumbs/" . $generatedString_en . "." . $extension_en;
                $newFileThumb_150_en = "uploads/articles/eng/150/" . $generatedString_en. "." . $extension_en;
                $filepath_en = $generatedString_en . "." . $extension_en;

                Image::make($image_en->getRealPath())->resize(null, 546, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFile_en);
                Image::make($image_en->getRealPath())->resize(260, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb_en);
                Image::make($image_en->getRealPath())->resize(150, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb_150_en);
            } else {
                $filepath_en = "";
            }

            if ($request->file("image_ru")) {
                $image_ru = $request->file("image_ru");
                $generatedString_ru = time();
                $extension_ru = $request->file("image_ru")->getClientOriginalExtension();
                $newFile_ru = "uploads/articles/ru/" . $generatedString_ru . "." . $extension_ru;
                $newFileThumb_ru = "uploads/articles/ru/thumbs/" . $generatedString_ru . "." . $extension_ru;
                $newFileThumb_150_ru = "uploads/articles/ru/150/" . $generatedString_ru. "." . $extension_ru;
                $filepath_ru = $generatedString_ru . "." . $extension_ru;

                Image::make($image_ru->getRealPath())->resize(null, 546, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFile_ru);
                Image::make($image_ru->getRealPath())->resize(260, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb_ru);
                Image::make($image_ru->getRealPath())->resize(150, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb_150_ru);
            } else {
                $filepath_ru = "";
            }



            $article_cat = $request->cat_id;
            $video_link = $request->video_link;
            $id = DB::table('articles')->insertGetId(
                [
                    'title_am' => $title_am,
                    'short_desc_am' => $short_desc_am,
                    'description_am' => $desc_am,
                    'meta_title_am' => $meta_title_am,
                    'meta_desc_am' => $meta_desc_am,
                    'meta_key_am' => $meta_key_am,
                    'image_am'=> $filepath,


                    'title_ru' => $title_ru,
                    'short_desc_ru' => $short_desc_ru,
                    'description_ru' => $desc_ru,
                    'title_en' => $title_en,
                    'short_desc_en' => $short_desc_en,
                    'description_en' => $desc_en,
                    'meta_title_en' => $meta_title_en,
                    'meta_desc_en' => $meta_desc_en,
                    'meta_key_en' => $meta_key_en,
                    'meta_title_ru' => $meta_title_ru,
                    'meta_desc_ru' => $meta_desc_ru,
                    'meta_key_ru' => $meta_key_ru,

                    'image_ru'=> $filepath_ru,
                    'image_en'=> $filepath_en,
                    'cat_id'=> $article_cat,
                    'video'=> $video_link,
                    'slug'=> "slug",
                ]
            );

            if(config('app.locale') == 'en'){
                $slug = $this->create_slug($title_en,$id);
            }elseif (config('app.locale') == 'ru'){
                $slug = $this->create_slug($title_ru,$id);
            }else{
                $slug = $this->create_slug($title_am,$id);
            }

            DB::table('articles')
                ->where('id', $id)
                ->update(
                    [
                        'slug'=> $slug
                    ]
                );



            $aurhors_nik = $request->author_nik_name;
            $author_nik_name_ru = $request->author_nik_name_ru;
            $author_nik_name_en = $request->author_nik_name_en;
            $aurhors_id = $request->author_name;
            if(!empty($aurhors_nik)){
                foreach ($aurhors_nik as  $index => $val) {
                    DB::table('article_authors')->insert(
                        [
                            'article_id' => $id,
                            'author_name_am' => $aurhors_nik[$index],
                            'author_name_ru' => $author_nik_name_ru[$index],
                            'author_name_en' => $author_nik_name_en[$index],
                            'author_id' => $aurhors_id[$index],
                        ]
                    );
                }
            }



            $articles_pdf = $request->pdf;
            $articles_pdf_ru = $request->pdf_ru;
            $articles_pdf_en = $request->pdf_en;

            $article_pdf_titles = $request->article_pdf_title;
            $article_pdf_titles_ru = $request->article_pdf_title_ru;
            $article_pdf_titles_en = $request->article_pdf_title_en;
            if(!empty($articles_pdf)){
                foreach ($articles_pdf as  $key=> $item) {
                    if ($item) {
                        $pdf = $item->getClientOriginalName();
                        $item->move(
                            base_path() . '/public/uploads/articles/pdf', $pdf
                        );
                    } else {
                        $pdf = "";
                    }




                    if (isset($articles_pdf_ru[$key])) {
                        $pdf_ru = $articles_pdf_ru[$key]->getClientOriginalName();
                        $articles_pdf_ru[$key]->move(
                            base_path() . '/public/uploads/articles/pdf_ru', $pdf_ru
                        );
                    } else {
                        $pdf_ru = "";
                    }

                    if (isset($articles_pdf_en[$key])) {
                        $pdf_en = $articles_pdf_en[$key]->getClientOriginalName();
                        $articles_pdf_en[$key]->move(
                            base_path() . '/public/uploads/articles/pdf_en', $pdf_en
                        );
                    } else {
                        $pdf_en = "";
                    }


                    DB::table('article_pdf')->insert(
                        [
                            'article_id' => $id,
                            'pdf_file_am' => $pdf,
                            'pdf_file_ru' => $pdf_ru,
                            'pdf_file_en' => $pdf_en,
                            'pdf_name_am' => $article_pdf_titles[$key],
                            'pdf_name_ru' => $article_pdf_titles_ru[$key],
                            'pdf_name_en' => $article_pdf_titles_en[$key],
                        ]
                    );
                }
            }

            return redirect('admin/articles');


        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $settings = DB::table('settings')->where('id', 1)->first();
        $categories = DB::table('categories')->where( 'id', '!=', '4')->get();
        $article = DB::table('articles')->where('id', $id)->first();
        $article_authors = DB::table('article_authors')->where('article_id', $id)->get();
        $all_authors = DB::table('authors')->get();
        $article_pdfs = DB::table('article_pdf')->where('article_id', $id)->get();
        return view('admin.articles.edit', compact('article','categories','article_authors', 'all_authors','article_pdfs','settings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (isset($request->edit_articles) && $request->edit_articles == "ok"){
            //Armenian
            $video_link = $request->video_link;
            $title_am = $request->title;
            $short_desc_am = $request->short_desc;
            $desc_am = $request->desc;
            $meta_key_am = $request->meta_key;
            if($request->meta_title == ''){
                $meta_title_am = $title_am;
            }else{
                $meta_title_am = $request->meta_title;
            }
            if($request->meta_desc == ''){
                $meta_desc_am = substr(strip_tags($short_desc_am),0,255);
            }else{
                $meta_desc_am = $request->meta_desc;
            }




            //Russian
            $title_ru = $request->title_ru;
            $short_desc_ru = $request->short_desc_ru;
            $desc_ru = $request->desc_ru;
            $meta_key_ru = $request->meta_key_ru;
            if($request->meta_title_ru == ''){
                $meta_title_ru = $title_ru;
            }else{
                $meta_title_ru = $request->meta_title_ru;
            }
            if($request->meta_desc_ru == ''){
                $meta_desc_ru = substr(strip_tags($short_desc_ru),0,255);
            }else{
                $meta_desc_ru = $request->meta_desc_ru;
            }




            //English
            $title_en = $request->title_en;
            $short_desc_en = $request->short_desc_en;
            $desc_en = $request->desc_en;
            $meta_key_en = $request->meta_key_en;
            if($request->meta_title_en == ''){
                $meta_title_en = $title_en;
            }else{
                $meta_title_en = $request->meta_title_en;
            }
            if($request->meta_desc_en == ''){
                $meta_desc_en = substr(strip_tags($short_desc_en),0,255);
            }else{
                $meta_desc_en = $request->meta_desc_en;
            }
            if ($request->file("image")) {
                $image = $request->file("image");
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();
                $newFile = "uploads/articles/" . $generatedString . "." . $extension;
                $newFileThumb = "uploads/articles/thumbs/" . $generatedString . "." . $extension;
                $newFileThumb_150 = "uploads/articles/150/" . $generatedString . "." . $extension;
                $filepath = $generatedString . "." . $extension;

                Image::make($image->getRealPath())->resize(null, 546, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFile);
                Image::make($image->getRealPath())->resize(260, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb);
                Image::make($image->getRealPath())->resize(150, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb_150);
            }else{
                $filepath= $request->old_image;
            }

            if ($request->file("image_en")) {
                $image_en = $request->file("image_en");
                $generatedString_en = time();
                $extension_en = $request->file("image_en")->getClientOriginalExtension();
                $newFile_en = "uploads/articles/eng/" . $generatedString_en . "." . $extension_en;
                $newFileThumb_en = "uploads/articles/eng/thumbs/" . $generatedString_en . "." . $extension_en;
                $newFileThumb_150_en = "uploads/articles/eng/150/" . $generatedString_en. "." . $extension_en;
                $filepath_en = $generatedString_en . "." . $extension_en;

                Image::make($image_en->getRealPath())->resize(null, 546, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFile_en);
                Image::make($image_en->getRealPath())->resize(260, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb_en);
                Image::make($image_en->getRealPath())->resize(150, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb_150_en);
            } else {
                $filepath_en = $request->old_image_en;
            }

            if ($request->file("image_ru")) {
                $image_ru = $request->file("image_ru");
                $generatedString_ru = time();
                $extension_ru = $request->file("image_ru")->getClientOriginalExtension();
                $newFile_ru = "uploads/articles/ru/" . $generatedString_ru . "." . $extension_ru;
                $newFileThumb_ru = "uploads/articles/ru/thumbs/" . $generatedString_ru . "." . $extension_ru;
                $newFileThumb_150_ru = "uploads/articles/ru/150/" . $generatedString_ru. "." . $extension_ru;
                $filepath_ru = $generatedString_ru . "." . $extension_ru;

                Image::make($image_ru->getRealPath())->resize(null, 546, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFile_ru);
                Image::make($image_ru->getRealPath())->resize(260, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb_ru);
                Image::make($image_ru->getRealPath())->resize(150, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb_150_ru);
            } else {
                $filepath_ru = $request->old_image_ru;
            }


            $article_cat = $request->cat_id;


            $slug = $request->slug;
            if($slug ==''){
                if(config('app.locale') == 'en'){
                    $new_slug = $this->create_slug($title_en,$id);
                }elseif (config('app.locale') == 'ru'){
                    $new_slug = $this->create_slug($title_ru,$id);
                }else{
                    $new_slug = $this->create_slug($title_am,$id);
                }

            }else {
                $article_isset_slug = DB::table('articles')->where('slug', '=', $slug)->where('id', '!=', $id)->orderBy('id', 'desc')->count();
                if ($article_isset_slug > 0) {
                    $new_slug = $slug . "-" . $id;
                } else {
                    $new_slug = $slug;
                }
            }


            DB::table('articles')
                ->where('id', $id)
                ->update(
                    [
                        'title_am' => $title_am,
                        'short_desc_am' => $short_desc_am,
                        'title_ru' => $title_ru,
                        'short_desc_ru' => $short_desc_ru,
                        'title_en' => $title_en,
                        'short_desc_en' => $short_desc_en,
                        'meta_title_am' => $meta_title_am,
                        'meta_desc_am' => $meta_desc_am,
                        'meta_key_am' => $meta_key_am,
                        'meta_title_en' => $meta_title_en,
                        'meta_desc_en' => $meta_desc_en,
                        'meta_key_en' => $meta_key_en,
                        'meta_title_ru' => $meta_title_ru,
                        'meta_desc_ru' => $meta_desc_ru,
                        'meta_key_ru' => $meta_key_ru,
                        'image_am'=> $filepath,
                        'image_ru'=> $filepath_ru,
                        'image_en'=> $filepath_en,
                        'cat_id'=> $article_cat,
                        'description_am' => $desc_am,
                        'description_ru' => $desc_ru,
                        'description_en' => $desc_en,
                        'video'=> $video_link,
                        'slug'=> $new_slug,
                    ]
                );

            DB::table('article_authors')->where('article_id', '=', $id)->delete();

            $aurhors_nik = $request->author_nik_name;
            $author_nik_name_ru = $request->author_nik_name_ru;
            $author_nik_name_en = $request->author_nik_name_en;
            $aurhors_id = $request->author_name;
            if(!empty($aurhors_nik)){
                foreach ($aurhors_nik as  $index => $val) {
                    DB::table('article_authors')->insert(
                        [
                            'article_id' => $id,
                            'author_name_am' => $aurhors_nik[$index],
                            'author_name_ru' => $author_nik_name_ru[$index],
                            'author_name_en' => $author_nik_name_en[$index],
                            'author_id' => $aurhors_id[$index],
                        ]
                    );
                }
            }

            DB::table('article_pdf')->where('article_id', '=', $id)->delete();



            $article_old_update_pdf = $request->old_pdf;
            $article_old_update_pdf_ru = $request->old_pdf_ru;
            $article_old_update_pdf_en = $request->old_pdf_en;


            $article_old_pdf = $request->old_pdf_file;
            $article_old_pdf_ru = $request->old_pdf_file_ru;
            $article_old_pdf_en = $request->old_pdf_file_en;


            $article_old_pdf_name = $request->old_pdf_title;
            $article_old_pdf_name_ru = $request->old_pdf_title_ru;
            $article_old_pdf_name_en = $request->old_pdf_title_en;

            if(!empty($article_old_pdf)){
                foreach ($article_old_pdf as $pdf_index=> $pdf_val){
                    if (isset($article_old_update_pdf[$pdf_index])) {
                        $update_pdf = $article_old_update_pdf[$pdf_index]->getClientOriginalName();
                        $article_old_update_pdf[$pdf_index]->move(
                            base_path() . '/public/uploads/articles/pdf', $update_pdf
                        );
                    } else {
                        $update_pdf = $pdf_val;
                    }


                    if (isset($article_old_update_pdf_ru[$pdf_index])) {
                        $update_pdf_ru = $article_old_update_pdf_ru[$pdf_index]->getClientOriginalName();
                        $article_old_update_pdf_ru[$pdf_index]->move(
                            base_path() . '/public/uploads/articles/pdf_ru', $update_pdf_ru
                        );
                    } else {
                        $update_pdf_ru = $article_old_pdf_ru[$pdf_index];;
                    }

                    if (isset($article_old_update_pdf_en[$pdf_index])) {
                        $update_pdf_en = $article_old_update_pdf_en[$pdf_index]->getClientOriginalName();
                        $article_old_update_pdf_en[$pdf_index]->move(
                            base_path() . '/public/uploads/articles/pdf_en', $update_pdf_en
                        );
                    } else {
                        $update_pdf_en = $article_old_pdf_en[$pdf_index];;
                    }


                    DB::table('article_pdf')->insert(
                        [
                            'article_id' => $id,
                            'pdf_file_am' => $update_pdf,
                            'pdf_file_ru' => $update_pdf_ru,
                            'pdf_file_en' => $update_pdf_en,
                            'pdf_name_am' => $article_old_pdf_name[$pdf_index],
                            'pdf_name_ru' => $article_old_pdf_name_ru[$pdf_index],
                            'pdf_name_en' => $article_old_pdf_name_en[$pdf_index],
                        ]
                    );
                }
            }


            $articles_pdf = $request->pdf;
            $articles_pdf_ru = $request->pdf_ru;
            $articles_pdf_en = $request->pdf_en;

            $article_pdf_titles = $request->article_pdf_title;
            $article_pdf_titles_ru = $request->article_pdf_title_ru;
            $article_pdf_titles_en = $request->article_pdf_title_en;
            if(!empty($articles_pdf)){
                foreach ($articles_pdf as  $key=> $item) {
                    if ($item) {
                        $pdf = $item->getClientOriginalName();
                        $item->move(
                            base_path() . '/public/uploads/articles/pdf', $pdf
                        );
                    } else {
                        $pdf = "";
                    }




                    if ($articles_pdf_ru[$key]) {
                        $pdf_ru = $articles_pdf_ru[$key]->getClientOriginalName();
                        $articles_pdf_ru[$key]->move(
                            base_path() . '/public/uploads/articles/pdf_ru', $pdf_ru
                        );
                    } else {
                        $pdf_ru = "";
                    }

                    if ($articles_pdf_en[$key]) {
                        $pdf_en = $articles_pdf_en[$key]->getClientOriginalName();
                        $articles_pdf_en[$key]->move(
                            base_path() . '/public/uploads/articles/pdf_en', $pdf_en
                        );
                    } else {
                        $pdf_en = "";
                    }


                    DB::table('article_pdf')->insert(
                        [
                            'article_id' => $id,
                            'pdf_file_am' => $pdf,
                            'pdf_file_ru' => $pdf_ru,
                            'pdf_file_en' => $pdf_en,
                            'pdf_name_am' => $article_pdf_titles[$key],
                            'pdf_name_ru' => $article_pdf_titles_ru[$key],
                            'pdf_name_en' => $article_pdf_titles_en[$key],
                        ]
                    );
                }
            }




            $data['edit_successful'] = "yes";
            $categories = DB::table('categories')->get();
            $all_authors = DB::table('authors')->get();
            $article = DB::table('articles')->where('id', $id)->first();
            $article_authors = DB::table('article_authors')->where('article_id', $id)->get();
            $article_pdfs = DB::table('article_pdf')->where('article_id', $id)->get();
            $settings = DB::table('settings')->where('id', 1)->first();
            return view('admin.articles.edit', compact('settings','article','data', 'categories','article_authors','all_authors','article_pdfs'));


        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('articles')->where('id', '=', $id)->delete();
    }


public function add_pdf(){
        ?>

        <div class="new_author_block author_fields">
            <div style="<?=(config('app.locale') != 'am') ? "display: none" : ""; ?>" class="arm_block">
                <input type="file" name="pdf[]" class="article_pdf_file" >
                <input type="text" placeholder="Arminian Pdf title" class="article_pdf_title form-control" name="article_pdf_title[]" value="" required>
            </div>
            <div style="<?=(config('app.locale') != 'ru') ? "display: none" : ""; ?>" class="rus_block">
                <input type="file" name="pdf_ru[]" class="article_pdf_file" >
                <input type="text" placeholder="Russian Pdf title" class="article_pdf_title form-control" name="article_pdf_title_ru[]" value="">
            </div>
            <div style="<?=(config('app.locale') != 'en') ? "display: none" : ""; ?>" class="eng_block">
                <input type="file" name="pdf_en[]" class="article_pdf_file" >
                <input type="text" placeholder="English Pdf title" class="article_pdf_title form-control" name="article_pdf_title_en[]" value="">
            </div>
            <div onclick="remove_from_authors(this)" class="del_author_block"><i class="fa fa-trash-o"></i></div>
        </div>
        <?php
    }
}
