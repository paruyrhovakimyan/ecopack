<?php

namespace App\Http\Controllers\Admin;

use App\Team;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $partners = DB::table('teams')->select("id","image","hidden","title_$default_lang as title","team_position_$default_lang as team_position")->get();
        return view("admin.teams.all", compact('partners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $settings = DB::table('settings')->where('id', 1)->first();
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();

        return view('admin.teams.create',compact('languages','default_lang','settings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(isset($request->add_partner) && $request->add_partner == "ok"){


            $partner = new Team();


            if ($request->file("image")) {

                $image = Input::file('image');
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();

                $newFile = public_path('uploads/partners/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->orientate()->save($newFile);


                $newFileThumb = public_path('uploads/partners/thumbs/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(263, 200)->orientate()->save($newFileThumb);

                $newFileThumb_150 = public_path('uploads/partners/150/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(null, 150, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb_150);
                $filepath = $generatedString . "." . $extension;
            } else {
                $filepath = "";
            }
            foreach ($request->all() as $key => $val) {
                if ($key != '_token' && $key != 'add_partner') {
                    $partner->$key = $val;
                }
            }
            if (isset($request->stick) && $request->stick != '') {
                $stick = $request->stick;
            } else {
                $stick = 0;
            }
            $partner->stick = $stick;
            $partner->image = $filepath;
            $partner->save();
        }
        return redirect('admin/teams');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dropzoneDefaultHidden = '';
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();


        $partner = DB::table('teams')->where('id', $id)->first();
        $settings = DB::table('settings')->where('id', 1)->first();
        return view('admin.teams.edit', compact('default_lang','languages','partner', 'dropzoneDefaultHidden','settings'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(isset($request->edit_partner) && $request->edit_partner =="ok"){
            $partner = Team::find($id);


            if ($request->file("image")) {

                $image = Input::file('image');
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();

                $newFile = public_path('uploads/partners/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->orientate()->save($newFile);


                $newFileThumb = public_path('uploads/partners/thumbs/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(263, 200)->orientate()->save($newFileThumb);

                $newFileThumb_150 = public_path('uploads/partners/150/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(null, 150, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb_150);
                $filepath = $generatedString . "." . $extension;
            } else {
                $filepath = $request->old_image;
            }
            foreach ($request->all() as $key => $val) {
                if ($key != 'old_image' &&  $key != '_token'  &&  $key!='_method' && $key != 'edit_partner') {
                    $partner->$key = $val;
                }
            }
            if (isset($request->stick) && $request->stick != '') {
                $stick = $request->stick;
            } else {
                $stick = 0;
            }
            $partner->stick = $stick;
            $partner->image = $filepath;
            $partner->save();

        }
        return redirect('admin/teams');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('teams')->where('id', '=', $id)->delete();
    }
}
