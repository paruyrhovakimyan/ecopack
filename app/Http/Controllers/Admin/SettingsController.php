<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class SettingsController extends Controller
{
    public function index(Request $request)
    {
        if(isset($request->lang) && is_numeric($request->lang)){
            $edit_language = DB::table('languages')->where('id', $request->lang)->first();
        }else{
            $edit_language = array();
    }


        $languages = DB::table('languages')->orderBy('first', "desc")->get();
        return view('admin.settings.settings', compact('edit_language','languages'));
    }

    public function save(Request $request)
    {
        if (isset($request->save_settings)) {





            $title = $request->lang_name;
            $short_code = $request->short_code;

            if (!Schema::hasColumn('pages', "title_$short_code")) {
                Schema::table('pages', function($table) use ($short_code) {
                    $table->text("title_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            if (!Schema::hasColumn('pages', "description_$short_code")) {

                Schema::table('pages', function($table) use ($short_code) {
                    $table->text("description_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            if (!Schema::hasColumn('pages', "short_desc_$short_code")) {
                Schema::table('pages', function($table) use ($short_code) {
                    $table->text("short_desc_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            if (!Schema::hasColumn('pages', "meta_title_$short_code")) {
                Schema::table('pages', function($table) use ($short_code) {
                    $table->text("meta_title_$short_code")->nullable();
                });

            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            if (!Schema::hasColumn('pages', "meta_desc_$short_code")) {
                Schema::table('pages', function($table) use ($short_code) {
                    $table->text("meta_desc_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            if (!Schema::hasColumn('pages', "meta_key_$short_code")) {
                Schema::table('pages', function($table) use ($short_code) {
                    $table->text("meta_key_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }




            //Gallery info gallery_info
            $gallery_info = "gallery_infos";


            if (!Schema::hasColumn('gallery_infos', "title_$short_code")) {
                Schema::table('gallery_infos', function($table) use ($short_code) {
                    $table->text("title_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
             if (!Schema::hasColumn('gallery_info', "short_desc_$short_code")) {
                Schema::table('gallery_info', function($table) use ($short_code) {
                    $table->text("short_desc_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }



            //Home info
            if (!Schema::hasColumn('home_page', "meta_title_$short_code")) {
                Schema::table('home_page', function($table) use ($short_code) {
                    $table->text("meta_title_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            if (!Schema::hasColumn('home_page', "meta_desc_$short_code")) {
                Schema::table('home_page', function($table) use ($short_code) {
                    $table->text("meta_desc_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            if (!Schema::hasColumn('home_page', "meta_key_$short_code")) {
                Schema::table('home_page', function($table) use ($short_code) {
                    $table->text("meta_key_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }


            //Contact us info page
            $contact_page = "contact_page_$short_code";
            if (!Schema::hasTable($contact_page)) {
                Schema::create($contact_page, function (Blueprint $table) {
                    $table->increments('id');
                    $table->text('captcha')->nullable();
                    $table->text('title')->nullable();
                    $table->text('subtitle')->nullable();
                    $table->text('firstname')->nullable();
                    $table->text('name_contact')->nullable();
                    $table->text('lastname')->nullable();
                    $table->text('lastname_contact')->nullable();
                    $table->text('mail')->nullable();
                    $table->text('email_contact')->nullable();
                    $table->text('phone')->nullable();
                    $table->text('phone_contact')->nullable();
                    $table->text('message')->nullable();
                    $table->text('message_contact')->nullable();
                    $table->text('send_label')->nullable();
                    $table->text('address_label')->nullable();
                    $table->text('address')->nullable();
                    $table->text('help_label')->nullable();
                    $table->text('mail_right')->nullable();
                    $table->text('need_right')->nullable();
                    $table->text('need_help_right')->nullable();
                    $table->text('help_phone_right')->nullable();
                    $table->text('help_work_right')->nullable();
                    $table->text('social_media_title')->nullable();
                    $table->text('facebook')->nullable();
                    $table->text('youtube')->nullable();
                    $table->text('gplus')->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }



            //News


            if (!Schema::hasColumn('news', "title_$short_code")) {
                Schema::table('news', function($table) use ($short_code) {
                    $table->text("title_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            if (!Schema::hasColumn('news', "description_$short_code")) {
                Schema::table('news', function($table) use ($short_code) {
                    $table->longText("description_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }

            if (!Schema::hasColumn('news', "short_desc_$short_code")) {
                Schema::table('news', function($table) use ($short_code) {
                    $table->text("short_desc_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            if (!Schema::hasColumn('news', "meta_title_$short_code")) {
                Schema::table('news', function($table) use ($short_code) {
                    $table->text("meta_title_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            if (!Schema::hasColumn('news', "meta_desc_$short_code")) {
                Schema::table('news', function($table) use ($short_code) {
                    $table->text("meta_desc_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }

            if (!Schema::hasColumn('news', "meta_key_$short_code")) {
                Schema::table('news', function($table) use ($short_code) {
                    $table->text("meta_key_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            if (!Schema::hasColumn('news', "tag_$short_code")) {
                Schema::table('news', function($table) use ($short_code) {
                    $table->text("tag_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }

            //News Gallery info gallery_info
            if (!Schema::hasColumn('news_gallery_info', "title_$short_code")) {
                Schema::table('news_gallery_info', function($table) use ($short_code) {
                    $table->text("title_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            if (!Schema::hasColumn('news_gallery_info', "short_desc_$short_code")) {
                Schema::table('news_gallery_info', function($table) use ($short_code) {
                    $table->text("short_desc_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }


            //News Pdf
            $news_pdf_info = "news_pdf_$short_code";
            if (!Schema::hasColumn('news_pdf', "pdf_name_$short_code")) {
                Schema::table('news_pdf', function($table) use ($short_code) {
                    $table->text("pdf_name_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
              if (!Schema::hasColumn('news_pdf', "pdf_file_$short_code")) {
                Schema::table('news_pdf', function($table) use ($short_code) {
                    $table->text("pdf_file_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
              if (!Schema::hasColumn('news_pdf', "pdf_text_$short_code")) {
                Schema::table('news_pdf', function($table) use ($short_code) {
                    $table->text("pdf_text_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            //News sub page

            if (!Schema::hasColumn('news_sub_page', "title_$short_code")) {
                Schema::table('news_sub_page', function($table) use ($short_code) {
                    $table->text("title_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            if (!Schema::hasColumn('news_sub_page', "description_$short_code")) {
                Schema::table('news_sub_page', function($table) use ($short_code) {
                    $table->text("description_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }

            //News sub page pdf
            if (!Schema::hasColumn('news_sub_page_pdf', "title_$short_code")) {
                Schema::table('news_sub_page_pdf', function($table) use ($short_code) {
                    $table->text("title_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            if (!Schema::hasColumn('news_sub_page_pdf', "short_desc_$short_code")) {
                Schema::table('news_sub_page_pdf', function($table) use ($short_code) {
                    $table->text("short_desc_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            if (!Schema::hasColumn('news_sub_page_pdf', "file_$short_code")) {
                Schema::table('news_sub_page_pdf', function($table) use ($short_code) {
                    $table->text("file_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }



            //Page pdf
            if (!Schema::hasColumn('page_pdf', "pdf_name_$short_code")) {
                Schema::table('page_pdf', function($table) use ($short_code) {
                    $table->text("pdf_name_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            if (!Schema::hasColumn('page_pdf', "pdf_file_$short_code")) {
                Schema::table('page_pdf', function($table) use ($short_code) {
                    $table->text("pdf_file_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            if (!Schema::hasColumn('page_pdf', "pdf_text_$short_code")) {
                Schema::table('page_pdf', function($table) use ($short_code) {
                    $table->text("pdf_text_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }


            //Page pdf
            $partners = "partners_$short_code";

            if (!Schema::hasColumn('partners', "title_$short_code")) {
                Schema::table('page_pdf', function($table) use ($short_code) {
                    $table->text("title_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            if (!Schema::hasColumn('partners', "short_desc_$short_code")) {
                Schema::table('page_pdf', function($table) use ($short_code) {
                    $table->text("short_desc_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }



            //private_area_config


            if (!Schema::hasColumn('private_area_config', "title_$short_code")) {
                Schema::table('private_area_config', function($table) use ($short_code) {
                    $table->text("title_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            if (!Schema::hasColumn('private_area_config', "description_$short_code")) {
                Schema::table('private_area_config', function($table) use ($short_code) {
                    $table->text("description_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }



            //private_area_pdf
            $private_area_pdf = "private_area_pdf_$short_code";

            if (!Schema::hasColumn('private_area_pdf', "title_$short_code")) {
                Schema::table('private_area_pdf', function($table) use ($short_code) {
                    $table->text("title_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            if (!Schema::hasColumn('private_area_pdf', "short_desc_$short_code")) {
                Schema::table('private_area_pdf', function($table) use ($short_code) {
                    $table->text("short_desc_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            if (!Schema::hasColumn('private_area_pdf', "file_$short_code")) {
                Schema::table('private_area_pdf', function($table) use ($short_code) {
                    $table->text("file_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }



            //slider_items



            if (!Schema::hasColumn('slider_items', "image_$short_code")) {
                Schema::table('slider_items', function($table) use ($short_code) {
                    $table->text("image_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }

            if (!Schema::hasColumn('slider_items', "slide_title_$short_code")) {
                Schema::table('slider_items', function($table) use ($short_code) {
                    $table->text("slide_title_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }

            if (!Schema::hasColumn('slider_items', "slide_sub_title_$short_code")) {
                Schema::table('slider_items', function($table) use ($short_code) {
                    $table->text("slide_sub_title_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }


            //sub_pages_with_pdf
            $sub_pages_with_pdf = "sub_pages_with_pdf_$short_code";


            if (!Schema::hasColumn('sub_pages_with_pdf', "title_$short_code")) {
                Schema::table('sub_pages_with_pdf', function($table) use ($short_code) {
                    $table->text("title_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }

            if (!Schema::hasColumn('sub_pages_with_pdf', "description_$short_code")) {
                Schema::table('sub_pages_with_pdf', function($table) use ($short_code) {
                    $table->longText("description_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }


            if (!Schema::hasColumn('sub_pages_with_pdf', "meta_title_$short_code")) {
                Schema::table('sub_pages_with_pdf', function($table) use ($short_code) {
                    $table->text("meta_title_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            if (!Schema::hasColumn('sub_pages_with_pdf', "meta_key_$short_code")) {
                Schema::table('sub_pages_with_pdf', function($table) use ($short_code) {
                    $table->text("meta_key_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            if (!Schema::hasColumn('sub_pages_with_pdf', "meta_desc_$short_code")) {
                Schema::table('sub_pages_with_pdf', function($table) use ($short_code) {
                    $table->text("meta_desc_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }


            //sub_pages_with_pdf_files
            $sub_pages_with_pdf_files = "sub_pages_with_pdf_files_$short_code";


            if (!Schema::hasColumn('sub_pages_with_pdf_files', "title_$short_code")) {
                Schema::table('sub_pages_with_pdf_files', function($table) use ($short_code) {
                    $table->text("title_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            if (!Schema::hasColumn('sub_pages_with_pdf_files', "short_desc_$short_code")) {
                Schema::table('sub_pages_with_pdf_files', function($table) use ($short_code) {
                    $table->text("short_desc_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            if (!Schema::hasColumn('sub_pages_with_pdf_files', "file_$short_code")) {
                Schema::table('sub_pages_with_pdf_files', function($table) use ($short_code) {
                    $table->text("file_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }


            //news category
            $categories = "categories_$short_code";


            if (!Schema::hasColumn('categories', "title_$short_code")) {
                Schema::table('categories', function($table) use ($short_code) {
                    $table->text("title_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            if (!Schema::hasColumn('categories', "description_$short_code")) {
                Schema::table('categories', function($table) use ($short_code) {
                    $table->longText("description_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            if (!Schema::hasColumn('categories', "meta_title_$short_code")) {
                Schema::table('categories', function($table) use ($short_code) {
                    $table->longText("meta_title_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            if (!Schema::hasColumn('categories', "meta_desc_$short_code")) {
                Schema::table('categories', function($table) use ($short_code) {
                    $table->longText("meta_desc_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }
            if (!Schema::hasColumn('categories', "meta_key_$short_code")) {
                Schema::table('categories', function($table) use ($short_code) {
                    $table->longText("meta_key_$short_code")->nullable();
                });
            }else{
                $request->session()->flash('hasTable', 'Base table or view already exists!');
            }

            DB::table('languages')
                ->insert([
                    'title' => $title,
                    'short' => $short_code
                ]);
        }
        return redirect('admin/settings');
    }


    public function update(Request $request, $id){
        if (isset($request->save_settings)) {

            $curr_lang = DB::table('languages')->where('id', $id)->first();
            $old_short_code = $curr_lang->short;

            $old_sub_pages_with_pdf_files = "sub_pages_with_pdf_files_$old_short_code";
            $old_sub_pages_with_pdf = "sub_pages_with_pdf_$old_short_code";
            $old_slider_items = "slider_items_$old_short_code";
            $old_private_area_pdf = "private_area_pdf_$old_short_code";
            $old_private_area_config = "private_area_config_$old_short_code";
            $old_partners = "partners_$old_short_code";
            $old_page_pdf = "page_pdf_$old_short_code";
            $old_news_sub_page_pdf = "news_sub_page_pdf_$old_short_code";
            $old_news_sub_page = "news_sub_page_$old_short_code";
            $old_news_pdf_info = "news_pdf_$old_short_code";
            $old_news_gallery_info = "news_gallery_info_$old_short_code";
            $old_news_table = "news_$old_short_code";
            $old_contact_page = "contact_page_$old_short_code";
            $old_home_page = "home_page_$old_short_code";
            $old_gallery_info = "gallery_info_$old_short_code";
            $old_pages_table = "pages_$old_short_code";
            $old_categories = "categories_$old_short_code";


            $title = $request->lang_name;
            $short_code = $request->short_code;

            $sub_pages_with_pdf_files = "sub_pages_with_pdf_files_$short_code";
            $sub_pages_with_pdf = "sub_pages_with_pdf_$short_code";
            $slider_items = "slider_items_$short_code";
            $private_area_pdf = "private_area_pdf_$short_code";
            $private_area_config = "private_area_config_$short_code";
            $partners = "partners_$short_code";
            $page_pdf = "page_pdf_$short_code";
            $news_sub_page_pdf = "news_sub_page_pdf_$short_code";
            $news_sub_page = "news_sub_page_$short_code";
            $news_pdf_info = "news_pdf_$short_code";
            $news_gallery_info = "news_gallery_info_$short_code";
            $news_table = "news_$short_code";
            $contact_page = "contact_page_$short_code";
            $home_page = "home_page_$short_code";
            $gallery_info = "gallery_info_$short_code";
            $pages_table = "pages_$short_code";
            $categories = "categories_$short_code";
            if (Schema::hasTable($old_sub_pages_with_pdf_files)) {
                Schema::rename($old_sub_pages_with_pdf_files, $sub_pages_with_pdf_files);
            }
            if (Schema::hasTable($old_sub_pages_with_pdf)) {
                Schema::rename($old_sub_pages_with_pdf, $sub_pages_with_pdf);
            }
            if (Schema::hasTable($old_slider_items)) {
                Schema::rename($old_slider_items, $slider_items);
            }
            if (Schema::hasTable($old_private_area_pdf)) {
                Schema::rename($old_private_area_pdf, $private_area_pdf);
            }
            if (Schema::hasTable($old_private_area_config)) {
                Schema::rename($old_private_area_config, $private_area_config);
            }
            if (Schema::hasTable($old_partners)) {
                Schema::rename($old_partners, $partners);
            }
            if (Schema::hasTable($old_page_pdf)) {
                Schema::rename($old_page_pdf, $page_pdf);
            }
            if (Schema::hasTable($old_news_sub_page_pdf)) {
                Schema::rename($old_news_sub_page_pdf, $news_sub_page_pdf);
            }
            if (Schema::hasTable($old_news_sub_page)) {
                Schema::rename($old_news_sub_page, $news_sub_page);
            }
            if (Schema::hasTable($old_news_pdf_info)) {
                Schema::rename($old_news_pdf_info, $news_pdf_info);
            }
            if (Schema::hasTable($old_news_gallery_info)) {
                Schema::rename($old_news_gallery_info, $news_gallery_info);
            }
            if (Schema::hasTable($old_news_table)) {
                Schema::rename($old_news_table, $news_table);
            }
            if (Schema::hasTable($old_contact_page)) {
                Schema::rename($old_contact_page, $contact_page);
            }
            if (Schema::hasTable($old_home_page)) {
                Schema::rename($old_home_page, $home_page);
            }
            if (Schema::hasTable($old_gallery_info)) {
                Schema::rename($old_gallery_info, $gallery_info);
            }
            if (Schema::hasTable($old_pages_table)) {
                Schema::rename($old_pages_table, $pages_table);
            }
            if (Schema::hasTable($old_categories)) {
                Schema::rename($old_categories, $categories);
            }



            DB::table('languages')
                ->where("id", $id)
                ->update([
                    'title' => $title,
                    'short' => $short_code
                ]);

        }
        return redirect('admin/settings');
    }


    public function language_count()
    {
        $settings = DB::table('settings')->where('id', 1)->first();
        $count = 0;
        if ($settings->lang_am == 1) {
            $count++;
        }
        if ($settings->lang_ru == 1) {
            $count++;
        }
        if ($settings->lang_en == 1) {
            $count++;
        }
        return $count;
    }
    public function delete(Request $request, $id){



        $curr_lang = DB::table('languages')->where('id', $id)->first();
        $short_code = $curr_lang->short;
        if (Schema::hasColumn('pages', "title_$short_code")) {
            Schema::table('pages', function($table) use ($short_code) {
                $table->dropColumn("title_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('pages', "description_$short_code")) {

            Schema::table('pages', function($table) use ($short_code) {
                $table->dropColumn("description_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('pages', "short_desc_$short_code")) {
            Schema::table('pages', function($table) use ($short_code) {
                $table->dropColumn("short_desc_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('pages', "meta_title_$short_code")) {
            Schema::table('pages', function($table) use ($short_code) {
                $table->dropColumn("meta_title_$short_code");
            });

        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('pages', "meta_desc_$short_code")) {
            Schema::table('pages', function($table) use ($short_code) {
                $table->dropColumn("meta_desc_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('pages', "meta_key_$short_code")) {
            Schema::table('pages', function($table) use ($short_code) {
                $table->dropColumn("meta_key_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }




        //Gallery info gallery_info
        $gallery_info = "gallery_infos";


        if (Schema::hasColumn('gallery_infos', "title_$short_code")) {
            Schema::table('gallery_infos', function($table) use ($short_code) {
                $table->dropColumn("title_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('gallery_info', "short_desc_$short_code")) {
            Schema::table('gallery_info', function($table) use ($short_code) {
                $table->dropColumn("short_desc_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }



        //Home info
        if (Schema::hasColumn('home_page', "meta_title_$short_code")) {
            Schema::table('home_page', function($table) use ($short_code) {
                $table->dropColumn("meta_title_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('home_page', "meta_desc_$short_code")) {
            Schema::table('home_page', function($table) use ($short_code) {
                $table->dropColumn("meta_desc_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('home_page', "meta_key_$short_code")) {
            Schema::table('home_page', function($table) use ($short_code) {
                $table->dropColumn("meta_key_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }


        //Contact us info page
        $contact_page = "contact_page_$short_code";
        Schema::dropIfExists($contact_page);


        //News


        if (Schema::hasColumn('news', "title_$short_code")) {
            Schema::table('news', function($table) use ($short_code) {
                $table->dropColumn("title_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('news', "description_$short_code")) {
            Schema::table('news', function($table) use ($short_code) {
                $table->dropColumn("description_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }

        if (Schema::hasColumn('news', "short_desc_$short_code")) {
            Schema::table('news', function($table) use ($short_code) {
                $table->dropColumn("short_desc_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('news', "meta_title_$short_code")) {
            Schema::table('news', function($table) use ($short_code) {
                $table->dropColumn("meta_title_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('news', "meta_desc_$short_code")) {
            Schema::table('news', function($table) use ($short_code) {
                $table->dropColumn("meta_desc_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }

        if (Schema::hasColumn('news', "meta_key_$short_code")) {
            Schema::table('news', function($table) use ($short_code) {
                $table->dropColumn("meta_key_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('news', "tag_$short_code")) {
            Schema::table('news', function($table) use ($short_code) {
                $table->dropColumn("tag_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }

        //News Gallery info gallery_info
        if (Schema::hasColumn('news_gallery_info', "title_$short_code")) {
            Schema::table('news_gallery_info', function($table) use ($short_code) {
                $table->dropColumn("title_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('news_gallery_info', "short_desc_$short_code")) {
            Schema::table('news_gallery_info', function($table) use ($short_code) {
                $table->dropColumn("short_desc_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }


        //News Pdf
        $news_pdf_info = "news_pdf_$short_code";
        if (Schema::hasColumn('news_pdf', "pdf_name_$short_code")) {
            Schema::table('news_pdf', function($table) use ($short_code) {
                $table->text("pdf_name_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('news_pdf', "pdf_file_$short_code")) {
            Schema::table('news_pdf', function($table) use ($short_code) {
                $table->dropColumn("pdf_file_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('news_pdf', "pdf_text_$short_code")) {
            Schema::table('news_pdf', function($table) use ($short_code) {
                $table->dropColumn("pdf_text_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        //News sub page

        if (Schema::hasColumn('news_sub_page', "title_$short_code")) {
            Schema::table('news_sub_page', function($table) use ($short_code) {
                $table->dropColumn("title_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('news_sub_page', "description_$short_code")) {
            Schema::table('news_sub_page', function($table) use ($short_code) {
                $table->dropColumn("description_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }

        //News sub page pdf
        if (Schema::hasColumn('news_sub_page_pdf', "title_$short_code")) {
            Schema::table('news_sub_page_pdf', function($table) use ($short_code) {
                $table->dropColumn("title_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('news_sub_page_pdf', "short_desc_$short_code")) {
            Schema::table('news_sub_page_pdf', function($table) use ($short_code) {
                $table->dropColumn("short_desc_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('news_sub_page_pdf', "file_$short_code")) {
            Schema::table('news_sub_page_pdf', function($table) use ($short_code) {
                $table->dropColumn("file_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }



        //Page pdf
        if (Schema::hasColumn('page_pdf', "pdf_name_$short_code")) {
            Schema::table('page_pdf', function($table) use ($short_code) {
                $table->dropColumn("pdf_name_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('page_pdf', "pdf_file_$short_code")) {
            Schema::table('page_pdf', function($table) use ($short_code) {
                $table->dropColumn("pdf_file_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('page_pdf', "pdf_text_$short_code")) {
            Schema::table('page_pdf', function($table) use ($short_code) {
                $table->dropColumn("pdf_text_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }


        //Page pdf
        $partners = "partners_$short_code";

        if (Schema::hasColumn('partners', "title_$short_code")) {
            Schema::table('page_pdf', function($table) use ($short_code) {
                $table->dropColumn("title_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('partners', "short_desc_$short_code")) {
            Schema::table('page_pdf', function($table) use ($short_code) {
                $table->dropColumn("short_desc_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }



        //private_area_config


        if (Schema::hasColumn('private_area_config', "title_$short_code")) {
            Schema::table('private_area_config', function($table) use ($short_code) {
                $table->dropColumn("title_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('private_area_config', "description_$short_code")) {
            Schema::table('private_area_config', function($table) use ($short_code) {
                $table->dropColumn("description_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }



        //private_area_pdf
        $private_area_pdf = "private_area_pdf_$short_code";

        if (Schema::hasColumn('private_area_pdf', "title_$short_code")) {
            Schema::table('private_area_pdf', function($table) use ($short_code) {
                $table->dropColumn("title_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('private_area_pdf', "short_desc_$short_code")) {
            Schema::table('private_area_pdf', function($table) use ($short_code) {
                $table->dropColumn("short_desc_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('private_area_pdf', "file_$short_code")) {
            Schema::table('private_area_pdf', function($table) use ($short_code) {
                $table->dropColumn("file_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }



        //slider_items



        if (Schema::hasColumn('slider_items', "image_$short_code")) {
            Schema::table('slider_items', function($table) use ($short_code) {
                $table->dropColumn("image_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }

        if (Schema::hasColumn('slider_items', "slide_title_$short_code")) {
            Schema::table('slider_items', function($table) use ($short_code) {
                $table->dropColumn("slide_title_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }

        if (Schema::hasColumn('slider_items', "slide_sub_title_$short_code")) {
            Schema::table('slider_items', function($table) use ($short_code) {
                $table->dropColumn("slide_sub_title_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }


        //sub_pages_with_pdf
        $sub_pages_with_pdf = "sub_pages_with_pdf_$short_code";


        if (Schema::hasColumn('sub_pages_with_pdf', "title_$short_code")) {
            Schema::table('sub_pages_with_pdf', function($table) use ($short_code) {
                $table->dropColumn("title_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }

        if (Schema::hasColumn('sub_pages_with_pdf', "description_$short_code")) {
            Schema::table('sub_pages_with_pdf', function($table) use ($short_code) {
                $table->dropColumn("description_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }


        if (Schema::hasColumn('sub_pages_with_pdf', "meta_title_$short_code")) {
            Schema::table('sub_pages_with_pdf', function($table) use ($short_code) {
                $table->dropColumn("meta_title_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('sub_pages_with_pdf', "meta_key_$short_code")) {
            Schema::table('sub_pages_with_pdf', function($table) use ($short_code) {
                $table->dropColumn("meta_key_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('sub_pages_with_pdf', "meta_desc_$short_code")) {
            Schema::table('sub_pages_with_pdf', function($table) use ($short_code) {
                $table->dropColumn("meta_desc_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }


        //sub_pages_with_pdf_files


        if (Schema::hasColumn('sub_pages_with_pdf_files', "title_$short_code")) {
            Schema::table('sub_pages_with_pdf_files', function($table) use ($short_code) {
                $table->dropColumn("title_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('sub_pages_with_pdf_files', "short_desc_$short_code")) {
            Schema::table('sub_pages_with_pdf_files', function($table) use ($short_code) {
                $table->dropColumn("short_desc_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('sub_pages_with_pdf_files', "file_$short_code")) {
            Schema::table('sub_pages_with_pdf_files', function($table) use ($short_code) {
                $table->dropColumn("file_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }


        //news category
        $categories = "categories_$short_code";


        if (Schema::hasColumn('categories', "title_$short_code")) {
            Schema::table('categories', function($table) use ($short_code) {
                $table->dropColumn("title_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('categories', "description_$short_code")) {
            Schema::table('categories', function($table) use ($short_code) {
                $table->dropColumn("description_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('categories', "meta_title_$short_code")) {
            Schema::table('categories', function($table) use ($short_code) {
                $table->dropColumn("meta_title_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('categories', "meta_desc_$short_code")) {
            Schema::table('categories', function($table) use ($short_code) {
                $table->dropColumn("meta_desc_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }
        if (Schema::hasColumn('categories', "meta_key_$short_code")) {
            Schema::table('categories', function($table) use ($short_code) {
                $table->dropColumn("meta_key_$short_code");
            });
        }else{
            $request->session()->flash('hasTable', 'Base table or view already exists!');
        }

        DB::table('languages')->where('id', '=', $id)->delete();
        return redirect('admin/settings');
    }
}
