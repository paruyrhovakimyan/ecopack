<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class ProjectsController extends Controller
{



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $default_lang = config('app.locale');
        $projects = DB::table('projects')->select("created_at","id","hidden","project_order","title_$default_lang as title")->get();
        foreach ($projects as $project){
            $id = $project->id;
            $projectImage = DB::table('project_images')->where('project_id', '=',$id )->first();
            if(isset($projectImage->image)){
                $project->image = $projectImage->image;
            }else{
                $project->image = '';
            }
        }
        return view("admin.projects.all", compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $settings = DB::table('settings')->where('id', 1)->first();
        return view('admin.projects.create', compact('settings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(isset($request->add_project) && $request->add_project == "ok"){
            $title = $request->title;
            $short_desc = $request->short_desc;
            $description = $request->desc;
            if($request->meta_title == ''){
                $meta_title = $title;
            }else{

                $meta_title = $request->meta_title;
            }
            if($request->meta_desc == ''){
                $meta_desc = substr(strip_tags($description),0,255);
            }else{
                $meta_desc = strip_tags($request->meta_desc);
            }
            $meta_key = $request->meta_key;



            $title_ru = $request->title_ru;
            $short_desc_ru = $request->short_desc_ru;
            $description_ru = $request->desc_ru;
            if($request->meta_title_ru == ''){
                $meta_title_ru = $title_ru;
            }else{

                $meta_title_ru = $request->meta_title_ru;
            }
            if($request->meta_desc == ''){
                $meta_desc_ru = substr(strip_tags($description_ru),0,255);
            }else{
                $meta_desc_ru = strip_tags($request->meta_desc_ru);
            }
            $meta_key_ru = $request->meta_key_ru;


            $title_en = $request->title_en;
            $short_desc_en = $request->short_desc_en;
            $description_en = $request->desc_en;
            if($request->meta_title_en == ''){
                $meta_title_en = $title_en;
            }else{

                $meta_title_en = $request->meta_title_en;
            }
            if($request->meta_desc_en == ''){
                $meta_desc_en = substr(strip_tags($description_en),0,255);
            }else{
                $meta_desc_en = strip_tags($request->meta_desc_en);
            }
            $meta_key_en = $request->meta_key_en;



            $order = $request->order;

            $id = DB::table('projects')->insertGetId(
                [
                    'title_am' => $title,
                    'short_desc_am' => $short_desc,
                    'description_am' => $description,
                    'meta_title_am' => $meta_title,
                    'meta_key_am' => $meta_key,
                    'meta_desc_am' => $meta_desc,
                    'title_ru' => $title_ru,
                    'short_desc_ru' => $short_desc_ru,
                    'description_ru' => $description_ru,
                    'meta_title_ru' => $meta_title_ru,
                    'meta_key_ru' => $meta_key_ru,
                    'meta_desc_ru' => $meta_desc_ru,
                    'title_en' => $title_en,
                    'short_desc_en' => $short_desc_en,
                    'description_en' => $description_en,
                    'meta_title_en' => $meta_title_en,
                    'meta_key_en' => $meta_key_en,
                    'meta_desc_en' => $meta_desc_en,
                    'project_order' => $order,
                ]
            );


            if (isset($request->filename) && $request->filename != '') {
                foreach ($request->filename as $filename) {

                    DB::table('project_images')->insert(
                        [
                            'image' => $filename,
                            'project_id' => $id
                        ]
                    );
                }
            }

        }
        return redirect('admin/projects');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dropzoneDefaultHidden = '';
        $project = DB::table('projects')->where('id', $id)->first();
        $projectImages = DB::table('project_images')->where('project_id', $id)->get();
        if (count($projectImages) != 0) {
            $dropzoneDefaultHidden = 'dz-default-hidden';
        }
        $settings = DB::table('settings')->where('id', 1)->first();
        return view('admin.projects.edit', compact('project', 'projectImages','dropzoneDefaultHidden','settings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(isset($request->edit_project) && $request->edit_project == "ok"){
            $title = $request->title;
            $short_desc = $request->short_desc;
            $description = $request->desc;
            if($request->meta_title == ''){
                $meta_title = $title;
            }else{

                $meta_title = $request->meta_title;
            }
            if($request->meta_desc == ''){
                $meta_desc = substr(strip_tags($description),0,255);
            }else{
                $meta_desc = strip_tags($request->meta_desc);
            }
            $meta_key = $request->meta_key;


            $title_ru = $request->title_ru;
            $short_desc_ru = $request->short_desc_ru;
            $description_ru = $request->desc_ru;
            if($request->meta_title_ru == ''){
                $meta_title_ru = $title_ru;
            }else{

                $meta_title_ru = $request->meta_title_ru;
            }
            if($request->meta_desc == ''){
                $meta_desc_ru = substr(strip_tags($description_ru),0,255);
            }else{
                $meta_desc_ru = strip_tags($request->meta_desc_ru);
            }
            $meta_key_ru = $request->meta_key_ru;


            $title_en = $request->title_en;
            $short_desc_en = $request->short_desc_en;
            $description_en = $request->desc_en;
            if($request->meta_title_en == ''){
                $meta_title_en = $title_en;
            }else{

                $meta_title_en = $request->meta_title_en;
            }
            if($request->meta_desc_en == ''){
                $meta_desc_en = substr(strip_tags($description_en),0,255);
            }else{
                $meta_desc_en = strip_tags($request->meta_desc_en);
            }
            $meta_key_en = $request->meta_key_en;


            $order = $request->order;

            DB::table('projects')
                ->where('id', $id)
                ->update(
                [
                    'title_am' => $title,
                    'short_desc_am' => $short_desc,
                    'description_am' => $description,
                    'meta_title_am' => $meta_title,
                    'meta_key_am' => $meta_key,
                    'meta_desc_am' => $meta_desc,
                    'title_ru' => $title_ru,
                    'short_desc_ru' => $short_desc_ru,
                    'description_ru' => $description_ru,
                    'meta_title_ru' => $meta_title_ru,
                    'meta_key_ru' => $meta_key_ru,
                    'meta_desc_ru' => $meta_desc_ru,
                    'title_en' => $title_en,
                    'short_desc_en' => $short_desc_en,
                    'description_en' => $description_en,
                    'meta_title_en' => $meta_title_en,
                    'meta_key_en' => $meta_key_en,
                    'meta_desc_en' => $meta_desc_en,
                    'project_order' => $order,
                ]
            );

            DB::table('project_images')->where('project_id', '=', $id)->delete();
            if (isset($request->filename) && $request->filename != '') {
                foreach ($request->filename as $filename) {

                    DB::table('project_images')->insert(
                        [
                            'image' => $filename,
                            'project_id' => $id
                        ]
                    );
                }
            }

        }
        return redirect('admin/projects/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('project_images')->where('project_id', '=', $id)->delete();
        DB::table('projects')->where('id', '=', $id)->delete();
    }

    public function upload(Request $request)
    {
        $imageName = substr(md5(microtime()),rand(0,26),10);
        $image = $request->file('file');
        $generatedString = time();
        $extension = $request->file("file")->getClientOriginalExtension();
        $newFile = "uploads/projects/" .$imageName.'_'. $generatedString . "." . $extension;
        $newFileThumb = "uploads/projects/thumbs/" .$imageName.'_'. $generatedString . "." . $extension;
        $newFileThumb_150 = "uploads/projects/150/" .$imageName.'_'. $generatedString . "." . $extension;
        $filepath = $imageName.'_'.$generatedString . "." . $extension;

        Image::make($image->getRealPath())->resize(null, 546, function ($constraint) {
            $constraint->aspectRatio();
        })->orientate()->save($newFile);
        Image::make($image->getRealPath())->resize(180, 94)->orientate()->save($newFileThumb);
        Image::make($image->getRealPath())->resize(null, 150, function ($constraint) {
            $constraint->aspectRatio();
        })->orientate()->save($newFileThumb_150);
        echo $filepath;
    }

    public function changeHidden( Request $request){
        if(isset($_REQUEST)){
            $id = $request->id;
            $hidden = $request->update_hidden;
            DB::table('projects')
                ->where('id', $id)
                ->update(['hidden' => $hidden]);
            if($hidden == 1){
                echo "0";
            }else{
                echo "1";
            }
        }


    }
}
