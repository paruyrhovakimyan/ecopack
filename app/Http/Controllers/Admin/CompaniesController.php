<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = DB::table('settings')->where('id', 1)->first();
        $companies = DB::table('companies')->get();
        return view("admin.companies.all", compact('companies','settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(isset($request->add_company) && $request->add_company == "ok"){

            if ($request->file("image")) {
                $image = $request->file("image");
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();
                $newFile = "uploads/companies/" . $generatedString . "." . $extension;
                $newFileThumb = "uploads/companies/thumbs/" . $generatedString . "." . $extension;
                $newFileThumb_150 = "uploads/companies/150/" . $generatedString . "." . $extension;
                $filepath = $generatedString . "." . $extension;

                Image::make($image->getRealPath())->resize(null, 546, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFile);
                Image::make($image->getRealPath())->resize(180, 94)->orientate()->save($newFileThumb);
                Image::make($image->getRealPath())->resize(null, 150, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb_150);
            } else {
                $filepath = "";
            }

            if ($request->file("image_bg")) {
                $image_bg = $request->file("image_bg");
                $generatedString = time();
                $extension = $request->file("image_bg")->getClientOriginalExtension();
                $newFile = "uploads/companies/background/" . $generatedString . "." . $extension;
                $filepathBg = $generatedString . "." . $extension;

                Image::make($image_bg->getRealPath())->save($newFile);

            }else{
                $filepathBg ='';
            }

            if ($request->file("image_logo")) {
                $image_bg = $request->file("image_logo");
                $generatedString = time();
                $extension = $request->file("image_logo")->getClientOriginalExtension();
                $newFile = "uploads/companies/logo/" . $generatedString . "." . $extension;
                $filepathLogo = $generatedString . "." . $extension;

                Image::make($image_bg->getRealPath())->resize(null, 250, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFile);
            }else{
                $filepathLogo = '';
            }



            $title = $request->title;
            $subtitle = $request->subtitle;
            $img_position = $request->img_position;

            $description = $request->desc;
            if($request->meta_title == ''){
                $meta_title = $title;
            }else{

                $meta_title = $request->meta_title;
            }
            if($request->meta_desc == ''){
                $meta_desc = substr(strip_tags($description),0,255);
            }else{
                $meta_desc = strip_tags($request->meta_desc);
            }
            $meta_key = $request->meta_key;
            $order = $request->order;

            $video = $request->video;
            $fb_link = $request->fb_link;
            $tw_link = $request->tw_link;
            $in_link = $request->in_link;
            $gl_link = $request->gl_link;
            $back_color = $request->back_color;
            $id = DB::table('companies')->insertGetId(
                [
                    'title' => $title,
                    'subtitle' => $subtitle,
                    'description' => $description,
                    'meta_title' => $meta_title,
                    'meta_key' => $meta_key,
                    'meta_desc' => $meta_desc,
                    'company_order' => $order,
                    'image_position'=> $img_position,
                    'image'=> $filepath,
                    'logo'=> $filepathLogo,
                    'bacground_image'=> $filepathBg,
                    'bg_color'=> $back_color,
                    'video'=> $video,
                    'fb_link'=> $fb_link,
                    'tw_link'=> $tw_link,
                    'in_link'=> $in_link,
                    'gl_link'=> $gl_link
                ]
            );


            if (isset($request->filename) && $request->filename != '') {
                foreach ($request->filename as $filename) {

                    DB::table('gallery')->insert(
                        [
                            'image' => $filename,
                            'company_id' => $id
                        ]
                    );
                }
            }

        }
        return redirect('admin/companies');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $settings = DB::table('settings')->where('id', 1)->first();
        $dropzoneDefaultHidden = '';
        $company = DB::table('companies')->where('id', $id)->first();
        $companyImages = DB::table('gallery')->where('company_id', $id)->get();
        if (count($companyImages) != 0) {
            $dropzoneDefaultHidden = 'dz-default-hidden';
        }
        return view('admin.companies.edit', compact('company','settings', 'companyImages','dropzoneDefaultHidden'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if(isset($request->edit_company) && $request->edit_company='ok'){
            $title = $request->title;
            $subtitle = $request->subtitle;
            $img_position = $request->img_position;
            $back_color = $request->back_color;

            $description = $request->desc;
            if($request->meta_title == ''){
                $meta_title = $title;
            }else{

                $meta_title = $request->meta_title;
            }
            if($request->meta_desc == ''){
                $meta_desc = substr(strip_tags($description),0,255);
            }else{
                $meta_desc = strip_tags($request->meta_desc);
            }
            $meta_key = $request->meta_key;
            $order = $request->order;


            if ($request->file("image")) {
                    $image = $request->file("image");
                    $generatedString = time();
                    $extension = $request->file("image")->getClientOriginalExtension();
                    $newFile = "uploads/companies/" . $generatedString . "." . $extension;
                    $newFileThumb = "uploads/companies/thumbs/" . $generatedString . "." . $extension;
                    $newFileThumb_150 = "uploads/companies/150/" . $generatedString . "." . $extension;
                    $filepath = $generatedString . "." . $extension;

                    Image::make($image->getRealPath())->resize(null, 546, function ($constraint) {
                        $constraint->aspectRatio();
                    })->orientate()->save($newFile);
                    Image::make($image->getRealPath())->resize(180, 94)->orientate()->save($newFileThumb);
                    Image::make($image->getRealPath())->resize(null, 150, function ($constraint) {
                        $constraint->aspectRatio();
                    })->orientate()->save($newFileThumb_150);

                }else{
                $filepath = $request->old_image;
            }


            if ($request->file("image_bg")) {
                $image_bg = $request->file("image_bg");
                $generatedString = time();
                $extension = $request->file("image_bg")->getClientOriginalExtension();
                $newFile = "uploads/companies/background/" . $generatedString . "." . $extension;
                $filepathBg = $generatedString . "." . $extension;

                Image::make($image_bg->getRealPath())->save($newFile);

            }else{
                $filepathBg = $request->old_image_bg;
            }

            if ($request->file("image_logo")) {
                $image_bg = $request->file("image_logo");
                $generatedString = time();
                $extension = $request->file("image_logo")->getClientOriginalExtension();
                $newFile = "uploads/companies/logo/" . $generatedString . "." . $extension;
                $filepathLogo = $generatedString . "." . $extension;

                Image::make($image_bg->getRealPath())->resize(null, 250, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFile);
            }else{
                $filepathLogo = $request->old_image_logo;
            }

            $video = $request->video;
            $fb_link = $request->fb_link;
            $tw_link = $request->tw_link;
            $in_link = $request->in_link;
            $gl_link = $request->gl_link;
            DB::table('companies')
                ->where('id', $id)
                ->update(
                    [
                        'title' => $title,
                        'subtitle' => $subtitle,
                        'description' => $description,
                        'meta_title' => $meta_title,
                        'meta_key' => $meta_key,
                        'meta_desc' => $meta_desc,
                        'company_order' => $order,
                        'image_position'=> $img_position,
                        'image'=> $filepath,
                        'logo'=> $filepathLogo,
                        'bacground_image'=> $filepathBg,
                        'bg_color'=> $back_color,
                        'video'=> $video,
                        'fb_link'=> $fb_link,
                        'tw_link'=> $tw_link,
                        'in_link'=> $in_link,
                        'gl_link'=> $gl_link
                    ]
                );









            DB::table('gallery')->where('company_id', '=', $id)->delete();
            if (isset($request->filename) && $request->filename != '') {
                foreach ($request->filename as $filename) {

                    DB::table('gallery')->insert(
                        [
                            'image' => $filename,
                            'company_id' => $id
                        ]
                    );
                }
            }
        }
        return redirect('admin/companies/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('gallery')->where('company_id', '=', $id)->delete();
        DB::table('companies')->where('id', '=', $id)->delete();
    }


    public function upload(Request $request)
    {
        $imageName = substr(md5(microtime()),rand(0,26),10);
        $image = $request->file('file');
        $generatedString = time();
        $extension = $request->file("file")->getClientOriginalExtension();
        $newFile = "uploads/gallery/" .$imageName.'_'. $generatedString . "." . $extension;
        $newFileThumb = "uploads/gallery/thumbs/" .$imageName.'_'. $generatedString . "." . $extension;
        $newFileThumb_150 = "uploads/gallery/150/" .$imageName.'_'. $generatedString . "." . $extension;
        $filepath = $imageName.'_'.$generatedString . "." . $extension;

        Image::make($image->getRealPath())->resize(null, 546, function ($constraint) {
            $constraint->aspectRatio();
        })->orientate()->save($newFile);
        Image::make($image->getRealPath())->resize(180, 94)->orientate()->save($newFileThumb);
        Image::make($image->getRealPath())->resize(null, 150, function ($constraint) {
            $constraint->aspectRatio();
        })->orientate()->save($newFileThumb_150);
        echo $filepath;
    }

    public function changeHidden( Request $request){
        if(isset($_REQUEST)){
            $id = $request->id;
            $hidden = $request->update_hidden;
            DB::table('companies')
                ->where('id', $id)
                ->update(['hidden' => $hidden]);
            if($hidden == 1){
                echo "0";
            }else{
                echo "1";
            }
        }


    }
}
