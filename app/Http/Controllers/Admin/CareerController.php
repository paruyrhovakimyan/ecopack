<?php

namespace App\Http\Controllers\Admin;

use App\Career;
use App\CareerOrder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class CareerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $partners = DB::table('careers')->select("id","hidden","title_$default_lang as title")->get();
        return view("admin.career.all", compact('partners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $settings = DB::table('settings')->where('id', 1)->first();
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();

        return view('admin.career.create',compact('languages','default_lang','settings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(isset($request->add_partner) && $request->add_partner == "ok"){


            $partner = new Career();


            foreach ($request->all() as $key => $val) {
                if ($key != '_token' && $key != 'add_partner') {
                    $partner->$key = $val;
                }
            }
            $partner->save();
        }
        return redirect('admin/career');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $career = Career::find($id);
        $careerorders = CareerOrder::where("career_id", '=', $id)->get();
        return view('admin.career.show', compact('careerorders','career','default_lang'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();


        $partner = DB::table('careers')->where('id', $id)->first();
        $settings = DB::table('settings')->where('id', 1)->first();
        return view('admin.career.edit', compact('default_lang','languages','partner','settings'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(isset($request->edit_partner) && $request->edit_partner =="ok"){
            $career = Career::find($id);


            foreach ($request->all() as $key => $val) {
                if ($key != '_token'  &&  $key!='_method' && $key != 'edit_partner') {
                    $career->$key = $val;
                }
            }
            $career->save();

        }
        return redirect('admin/career');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('careers')->where('id', '=', $id)->delete();
    }
}
