<?php
/**
 * Created by PhpStorm.
 * User: Ной
 * Date: 07.02.2019
 * Time: 14:35
 */

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ProductCat;
use App\Brand;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;

        $brands = Brand::all();
        return view("admin.brand.all", compact('brands','default_lang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();
        return view('admin.brand.create', compact('default_lang','languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($request->add_category) && $request->add_category == "ok"){

            $brand = new Brand();

            if ($request->file("image")) {

                $image = Input::file('image');
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();
                $newFile = public_path('uploads/brand/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->orientate()->save($newFile);
                $filepath = $generatedString . "." . $extension;


            } else {
                $filepath = "";
            }

            foreach ($request->all() as $key => $val) {
                if ($key != 'image' && $key != '_token' && $key != 'add_category') {
                    $brand->$key = $val;
                }
            }
            $brand->image = $filepath;
            $brand->save();

            return redirect('admin/prcats');


        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $brand = Brand::find($id);

        $languages = DB::table('languages')->where('hidden', '=', 0)->get();
        return view('admin.brand.edit', compact('default_lang','languages','brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (isset($request->edit_category) && $request->edit_category == "ok"){
            $brand = Brand::find($id);
            if ($request->file("image")) {

                $image = Input::file('image');
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();
                $newFile = public_path('uploads/brand/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->orientate()->save($newFile);
                $filepath = $generatedString . "." . $extension;


            } else {
                $filepath = $request->old_image;
            }

            foreach ($request->all() as $key => $val) {
                if ($key != 'old_image' && $key != 'image' && $key != '_method' && $key != '_token' && $key != 'edit_category') {
                    $brand->$key = $val;
                }
            }
            $brand->image = $filepath;
            $brand->save();

            return redirect('admin/brands/');


        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('brands')->where('id', '=', $id)->delete();
    }
}
