<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ProductCat;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class ProductCatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;

        $categories = ProductCat::where("parent_cat", '=', 0)->get();
        foreach ($categories as $category){
            $category->childs = ProductCat::where("parent_cat", '=', $category->id)->get();
        }
        return view("admin.prcat.all", compact('categories','default_lang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();
        $categories = ProductCat::where("parent_cat", '=', 0)->get();
        foreach ($categories as $category){
            $category->childs = ProductCat::where("parent_cat", '=', $category->id)->get();
        }
        return view('admin.prcat.create', compact('categories','default_lang','languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($request->add_category) && $request->add_category == "ok"){

            $category = new ProductCat();

            if ($request->file("image")) {

                $image = Input::file('image');
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();
                $newFile = public_path('uploads/prcat/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->orientate()->save($newFile);
                $filepath = $generatedString . "." . $extension;


            } else {
                $filepath = "";
            }

            foreach ($request->all() as $key => $val) {
                if ($key != 'image' && $key != '_token' && $key != 'add_category') {
                    $category->$key = $val;
                }
            }
            $category->pr_cat_image = $filepath;
            $category->save();

            return redirect('admin/prcats');


        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $category = ProductCat::find($id);
        $categories = ProductCat::where("parent_cat", '=', 0)->get();
        foreach ($categories as $category_all){
            $category_all->childs = ProductCat::where("parent_cat", '=', $category_all->id)->get();
        }
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();
        return view('admin.prcat.edit', compact('categories','default_lang','languages','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (isset($request->edit_category) && $request->edit_category == "ok"){
            $category = ProductCat::find($id);
            if ($request->file("image")) {

                $image = Input::file('image');
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();
                $newFile = public_path('uploads/prcat/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->orientate()->save($newFile);
                $filepath = $generatedString . "." . $extension;


            } else {
                $filepath = $request->old_image;
            }

            foreach ($request->all() as $key => $val) {
                if ($key != 'old_image' && $key != 'image' && $key != '_method' && $key != '_token' && $key != 'edit_category') {
                    $category->$key = $val;
                }
            }
            $category->pr_cat_image = $filepath;
            $category->save();

            return redirect('admin/prcats/');


        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('product_cats')->where('id', '=', $id)->delete();
    }
}
