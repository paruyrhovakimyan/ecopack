<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AdminUsersController extends Controller
{
    public function index(){
        $users = DB::table('admins')->get();
        return view("admin.admin_users.all", compact('users'));
    }


    public function create(){
        return view("admin.admin_users.create");
    }
    public function store(Request $request){

        if(isset($request->add_admin) && $request->add_admin == "ok"){
            $name = $request->title;
            $login = $request->login;
            $password = $request->password;
            $role = $request->role;

            DB::table('admins')->insert(
                [
                    'admin_name' => $name,
                    'username' => $login,
                    'password' => $password,
                    'role' => $role

                ]
            );
        }
        return redirect("/admin/admin_users");
    }
    public function update(Request $request, $id){

        if(isset($request->edit_admin) && $request->edit_admin == "ok"){
            $name = $request->title;
            $login = $request->login;
            $password = $request->password;
            $role = $request->role;

            DB::table('admins')
                ->where('id', $id)->update(
                [
                    'admin_name' => $name,
                    'username' => $login,
                    'password' => $password,
                    'role' => $role

                ]
            );
        }
        return redirect("/admin/admin_users");
    }

    public function edit($id){
        $user = DB::table('admins')->where('id', $id)->first();
        return view("admin.admin_users.edit", compact('user'));
    }


    public function destroy($id){
        DB::table('users')->where('id', '=', $id)->delete();
    }
}
