<?php

namespace App\Http\Controllers\Admin;

use App\NewsGalleryInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class NewsGalleryController extends Controller
{

    public function add_gallery($parent_id){
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $page_bredcrump = '';
        $page_child = DB::table('news')->select("title_$default_lang as title","parent_id")->where('id', $parent_id)->first();
        $page_bredcrump = "<h4>";
        if($page_child->parent_id != 0){
            $page_parent = DB::table('pages')->select("title_$default_lang as title")->where('id', $page_child->parent_id)->first();
            $page_bredcrump .= '<a href="'.url('/').'/admin/pages/'.$page_child->parent_id.'">'.$page_parent->title.'</a> | ';
        }
        $page_bredcrump .= $page_child->title."</h4>";
        $languages = DB::table('languages')->where('hidden','=', 0)->get();

        return view("admin.news_gallery.create", compact('languages','page_bredcrump','parent_id'));
    }


    public function storeGallery(Request $request,$parent_id){




        if(isset($request->add_gallery) && $request->add_gallery == "ok"){
            $galleryInfo = new NewsGalleryInfo();
            $galleryInfo->parent_id = $parent_id;
            foreach($request->all() as $key => $val){
                if($key !='_token' && $key !='add_gallery' && $key !='filename'){
                    $galleryInfo->$key = $val;
                }
            }
            $galleryInfo->save();
            if (isset($request->filename) && $request->filename != '') {
                foreach ($request->filename as $filename) {

                    DB::table('news_gallery')->insert(
                        [
                            'image' => $filename,
                            'parent_id' => $galleryInfo->id
                        ]
                    );
                }
            }
        }


        $parent_page = DB::table('news')
            ->select(
                "parent_id"
            )->where('id', $parent_id)->first();
        if($parent_page->parent_id !=0){
            $super_parent = $parent_page->parent_id;
        }else{
            $super_parent =$parent_id;
        }

        return redirect('admin/pages/'.$super_parent);

    }


    public function edit_gallery($id){

        $gallery = DB::table('news_gallery_infos')->where('id',$id)->first();

        $gallery_parent_page = $gallery->parent_id;

        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;


        $page_child = DB::table('news')->select("title_$default_lang as title","parent_id")->where('id', $gallery_parent_page)->first();
        $page_bredcrump = "<h4>";
        if($page_child->parent_id != 0){
            $page_parent = DB::table('pages')->select("title_$default_lang as title")->where('id', $page_child->parent_id)->first();
            $page_bredcrump .= '<a href="'.url('/').'/admin/pages/'.$page_child->parent_id.'">'.$page_parent->title.'</a> | ';
        }
        $page_bredcrump .= $page_child->title."</h4>";

        $projectImages = DB::table('news_gallery')->where('parent_id',$id)->get();
        $dropzoneDefaultHidden = '';
        if (count($projectImages) != 0) {
            $dropzoneDefaultHidden = 'dz-default-hidden';
        }
        $languages = DB::table('languages')->where('hidden','=', 0)->get();


        return view("admin.news_gallery.edit", compact('languages','gallery','projectImages','dropzoneDefaultHidden','default_lang','page_bredcrump'));
    }


    public function updateGallery(Request $request,$id){

        if(isset($request->edit_gallery) && $request->edit_gallery == "ok"){
            $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
            $default_lang = $first_lang->short;

            $page_gallery = NewsGalleryInfo::find($id);
            foreach($request->all() as $key => $val){
                if($key !='_token' && $key !='edit_gallery' && $key !='filename'){
                    $page_gallery->$key = $val;
                }
            }
            $page_gallery->save();

            DB::table('news_gallery')->where('parent_id', '=', $id)->delete();
            if (isset($request->filename) && $request->filename != '') {
                foreach ($request->filename as $filename) {

                    DB::table('news_gallery')->insert(
                        [
                            'image' => $filename,
                            'parent_id' => $id
                        ]
                    );
                }
            }
        }


        $gallery_parent = DB::table('news_gallery_infos')->select("parent_id")->where('id', $id)->first();
        $page_parent = DB::table('news')->select("parent_id")->where('id', $gallery_parent->parent_id)->first();

        return redirect('admin/pages/'.$page_parent->parent_id);
    }

    public function upload(Request $request)
    {
        $imageName = substr(md5(microtime()),rand(0,26),10);
        $image = Input::file('file');
        $generatedString = time();
        $extension = $request->file("file")->getClientOriginalExtension();
        $newFile = public_path('uploads/gallery/' . $imageName.'_'. $generatedString . "." . $extension);
        $newFileThumb = public_path('uploads/gallery/thumbs/' . $imageName.'_'. $generatedString . "." . $extension);
        $newFileThumb_150 = public_path('uploads/gallery/150/' . $imageName.'_'. $generatedString . "." . $extension);
        $filepath = $imageName.'_'.$generatedString . "." . $extension;

        Image::make($image->getRealPath())->resize(null, 500, function ($constraint) {
            $constraint->aspectRatio();
        })->orientate()->save($newFile);
        Image::make($image->getRealPath())->resize(180, 94)->orientate()->save($newFileThumb);
        Image::make($image->getRealPath())->resize(null, 200, function ($constraint) {
            $constraint->aspectRatio();
        })->orientate()->save($newFileThumb_150);
        echo $filepath;
    }

    public function gallery_delete(Request $request){
        $back_url = app('Illuminate\Routing\UrlGenerator')->previous();
        $id = $request->id;
        $images_for_delete = DB::table('news_gallery')->where('parent_id', '=', $id)->get();

        foreach ($images_for_delete as $image_for_delete){
            $newFile = "uploads/gallery/" .$image_for_delete->image;
            $newFileThumb = "uploads/gallery/thumbs/" .$image_for_delete->image;
            $newFileThumb_150 = "uploads/gallery/150/".$image_for_delete->image;
            if(file_exists($newFile)){
                unlink($newFile);
            }
            if(file_exists($newFile)){
                unlink($newFileThumb);
            }
            if(file_exists($newFile)){
                unlink($newFileThumb_150);
            }
            

        }

        DB::table('news_gallery')->where('parent_id', '=', $id)->delete();
        DB::table('news_gallery_infos')->where('id', '=', $id)->delete();
        return redirect($back_url);
    }
}
