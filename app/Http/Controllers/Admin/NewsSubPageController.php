<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\UrlGenerator;

class NewsSubPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($parent_id)
    {
        $default_lang = config('app.locale');
        $pages = DB::table('news_pdf')->select("pdf_name_$default_lang as title", "id")->where('news_id',$parent_id)->get();
        return view("admin.page_pdf.all", compact('pages','default_lang','parent_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($parent_id)
    {
        $default_lang = config('app.locale');
        $page_child = DB::table('news')->select("title_$default_lang as title","parent_id")->where('id', $parent_id)->first();
        $page_bredcrump = "<h4>";
        if($page_child->parent_id != 0){
            $page_parent = DB::table('pages')->select("title_$default_lang as title")->where('id', $page_child->parent_id)->first();
            $page_bredcrump .= '<a href="'.url('/').'/admin/pages/'.$page_child->parent_id.'">'.$page_parent->title.'</a> | ';
        }
        $page_bredcrump .= $page_child->title."</h4>";
        $settings = DB::table('settings')->where('id', 1)->first();
        return view('admin.news_sub_page_pdf.create_sub',compact('settings','parent_id','page_bredcrump'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($parent_id,Request $request)
    {
        if(isset($request->add_news_sub_page) && $request->add_news_sub_page == "ok"){
            $title = $request->title;
            $description = $request->description;

            $news_sub_page = DB::table('news_sub_page')->insertGetId(
                [
                    'news_id' => $parent_id,
                    'title' => $title,
                    'description' => $description
                ]
            );


            $sub_pdfs_ids = $request->sub_pdfs_ids;
            if (isset($sub_pdfs_ids) && !empty($sub_pdfs_ids)) {
                foreach ($sub_pdfs_ids as $sub_pdfs_id) {

                    DB::table('news_sub_page_pdf')
                        ->where('id', $sub_pdfs_id)
                        ->update(
                        [
                            'sub_page_id' => $news_sub_page,
                        ]
                    );
                }
            }


            $parent_page = DB::table('news')
                ->select(
                    "parent_id"
                )->where('id', $parent_id)->first();
            if($parent_page->parent_id !=0){
                $super_parent = $parent_page->parent_id;
            }else{
                $super_parent =$parent_id;
            }

            return redirect('admin/pages/'.$super_parent);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($parent_id, $id)
    {
        $default_lang = config('app.locale');
        $page_child = DB::table('news')->select("title_$default_lang as title","parent_id")->where('id', $parent_id)->first();
        $page_bredcrump = "<h4>";
        if($page_child->parent_id != 0){
            $page_parent = DB::table('pages')->select("title_$default_lang as title")->where('id', $page_child->parent_id)->first();
            $page_bredcrump .= '<a href="'.url('/').'/admin/pages/'.$page_child->parent_id.'">'.$page_parent->title.'</a> | ';
        }
        $page_bredcrump .= $page_child->title."</h4>";
        $settings = DB::table('settings')->where('id', 1)->first();

        $news_sub_page = DB::table('news_sub_page')->select("title","id","description")->where("id", $id)->where('news_id',$parent_id)->first();

        $news_sub_page_pdfs = DB::table('news_sub_page_pdf')->where("sub_page_id", $id)->get();

        return view('admin.news_sub_page_pdf.edit_sub',compact('news_sub_page_pdfs','settings','parent_id','page_bredcrump','news_sub_page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $parent_id, $id)
    {
        if(isset($request->edit_news_sub_page) && $request->edit_news_sub_page == "ok"){
            $title = $request->title;
            $description = $request->description;

            DB::table('news_sub_page')
                ->where('id', $id)
                ->update(
                    [
                        'title' => $title,
                        'description' => $description
                    ]
                );

        }
        $parent_page = DB::table('news')
            ->select(
                "parent_id"
            )->where('id', $parent_id)->first();
        if($parent_page->parent_id !=0){
            $super_parent = $parent_page->parent_id;
        }else{
            $super_parent =$parent_id;
        }

        return redirect('admin/pages/'.$super_parent);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete_sub_pdf(Request $request){

        $id = $request->id;
        DB::table('news_sub_page_pdf')->where('id', $id)->delete();
        return "ok";

    }
    public function news_sub_page_pdf_delete(Request $request){

        $id = $request->id;

        $images_for_delete = DB::table('news_sub_page_pdf')->where('sub_page_id', $id)->get();

        foreach ($images_for_delete as $image_for_delete){
            $newFile = "uploads/news/pdfs/" .$image_for_delete->file;
            unlink($newFile);
        }
        DB::table('news_sub_page')->where('id', $id)->delete();
        return "ok";

    }

    public function edit_sub_pdf_block(Request $request){
        $id = $request->id;
        $pdf_result = DB::table('news_sub_page_pdf')->where('id', $id)->first();
        $result = '
        <div class="sub_pdf_append_block show_block">
            <div id="sub_pdf_append_head" class="sub_pdf_append_head">
                <div class="sub_pdf_append_title">Upload pdf</div>
                <div class="sub_pdf_append_dragh"></div>
                <button type="button" class="sub_pdf_append_close_edit" aria-hidden="true"><i class="mce-ico mce-i-remove"></i></button>
            </div>
            <div class="form-group ">
                <label for="title">Title</label>
                <input id="about_title" type="text" class="append_file_title_edit form-control" name="pdf_title" value="'.$pdf_result->title.'"
                       autofocus>
            </div>
            <div class="form-group">
                <label for="Description">Short Description</label>
                <textarea class="form-control append_file_desc_edit short_desc" name="pdf_desc">'.$pdf_result->short_desc.'</textarea>
            </div>
            <div class="sub_pdf_append_box form-group">
                <div class="">
                    <label>Upload PDF</label>
                </div>
                <div class="pdf_file_title">
                    <input type="text" name="old_pdf_title" readonly="" class="append_pdf_change old_pdf_title" value="'.$pdf_result->file.'">
                </div>
                <input type="hidden" class="current_pdf_file_name" name="current_pdf_file_name" value="'.$pdf_result->file.'" >
                <div class="file-upload btn btn_1 green" style="float:left;">
                    <span>Choose file</span>
                    <input type="file" name="pdf_file" id="editMyFile" class="image upload news_sub_pdf_file">
                </div>
            </div>
            <input type="hidden" class="news_sub_pdf_parent" name="parent_id" value="">
            <input type="hidden" class="news_sub_pdf_id" name="pdf_id" value="'.$pdf_result->id.'">
            <div class="sub_pdf_append_footer">
                <button type="button" class="sub_pdf_append_update">Save</button>
            </div>
        </div>';
        return $result;
    }

    public function delete_pdf(Request $request)
    {
        $back_url = app('Illuminate\Routing\UrlGenerator')->previous();
        $id = $request->id;
        DB::table('news_pdf')->where('id', '=', $id)->delete();
        return redirect($back_url);
    }

    public function plus_pdf(Request $request){
        $pdf_block = '<div class="sub_pdf_append_block show_block"><div class="form-group ">
                                    <label for="title">Title</label>
                                    <input id="about_title" type="text" class="form-control" name="pdf_title[]" value=""
                                           autofocus>
                                </div>
                                <div class="form-group">
                                    <label for="Description">Short Description</label>
                                    <textarea class="form-control short_desc" name="pdf_desc[]"></textarea>
                                </div>
                                <div class="not_media_box form-group">
                                    <div class="">
                                        <label>Upload PDF</label>
                                    </div>
                                   
                                    <div class="file-upload btn btn_1 green" style="float:left;">
                                        <span>Choose file</span>
                                        <input type="file" name="pdf_file[]" class="image upload  news_sub_pdf_file" >
                                    </div>
<div class="pdf_append_block_delete">
                                                    <i class="icon-trash"></i>
                                                </div>
                                </div></div>';
        return $pdf_block;
    }

    public function upload_pdf(Request $request){

        $parent_id =  $request->parent_id;
        $pdf_title =  $request->title;
        $pdf_desc =  $request->desc;
        $filename = request()->file('file');
        $pdf_name = $filename->getClientOriginalName();
        $filename->move(
            base_path() . '/public/uploads/news/pdfs', $pdf_name
        );
        if(isset($parent_id) && $parent_id !='undefined'){
            $parent_id =  $request->parent_id;
        }else{
            $parent_id =  0;
        }
        $insertId = DB::table('news_sub_page_pdf')->insertGetId(
            [
                'sub_page_id' => $parent_id,
                'title' => $pdf_title,
                'short_desc' => $pdf_desc,
                'file' => $pdf_name,
            ]
        );

        $return = '<div class="open_sub_pdf_block">
<input type="hidden" name="sub_pdfs_ids[]" value="'.$insertId.'">
                                            <p class="open_sub_pdf_block_title_'.$insertId.'" data-id="'.$insertId.'">'.$pdf_title.'</p>
                                            <div class="pdf_append_block_delete">
                                                    <i class="icon-trash"></i>
                                                </div>
                                        </div>';

        return $return;
    }

    public function update_sub_pdf(Request $request){

        $parent_id =  $request->parent_id;
        $pdf_title =  $request->title;
        $pdf_desc =  $request->desc;
        $old_file =  $request->old_file;
        $news_sub_pdf_id =  $request->news_sub_pdf_id;
        $filename = request()->file('file');
        if($filename){
            $pdf_name = $filename->getClientOriginalName();
            $filename->move(
                base_path() . '/public/uploads/news/pdfs', $pdf_name
            );
        }else{
            $pdf_name = $old_file;
        }


        DB::table('news_sub_page_pdf')
            ->where('id', $news_sub_pdf_id)
            ->update(
            [
                'title' => $pdf_title,
                'short_desc' => $pdf_desc,
                'file' => $pdf_name,
            ]
        );


        return $pdf_title;
    }
}
