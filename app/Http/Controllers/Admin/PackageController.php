<?php

namespace App\Http\Controllers\Admin;

use App\Package;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden','=', 0)->first();
        $default_lang = $first_lang->short;
        $packages = Package::all();
        return view("admin.packages.all", compact('packages','default_lang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $settings = DB::table('settings')->where('id', 1)->first();
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();

        return view('admin.packages.create',compact('languages','default_lang','settings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(isset($request->add_package) && $request->add_package == 'ok'){
            $package = new Package();


            if ($request->file("image")) {

                $image = Input::file('image');
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();

                $newFile = public_path('uploads/news/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->orientate()->save($newFile);


                $newFileThumb = public_path('uploads/news/thumbs/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(360, 250)->save($newFileThumb);

                $newFileThumb_150 = public_path('uploads/news/150/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(null, 150, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb_150);
                $filepath = $generatedString . "." . $extension;
            } else {
                $filepath = "";
            }

            foreach ($request->all() as $key => $val) {
                if ($key != 'old_image' && $key != '_token' && $key != 'add_package') {
                    $package->$key = $val;
                }

            }
            $package->image = $filepath;
            $package->package_desc_am = json_encode($request->package_desc_am);
            $package->package_desc_ru = json_encode($request->package_desc_ru);
            $package->package_desc_en = json_encode($request->package_desc_en);
            $package->save();
            return redirect('admin/package');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();


        $package = Package::find($id);


        return view('admin.packages.edit', compact('default_lang','languages','package'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(isset($request->edit_package) && $request->edit_package == "ok"){
            $package = Package::find($id);

            if ($request->file("image")) {

                $image = Input::file('image');
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();

                $newFile = public_path('uploads/news/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->orientate()->save($newFile);


                $newFileThumb = public_path('uploads/news/thumbs/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(360, 250)->save($newFileThumb);

                $newFileThumb_150 = public_path('uploads/news/150/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(null, 150, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb_150);
                $filepath = $generatedString . "." . $extension;
            }else{
                $filepath= $request->old_image;
            }


            foreach ($request->all() as $key => $val) {
                if ($key != 'old_image'  &&  $key != '_token'  &&  $key!='_method' && $key != 'edit_package') {
                    $package->$key = $val;
                }

            }
            $package->package_desc_am = json_encode($request->package_desc_am);
            $package->package_desc_ru = json_encode($request->package_desc_ru);
            $package->package_desc_en = json_encode($request->package_desc_en);
            $package->image = $filepath;
            $package->save();

        }
        return redirect('admin/package');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('packages')->where('id', '=', $id)->delete();
    }

    public function packageAdd(){

        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();
        $result = '<div class="package_services_block">';
        foreach($languages as $language){
            if($language->first == 1) { $class= "active_field"; } else { $class ="hidden_field"; }
            $result .= ' <div class="form-group lang_field lang_field_'.$language->short .'  '.$class.'">
                                <label for="title">'. $language->title .' Title</label>
                                <input id="about_title" type="text" class="form-control" name="package_desc_'.$language->short.'[]" value=""
                                       autofocus>
                                       
                            </div>';
        }
        $result .='<div class="new_remove_package">X</div></div>';
        return $result;


    }
}
