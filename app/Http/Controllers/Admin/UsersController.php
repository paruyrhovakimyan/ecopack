<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    public function index(){
        $users = DB::table('users')->get();
        return view("admin.users.all", compact('users'));
    }
    public function show($id){
        $user = DB::table('users')->where('id', $id)->first();
        $user_comments = DB::table('comments')->where('user_id', $id)->get();
        foreach ($user_comments as $user_comment){
            $article_id = $user_comment->article_id;
            $article_result = DB::table('articles')->where('id', '=',$article_id )->first();
            if(isset($article_result->title) && $article_result->title != ''){
                $user_comment->article_name = $article_result->title;
            }else{
                $user_comment->article_name = '';
            }
        }
        return view('admin.users.show', compact('user','user_comments'));
    }
    public function destroy($id){
        DB::table('users')->where('id', '=', $id)->delete();
    }
}
