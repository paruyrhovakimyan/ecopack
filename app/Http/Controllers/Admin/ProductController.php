<?php

namespace App\Http\Controllers\Admin;

use App\Brand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use App\Product;
use App\ProductCat;
use App\ProductType;

class ProductController extends Controller
{


    private function mb_str_split( $string ) {
        # Split at all position not after the start: ^
        # and not before the end: $
        return preg_split('/(?<!^)(?!$)/u', $string );
    }
    private function create_english($title){
        $charlist = $this->mb_str_split( $title );
        $array_hayeren = array('.','ա','բ','գ','դ','ե','զ','է','ը','թ','ժ','ի','լ','խ','ծ','կ','հ','ձ','ղ','ճ','մ','յ','ն','շ','ո','չ','պ','ջ','ռ','ս','վ','տ','ր','ց','ւ','փ','ք','և','օ','ֆ',' ','  ','/','\\','&','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','1','2','3','4','5','6','7','8','9','0');
        $array_english = array('-','a','b','g','d','e','z','e','y','t','zh','i','l','kh','ts','k','h','dz','gh','ch','m','y','n','sh','o','ch','p','j','r','s','v','t','r','c','u','p','q','ev','o','f','-','-','-','-','-','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','1','2','3','4','5','6','7','8','9','0');
        $new ='';
        if(count($charlist)<41){
            $counts = count($charlist);
        }
        else{
            $counts = 40;
        }
        for($i=0;$i<$counts;$i++){
            $key = array_search(mb_strtolower($charlist[$i],'UTF-8'),$array_hayeren);
            $new .= $array_english[$key];

        }
        return $new;
    }
    private function create_slug($title,$id_obj){

        $slug = $this->create_english($title);
        $article_isset_slug = DB::table('news')->where('slug', '=', $slug)->orderBy('id', 'desc')->count();
        if($article_isset_slug > 0){
            $new_slug = $slug."-".$id_obj;
        }else{
            $new_slug = $slug;
        }

        return $new_slug;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $not_filter = 1;
        if(isset($request->search_pr) && $request->search_pr !=''){
            $products = Product::where('title_am', 'like', '%'.$request->search_pr.'%')->orderby('id', 'desc')->paginate(20);
        }elseif(isset($request->pr_cat) && $request->pr_cat !='' && $request->pr_cat !=0){
            $products = DB::table('products')
                ->join('pr_cat_ids', function ($join) use ($request) {
                    $join->on('products.id', '=', 'pr_cat_ids.pr_id')
                        ->where('pr_cat_ids.cat_id', '=', $request->pr_cat);
                })
                ->paginate(20);
            $not_filter = 0;
        }else{
            $products = Product::orderby('id', 'desc')->paginate(20);
        }
        $categories = ProductCat::where("parent_cat", '=', 0)->get();
        foreach ($categories as $category_all){
            $category_all->childs = ProductCat::where("parent_cat", '=', $category_all->id)->get();
        }
        return view("admin.products.all", compact('not_filter','categories','products','default_lang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();

        $categories = ProductCat::where("parent_cat", '=', 0)->get();

        foreach ($categories as $category_all){
            $category_all->childs = ProductCat::where("parent_cat", '=', $category_all->id)->get();
        }
        $brands = Brand::all();
        $ProductTypes = ProductType::all();
        return view('admin.products.create', compact('ProductTypes','brands','default_lang','categories','languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($request->add_product) && $request->add_product == "ok"){

            $product = new Product();


            if ($request->file("image")) {

                $image = Input::file('image');
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();

                $newFile = public_path('uploads/product/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->orientate()->save($newFile);


                $newFileThumb = public_path('uploads/product/thumbs/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(260, 350)->save($newFileThumb);

                $newFileThumb_150 = public_path('uploads/product/150/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(null, 150, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb_150);
                $filepath = $generatedString . "." . $extension;
            } else {
                $filepath = "";
            }



            $charecter_am = array();
            $charecter_ru = array();
            $charecter_en = array();
            if(isset($request->charecter_title_am) && !empty($charecter_title_am)){
                $charecter_title_am = $request->charecter_title_am;
                $charecter_desc_am = $request->charecter_desc_am;
                $charecter_title_ru = $request->charecter_title_ru;
                $charecter_desc_ru = $request->charecter_desc_ru;
                $charecter_title_en = $request->charecter_title_en;
                $charecter_desc_en = $request->charecter_desc_en;

                foreach ($charecter_title_am as $char_index => $charecter_title_am_one){
                    $charecter_am[$charecter_title_am_one] = $charecter_desc_am[$char_index];
                    $charecter_ru[$charecter_title_ru[$char_index]] = $charecter_desc_ru[$char_index];
                    $charecter_en[$charecter_title_en[$char_index]] = $charecter_desc_en[$char_index];
                }

            }
            $charecter_am_end = json_encode($charecter_am);
            $charecter_ru_end = json_encode($charecter_ru);
            $charecter_en_end = json_encode($charecter_en);
            $recomeded_cats = json_encode($request->recomeded_cat);
            foreach ($request->all() as $key => $val) {
                if ($key != 'product_cat_id' && $key != 'pr_old_price' && $key != 'recomeded_cat' && $key != 'charecter_title_en' && $key != 'charecter_title_ru' && $key != 'charecter_title_am' && $key != 'charecter_desc_en' && $key != 'charecter_desc_ru' && $key != 'charecter_desc_am' && $key != '_token' && $key != 'add_product' && $key != 'filename') {
                    $product->$key = $val;
                }
            }
            if($request->pr_old_price !=''){
                $product->pr_old_price = $request->pr_old_price;
            }else{
                $product->pr_old_price = 0;
            }
            if($request->pr_new_price !=''){
                $product->pr_new_price = $request->pr_new_price;
            }else{
                $product->pr_new_price = 0;
            }
            $product->pr_charecters_am = $charecter_am_end;
            $product->pr_charecters_ru = $charecter_ru_end;
            $product->pr_charecters_en = $charecter_en_end;
            $product->recomended = $recomeded_cats;
            $product->image = $filepath;
            $product->save();
            if (isset($request->filename) && $request->filename != '') {
                foreach ($request->filename as $filename) {

                    DB::table('product_gallery')->insert(
                        [
                            'image' => $filename,
                            'product_id' => $product->id
                        ]
                    );
                }
            }
            if(isset($request->product_cat_id) && $request->product_cat_id != ''){
                $isset_cats = array();
                foreach ($request->product_cat_id as $product_cat_id) {
                    $cat_parent = ProductCat::find($product_cat_id);
                    if($cat_parent->parent_cat != 0){
                        if(!in_array($cat_parent->parent_cat,$isset_cats)){
                            DB::table('pr_cat_ids')->insert(
                                [
                                    'cat_id' => $cat_parent->parent_cat,
                                    'pr_id' => $product->id
                                ]
                            );
                            $isset_cats[] = $cat_parent->parent_cat;
                        }

                    }

                    if(!in_array($product_cat_id,$isset_cats)){
                        DB::table('pr_cat_ids')->insert(
                            [
                                'cat_id' => $product_cat_id,
                                'pr_id' => $product->id
                            ]
                        );
                        $isset_cats[] = $product_cat_id;
                    }

                }
            }
            return redirect('admin/products');


        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $default_lang = $first_lang->short;
        $product = Product::find($id);

        $product_cat_ids = DB::table('pr_cat_ids')->select("cat_id")->where("pr_id", "=",$id )->get();
        $cat_array = array();
        if(count($product_cat_ids) > 0){
            foreach ($product_cat_ids as $product_cat_id) {
                $cat_array[] = $product_cat_id->cat_id;
            }
        }

        if(isset($product->pr_charecters_am) && $product->pr_charecters_am!=''){
            $product->keys_am = array_keys((array)json_decode($product->pr_charecters_am));
            $product->vals_am = array_values((array)json_decode($product->pr_charecters_am));
            $product->keys_ru = array_keys((array)json_decode($product->pr_charecters_ru));
            $product->vals_ru = array_values((array)json_decode($product->pr_charecters_ru));
            $product->keys_en = array_keys((array)json_decode($product->pr_charecters_en));
            $product->vals_en = array_values((array)json_decode($product->pr_charecters_en));
        }

        $projectImages = DB::table('product_gallery')->where('product_id',$id)->get();
        $dropzoneDefaultHidden = '';
        if (count($projectImages) != 0) {
            $dropzoneDefaultHidden = 'dz-default-hidden';
        }
        $categories = ProductCat::where("parent_cat", '=', 0)->get();
        foreach ($categories as $category_all){
            $category_all->childs = ProductCat::where("parent_cat", '=', $category_all->id)->get();
        }
        $brands = Brand::all();
        $ProductTypes = ProductType::all();
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();
        return view('admin.products.edit', compact('cat_array','ProductTypes','brands','default_lang','projectImages','dropzoneDefaultHidden','languages','product','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (isset($request->edit_category) && $request->edit_category == "ok"){



            $coach = Product::find($id);

            if ($request->file("image")) {

                $image = Input::file('image');
                $generatedString = time();
                $extension = $request->file("image")->getClientOriginalExtension();

                $newFile = public_path('uploads/product/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->orientate()->save($newFile);


                $newFileThumb = public_path('uploads/product/thumbs/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(260, 350)->save($newFileThumb);

                $newFileThumb_150 = public_path('uploads/product/150/' . $generatedString . "." . $extension);
                Image::make($image->getRealPath())->resize(null, 150, function ($constraint) {
                    $constraint->aspectRatio();
                })->orientate()->save($newFileThumb_150);
                $filepath = $generatedString . "." . $extension;
            }else{
                $filepath= $request->old_image;
            }

            $charecter_am = array();
            $charecter_ru = array();
            $charecter_en = array();
            if(isset($request->charecter_title_am) && !empty($charecter_title_am)){
                $charecter_title_am = $request->charecter_title_am;
                $charecter_desc_am = $request->charecter_desc_am;
                $charecter_title_ru = $request->charecter_title_ru;
                $charecter_desc_ru = $request->charecter_desc_ru;
                $charecter_title_en = $request->charecter_title_en;
                $charecter_desc_en = $request->charecter_desc_en;

                foreach ($charecter_title_am as $char_index => $charecter_title_am_one){
                    $charecter_am[$charecter_title_am_one] = $charecter_desc_am[$char_index];
                    $charecter_ru[$charecter_title_ru[$char_index]] = $charecter_desc_ru[$char_index];
                    $charecter_en[$charecter_title_en[$char_index]] = $charecter_desc_en[$char_index];
                }

            }
            $charecter_am_end = json_encode($charecter_am);
            $charecter_ru_end = json_encode($charecter_ru);
            $charecter_en_end = json_encode($charecter_en);

            $recomeded_cats = json_encode($request->recomeded_cat);
            foreach ($request->all() as $key => $val) {
                if ($key != 'product_cat_id' && $key != 'recomeded_cat' && $key != 'charecter_desc_en' && $key != 'charecter_desc_ru' && $key != 'charecter_desc_am' && $key != 'charecter_title_en' && $key != 'charecter_title_ru' && $key != 'charecter_title_am' && $key != 'old_image' && $key != '_method' && $key != '_token' && $key != 'edit_category' && $key != 'filename') {
                    $coach->$key = $val;
                }
            }
            if(isset($request->new_collection)){
                $coach->new_collection = 1;
            }else{
                $coach->new_collection = 0;
            }
            if(isset($request->in_stock)){
                $coach->in_stock = 1;
            }else{
                $coach->in_stock = 0;
            }
            if($coach->pr_new_price !=''){
                $coach->pr_new_price = $request->pr_new_price;
            }else{
                $coach->pr_new_price = 0;
            }
            //return $recomeded_cats;
            $coach->pr_charecters_am = $charecter_am_end;
            $coach->pr_charecters_ru = $charecter_ru_end;
            $coach->pr_charecters_en = $charecter_en_end;
            $coach->recomended = $recomeded_cats;
            $coach->image = $filepath;
            $coach->save();
            DB::table('product_gallery')->where('product_id', '=', $id)->delete();
            if (isset($request->filename) && $request->filename != '') {
                foreach ($request->filename as $filename) {

                    DB::table('product_gallery')->insert(
                        [
                            'image' => $filename,
                            'product_id' => $id
                        ]
                    );
                }
            }
            DB::table('pr_cat_ids')->where('pr_id', '=', $id)->delete();
            if(isset($request->product_cat_id) && $request->product_cat_id != ''){
                $isset_cats = array();
                foreach ($request->product_cat_id as $product_cat_id) {
                    $cat_parent = ProductCat::find($product_cat_id);
                    if($cat_parent->parent_cat != 0){
                        if(!in_array($cat_parent->parent_cat,$isset_cats)){
                            DB::table('pr_cat_ids')->insert(
                                [
                                    'cat_id' => $cat_parent->parent_cat,
                                    'pr_id' => $id
                                ]
                            );
                            $isset_cats[] = $cat_parent->parent_cat;
                        }

                    }

                    if(!in_array($product_cat_id,$isset_cats)){
                        DB::table('pr_cat_ids')->insert(
                            [
                                'cat_id' => $product_cat_id,
                                'pr_id' => $id
                            ]
                        );
                        $isset_cats[] = $product_cat_id;
                    }

                }
            }

            return redirect('admin/products/');


        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('products')->where('id', '=', $id)->delete();
    }
}
