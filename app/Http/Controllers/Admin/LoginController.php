<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{

    protected $redirectTo = '/admin/dashboard';

    public function index()
    {
        if (session('admin')) {
            return redirect('admin/dashboard');
        } else {

            return view('admin.login');
        }
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        $username = $request->username;
        $password = $request->password;


        $login_admin = DB::table('admins')
            ->where('username', '=', $username)
            ->where('password', '=', $password)
            ->first();

        if ($login_admin) {
            if ($login_admin->role == 2) {
                session(['admin' => 'yes']);
                session(['super_admin' => 'yes']);
            } else {
                session(['admin' => 'yes']);
            }
            return redirect('admin/dashboard');
        } else {
            $request->session()->flash('error_login', 'Invalid user login or password');
            return view('admin.login');
        }


    }


    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
    }

    public function username()
    {
        return 'username';
    }

    public function logout(Request $request)
    {
        $request->session()->forget('admin');
        $request->session()->forget('super_admin');
        return redirect('admin');
    }
}
