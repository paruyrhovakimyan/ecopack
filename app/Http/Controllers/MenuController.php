<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MenuController extends Controller
{

    public function getTopMenu($lang)
    {

        $url = $_SERVER['REQUEST_URI'];
        $url_array = explode("/", $url);
        // Curent active page
        $current_page = '';
        if ($url_array[1] == "page") {
            $page_slug = $url_array[2];
            $page_slug_array = explode("?", $page_slug);
            $page_slug = $page_slug_array[0];
            $parent_page = DB::table('pages')->select("parent")->where('slug', $page_slug)->first();
            //return $parent_page->parent;
            if($parent_page->parent !=0){
                $parent_page_slug = DB::table('pages')->select("slug")->where('id', $parent_page->parent)->first();

                $current_page= $parent_page_slug->slug;
            }else{
                $current_page = $url_array[2];
            }

        }elseif ($url_array[1] == "category"){
            $cat_slug = $url_array[2];
            $cat_slug_array = explode("?", $cat_slug);
            $cat_slug = $cat_slug_array[0];
            $categories = DB::table('categories')->select("page_parent")
                ->where('slug','=', $cat_slug)
                ->where('hidden', 0)
                ->first();

            $cat_parent_page = DB::table('pages')
                ->select("slug")
                ->where('id', $categories->page_parent)
                ->first();
            $current_page = $cat_parent_page->slug;

        } else {
            $current_page = '';
        }


        $menu_parent_items = DB::table('pages')->select("config_project", "slug", "config_cat","sub_pages","page_with_pdf", "title_$lang as title", "id", "parent", "hidden")->where('id', '!=', '2')->where('parent', 0)->orderBy("page_order", "asc")->get();

        $menu_one_block = '';
        foreach ($menu_parent_items as $menu_parent_item) {

            $sub_menu = '';
            if($menu_parent_item->id == 3){

                $sub_menu .= '<ul class="events_menu">';
                     $product_cats = DB::table('pages')->select("id", "hidden", "slug", "config_cat","sub_pages", "title_$lang as title")->where('hidden', 0)->where('parent', $menu_parent_item->id)->get();

                if (count($product_cats) > 0) {
                    foreach ($product_cats as $category) {
                        if ($lang == "am") {
                            $sub_menu .= '<li class="news_li events_li"><a class="news_a" href="' . url('/') . '/page/' . $category->slug . '">' . $category->title . '</a>';
                           // $product_sub_cats = DB::table('product_cats')->select("id", "hidden", "title_$lang as title")->where('parent_cat', $category->id)->where('hidden', 0)->get();

//                            if(isset($product_sub_cats) && count($product_sub_cats) > 0){
//                                $sub_menu .= '<ul class="cat_second_sub">';
//                                foreach ($product_sub_cats as $product_sub_cat){
//                                    $sub_menu .= '<li class="news_li events_li"><a class="news_a" href="' . url('/') . '/products/' . $product_sub_cat->id . '">' . $product_sub_cat->title . '</a>';
//
//                                }
//                                $sub_menu .= '</ul>';
//                            }


                            $sub_menu .= '</li>';
                        } else {
                            $sub_menu .= '<li class="news_li events_li"><a class="news_a" href="' . url('/') . '/' . $lang . '/page/' . $category->slug . '">' . $category->title . '</a>';

//                            $product_sub_cats = DB::table('product_cats')->select("id", "hidden", "title_$lang as title")->where('parent_cat', $category->id)->where('hidden', 0)->get();

//                            if(isset($product_sub_cats) && count($product_sub_cats) > 0){
//                                $sub_menu .= '<ul class="cat_second_sub">';
//                                foreach ($product_sub_cats as $product_sub_cat){
//                                    $sub_menu .= '<li class="news_li events_li"><a class="news_a" href="' . url('/') . '/' . $lang . '/products/' . $product_sub_cat->id . '">' . $product_sub_cat->title . '</a>';
//
//                                }
//                                $sub_menu .= '</ul>';
//                            }
                            $sub_menu .= '</li>';
                        }
//                        $sub_menu .= '<li class="news_li events_li"><a class="news_a" href="' . url('/') . '/projects/' . $category->slug . '">' . $category->title . '</a></li>';
                    }
                }

                    $sub_menu .= '</ul>';
            }elseif($menu_parent_item->config_project == 1){
                $categories = DB::table('project_categories')->select("id", "slug", "hidden", "title_$lang as title")->where('hidden', 0)->where('page_parent', $menu_parent_item->id)->get();
                $sub_menu .= '<ul class="events_menu">';
                if (count($categories) > 0) {
                    foreach ($categories as $category) {
                        if ($lang == "am") {
                            $sub_menu .= '<li class="news_li events_li"><a class="news_a" href="' . url('/') . '/projects/' . $category->slug . '">' . $category->title . '</a></li>';
                        } else {
                            $sub_menu .= '<li class="news_li events_li"><a class="news_a" href="' . url('/') . '/' . $lang . '/projects/' . $category->slug . '">' . $category->title . '</a></li>';
                        }
//                        $sub_menu .= '<li class="news_li events_li"><a class="news_a" href="' . url('/') . '/projects/' . $category->slug . '">' . $category->title . '</a></li>';
                    }
                }
                $sub_menu .= '</ul>';

            }elseif ($menu_parent_item->sub_pages == 1){
                $child_pages = DB::table('pages')->select("slug", "config_cat","sub_pages", "title_$lang as title", "id", "parent")->where('hidden', 0)->where('parent', $menu_parent_item->id)->get();

                if($menu_parent_item->id == 74){

                    $sub_menu .= '<ul class="events_menu  menu-wrapper">';
                    if (count($child_pages) > 0) {

                        $row_width = 12 / count($child_pages);

                        foreach ($child_pages as $child_page) {



                            $sub_menu .='<div class="col-md-'.$row_width.'">
                                        <h3>' . $child_page->title . '</h3>';
                            $sub_page_subs = DB::table('sub_pages_with_pdfs')->select("title_$lang as title", "slug")->where("parent_id", "=",$child_page->id)->where("folder_parent", "=",0)->get();
                            if(isset($sub_page_subs) && count($sub_page_subs) > 0){
                                $sub_menu .= '<ul>';
                                foreach ($sub_page_subs as $sub_page_sub){
                                    if ($lang == "am") {
                                        $sub_menu .= ' <li><a href="' . url('/') . '/page_pdf/' . $sub_page_sub->slug . '">' . $sub_page_sub->title . '</a></li>';
                                    } else {
                                        $sub_menu .= ' <li><a href="' . url('/') . '/' . $lang . '/page_pdf/' . $sub_page_sub->slug . '">' . $sub_page_sub->title . '</a></li>';
                                    }
//                                    $sub_menu .= ' <li><a href="' . url('/') . '/page_pdf/' . $sub_page_sub->slug . '">' . $sub_page_sub->title . '</a></li>';
                                }
                                $sub_menu .= '</ul>';
                            }
                            $sub_menu .='</div>';




                        }
                    }

                    if ($menu_parent_item->page_with_pdf == 1){
                        $child_pages = DB::table('sub_pages_with_pdf')->select("slug", "title", "id", "parent_id")->where('hidden', 0)->where('parent_id', $menu_parent_item->id)->get();
                        if (count($child_pages) > 0) {
                            foreach ($child_pages as $child_page) {
                                if ($lang == "am") {
                                    $sub_menu .= '<li class="news_li events_li"><a class="news_a" href="' . url('/') . '/page_pdf/' . $child_page->slug . '">' . $child_page->title . '</a></li>';
                                } else {
                                    $sub_menu .= '<li class="news_li events_li"><a class="news_a" href="' . url('/') . '/' . $lang . '/page_pdf/' . $child_page->slug . '">' . $child_page->title . '</a></li>';
                                }
//                                $sub_menu .= '<li class="news_li events_li"><a class="news_a" href="' . url('/') . '/page_pdf/' . $child_page->slug . '">' . $child_page->title . '</a></li>';
                            }
                        }
                    }

                    $sub_menu .= '</ul>';



                }else{
                    $sub_menu .= '<ul class="events_menu sub_menu_'.$menu_parent_item->slug.'">';
                    if (count($child_pages) > 0) {
                        foreach ($child_pages as $child_page) {
                            if ($lang == "am") {
                                if($child_page->id == 127){
                                    $sub_menu .= '<li class="news_li events_li">

                                        <a class="news_a '.$child_page->id.'" href="' . url('/') . '/category/mijocaroumner/">' . $child_page->title . '</a>';

                                }else{
                                    $sub_menu .= '<li class="news_li events_li">

                                        <a class="news_a '.$child_page->id.'" href="' . url('/') . '/page/' . $child_page->slug . '">' . $child_page->title . '</a>';

                                }
                               }else {

                                if($child_page->id == 127){
                                    $sub_menu .= '<li class="news_li events_li">

                                        <a class="news_a" href="' . url('/') . '/' . $lang . '/category/mijocaroumner/">' . $child_page->title . '</a>';

                                }else{
                                    $sub_menu .= '<li class="news_li events_li">

                                        <a class="news_a" href="' . url('/') . '/' . $lang . '/page/' . $child_page->slug . '">' . $child_page->title . '</a>';

                                }

                               }
//
                            $sub_page_subs = DB::table('sub_pages_with_pdfs')->select("title_$lang as title", "slug")->where("parent_id", "=",$child_page->id)->where("folder_parent","=", 0)->get();
                            if(isset($sub_page_subs) && count($sub_page_subs) > 0){
                                $sub_menu .= '<ul>';
                                foreach ($sub_page_subs as $sub_page_sub){
                                    if ($lang == "am") {
                                        $sub_menu .= ' <li><a href="' . url('/') . '/page_pdf/' . $sub_page_sub->slug . '">' . $sub_page_sub->title . '</a></li>';
                                    }else {
                                        $sub_menu .= ' <li><a href="' . url('/') . '/' . $lang . '/page_pdf/' . $sub_page_sub->slug . '">' . $sub_page_sub->title . '</a></li>';
                                    }
//                                    $sub_menu .= ' <li><a href="' . url('/') . '/page_pdf/' . $sub_page_sub->slug . '">' . $sub_page_sub->title . '</a></li>';
                                }
                                $sub_menu .= '</ul>';
                            }

                            $sub_menu .= ' </li>';
                        }
                    }

                    if ($menu_parent_item->page_with_pdf == 1){
                        $child_pages = DB::table('sub_pages_with_pdf')->select("slug", "title", "id", "parent_id")->where('hidden', 0)->where('parent_id', $menu_parent_item->id)->get();
                        if (count($child_pages) > 0) {
                            foreach ($child_pages as $child_page) {
                                if ($lang == "am") {
                                    $sub_menu .= '<li class="news_li events_li"><a class="news_a" href="' . url('/') . '/page_pdf/' . $child_page->slug . '">' . $child_page->title . '</a></li>';
                                }else {
                                    $sub_menu .= '<li class="news_li events_li"><a class="news_a" href="' . url('/') . '/' . $lang . '/page_pdf/' . $child_page->slug . '">' . $child_page->title . '</a></li>';
                                }
//                                $sub_menu .= '<li class="news_li events_li"><a class="news_a" href="' . url('/') . '/page_pdf/' . $child_page->slug . '">' . $child_page->title . '</a></li>';
                            }
                        }
                    }

                    $sub_menu .= '</ul>';
                }


            }

            if ($current_page == $menu_parent_item->slug) {
                $active_page = "active";
            } else {
                $active_page = "";
            }
            if($menu_parent_item->title == "News & Events"){
                if($lang == 'am'){
                    $menu_one_block .= '<li class="menu_li ' . $active_page . '">
                    <a class="menu_href menu_href_'.$menu_parent_item->id.'" href="' . url('/') . '/category/news">' . $menu_parent_item->title . '</a>
                    ' . $sub_menu . '
                </li>';
                }else{
                    $menu_one_block .= '<li class="menu_li ' . $active_page . '">
                    <a class="menu_href menu_href_'.$menu_parent_item->id.'" href="' . url('/') . '/' . $lang . '/category/news">' . $menu_parent_item->title . '</a>
                    ' . $sub_menu . '
                </li>';
                }
//
            }else{
                if($menu_parent_item->hidden == 0){
                    if($menu_parent_item->id == 3){
                        if($lang == "am"){
                            $menu_one_block .= '<li class="megamenu megamenu_740 submenu menu_li ' . $active_page . '">
                            <a class="menu_href menu_href_'.$menu_parent_item->id.'">' . $menu_parent_item->title . '</a>
                            ' . $sub_menu . '
                        </li>';
                        }else {
                            $menu_one_block .= '<li class="megamenu megamenu_740 submenu menu_li ' . $active_page . '">
                            <a class="menu_href menu_href_'.$menu_parent_item->id.'" >' . $menu_parent_item->title . '</a>
                            ' . $sub_menu . '
                        </li>';
                        }
//                        $menu_one_block .= '<li class="megamenu megamenu_740 submenu menu_li ' . $active_page . '">
//                            <a class="menu_href" href="' . url('/') . '/page/' . $menu_parent_item->slug . '">' . $menu_parent_item->title . '</a>
//                            ' . $sub_menu . '
//                        </li>';
                    }else{
                        if($lang == 'am'){
                            $menu_one_block .= '<li class="menu_li ' . $active_page . '">
                            <a class="menu_href menu_href_'.$menu_parent_item->id.'" href="' . url('/') . '/page/'.$menu_parent_item->slug.'">' . $menu_parent_item->title . '</a>
                            ' . $sub_menu . '
                        </li>';
                        }else{
                            $menu_one_block .= '<li class="menu_li ' . $active_page . '">
                            <a class="menu_href menu_href_'.$menu_parent_item->id.'" href="' . url('/') . '/' . $lang . '/page/'.$menu_parent_item->slug.'">' . $menu_parent_item->title . '</a>
                            ' . $sub_menu . '
                        </li>';
                        }
//                        $menu_one_block .= '<li class="menu_li ' . $active_page . '">
//                            <a class="menu_href" href="' . url('/') . '/page/' . $menu_parent_item->slug . '">' . $menu_parent_item->title . '</a>
//                            ' . $sub_menu . '
//                        </li>';
                    }

                }

            }


        }
        if ($current_page == '') {
            $active_home = "active";
        } else {
            $active_home = "";
        }
        $menu = '<ul class="my_menu">' . $menu_one_block . '</ul>';
        return $menu;
    }


public function getMenu($lang)
    {


        $menu_parent_items = DB::table('menu_items')
            ->where('menu_id', 1)
            ->where("item_parent", 1)
            ->orderBy('item_order', 'asc')
            ->get();
        foreach ($menu_parent_items as $row_parent_item) {
            $row_parent_item->child[] = array();
            $row_parent_item->i = 1;
            $resourse_id = $row_parent_item->resourse_id;

            switch ($row_parent_item->item_type) {
                case 'page': {

                    $row_page = DB::table('pages')
                        ->select("title_$lang as title", 'slug')
                        ->where('id', $resourse_id)
                        ->first();
                    $menu_item_name = $row_page->title;
                    $row_parent_item->name = $menu_item_name;
                    if ($lang == "am") {
                        $row_parent_item->slug = url('/') . '/page/' . $row_page->slug;
                    } else {
                        $row_parent_item->slug = url('/') . '/' . $lang . '/page/' . $row_page->slug;
                    }
                    break;
                }

                case 'category': {
                    $row_page = DB::table('categories')
                        ->select("title_$lang as title", 'slug')
                        ->where('id', $resourse_id)
                        ->first();
                    $menu_item_name = $row_page->title;
                    $row_parent_item->name = $menu_item_name;
                    if ($lang == "am") {
                        $row_parent_item->slug = url('/') . '/category/' . $row_page->slug;
                    } else {
                        $row_parent_item->slug = url('/') . '/' . $lang . '/category/' . $row_page->slug;
                    }
                    break;
                }

                case 'link': {
                    $row_page = DB::table('menu_links')
                        ->select("link_name_$lang as link_name", "link_href_$lang as link_href")
                        ->where('link_id', $resourse_id)
                        ->first();
                    $menu_item_name = $row_page->link_name;
                    $row_parent_item->name = $menu_item_name;
                    $row_parent_item->slug = $row_page->link_href;
                    break;
                }
            }


            $id_item_p = $row_parent_item->item_id;


            $result_items1 = DB::table('menu_items')
                ->where('menu_id', 1)
                ->where('item_parent', $id_item_p)
                ->orderBy('item_order', 'asc')
                ->get();
            $j = 0;
            foreach ($result_items1 as $row_item1) {
                $resourse_id1 = $row_item1->resourse_id;
                switch ($row_item1->item_type) {
                    case 'page': {
                        $row_page = DB::table('pages')
                            ->select("title_$lang as title", 'slug')
                            ->where('id', $resourse_id1)
                            ->first();
                        $menu_item_name = $row_page->title;
                        $row_parent_item->child[$j]['name'] = $menu_item_name;
                        $row_parent_item->child[$j]['id'] = $row_item1->item_id;
                        $row_parent_item->child[$j]['order'] = $row_item1->item_order;
                        if ($lang == "am") {
                            $row_parent_item->child[$j]['slug'] = url('/') . '/page/' . $row_page->slug;
                        } else {
                            $row_parent_item->child[$j]['slug'] = url('/') . '/' . $lang . '/page/' . $row_page->slug;
                        }

                        break;
                    }
                    case 'category': {
                        $row_page = DB::table('categories')
                            ->select("title_$lang as title", 'slug')
                            ->where('id', $resourse_id1)
                            ->first();
                        $menu_item_name = $row_page->title;
                        $row_parent_item->child[$j]['name'] = $menu_item_name;
                        $row_parent_item->child[$j]['id'] = $row_item1->item_id;
                        $row_parent_item->child[$j]['order'] = $row_item1->item_order;
                        if ($lang == "am") {
                            $row_parent_item->child[$j]['slug'] = url('/') . '/category/' . $row_page->slug;
                        } else {
                            $row_parent_item->child[$j]['slug'] = url('/') . '/' . $lang . '/category/' . $row_page->slug;
                        }

                        break;
                    }
                    case 'link': {
                        $row_page = DB::table('menu_links')
                            ->select("link_name_$lang as link_name", "link_href_$lang as link_href")
                            ->where('link_id', $resourse_id1)
                            ->first();
                        $menu_item_name = $row_page->link_name;
                        $row_parent_item->child[$j]['name'] = $menu_item_name;
                        $row_parent_item->child[$j]['id'] = $row_item1->item_id;
                        $row_parent_item->child[$j]['order'] = $row_item1->item_order;
                        $row_parent_item->child[$j]['slug'] = $row_item1->link_href;
                        break;
                    }
                }


                //errord

                $id_item_p2 = $row_item1->item_id;


                $result_items2 = DB::table('menu_items')
                    ->where('menu_id', 1)
                    ->where('item_parent', $id_item_p2)
                    ->orderBy('item_order', 'asc')
                    ->get();
                $j2 = 0;
                foreach ($result_items2 as $row_item2) {
                    $resourse_id2 = $row_item2->resourse_id;
                    switch ($row_item2->item_type) {
                        case 'page': {
                            $row_page2 = DB::table('pages')
                                ->select("title_$lang as title", 'slug')
                                ->where('id', $resourse_id2)
                                ->first();
                            $menu_item_name2 = $row_page2->title;
                            $row_parent_item->child[$j]['child2'][$j2]['name'] = $menu_item_name2;
                            $row_parent_item->child[$j]['child2'][$j2]['id'] = $row_item2->item_id;
                            $row_parent_item->child[$j]['child2'][$j2]['order'] = $row_item2->item_order;
                            if ($lang == "am") {
                                $row_parent_item->child[$j]['child2'][$j2]['slug'] = url('/') . '/page/' . $row_page2->slug;
                            } else {
                                $row_parent_item->child[$j]['child2'][$j2]['slug'] = url('/') . '/' . $lang . '/page/' . $row_page2->slug;
                            }

                            break;
                        }
                        case 'category': {
                            $row_page2 = DB::table('categories')
                                ->select("title_$lang as title", 'slug')
                                ->where('id', $resourse_id2)
                                ->first();
                            $menu_item_name2 = $row_page2->title;
                            $row_parent_item->child[$j]['child2'][$j2]['name'] = $menu_item_name2;
                            $row_parent_item->child[$j]['child2'][$j2]['id'] = $row_item2->item_id;
                            $row_parent_item->child[$j]['child2'][$j2]['order'] = $row_item2->item_order;
                            if ($lang == "am") {
                                $row_parent_item->child[$j]['child2'][$j2]['slug'] = url('/') . '/category/' . $row_page2->slug;
                            } else {
                                $row_parent_item->child[$j]['child2'][$j2]['slug'] = url('/') . '/' . $lang . '/category/' . $row_page2->slug;
                            }

                            break;
                        }
                        case 'link': {
                            $row_page2 = DB::table('menu_links')
                                ->select("link_name_$lang as link_name", "link_href_$lang as link_href")
                                ->where('link_id', $resourse_id2)
                                ->first();
                            $menu_item_name2 = $row_page2->link_name;
                            $row_parent_item->child[$j]['child2'][$j2]['name'] = $menu_item_name2;
                            $row_parent_item->child[$j]['child2'][$j2]['id'] = $row_item2->item_id;
                            $row_parent_item->child[$j]['child2'][$j2]['order'] = $row_item2->item_order;
                            $row_parent_item->child[$j]['child2'][$j2]['slug'] = $row_item2->link_href;
                            break;
                        }
                    }


                    //errord

                    $id_item_p3 = $row_item2->item_id;


                    $result_items3 = DB::table('menu_items')
                        ->where('menu_id', 1)
                        ->where('item_parent', $id_item_p3)
                        ->orderBy('item_order', 'asc')
                        ->get();
                    $j3 = 0;
                    foreach ($result_items3 as $row_item3) {
                        $resourse_id3 = $row_item3->resourse_id;
                        switch ($row_item3->item_type) {
                            case 'page': {
                                $row_page3 = DB::table('pages')
                                    ->select("title_$lang as title", 'slug')
                                    ->where('id', $resourse_id3)
                                    ->first();
                                $menu_item_name3 = $row_page3->title;
                                $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['name'] = $menu_item_name3;
                                $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['id'] = $row_item3->item_id;
                                $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['order'] = $row_item3->item_order;
                                if ($lang == "am") {
                                    $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['slug'] = url('/') . '/page/' . $row_page3->slug;
                                } else {
                                    $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['slug'] = url('/') . '/' . $lang . '/page/' . $row_page3->slug;

                                }
                                break;
                            }
                            case 'category': {
                                $row_page3 = DB::table('categories')
                                    ->select("title_$lang as title", 'slug')
                                    ->where('id', $resourse_id3)
                                    ->first();
                                $menu_item_name3 = $row_page3->title;
                                $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['name'] = $menu_item_name3;
                                $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['id'] = $row_item3->item_id;
                                $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['order'] = $row_item3->item_order;
                                if ($lang == "am") {
                                    $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['slug'] = url('/') . '/category/' . $row_page3->slug;
                                } else {
                                    $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['slug'] = url('/') . '/' . $lang . '/category/' . $row_page3->slug;
                                }
                                break;
                            }
                            case 'link': {
                                $row_page3 = DB::table('menu_links')
                                    ->select("link_name_$lang as link_name", "link_href_$lang as link_href")
                                    ->where('link_id', $resourse_id3)
                                    ->first();
                                $menu_item_name3 = $row_page3->link_name;
                                $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['name'] = $menu_item_name3;
                                $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['id'] = $row_item3->item_id;
                                $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['order'] = $row_item3->item_order;
                                $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['slug'] = $row_item3->link_href;
                                break;
                            }
                        }


                        //errord

                        $id_item_p4 = $row_item3->item_id;


                        $result_items4 = DB::table('menu_items')
                            ->where('menu_id', 1)
                            ->where('item_parent', $id_item_p4)
                            ->orderBy('item_order', 'asc')
                            ->get();
                        $j4 = 0;
                        foreach ($result_items4 as $row_item4) {
                            $resourse_id4 = $row_item4->resourse_id;
                            switch ($row_item4->item_type) {
                                case 'page': {
                                    $row_page4 = DB::table('pages')
                                        ->select("title_$lang as title", 'slug')
                                        ->where('id', $resourse_id4)
                                        ->first();
                                    $menu_item_name4 = $row_page4->title;
                                    $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['name'] = $menu_item_name4;
                                    $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['id'] = $row_item4->item_id;
                                    $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['order'] = $row_item4->item_order;
                                    if ($lang == "am") {
                                        $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['slug'] = url('/') . '/page/' . $row_page4->slug;
                                    } else {
                                        $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['slug'] = url('/') . '/' . $lang . '/page/' . $row_page4->slug;
                                    }
                                    break;
                                }
                                case 'category': {
                                    $row_page4 = DB::table('categories')
                                        ->select("title_$lang as title", 'slug')
                                        ->where('id', $resourse_id4)
                                        ->first();
                                    $menu_item_name4 = $row_page4->title;
                                    $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['name'] = $menu_item_name4;
                                    $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['id'] = $row_item4->item_id;
                                    $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['order'] = $row_item4->item_order;
                                    if ($lang == "am") {
                                        $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['slug'] = url('/') . '/category/' . $row_page4->slug;
                                    } else {
                                        $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['slug'] = url('/') . '/' . $lang . '/category/' . $row_page4->slug;
                                    }

                                    break;
                                }
                                case 'link': {
                                    $row_page4 = DB::table('menu_links')
                                        ->select("link_name_$lang as link_name", "link_href_$lang as link_href")
                                        ->where('link_id', $resourse_id4)
                                        ->first();
                                    $menu_item_name4 = $row_page4->link_name;
                                    $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['name'] = $menu_item_name4;
                                    $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['id'] = $row_item4->item_id;
                                    $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['order'] = $row_item4->item_order;
                                    $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['slug'] = $row_item4->link_href;
                                    break;
                                }
                            }


                            //errord

                            $id_item_p5 = $row_item4->item_id;


                            $result_items5 = DB::table('menu_items')
                                ->where('menu_id', 1)
                                ->where('item_parent', $id_item_p5)
                                ->orderBy('item_order', 'asc')
                                ->get();
                            $j5 = 0;
                            foreach ($result_items5 as $row_item5) {
                                $resourse_id5 = $row_item5->resourse_id;
                                switch ($row_item5->item_type) {
                                    case 'page': {
                                        $row_page5 = DB::table('pages')
                                            ->select("title_$lang as title", 'slug')
                                            ->where('id', $resourse_id5)
                                            ->first();
                                        $menu_item_name5 = $row_page5->title;
                                        $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['name'] = $menu_item_name5;
                                        $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['id'] = $row_item5->item_id;
                                        $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['order'] = $row_item5->item_order;
                                        if ($lang == "am") {
                                            $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['slug'] = url('/') . '/page/' . $row_page5->slug;
                                        } else {
                                            $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['slug'] = url('/') . '/' . $lang . '/page/' . $row_page5->slug;
                                        }
                                        break;
                                    }
                                    case 'category': {
                                        $row_page5 = DB::table('categories')
                                            ->select("title_$lang as title", 'slug')
                                            ->where('id', $resourse_id5)
                                            ->first();
                                        $menu_item_name5 = $row_page5->title;
                                        $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['name'] = $menu_item_name5;
                                        $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['id'] = $row_item5->item_id;
                                        $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['order'] = $row_item5->item_order;
                                        if ($lang == "am") {
                                            $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['slug'] = url('/') . '/category/' . $row_page5->slug;
                                        } else {
                                            $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['slug'] = url('/') . '/' . $lang . '/category/' . $row_page5->slug;
                                        }

                                        break;
                                    }
                                    case 'link': {
                                        $row_page5 = DB::table('menu_links')
                                            ->select("link_name_$lang as link_name", "link_href_$lang as link_href")
                                            ->where('link_id', $resourse_id5)
                                            ->first();
                                        $menu_item_name5 = $row_page5->link_name;
                                        $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['name'] = $menu_item_name5;
                                        $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['id'] = $row_item5->item_id;
                                        $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['order'] = $row_item5->item_order;
                                        $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['slug'] = $row_item5->link_href;
                                        break;
                                    }
                                }


                                //errord

                                $id_item_p6 = $row_item5->item_id;


                                $result_items6 = DB::table('menu_items')
                                    ->where('menu_id', 1)
                                    ->where('item_parent', $id_item_p6)
                                    ->orderBy('item_order', 'asc')
                                    ->get();
                                $j6 = 0;
                                foreach ($result_items6 as $row_item6) {
                                    $resourse_id6 = $row_item6->resourse_id;
                                    switch ($row_item6->item_type) {
                                        case 'page': {
                                            $row_page6 = DB::table('pages')
                                                ->select("title_$lang as title", 'slug')
                                                ->where('id', $resourse_id6)
                                                ->first();
                                            $menu_item_name6 = $row_page6->title;
                                            $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['child6'][$j6]['name'] = $menu_item_name6;
                                            $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['child6'][$j6]['id'] = $row_item6->item_id;
                                            $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['child6'][$j6]['order'] = $row_item6->item_order;
                                            if ($lang == "am") {
                                                $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['child6'][$j6]['slug'] = url('/') . '/page/' . $row_page6->slug;
                                            } else {
                                                $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['child6'][$j6]['slug'] = url('/') . '/' . $lang . '/page/' . $row_page6->slug;
                                            }

                                            break;
                                        }
                                        case 'category': {
                                            $row_page6 = DB::table('categories')
                                                ->select("title_$lang as title", 'slug')
                                                ->where('id', $resourse_id6)
                                                ->first();
                                            $menu_item_name6 = $row_page6->title;
                                            $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['child6'][$j6]['name'] = $menu_item_name6;
                                            $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['child6'][$j6]['id'] = $row_item6->item_id;
                                            $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['child6'][$j6]['order'] = $row_item6->item_order;
                                            if ($lang == "am") {
                                                $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['child6'][$j6]['slug'] = url('/') . '/category/' . $row_page6->slug;
                                            } else {
                                                $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['child6'][$j6]['slug'] = url('/') . '/' . $lang . '/category/' . $row_page6->slug;

                                            }
                                            break;
                                        }
                                        case 'link': {
                                            $row_page6 = DB::table('menu_links')
                                                ->select("link_name_$lang as link_name", "link_href_$lang as link_href")
                                                ->where('link_id', $resourse_id6)
                                                ->first();
                                            $menu_item_name6 = $row_page6->link_name;
                                            $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['child6'][$j6]['name'] = $menu_item_name6;
                                            $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['child6'][$j6]['id'] = $row_item6->item_id;
                                            $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['child6'][$j6]['order'] = $row_item6->item_order;
                                            $row_parent_item->child[$j]['child2'][$j2]['child3'][$j3]['child4'][$j4]['child5'][$j5]['child6'][$j6]['slug'] = $row_item6->link_href;
                                            break;
                                        }
                                    }

                                    $j5++;
                                    $row_parent_item->i++;
                                }
                                //errord


                                $j5++;
                                $row_parent_item->i++;
                            }
                            //errord


                            $j4++;
                            $row_parent_item->i++;
                        }
                        //errord


                        $j3++;
                        $row_parent_item->i++;
                    }
                    //errord


                    $j2++;
                    $row_parent_item->i++;
                }
                //errord


                $j++;
                $row_parent_item->i++;
            }

            $row_parent_item->i++;
        }


        return $menu_parent_items;

    }


public function getSocLink($elem){
        $link = '';
        switch ($elem){
            case "fb": {
                $welcome_text = DB::table('home_pages')->select("fb_link as link")->where('id', 1)->first();
                $link = $welcome_text->link;
                break;
            }
            case "youtube": {
                $welcome_text = DB::table('home_pages')->select("youtube_link as link")->where('id', 1)->first();
                $link = $welcome_text->link;
                break;
            }
            case "tw": {
                $welcome_text = DB::table('home_pages')->select("tw_link as link")->where('id', 1)->first();
                $link = $welcome_text->link;
                break;
            }

        }
    return $link;
}

}