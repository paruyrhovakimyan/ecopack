<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class ProjectController extends Controller
{
    public function index($slug)
    {

        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $lang = $first_lang->short;
        $bradcramp = app('App\Http\Controllers\BredcrampController')->getBredcramp($lang, "projects", $slug);

        $current_project_categories = DB::table('project_categories')->where('slug', $slug)->first();

        $pages = DB::table('sub_pages_with_pdfs')->where('project_category', $current_project_categories->id)
            ->where('hidden', 0)
            ->get();

        $sidebar_pages = DB::table('project_categories')
            ->select(
                "id",
                "title_$lang as title",
                "slug"
            )
            ->where('page_parent', $current_project_categories->page_parent)
            ->where('hidden', 0)
            ->get();
        foreach ($sidebar_pages as $sidebar_page) {
            $sidebar_page->link = url('/') . "/projects/" . $sidebar_page->slug;
        }




        $meta_data['meta_title'] = $current_project_categories->{"meta_title_".$lang};
        $meta_data['meta_desc'] = $current_project_categories->{"meta_desc_".$lang};
        $meta_data['meta_key'] = $current_project_categories->{"meta_key_".$lang};

        return view('frontend.project_category', compact('current_project_categories','banner_page','meta_data', 'pages', 'bradcramp', 'lang', 'sidebar_pages', 'slug'));

    }
    public function indexLang($local_lang, $slug)
    {

        $lang = $local_lang;
        $bradcramp = app('App\Http\Controllers\BredcrampController')->getBredcramp($lang, "projects", $slug);

        $current_project_categories = DB::table('project_categories')->where('slug', $slug)->first();

        $pages = DB::table('sub_pages_with_pdfs')->where('project_category', $current_project_categories->id)
            ->where('hidden', 0)
            ->get();

        $sidebar_pages = DB::table('project_categories')
            ->select(
                "id",
                "title_$lang as title",
                "slug"
            )
            ->where('page_parent', $current_project_categories->page_parent)
            ->where('hidden', 0)
            ->get();
        foreach ($sidebar_pages as $sidebar_page) {
            $sidebar_page->link = url('/') . "/projects/" . $sidebar_page->slug;
        }




        $meta_data['meta_title'] = $current_project_categories->{"meta_title_".$lang};
        $meta_data['meta_desc'] = $current_project_categories->{"meta_desc_".$lang};
        $meta_data['meta_key'] = $current_project_categories->{"meta_key_".$lang};

        return view('frontend.project_category', compact('current_project_categories','banner_page','meta_data', 'pages', 'bradcramp', 'lang', 'sidebar_pages', 'slug'));

    }
}
