<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Product;
use App\ProductCat;
use App\CartMeta;
use App\Card;

class ProductController extends Controller
{
    public function index($id)
    {
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $lang = $first_lang->short;

        $product = Product::find($id);
$curr_pr_cats = DB::table('pr_cat_ids')->select("cat_id")->where('pr_id', '=', $id)->get();
        $curr_pr_cats_result = array();
foreach ($curr_pr_cats as $curr_pr_cat){
    $curr_parent_cat = ProductCat::find($curr_pr_cat->cat_id);
    if($curr_parent_cat->parent_cat != 0){
        $curr_pr_cats_result[] = $curr_pr_cat->cat_id;
    }
}
        if (isset($product) && !empty($product)) {

            $product->keys = array_keys((array)json_decode($product->pr_charecters_am));
            $product->vals = array_values((array)json_decode($product->pr_charecters_am));

            $bradcramp = app('App\Http\Controllers\BredcrampController')->getBredcramp($lang, "product", $id);
            $meta_data['meta_title'] = $product->{"title_$lang"};
            $meta_data['meta_desc'] = $product->{"title_$lang"};
            $meta_data['meta_key'] = $product->{"title_$lang"};

            //$related_products = Product::where('product_cat_id', '=', $product->product_cat_id)->where('id', '!=',$id)->limit(7)->get();


            $related_products = DB::table('products')
                ->join('pr_cat_ids', function ($join) use ($curr_pr_cats_result, $id) {
                    $join->on('products.id', '=', 'pr_cat_ids.pr_id')
                        ->where('products.id','!=',$id)->whereIn('pr_cat_ids.cat_id', (array)$curr_pr_cats_result);
                })
                ->paginate(10);
            $array_recomend = array();
            foreach ((array)json_decode($product->recomended) as $rec_cats){
                $array_recomend[] = (int)$rec_cats;
            }
            $recomended_products = DB::table('products')
                ->join('pr_cat_ids', function ($join) use ($array_recomend) {
                    $join->on('products.id', '=', 'pr_cat_ids.pr_id')
                        ->whereIn('pr_cat_ids.cat_id', (array)$array_recomend);
                })
                ->paginate(8);

            $product->images = DB::table('product_gallery')->where('product_id', '=', $id)->get();

            return view('frontend.product', compact('related_products', 'recomended_products', 'bradcramp', 'product', 'lang', 'meta_data'));
        } else {
            return view('frontend.errors.404', compact('lang'));
        }
    }

    public function indexLang($lang, $id)
    {


        $product = Product::find($id);
        if (isset($product) && !empty($product)) {

            $bradcramp = app('App\Http\Controllers\BredcrampController')->getBredcramp($lang, "product", $id);
            $meta_data['meta_title'] = $product->{"title_$lang"};
            $meta_data['meta_desc'] = $product->{"title_$lang"};
            $meta_data['meta_key'] = $product->{"title_$lang"};

            $product->images = DB::table('product_gallery')->where('product_id', '=', $id)->get();

            return view('frontend.product', compact('bradcramp', 'product', 'lang', 'meta_data'));
        } else {
            return view('frontend.errors.404', compact('lang'));
        }
    }

    public function productOrder(Request $request, $id)
    {
        if (isset($request->name_surname) && $request->name_surname != '') {
            $pr_name = $request->pr_name;
            $name = $request->name_surname;
            $email = $request->mail;
            $phone = $request->phone;
            $text = $request->message;
            if ($pr_name != '' && $name != '' && $email != '' && $phone != '' && $text != '' && filter_var($email, FILTER_VALIDATE_EMAIL)) {

                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
                $headers .= 'To: insport.am <info@insport.am>' . "\r\n";
                $headers .= 'From: ' . $name . ' <' . $email . '>' . "\r\n";
                $message = '<p>Name: ' . $name . '</p>';
                $message .= '<p>E-mail: ' . $email . '</p>';
                $message .= '<p>Phone: ' . $phone . '</p>';
                $message .= '<p>Message: ' . $text . '</p>';
                $message .= '<p>Product: ' . $pr_name . '</p>';
                if (mail('paruyrhovakimyan@gmail.com', 'inSport.am', $message, $headers)) {
                    $request->session()->flash('status', 'Message sent!');
                    return redirect(url('/') . "/product/$id");
                } else {
                    $request->session()->flash('status', 'Something wrong!');
                    return redirect(url('/') . "/product/$id");
                }
            }
        }
    }


    public function addToCard(Request $request)
    {
        if ($request) {
            $rate = $request->rate;
            $product_id = $request->product;
            $lang = $request->lang;
            $count = (int)$request->count;
            $product = Product::find($product_id);
            if ($product) {
                $image = $product->image;
                $user_id = session('account');
                $card_id = session('card_id');
                if ($count > 0) {
                    if (session('account') && session('account') > 0) {


                        if (session('card_id') && session('card_id') > 0) {

                            $product_in_cat = CartMeta::where("cart_id", '=', $card_id)->where('product_id', '=', $product_id)->first();

                            if (isset($product_in_cat->product_count) && $product_in_cat->product_count > 0) {
                                $new_count = $product_in_cat->product_count + $count;
                                $product_in_cat->product_count = $new_count;
                                $product_in_cat->save();
                            } else {
                                $new_cart_meta = new CartMeta();
                                $new_cart_meta->product_count = $count;
                                $new_cart_meta->product_id = $product_id;
                                $new_cart_meta->cart_id = $card_id;
                                $new_cart_meta->save();
                            }
                        } else {

                            $result_cart_id = Card::where('user_id', '=', $user_id)->first();
                            if (isset($result_cart_id) && $result_cart_id->cart_id > 0) {
                                $cart_id = $result_cart_id->cart_id;
                            } else {
                                $new_card = new Card();
                                $new_card->user_id = $user_id;
                                $new_card->save();
                                $card_id = $new_card->id;
                            }
                            session(['card_id' => $card_id]);
                            $new_card_meta__product = new CartMeta();
                            $new_card_meta__product->product_count = $count;
                            $new_card_meta__product->product_id = $product_id;
                            $new_card_meta__product->cart_id = $card_id;
                            $new_card_meta__product->save();
                        }
                    } else {

                        if (session('card_id') && session('card_id') > 0) {
                            $card_id = session('card_id');
                            $product_in_cat = CartMeta::where("cart_id", '=', $card_id)->where('product_id', '=', $product_id)->first();

                            if (isset($product_in_cat->product_count) && $product_in_cat->product_count > 0) {
                                $new_count = $product_in_cat->product_count + $count;
                                $product_in_cat->product_count = $new_count;
                                $product_in_cat->save();
                            } else {
                                $new_cart_meta = new CartMeta();
                                $new_cart_meta->product_count = $count;
                                $new_cart_meta->product_id = $product_id;
                                $new_cart_meta->cart_id = $card_id;
                                $new_cart_meta->save();
                            }
                        } else {
                            $new_card = new Card();
                            $new_card->user_id = 0;
                            $new_card->save();
                            $card_id = $new_card->id;
                            session(['card_id' => $card_id]);
                            $new_card_meta__product = new CartMeta();
                            $new_card_meta__product->product_count = $count;
                            $new_card_meta__product->product_id = $product_id;
                            $new_card_meta__product->cart_id = $card_id;
                            $new_card_meta__product->save();
                        }
                    }


                    $cart_id = (int)session('card_id');
                    $result_cart_meta_show = CartMeta::where("cart_id", '=', $cart_id)->get();

                    $all_price = 0;

                    $all_count = 0;

                    foreach ($result_cart_meta_show as $row_cart_meta_show) {
                        $all_count += $row_cart_meta_show->product_count;
                        $prods_id = (int)$row_cart_meta_show->product_id;
                        $pr_info = Product::find($prods_id);

                        if($pr_info->pr_new_price > 0){
                            $price = $pr_info->pr_new_price;
                        }else{
                            $price = $pr_info->pr_price;
                        }

                        $all_price += $price * $row_cart_meta_show->product_count;
                        if ($row_cart_meta_show->product_count == 0) {
                            CartMeta::where("cart_id", '=', $cart_id)->where("product_id", '=', $prods_id)->delete();

                        }
                    }
                } else {
                    $card_id = session('card_id');
                    CartMeta::where("cart_id", '=', $card_id)->where("product_id", '=', $product_id)->delete();
                }
                $result_array = array();
                $result_array['all'] = app('App\Http\Controllers\CardController')->getCount();
                $result_array['content'] = app('App\Http\Controllers\CardController')->getCardContent($lang);
                return $result_array;

            }
        }


    }


    public function updateCard(Request $request){
        if ($request) {
            $rate = $request->rate;
            $product_id = $request->product;
            $lang = $request->lang;
            $count = (int)$request->count;
            $product = Product::find($product_id);
            $card_id = session('card_id');
            if ($count > 0){

                $product_in_cat = CartMeta::where("cart_id", '=', $card_id)->where('product_id', '=', $product_id)->first();

                $product_in_cat->product_count = $count;
                $product_in_cat->save();
            } else {
                $card_id = session('card_id');
                CartMeta::where("cart_id", '=', $card_id)->where("product_id", '=', $product_id)->delete();
            }
            $result_array = array();
            $result_array['all'] = app('App\Http\Controllers\CardController')->getCount();
            $result_array['content'] = app('App\Http\Controllers\CardController')->getCardContent($lang);
            return $result_array;

        }

    }

}
