<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    public function index($slug){

            $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
            $lang = $first_lang->short;
            $category_info = DB::table('categories')
                ->select(
                    "title_$lang as title",
                    "description_$lang as description",
                    "id",
                    "slug",
                    "meta_title_$lang as meta_title",
                    "meta_desc_$lang as meta_desc",
                    "meta_key_$lang as meta_key",
                    "page_parent"

                )
                ->where("slug", $slug)->first();

            $cat_title = $category_info->title;
            $cat_id = $category_info->id;
            $page_parent = $category_info->page_parent;

            $sidebar_pages = DB::table('categories')
                ->select("id","title_$lang as title","slug")
                ->where('page_parent',$page_parent)
                ->get();
        $sidebar_pag_posts = DB::table('news')->select("title_$lang as title", "image", "slug","cat_id")
            ->where('cat_id', '=', 23)
            ->limit(3)
            ->get();
            $bradcramp = app('App\Http\Controllers\BredcrampController')->getBredcramp($lang,"category",$slug);

            if($slug == "news"){
                $category_news = DB::table('news')->select("cat_id","short_desc_$lang as short_desc","title_$lang as title","id","created_at","image","date1","date2","slug")
                    ->where('cat_id','!=', 11)
                    ->orderBy('date1', 'desc')
                    ->paginate(6);
            }else{
                if($category_info->id == 19){
                    $category_news = DB::table('news')->select("cat_id","short_desc_$lang as short_desc","title_$lang as title","id","created_at","image","date1","date2","slug")
                       ->orderBy('date1', 'desc')
                        ->paginate(6);
                }else{
                    $category_news = DB::table('news')->select("cat_id","short_desc_$lang as short_desc","title_$lang as title","id","created_at","image","date1","date2","slug")
                        ->where('cat_id', $cat_id)
                        ->orderBy('date1', 'desc')
                        ->paginate(6);
                }

            }

            $meta_data['meta_title'] = $category_info->meta_title;
            $meta_data['meta_desc'] = $category_info->meta_desc;
            $meta_data['meta_key'] = $category_info->meta_key;
            return view('frontend.category.list', compact('sidebar_pag_posts','meta_data',"slug",'category_info','sidebar_pages', 'category_news','bradcramp', 'lang'));


    }
    public function indexLang($local_lang, $slug){
        $lang = $local_lang;



        $category_info = DB::table('categories')
            ->select(
                "title_$lang as title",
                "description_$lang as description",
                "id",
                "slug",
                "meta_title_$lang as meta_title",
                "meta_desc_$lang as meta_desc",
                "meta_key_$lang as meta_key",
                "page_parent"

            )
            ->where("slug", $slug)->first();

        $cat_title = $category_info->title;
        $cat_id = $category_info->id;
        $page_parent = $category_info->page_parent;
        $sidebar_pages = DB::table('categories')
            ->select("id","title_$lang as title","slug")
            ->where('page_parent',$page_parent)
            ->get();
        $sidebar_pag_posts = DB::table('news')->select("cat_id","title_$lang as title", "image", "slug")
            ->where('cat_id', '=', 23)
            ->limit(3)
            ->get();
        $bradcramp = app('App\Http\Controllers\BredcrampController')->getBredcramp($lang,"category",$slug);

        if($slug == "news"){
            $category_news = DB::table('news')->select("cat_id","short_desc_$lang as short_desc","title_$lang as title","id","created_at","image","date1","date2","slug")
                ->where('cat_id','!=', 11)
                ->orderBy('date1', 'desc')
                ->paginate(6);
        }else{
            $category_news = DB::table('news')->select("cat_id","short_desc_$lang as short_desc","title_$lang as title","id","created_at","image","date1","date2","slug")
                ->where('cat_id', $cat_id)
                ->orderBy('date1', 'desc')
                ->paginate(6);
        }

        $meta_data['meta_title'] = $category_info->meta_title;
        $meta_data['meta_desc'] = $category_info->meta_desc;
        $meta_data['meta_key'] = $category_info->meta_key;
        return view('frontend.category.list', compact('sidebar_pag_posts','meta_data',"slug",'category_info','sidebar_pages', 'category_news','bradcramp', 'lang'));

    }

    public function getPermalink($slug, $curr_lang){
        $category_news = DB::table('news')->select("cat_id")
            ->where('slug', $slug)->first();
        $category_slug = DB::table('categories')->select("slug")
            ->where('id', $category_news->cat_id)->first();
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $lang = $first_lang->short;
        if($curr_lang == $lang){
            $permalink = url('/')."/category/".$category_slug->slug."/".$slug;
        }else{
            $permalink = url('/')."/".$curr_lang."/category/".$category_slug->slug."/".$slug;
        }

        return $permalink;
    }
}