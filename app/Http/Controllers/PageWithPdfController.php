<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class PageWithPdfController extends Controller
{
    public function index($slug)
    {

        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $lang = $first_lang->short;


        $page = DB::table('sub_pages_with_pdfs')
            ->select(
                "id",
                "title_$lang as title",
                "description_$lang as description",
                "short_desc_$lang as short_desc",
                "meta_title_$lang as meta_title",
                "meta_desc_$lang as meta_desc",
                "meta_key_$lang as meta_key",
                "image_main",
                "date1",
                "date2",
                "need_donate",
                "parent_id"
            )
            ->where('slug', $slug)
            ->where('hidden', 0)
            ->first();

        if(isset($page->id) && is_numeric($page->id)){
            $bradcramp = app('App\Http\Controllers\BredcrampController')->getBredcramp($lang, "page_pdf", $slug);
            if ($page->parent_id != 0) {
                $parent = $page->parent_id;
            } else {
                $parent = $page->id;
            }
            $page->gallery = '';
            $gallery_images = DB::table('sub_pages_with_pdfs_gallery')->where('parent_id',$page->id)->get();
            $page->gallery = $gallery_images;


            $page->sub_folders = '';

            $child_sub_pdfs_folder = DB::table("sub_pages_with_pdfs")
                ->select("title_$lang as title","description_$lang as description","id","hidden")
                ->where('folder_parent',$page->id)
                ->get();
            if (!empty($child_sub_pdfs_folder)){
                $page->sub_folders = $child_sub_pdfs_folder;

                foreach ($child_sub_pdfs_folder as $item) {
                    $item->files = '';
                    $item_page_pdfs = DB::table('sub_page_with_pdf_files')->where("sub_page_id", $item->id)->get();
                    if (!empty($item_page_pdfs)) {
                        $item->files = $item_page_pdfs;
                    }



                }

            }



            //Gallery
            $page->sub_gallery_folders = '';
            $page_gallery = DB::table("gallery_infos")->select("hidden","id","title_$lang as title","short_desc_$lang as short_desc")->where('hidden',0)->where('sub_parent_id',$page->id)->get();
            foreach ($page_gallery as $gallery){
                $gallery_images = DB::table('gallery')->where('sub_parent_id',$gallery->id)->get();
                $gallery->images = $gallery_images;
                $page->sub_gallery_folders = $page_gallery;
            }


            $banner_page = DB::table('pages')
                ->select(
                    "id",
                    "title_$lang as title",
                    "short_desc_$lang as short_desc",
                    "slug",
                    "image_bg"
                )
                ->where('id', $parent)
                ->where('hidden', 0)
                ->first();


            $sidebar_pages = DB::table('pages')
                ->select(
                    "id",
                    "title_$lang as title",
                    "short_desc_$lang as short_desc",
                    "slug"
                )
                ->where('parent', $parent)
                ->where('hidden', 0)
                ->get();
            foreach ($sidebar_pages as $sidebar_page) {
                $sidebar_page->link = url('/') . "/page/" . $sidebar_page->slug;
            }

            $page->parent_page_pdfs = '';
            $parent_page_pdfs = DB::table('sub_page_with_pdf_files')->where("sub_page_id", $page->id)->get();
            if (!empty($parent_page_pdfs)) {
                $page->parent_page_pdfs = $parent_page_pdfs;
            }

            $sidebar_pages1 = '';
            $sidebar_pages1_files = DB::table('sub_pages_with_pdfs')
                ->select(
                    "id",
                    "title_$lang as title",
                    "slug"
                )
                ->where('parent_id', $parent)
                ->where('hidden', 0)
                ->get();
            if (!empty($sidebar_pages1_files)) {

                foreach ($sidebar_pages1_files as $sidebar_pages1_file) {
                    $sidebar_pages1_file->link = url('/') . "/page_pdf/" . $sidebar_pages1_file->slug;
                }
                $sidebar_pages1 = $sidebar_pages1_files;
            }
            if($page->meta_title !=''){
                $meta_data['meta_title'] = $page->meta_title;
            }else{
                $meta_data['meta_title'] = $page->title;
            }
            $meta_data['meta_desc'] = $page->meta_desc;
            $meta_data['meta_key'] = $page->meta_key;
            $meta_data['meta_image'] = url('/')."/uploads/page/".$page->image_main;
            $meta_data['meta_url'] = url()->current();

            return view('frontend.page_pdf', compact('banner_page','meta_data', 'page', 'bradcramp', 'lang', 'sidebar_pages', 'sidebar_pages1', 'slug'));

        }else{
            return view('frontend.errors.404', compact( 'lang'));
        }



    }
    public function indexLang($local_lang, $slug)
    {

        $lang = $local_lang;


        $page = DB::table('sub_pages_with_pdfs')
            ->select(
                "id",
                "title_$lang as title",
                "description_$lang as description",
                "short_desc_$lang as short_desc",
                "meta_title_$lang as meta_title",
                "meta_desc_$lang as meta_desc",
                "meta_key_$lang as meta_key",
                "image_main",
                "date1",
                "date2",
                "need_donate",
                "parent_id"
            )
            ->where('slug', $slug)
            ->where('hidden', 0)
            ->first();

        if(isset($page->id) && is_numeric($page->id)){
            $bradcramp = app('App\Http\Controllers\BredcrampController')->getBredcramp($lang, "page_pdf", $slug);
            if ($page->parent_id != 0) {
                $parent = $page->parent_id;
            } else {
                $parent = $page->id;
            }
            $page->gallery = '';
            $gallery_images = DB::table('sub_pages_with_pdfs_gallery')->where('parent_id',$page->id)->get();
            $page->gallery = $gallery_images;

            $page->sub_folders = '';
            $child_sub_pdfs_folder = DB::table("sub_pages_with_pdfs")
                ->select("title_$lang as title","description_$lang as description","id","hidden")
                ->where('folder_parent',$page->id)
                ->get();
            if (!empty($child_sub_pdfs_folder)){
                $page->sub_folders = $child_sub_pdfs_folder;

                foreach ($child_sub_pdfs_folder as $item) {
                    $item->files = '';
                    $item_page_pdfs = DB::table('sub_page_with_pdf_files')->where("sub_page_id", $item->id)->get();
                    if (!empty($item_page_pdfs)) {
                        $item->files = $item_page_pdfs;
                    }



                }

            }
//Gallery
            $page->sub_gallery_folders = '';
            $page_gallery = DB::table("gallery_infos")->select("hidden","id","title_$lang as title","short_desc_$lang as short_desc")->where('hidden',0)->where('sub_parent_id',$page->id)->get();
            foreach ($page_gallery as $gallery){
                $gallery_images = DB::table('gallery')->where('sub_parent_id',$gallery->id)->get();
                $gallery->images = $gallery_images;
                $page->sub_gallery_folders = $page_gallery;
            }
            $banner_page = DB::table('pages')
                ->select(
                    "id",
                    "title_$lang as title",
                    "short_desc_$lang as short_desc",
                    "slug",
                    "image_bg"
                )
                ->where('id', $parent)
                ->where('hidden', 0)
                ->first();


            $sidebar_pages = DB::table('pages')
                ->select(
                    "id",
                    "title_$lang as title",
                    "short_desc_$lang as short_desc",
                    "slug"
                )
                ->where('parent', $parent)
                ->where('hidden', 0)
                ->get();
            foreach ($sidebar_pages as $sidebar_page) {
                $sidebar_page->link = url('/') . "/page/" . $sidebar_page->slug;
            }

            $page->parent_page_pdfs = '';
            $parent_page_pdfs = DB::table('sub_page_with_pdf_files')->where("sub_page_id", $page->id)->get();
            if (!empty($parent_page_pdfs)) {
                $page->parent_page_pdfs = $parent_page_pdfs;
            }

            $sidebar_pages1 = '';
            $sidebar_pages1_files = DB::table('sub_pages_with_pdfs')
                ->select(
                    "id",
                    "title_$lang as title",
                    "slug"
                )
                ->where('parent_id', $parent)
                ->where('hidden', 0)
                ->get();
            if (!empty($sidebar_pages1_files)) {

                foreach ($sidebar_pages1_files as $sidebar_pages1_file) {
                    $sidebar_pages1_file->link = url('/') . "/page_pdf/" . $sidebar_pages1_file->slug;
                }
                $sidebar_pages1 = $sidebar_pages1_files;
            }
            if($page->meta_title !=''){
                $meta_data['meta_title'] = $page->meta_title;
            }else{
                $meta_data['meta_title'] = $page->title;
            }
            $meta_data['meta_desc'] = $page->meta_desc;
            $meta_data['meta_key'] = $page->meta_key;
            $meta_data['meta_image'] = url('/')."/uploads/page/".$page->image_main;
            $meta_data['meta_url'] = url()->current();

            return view('frontend.page_pdf', compact('banner_page','meta_data', 'page', 'bradcramp', 'lang', 'sidebar_pages', 'sidebar_pages1', 'slug'));

        }else{
            return view('frontend.errors.404', compact( 'lang'));
        }



    }
}
