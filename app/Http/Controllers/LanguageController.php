<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LanguageController extends Controller
{

    public function language_switcher($code){
        $url = $_SERVER['REQUEST_URI'];
        $url_array = explode("/",$url);
        if($code=='am'){
            $code='';
            $return = url('/').'/';
            if($url_array[1]=='ru' || $url_array[1]=='en'){$skizb = 2;} else {$skizb = 1;}
            for($i=$skizb;$i<count($url_array);$i++){
                $return .= $url_array[$i]."/";
            }
        }
        else{
            $return = url('/').'/'.$code."/";
            if($url_array[1]=='ru' || $url_array[1]=='en'){$skizb = 2;} else {$skizb = 1;}
            for($i=$skizb;$i<count($url_array);$i++){
                $return .= $url_array[$i]."/";
            }
        }

        return substr($return,0,-1);
    }

}
