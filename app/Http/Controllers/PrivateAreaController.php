<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class PrivateAreaController extends Controller
{
    public function login(Request $request){

        $username = $request->usename;
        $password = $request->password;

        $private_area = DB::table('private_area_config')
            ->where('login',$username)
            ->where('password', $password)
            ->where('hidden', 0)
            ->first();
        if(!empty($private_area)){

            $private_area_files = DB::table('private_area_pdf')->where('private_area_id', $private_area->id)->where('hidden', 0)->get();

            $private_area_block = '
<div class="private_box">
<div class="page_left_title">
                <h2>'. $private_area->title.'</h2>
                <hr class="title_bottom">
            </div>
                        <div class="page_content">'.$private_area->description.'</div> ';

            if(!empty($private_area_files)){
                foreach ($private_area_files as $private_area_file) {
                    $private_area_block .='<div class="one_pdf_block">
                        <img title="'.$private_area_file->title.'" class="pdf_icon" src="'.url("/").'/img/pdf_icon.png" >
                        <div class="page_pdf_right">
                            <a title="'.$private_area_file->title.'" class="pdf_title" target="_blank" href="'.url("/").'/uploads/private_area_pdf/'.$private_area_file->file.'">'.$private_area_file->title.'</a>
                            <p class="pdf_desc">'.$private_area_file->short_desc.'</p>
                        </div>
                    </div>';
                }
            }
            $private_area_block .= '</div>';
            return $private_area_block;

        }else{
            return "false";
        }


    }

}
