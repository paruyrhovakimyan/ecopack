<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class RegistrationController extends Controller
{

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'surname' => 'required',
            'sex' => 'required',
            'email' => 'required',
            'password' => 'required|min:3|max:20|confirmed',
        ]);
    }


    public function index(){
        $menu_parent_items = DB::table('menu_items')
            ->where('menu_id', 1)
            ->where('item_parent', 0)
            ->orderBy('item_order', 'asc')
            ->get();
        foreach ($menu_parent_items as $row_parent_item){
            $row_parent_item->child[] = array();
            $row_parent_item->i=1;
            $resourse_id = $row_parent_item->resourse_id;
            switch($row_parent_item->item_type){
                case 'page':{
                    $row_page = DB::table('pages')
                        ->select('title')
                        ->where('id', $resourse_id)
                        ->first();
                    $menu_item_name = $row_page->title;
                    $row_parent_item->name = $menu_item_name;
                    break;
                }
                case 'link':{
                    $row_page = DB::table('menu_links')
                        ->select('link_name')
                        ->where('link_id', $resourse_id)
                        ->first();
                    $menu_item_name = $row_page->link_name;
                    $row_parent_item->name = $menu_item_name;
                    break;
                }
            }


            $id_item_p = $row_parent_item->item_id;


            $result_items1 = DB::table('menu_items')
                ->where('menu_id', 1)
                ->where('item_parent', $id_item_p)
                ->orderBy('item_order', 'asc')
                ->get();
            $j = 0;
            foreach ($result_items1 as $row_item1) {
                $resourse_id1 = $row_item1->resourse_id;
                switch($row_item1->item_type){
                    case 'page':{
                        $row_page = DB::table('pages')
                            ->select('title')
                            ->where('id', $resourse_id1)
                            ->first();
                        $menu_item_name = $row_page->title;
                        $row_parent_item->child[$j]['name'] = $menu_item_name;
                        $row_parent_item->child[$j]['id'] = $row_item1->item_id;
                        $row_parent_item->child[$j]['order'] = $row_item1->item_order;
                        break;
                    }
                    case 'link':{
                        $row_page = DB::table('menu_links')
                            ->select('link_name')
                            ->where('link_id', $resourse_id1)
                            ->first();
                        $menu_item_name = $row_page->link_name;
                        $row_parent_item->child[$j]['name'] = $menu_item_name;
                        $row_parent_item->child[$j]['id'] = $row_item1->item_id;
                        $row_parent_item->child[$j]['order'] = $row_item1->item_order;
                        break;
                    }
                }
                $j++;
                $row_parent_item->i++;
            }

            $row_parent_item->i++;
        }
        return view('frontend.auth.register', compact('menu_parent_items'));
    }

    public function register(Request $request){
        if(isset($request->register_add) && $request->register_add =="ok"){
            $this->validator($request->all())->validate();

            $name = $request->name;
            $surname = $request->surname;
            $mail = $request->email;
            $sex = $request->sex;
            $password = bcrypt($request->sex);

            $id = DB::table('users')->insertGetId(
                [
                    'name' => $name,
                    'surname' => $surname,
                    'sex' => $sex,
                    'email' => $mail,
                    'password' => $password,
                ]
            );
            session(['account' => $id]);
            return redirect('/account');

        }
        return redirect('/registration');
    }
}
