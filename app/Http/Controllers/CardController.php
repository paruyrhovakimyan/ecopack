<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Product;
use App\CartMeta;
use App\Card;
use App\UserOrder;
use App\OrderMeta;
use App\User;

class CardController extends Controller
{

    public function index(){
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $lang = $first_lang->short;
        $cart_id = (int)session('card_id');
        $result_cart_meta_show = '';
        $all_price_end = 0;
        if($cart_id){
            $result_cart_meta_show = CartMeta::where("cart_id", '=',$cart_id)->get();
            foreach ($result_cart_meta_show as $row_cart_meta_show){
                $pr_id = $row_cart_meta_show->product_id;
                $row_cart_meta_show->meta_product = Product::find($pr_id);
                $all_price_end += $row_cart_meta_show->product_count * $row_cart_meta_show->meta_product->pr_price;
            }
        }
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();
        return view('frontend.card', compact('all_price_end','result_cart_meta_show','languages', 'lang'));

    }
    public function indexLang($lang){
        $cart_id = (int)session('card_id');
        $result_cart_meta_show = '';
        $all_price_end = 0;
        if($cart_id){
            $result_cart_meta_show = CartMeta::where("cart_id", '=',$cart_id)->get();
            foreach ($result_cart_meta_show as $row_cart_meta_show){
                $pr_id = $row_cart_meta_show->product_id;
                $row_cart_meta_show->meta_product = Product::find($pr_id);
                $all_price_end += $row_cart_meta_show->product_count * $row_cart_meta_show->meta_product->pr_price;
            }
        }
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();
        return view('frontend.card', compact('all_price_end','result_cart_meta_show','languages', 'lang'));

    }

    public function checkout(){

        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $lang = $first_lang->short;
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();

        if(session('account')){
            $cart_id = (int)session('card_id');
            $card_info = Card::find($cart_id);
            $user_info = User::find($card_info->user_id);

            $result_cart_meta_show = '';
            $all_price_end = 0;
            $all_count_end = 0;
            if($cart_id){
                $result_cart_meta_show = CartMeta::where("cart_id", '=',$cart_id)->get();
                foreach ($result_cart_meta_show as $row_cart_meta_show){
                    $pr_id = $row_cart_meta_show->product_id;
                    $row_cart_meta_show->meta_product = Product::find($pr_id);
                    $all_price_end += $row_cart_meta_show->product_count * $row_cart_meta_show->meta_product->pr_price;
                    $all_count_end += $row_cart_meta_show->product_count;
                }
            }
            return view('frontend.checkout.checkoutLogedin', compact('user_info','all_count_end','all_price_end','languages', 'lang'));

        }else{
            return view('frontend.checkout', compact('languages', 'lang'));
        }


    }


    public function getCount(){
        $cart_id = (int)session('card_id');
        if($cart_id){
            $data = CartMeta::where("cart_id", '=',$cart_id)->sum('product_count');
        }else{
            $data = 0;
        }

        return $data;
    }

    public function getCardContent($lang){
        $cart_id = (int)session('card_id');
        $all_price = 0;
        $result ='<div class="block-subtitle hidden-xs">Ձեր զամբյուղը դատարկ է</div>';
        if($cart_id){

            $result_cart_meta_show = CartMeta::where("cart_id", '=',$cart_id)->get();
            if(count($result_cart_meta_show) > 0){
                $result = '<ul id="cart-sidebar" class="mini-products-list">';
                foreach ($result_cart_meta_show as $row_cart_meta_show){
                    $prods_id = (int)$row_cart_meta_show->product_id;
                    $pr_info = Product::find($prods_id);
                    if($pr_info->pr_new_price > 0){
                        $price = $pr_info->pr_new_price;
                    }else{
                        $price = $pr_info->pr_price;
                    }
                    $all_price += $price * $row_cart_meta_show->product_count;
                    $prod_title = $pr_info->{"title_".$lang};


                    $result .='<li class="item odd del'.$prods_id.'">
                                <a href="shopping_cart.html" title="'.$prod_title.'" class="product-image">
                                    <img src="'.url('/').'/public/uploads/product/'.$pr_info->image .'" alt="html Template" width="65">
                                </a>
                                <div class="product-details">
                                    <a data-id="'.$prods_id.'" data-lang="'.$lang.'" data-count="0" href="#" title="'.$prod_title.'" id="kupit_knopka" class="remove-cart"><i class="pe-7s-close"></i></a>
                                    <p class="product-name"><a href="http://prof.mycard.am/"> '.$prod_title.' </a> </p>
                                    <strong>'.$row_cart_meta_show->product_count.'</strong> x <span class="price">'.$price.'</span>
                                    <div class="top-subtotal">
                                        Ընդհանուր (AMD): <span class="price">'.$price * $row_cart_meta_show->product_count.'</span>
                                    </div>
                                </div>
                            </li>';
                }
                $result .='<div class="all_price"> <span class="all_price_number">Ընդհանուր   '.$all_price.' </span> AMD</div></ul>
<div class="actions">
                                                            <a href="'.url('/').'/checkout"><button class="btn-checkout" type="button"><i class="fa fa-check"></i><span>Պատվերի ձևակերպում</span></button></a>
                                                            <a href="'.url('/').'/card"><button class="view-cart" type="button"><i class="fa fa-shopping-cart"></i> <span>Գնումների զամբյուղ</span></button></a>
                                                        </div>
';
            }

        }
        return $result;
    }


    public function guest(){
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $lang = $first_lang->short;
        $cart_id = (int)session('card_id');
        $result_cart_meta_show = '';
        $all_price_end = 0;
        $all_count_end = 0;
        if($cart_id){
            $result_cart_meta_show = CartMeta::where("cart_id", '=',$cart_id)->get();
            foreach ($result_cart_meta_show as $row_cart_meta_show){
                $pr_id = $row_cart_meta_show->product_id;
                $row_cart_meta_show->meta_product = Product::find($pr_id);
                $all_price_end += $row_cart_meta_show->product_count * $row_cart_meta_show->meta_product->pr_price;
                $all_count_end += $row_cart_meta_show->product_count;
            }
        }
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();
        return view('frontend.checkout.guest', compact('all_count_end','all_price_end','languages', 'lang'));
    }


    public function guestRequest(Request $request){

        $this->validator($request->all())->validate();
        $order = new UserOrder();


        $cart_id = (int)session('card_id');
        if(session('account')){
            $user_id = session('account');
        }else{
            $user_id = 0;
        }

        $address = $request->address;
        if($request->order_street !=''){
            $address .=", Շենք ".$request->order_street;
        }
        if($request->order_mutq !=''){
            $address .=", Մուտք ".$request->order_mutq;
        }
        if($request->order_hark !=''){
            $address .=", Հարկ ".$request->order_hark;
        }
        if($request->order_home !=''){
            $address .=", Բնակարան ".$request->order_home;
        }
        $order->order_name = $request->order_name;
        $order->order_email = $request->order_email;
        $order->order_phone = $request->order_phone;
        $order->user_id = $user_id;


        $order->order_adress = $address;
        $order->order_status = 0;
        $order->order_comment = $request->order_comment;
        $order->save();

        $order_id = $order->id;
        $cart_id = (int)session('card_id');

        $result_cart_meta_show = CartMeta::where("cart_id", '=',$cart_id)->get();
        foreach ($result_cart_meta_show as $row_cart_meta_show){
            $orderMeta = new OrderMeta();
            $orderMeta->order_id = $order_id;
            $orderMeta->product_id = $row_cart_meta_show->product_id;
            $orderMeta->product_count = $row_cart_meta_show->product_count;
            $orderMeta->save();
        }


        return redirect(url('/').'/success');

    }



    public function checkoutRegistration(){
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $lang = $first_lang->short;
        $cart_id = (int)session('card_id');
        $result_cart_meta_show = '';
        $all_price_end = 0;
        $all_count_end = 0;
        if($cart_id){
            $result_cart_meta_show = CartMeta::where("cart_id", '=',$cart_id)->get();
            foreach ($result_cart_meta_show as $row_cart_meta_show){
                $pr_id = $row_cart_meta_show->product_id;
                $row_cart_meta_show->meta_product = Product::find($pr_id);
                $all_price_end += $row_cart_meta_show->product_count * $row_cart_meta_show->meta_product->pr_price;
                $all_count_end += $row_cart_meta_show->product_count;
            }
        }
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();
        return view('frontend.checkout.checkoutRegistration', compact('all_count_end','all_price_end','languages', 'lang'));
    }



    public function checkoutLogedin(){
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $lang = $first_lang->short;
        $cart_id = (int)session('card_id');

        $card_info = Card::find($cart_id);
        $user_info = User::find($card_info->user_id);

        $result_cart_meta_show = '';
        $all_price_end = 0;
        $all_count_end = 0;
        if($cart_id){
            $result_cart_meta_show = CartMeta::where("cart_id", '=',$cart_id)->get();
            foreach ($result_cart_meta_show as $row_cart_meta_show){
                $pr_id = $row_cart_meta_show->product_id;
                $row_cart_meta_show->meta_product = Product::find($pr_id);
                $all_price_end += $row_cart_meta_show->product_count * $row_cart_meta_show->meta_product->pr_price;
                $all_count_end += $row_cart_meta_show->product_count;
            }
        }
        $languages = DB::table('languages')->where('hidden', '=', 0)->get();
        return view('frontend.checkout.checkoutLogedin', compact('user_info','all_count_end','all_price_end','languages', 'lang'));

    }


    public function checkoutRegistrationRequest(Request $request){
        $this->validatorRegister($request->all())->validate();
        $order = new UserOrder();

        $newUser = new User();
        $cart_id = (int)session('card_id');


        $newUser->name = $request->order_name;
        $newUser->email = $request->email;
        $newUser->password = bcrypt($request->password);
        $newUser->save();
        $user_id = $newUser->id;
        $address = $request->address;
        if($request->order_street !=''){
            $address .=", Շենք ".$request->order_street;
        }
        if($request->order_mutq !=''){
            $address .=", Մուտք ".$request->order_mutq;
        }
        if($request->order_hark !=''){
            $address .=", Հարկ ".$request->order_hark;
        }
        if($request->order_home !=''){
            $address .=", Բնակարան ".$request->order_home;
        }
        $order->order_name = $request->order_name;
        $order->order_email = $request->email;
        $order->order_phone = $request->order_phone;
        $order->user_id = $user_id;


        $order->order_adress = $address;
        $order->order_status = 0;
        $order->order_comment = $request->order_comment;
        $order->save();

        $order_id = $order->id;
        $cart_id = (int)session('card_id');

        $result_cart_meta_show = CartMeta::where("cart_id", '=',$cart_id)->get();
        foreach ($result_cart_meta_show as $row_cart_meta_show){
            $orderMeta = new OrderMeta();
            $orderMeta->order_id = $order_id;
            $orderMeta->product_id = $row_cart_meta_show->product_id;
            $orderMeta->product_count = $row_cart_meta_show->product_count;
            $orderMeta->save();
        }


        return redirect(url('/').'/success');

    }


    public function checkoutLogin(Request $request){
        $this->validateLogin($request);
        $username = $request->email;
        $password = $request->password;
        $login_user = DB::table('users')
            ->where('email', '=', $username)
            ->first();



        /** @var TYPE_NAME $login_user */
        if ($login_user && password_verify($password, $login_user->password)) {
            session(['account' => $login_user->id]);
            $user_id =  $login_user->id;
            $cart_id = (int)session('card_id');
            $updateCard = Card::find($cart_id);
            $updateCard->user_id = $user_id;
            $updateCard->save();
            return redirect('/checkout-logedin');
        } else {
            $request->session()->flash('error_login', 'Invalid user login or password');
            return redirect('/checkout');
        }
    }





    protected function validator(array $data)
    {
        return Validator::make($data, [
            'order_name' => 'string|required',
            'order_email' => 'required|string|email|max:255',
            'order_phone' => 'required',
            'address' => 'string|required',
        ]);
    }
    protected function validatorRegister(array $data)
    {
        return Validator::make($data, [
            'order_name' => 'string|required',
            'email' => 'required|string|email|max:255|unique:users',
            'order_phone' => 'required',
            'address' => 'string|required',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->email() => 'required|string|email',
            'password' => 'required|string',
        ]);
    }
    public function email()
    {
        return 'email';
    }

}