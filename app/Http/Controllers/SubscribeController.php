<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SubscribeController extends Controller
{
    public function index(Request $request){
        if(isset($request->subscribe_now) && $request->subscribe_now == "ok"){
            $email = $request->subscribe_email;
            $subscribe_name = $request->subscribe_name;
            $subscribe_country = $request->subscribe_country;
            $subscribe_institution = $request->subscribe_institution;

            DB::table('users')->insert(
                [
                    'name' => $subscribe_name,
                    'email' => $email,
                    'country' => $subscribe_country,
                    'institution' => $subscribe_institution

                ]
            );
        }
        $request->session()->flash('subscribe_status', 'Task was successful!');
        return redirect("/");

    }

}
