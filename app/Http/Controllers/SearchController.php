<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{

    public function index(Request $request){


        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $lang = $first_lang->short;
        $search_word = $request->stext;
        $bredcramp = app('App\Http\Controllers\BredcrampController')->getBredcramp($lang,"search",$search_word);

        if(isset($search_word) && strlen($search_word) >= 3 ){

            $articles = DB::table('news')
                ->select("id","cat_id","title_$lang as title","short_desc_am as short_desc","slug","meta_title_$lang as meta_title",
                    "meta_desc_$lang as meta_desc",
                    "meta_key_$lang as meta_key")
                ->where("title_$lang", 'like', "%$search_word%")
                ->orWhere("short_desc_$lang", 'like', "%$search_word%")
                ->orWhere("description_$lang", 'like', "%$search_word%")
                ->orWhere("tag", 'like', "%$search_word%")
                ->get();

            $services  = DB::table('sub_pages_with_pdfs')
                ->select(
                    "id",
                    "title_$lang as title",
                    "slug",
                    "parent_id",
                    "project_category"
                )
                ->where("title_$lang", 'like', "%$search_word%")
                ->orWhere("short_desc_$lang", 'like', "%$search_word%")
                ->orWhere("description_$lang", 'like', "%$search_word%")
                ->where('hidden', 0)
                ->get();
            foreach ($services as $service) {
                $service_box_title = DB::table('project_categories')
                    ->select("title_$lang as title","slug","page_parent")
                    ->where("id", $service->project_category)->first();

                if($service_box_title->page_parent != 0){
                    $parent_page = DB::table('pages')
                        ->select("title_$lang as title","slug")
                        ->where("id", $service_box_title->page_parent)->first();
                    $parent_page = '<small> > </small><span>'.$parent_page->title.'</span>';
                }else{
                    $parent_page = '';
                }

                $service->result_title = '<span class="link_href">'.app('App\Http\Controllers\GlossaryController')->getTranslate("home",$lang).'</span>'.$parent_page.'<small> > </small>'. $service_box_title->title.' > '.$service->title;
                $service->permalink = url('/').'/page_pdf/'.$service_box_title->slug.'/'.$service->slug;
            }



            foreach ($articles as $article_info) {
                $article_box_title = DB::table('categories')
                    ->select("title_$lang as title","slug")
                    ->where("id", $article_info->cat_id)->first();
                $article_info->result_title = '<span class="link_href">'.app('App\Http\Controllers\GlossaryController')->getTranslate("home",$lang).'</span><small> > </small>'. $article_box_title->title.' > '.$article_info->title;
                $article_info->permalink = url('/').'/category/'.$article_box_title->slug.'/'.$article_info->slug;
            }
            $search_empty = 0;
            return view('frontend.search', compact('search_empty','services','meta_data','articles','menu_parent_items','footer_menu','author_info','lang','bredcramp'));

        }else{
            $search_empty = 1;
            return view('frontend.search', compact('search_empty','bredcramp','lang'));

        }


    }

}
