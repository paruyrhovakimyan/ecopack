<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\UserOrder;
use App\OrderMeta;
use App\Product;

class AccountController extends Controller
{


    public function index(){
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $lang = $first_lang->short;

        if(session('account')){
            $user_id = session('account');
            $user_info = User::find($user_id);

            $user_orders = UserOrder::where('user_id', '=', $user_id)->get();

            foreach ($user_orders as $user_order){
                $user_order->metas = OrderMeta::where('order_id', '=', $user_order->id)->get();
                $user_order->all_price = 0;
                foreach ($user_order->metas as $order_meta){
                    $order_meta->meta_product = Product::find($order_meta->product_id);

                    if($order_meta->meta_product->new_price  !=0){
                        $user_order->all_price += $order_meta->product_count * $order_meta->meta_product->new_price;
                    }else{
                        $user_order->all_price += $order_meta->product_count * $order_meta->meta_product->pr_price;
                    }

                }
            }

            return view('frontend.account.home', compact('user_orders','lang','user_info'));
        }else{
            return redirect(url('/'));
        }


    }
    public function login(){
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $lang = $first_lang->short;
        return view('frontend.account.login', compact('lang'));
    }

    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->email() => 'required|string|email',
            'password' => 'required|string',
        ]);
    }
    public function email()
    {
        return 'email';
    }
    public function singIn(Request $request){

        $this->validateLogin($request);
        $username = $request->email;
        $password = $request->password;
        $login_user = DB::table('users')
            ->where('email', '=', $username)
            ->first();



        /** @var TYPE_NAME $login_user */
        if ($login_user && password_verify($password, $login_user->password)) {
            session(['account' => $login_user->id]);
            return redirect('/account');
        } else {
            $request->session()->flash('error_login', 'Invalid user login or password');
            return redirect('/login');
        }

    }
    public function registration(){
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $lang = $first_lang->short;
        return view('frontend.account.registration', compact('lang'));
    }

    public function registrateUser(Request $request){


        if(isset($request->register_add) && $request->register_add =="ok"){

            $this->validator($request->all())->validate();

            $name = $request->name;
            $surname = $request->surname;
            $mail = $request->email;
            $phone = $request->phone;
            $password = bcrypt($request->password);

            $id = DB::table('users')->insertGetId(
                [
                    'name' => $name,
                    'surname' => $surname,
                    'email' => $mail,
                    'phone' => $phone,
                    'password' => $password,
                ]
            );
            session(['account' => $id]);
            return redirect('/');

        }
        return redirect('/registration');
    }

    public function profile(){

        $id = session('account');
        $user_info = User::find($id);
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $lang = $first_lang->short;
        return view('frontend.account.edit_profile', compact('user_info','lang'));
    }



    public function changeProfile(Request $request){

        $this->validatorChange($request->all())->validate();

        $id = session('account');
        $user_info = User::find($id);
        $user_info->name = $request->name;
        $user_info->surname = $request->surname;
        $user_info->phone = $request->phone;
        $user_info->save();
        $lang = $request->lang;
        $redirect_url = app('App\Http\Controllers\GlossaryController')->getTranslate("home_url",$lang)."account/change";

        return redirect($redirect_url);
    }



    public function changePassword(){
        $id = session('account');
        $user_info = User::find($id);
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $lang = $first_lang->short;
        return view('frontend.account.change_password', compact('user_info','lang'));
    }

    public function savePassword(Request $request){

        $this->validatorPassword($request->all())->validate();

        $id = session('account');
        $user_info = User::find($id);
        $password = bcrypt($request->password);
        $user_info->password = $password;
        $user_info->save();
        $lang = $request->lang;
        $redirect_url = app('App\Http\Controllers\GlossaryController')->getTranslate("home_url",$lang)."account";
        return redirect($redirect_url);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout(Request $request){
        $request->session()->forget('account');
        return redirect('/');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'phone' => 'required|string|max:50',
            'registration_terms1' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }
    protected function validatorPassword(array $data)
    {
        return Validator::make($data, [
            'password' => 'required|string|min:6|confirmed',
        ]);
    }
    protected function validatorChange(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'phone' => 'required|string|max:50',
        ]);
    }
}
