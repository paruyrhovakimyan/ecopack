<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class GalleryController extends Controller
{
    public function gallery_view($slug){

        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $lang = $first_lang->short;
        $bradcramp = app('App\Http\Controllers\BredcrampController')->getBredcramp($lang,"gallery_infos",$slug);
        $gallery = DB::table('gallery_infos')->where('slug','=',$slug)->first();
        $gallery_images = DB::table('gallery')->where('parent_id',$gallery->id)->get();
        $meta_data['meta_title'] = $gallery->{"title_$lang"};
        $meta_data['meta_desc'] = $gallery->{"short_desc_$lang"};
        $meta_data['meta_key'] = $gallery->{"title_$lang"};

        return view('frontend.gallery', compact('gallery','gallery_images','bradcramp','meta_data','lang'));

    }
}
