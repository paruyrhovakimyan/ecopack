<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GlossaryController extends Controller
{
    public function getTranslate($short_code,$lang){
        $get_glssary = DB::table('glossaries')->select("glossary_$lang as translate")->where('short_code','=', $short_code)->first();
        return $get_glssary->translate;
    }
}
