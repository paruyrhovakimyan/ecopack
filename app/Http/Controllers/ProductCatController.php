<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Product;
use App\ProductCat;
use App\Services\MobileDetect;

class ProductCatController extends Controller
{
    public function index($id){
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $lang = $first_lang->short;
        $detect = new MobileDetect;
        if($detect->isMobile()){
            $mobile_brows = 1;
        }else{
            $mobile_brows = 0;
        }
        $productcat = ProductCat::find($id);
        $productCats = ProductCat::where("parent_cat", '=', 0)->where('hidden', '=', 0)->get();
        if(isset($productcat) && !empty($productcat)){
            $bradcramp = app('App\Http\Controllers\BredcrampController')->getBredcramp($lang,"productCat",$id);
            $meta_data['meta_title'] = $productcat->{"title_$lang"};
            $meta_data['meta_desc'] = $productcat->{"title_$lang"};
            $meta_data['meta_key'] = $productcat->{"title_$lang"};

           /* //$all_products = DB::table('products')->where('product_cat_id', '=', $id)->paginate(8);*/
            $all_products = DB::table('products')
                ->join('pr_cat_ids', function ($join) use ($id) {
                    $join->on('products.id', '=', 'pr_cat_ids.pr_id')
                        ->where('pr_cat_ids.cat_id', '=', $id)->where('products.hidden', '=', 0);
                })
                ->paginate(8);



            return view('frontend.productcat', compact( 'mobile_brows','bradcramp','productcat','productCats','all_products','lang','meta_data'));
        }else{
            return view('frontend.errors.404', compact( 'lang'));
        }
    }
    public function indexLang($lang,$id){

        $detect = new MobileDetect;
        if($detect->isMobile()){
            $mobile_brows = 1;
        }else{
            $mobile_brows = 0;
        }
        $productcat = ProductCat::find($id);
        $productCats = ProductCat::all();
        if(isset($productcat) && !empty($productcat)){
            $bradcramp = app('App\Http\Controllers\BredcrampController')->getBredcramp($lang,"productCat",$id);
            $meta_data['meta_title'] = $productcat->{"title_$lang"};
            $meta_data['meta_desc'] = $productcat->{"title_$lang"};
            $meta_data['meta_key'] = $productcat->{"title_$lang"};

            $all_products = DB::table('products')
                ->join('pr_cat_ids', function ($join) use ($id) {
                    $join->on('products.id', '=', 'pr_cat_ids.pr_id')
                        ->where('pr_cat_ids.cat_id', '=', $id)->where('products.hidden', '=', 0);
                })
                ->paginate(8);

            return view('frontend.productcat', compact( 'mobile_brows','bradcramp','productcat','productCats','all_products','lang','meta_data'));
        }else{
            return view('frontend.errors.404', compact( 'lang'));
        }
    }

    public function newCollection(){
        $detect = new MobileDetect;
        if($detect->isMobile()){
            $mobile_brows = 1;
        }else{
            $mobile_brows = 0;
        }
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $lang = $first_lang->short;
        $productCats = ProductCat::where("parent_cat", '=', 0)->get();
        $all_products = DB::table('products')->where("new_collection", '=', 1)->where('hidden', '=', 0)->paginate(8);
        foreach ($all_products as $all_product){
            $all_product->pr_id = $all_product->id;
        }
        $meta_data['meta_title'] = app('App\Http\Controllers\GlossaryController')->getTranslate("new_collections",$lang);
        $meta_data['meta_desc'] = app('App\Http\Controllers\GlossaryController')->getTranslate("new_collections",$lang);
        $meta_data['meta_key'] = app('App\Http\Controllers\GlossaryController')->getTranslate("new_collections",$lang);
        $productcat_custom = app('App\Http\Controllers\GlossaryController')->getTranslate("new_collections",$lang);
        return view('frontend.productcat', compact( 'mobile_brows','productcat_custom','productCats','all_products','lang','meta_data'));
    }
    public function salesCollection(){
        $detect = new MobileDetect;
        if($detect->isMobile()){
            $mobile_brows = 1;
        }else{
            $mobile_brows = 0;
        }
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $lang = $first_lang->short;
        $productCats = ProductCat::where("parent_cat", '=', 0)->get();
        $all_products = DB::table('products')->where("pr_new_price", '!=', 0)->where('hidden', '=', 0)->paginate(8);
        foreach ($all_products as $all_product){
            $all_product->pr_id = $all_product->id;
        }
        $meta_data['meta_title'] = app('App\Http\Controllers\GlossaryController')->getTranslate("sales_collections",$lang);
        $meta_data['meta_desc'] = app('App\Http\Controllers\GlossaryController')->getTranslate("sales_collections",$lang);
        $meta_data['meta_key'] = app('App\Http\Controllers\GlossaryController')->getTranslate("sales_collections",$lang);
        $productcat_custom = app('App\Http\Controllers\GlossaryController')->getTranslate("sales_collections",$lang);
        return view('frontend.productcat', compact( 'mobile_brows','productcat_custom','productCats','all_products','lang','meta_data'));
    }
    public function newCollectionLang($lang){
        $detect = new MobileDetect;
        if($detect->isMobile()){
            $mobile_brows = 1;
        }else{
            $mobile_brows = 0;
        }
        $productCats = ProductCat::where("parent_cat", '=', 0)->get();
        $all_products = DB::table('products')->where("pr_new_price", '!=', 0)->where('hidden', '=', 0)->paginate(8);
        foreach ($all_products as $all_product){
            $all_product->pr_id = $all_product->id;
        }
        $meta_data['meta_title'] = app('App\Http\Controllers\GlossaryController')->getTranslate("sales_collections",$lang);
        $meta_data['meta_desc'] = app('App\Http\Controllers\GlossaryController')->getTranslate("sales_collections",$lang);
        $meta_data['meta_key'] = app('App\Http\Controllers\GlossaryController')->getTranslate("sales_collections",$lang);
        $productcat_custom = app('App\Http\Controllers\GlossaryController')->getTranslate("sales_collections",$lang);
        return view('frontend.productcat', compact( 'mobile_brows','productcat_custom','productCats','all_products','lang','meta_data'));
    }


}
