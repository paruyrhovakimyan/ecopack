<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SoapClient;
use App\DonateOrder as DonateOrder;
use App\Services\PaymentService;
use App\CartMeta;
use App\Card;

class DonateController extends Controller
{
   public function index(Request $request){
       $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
       $lang = $first_lang->short;


       if(!empty($request) && $request->first_name !=''){
           $amount = $request->amount;
           $package = new DonateOrder();
           $package->first_name = $request->first_name;
           $package->last_name = $request->last_name;
           $package->company = $request->company;
           $package->email = $request->email;
           $package->phone = $request->phone;
           $package->address = $request->address;
           $package->order_note = $request->order_note;
           $package->project_id = $request->project_id;
           $package->amount = $amount;
           $package->save();
           $insert_id = $package->id;
           $result = PaymentService::orderRegister($insert_id, $amount, $lang);
           return $result;
       }

   }
   public function indexLang($local_lang,Request $request){
       $lang = $local_lang;

       if(!empty($request) && $request->first_name !=''){
           $amount = $request->amount;
           $package = new DonateOrder();
           $package->first_name = $request->first_name;
           $package->last_name = $request->last_name;
           $package->company = $request->company;
           $package->email = $request->email;
           $package->phone = $request->phone;
           $package->address = $request->address;
           $package->order_note = $request->order_note;
           $package->project_id = $request->project_id;
           $package->amount = $amount;
           $package->save();
           $insert_id = $package->id;
           $result = PaymentService::orderRegister($insert_id, $amount,$lang);
           return $result;
       }

   }

    public function back(Request $request){
        $aa = "ss";
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $lang = $first_lang->short;
        if (isset($request->orderId) && (string)$request->orderId != ''){
            $orderId = (string)$_GET['orderId'];
            $aa = PaymentService::getOrderStatusExtended($orderId, $lang);
        }
        return $aa;

    }

    public function success(Request $request){
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $lang = $first_lang->short;
        $meta_data['meta_title'] = "Վճարումը հաջողությամբ կատարվել է:";
        $meta_data['meta_desc'] = "Վճարումը հաջողությամբ կատարվել է:";
        $meta_data['meta_key'] = "Վճարում";
        $cart_id = (int)session('card_id');

        $request->session()->forget('card_id');
        return view('frontend.donate.success', compact( 'lang','meta_data'));


    }

    public function fail(){
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $lang = $first_lang->short;
        $meta_data['meta_title'] = "Անհաջող վճարում";
        $meta_data['meta_desc'] = "Անհաջող վճարում";
        $meta_data['meta_key'] = "Վճարում";
        return view('frontend.donate.fail', compact( 'lang','meta_data'));


    }
    public function failLang($local_lang){
        $lang = $local_lang;
        $meta_data['meta_title'] = "Անհաջող վճարում";
        $meta_data['meta_desc'] = "Անհաջող վճարում";
        $meta_data['meta_key'] = "Վճարում";
        return view('frontend.donate.fail', compact( 'lang','meta_data'));


    }
    public function successLang($local_lang){
        $lang = $local_lang;
        $meta_data['meta_title'] = "Վճարումը հաջողությամբ կատարվել է:";
        $meta_data['meta_desc'] = "Վճարումը հաջողությամբ կատարվել է:";
        $meta_data['meta_key'] = "Վճարում";
        return view('frontend.donate.success', compact( 'lang','meta_data'));


    }


}
