<?php

namespace App\Http\Controllers;

use App\News;
use App\ProductCat;
use App\Team;
use App\Career;
use App\CareerOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use App\Package;
use App\Product;
use App\CoachCategory;
use App\Coach;

class PageController extends Controller
{
    public function index($slug){
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $lang = $first_lang->short;


            $page = DB::table('pages')
                ->select(
                    "id",
                    "title_$lang as title",
                    "short_desc_$lang as short_desc",
                    "description_$lang as description",
                    "meta_title_$lang as meta_title",
                    "meta_desc_$lang as meta_desc",
                    "meta_key_$lang as meta_key",
                    "sub_pages",
                    "pdf_uploder",
                    "gallery",
                    "parent",
                    "config_data1",
                    "config_cat",
                    "page_with_pdf",
                    "config_tag",
                    "image_bg",
                    "image_logo",
                    "bg_color",
                    "slug",
                    "custom_page"
                )
                ->where('slug', $slug)
                ->where('hidden',0)
                ->first();
        if(isset($page->id) && is_numeric($page->id)){
            $bradcramp = app('App\Http\Controllers\BredcrampController')->getBredcramp($lang,"page",$slug);

            $page->child_page_galleries = array();

            $page_gallery = DB::table("gallery_infos")->select("slug","hidden","id","title_$lang as title","short_desc_$lang as short_desc")->where('hidden',0)->where('gallery_parent',0)->where('parent_id',$page->id)->get();
            foreach ($page_gallery as $gallery){
                $gallery->sub_gallery =  '';
                $page_sub_gallery = DB::table("gallery_infos")->select("hidden","slug","id","title_$lang as title","short_desc_$lang as short_desc")->where('hidden',0)->where('gallery_parent',$gallery->id)->where('parent_id',$page->id)->get();

                if(count($page_sub_gallery) > 0){
                    $gallery->sub_gallery = $page_sub_gallery;
                }


                if($page->id == 11){
                    $gallery_images = DB::table('gallery')->where('parent_id',$gallery->id)->limit(4)->get();
                }else{

                    $gallery_images = DB::table('gallery')->where('parent_id',$gallery->id)->get();
                }

                $gallery->images = $gallery_images;
                $page->child_page_galleries = $page_gallery;
            }


            $page->parent_page_pdfs= array();
            $page->page_with_pdfs = array();
            if($page->parent !=0){
                if ($page->page_with_pdf == 1){
                    $sub_page_with_pdfs = DB::table('sub_pages_with_pdfs')
                        ->select(
                            "id",
                            "title_$lang as title",
                            "description_$lang as description",
                            "parent_id"
                        )
                        ->where('parent_id',$page->id)
                        ->where('folder_parent',0)
                        ->where('hidden',0)
                        ->orderBy('id', 'desc')->get();

                    //var_dump($sub_page_with_pdfs);return 1;

                    if(!empty($sub_page_with_pdfs)){
                        foreach ($sub_page_with_pdfs as $sub_page_with_pdf){
                            $gallery_images = DB::table('sub_pages_with_pdfs_gallery')->where('parent_id',$sub_page_with_pdf->id)->get();
                            $sub_page_with_pdf->gallery = $gallery_images;


                            $sub_page_with_pdf->sub_gallery_folders = '';
                            $page_gallery = DB::table("gallery_infos")->select("hidden","id","title_$lang as title","short_desc_$lang as short_desc")->where('hidden',0)->where('sub_parent_id',$sub_page_with_pdf->id)->get();
                            foreach ($page_gallery as $gallery){
                                $gallery_images = DB::table('gallery')->where('sub_parent_id',$gallery->id)->get();
                                $gallery->images = $gallery_images;
                                $sub_page_with_pdf->sub_gallery_folders = $page_gallery;
                            }



                            $sub_page_with_pdf->sub_folders = '';

                            $child_sub_pdfs_folder = DB::table("sub_pages_with_pdfs")
                                ->select("title_$lang as title","description_$lang as description","id","hidden")
                                ->where('folder_parent',$sub_page_with_pdf->id)
                                ->get();
                            if (!empty($child_sub_pdfs_folder)){
                                $sub_page_with_pdf->sub_folders = $child_sub_pdfs_folder;

                                foreach ($child_sub_pdfs_folder as $item) {
                                    $item->files = '';
                                    $item_page_pdfs = DB::table('sub_page_with_pdf_files')->where("sub_page_id", $item->id)->get();
                                    if (!empty($item_page_pdfs)) {
                                        $item->files = $item_page_pdfs;
                                    }



                                }

                            }

                        }
                    }

                    $page->page_with_pdfs = $sub_page_with_pdfs;
                }
            }

            if($page->parent != 0){
                $parent = $page->parent;
            }else{
                $parent = $page->id;
            }



            $meta_data['meta_title'] = $page->meta_title;
            $meta_data['meta_desc'] = $page->meta_desc;
            $meta_data['meta_key'] = $page->meta_key;

            if($page->id == 5){
                $page->contact_page = 1;
            }
            $invesment_page = 0;
            $invesment_sub_pages = '';
            if($page->id == 75){
                $invesment_sub_pages = DB::table('pages')
                    ->where('parent', $page->id)
                    ->where('hidden',0)
                    ->get();

                $invesment_page = 1;

            }




            if($page->id == 76){
                $page_projects = DB::table('sub_pages_with_pdfs')->where('project_category', '!=',0)
                    ->where('hidden', 0)
                    ->get();

                return view('frontend.page_for_project', compact('page_projects','all_news','page_news','meta_data','page','bradcramp','sidebar_pages1', 'lang','sidebar_pages','slug'));

            }
            if($page->custom_page == 1){

                $partners = DB::table('partners')->orderby('partner_order', "asc")
                    ->get();


                $new_collection_products = Product::where("new_collection", 1)->orderby("id", "desc")->limit(4)->get();
                $not_in_array = array();
                foreach ($new_collection_products as $new_collection_product){
                    $not_in_array[] = $new_collection_product->id;
                }

                $all_products = Product::whereNotIn('id', $not_in_array)->paginate(9);
                $productCats = ProductCat::all();


                $social_links = DB::table('home_pages')->where('id', 1)->first();

                $category_news = News::orderby("id","desc")->paginate(9);
                $the_bests = Product::where("the_best", '=', 1)->limit(4)->get();
                $welcome_text = DB::table('home_pages')->where('id', 1)->first();
                return view("frontend.pages.$page->slug", compact('welcome_text','the_bests','category_news','social_links','productCats','all_products','new_collection_products','coachCats','packages','partners','invesment_sub_pages','invesment_page','all_news','page_news','meta_data','page','bradcramp','sidebar_pages1', 'lang','sidebar_pages','slug'));
            }else{
                return view('frontend.page', compact('page_calendars','invesment_sub_pages','invesment_page','all_news','page_news','meta_data','page','bradcramp','sidebar_pages1', 'lang','sidebar_pages','slug'));
            }

        }else{
            return view('frontend.errors.404', compact( 'lang'));
        }




    }

    public function indexLang($local_lang, $slug){
        $lang = $local_lang;



        $page = DB::table('pages')
            ->select(
                "id",
                "title_$lang as title",
                "short_desc_$lang as short_desc",
                "description_$lang as description",
                "meta_title_$lang as meta_title",
                "meta_desc_$lang as meta_desc",
                "meta_key_$lang as meta_key",
                "sub_pages",
                "pdf_uploder",
                "gallery",
                "parent",
                "config_data1",
                "config_cat",
                "page_with_pdf",
                "config_tag",
                "image_bg",
                "image_logo",
                "bg_color",
                "slug",
                "custom_page"
            )
            ->where('slug', $slug)
            ->where('hidden',0)
            ->first();
        if(isset($page->id) && is_numeric($page->id)){
            $bradcramp = app('App\Http\Controllers\BredcrampController')->getBredcramp($lang,"page",$slug);

            $page->child_page_galleries = array();

            $page_gallery = DB::table("gallery_infos")->select("hidden","id","title_$lang as title","short_desc_$lang as short_desc")->where('hidden',0)->where('sub_parent_id',$page->id)->get();
            foreach ($page_gallery as $gallery){
                $gallery_images = DB::table('gallery')->where('sub_parent_id',$gallery->id)->get();
                $gallery->images = $gallery_images;
                $page->child_page_galleries = $page_gallery;
            }


            $page->parent_page_pdfs= array();
            $page->page_with_pdfs = array();
            if($page->parent !=0){
                if ($page->page_with_pdf == 1){
                    $sub_page_with_pdfs = DB::table('sub_pages_with_pdfs')
                        ->select(
                            "id",
                            "title_$lang as title",
                            "description_$lang as description",
                            "parent_id"
                        )
                        ->where('parent_id',$page->id)
                        ->where('folder_parent',0)
                        ->where('hidden',0)
                        ->orderBy('id', 'desc')->get();

                    //var_dump($sub_page_with_pdfs);return 1;

                    if(!empty($sub_page_with_pdfs)){
                        foreach ($sub_page_with_pdfs as $sub_page_with_pdf){
                            $gallery_images = DB::table('sub_pages_with_pdfs_gallery')->where('parent_id',$sub_page_with_pdf->id)->get();
                            $sub_page_with_pdf->gallery = $gallery_images;


                            $sub_page_with_pdf->sub_gallery_folders = '';
                            $page_gallery = DB::table("gallery_infos")->select("hidden","id","title_$lang as title","short_desc_$lang as short_desc")->where('hidden',0)->where('sub_parent_id',$sub_page_with_pdf->id)->get();
                            foreach ($page_gallery as $gallery){
                                $gallery_images = DB::table('gallery')->where('sub_parent_id',$gallery->id)->get();
                                $gallery->images = $gallery_images;
                                $sub_page_with_pdf->sub_gallery_folders = $page_gallery;
                            }



                            $sub_page_with_pdf->sub_folders = '';

                            $child_sub_pdfs_folder = DB::table("sub_pages_with_pdfs")
                                ->select("title_$lang as title","description_$lang as description","id","hidden")
                                ->where('folder_parent',$sub_page_with_pdf->id)
                                ->get();
                            if (!empty($child_sub_pdfs_folder)){
                                $sub_page_with_pdf->sub_folders = $child_sub_pdfs_folder;

                                foreach ($child_sub_pdfs_folder as $item) {
                                    $item->files = '';
                                    $item_page_pdfs = DB::table('sub_page_with_pdf_files')->where("sub_page_id", $item->id)->get();
                                    if (!empty($item_page_pdfs)) {
                                        $item->files = $item_page_pdfs;
                                    }



                                }

                            }

                        }
                    }

                    $page->page_with_pdfs = $sub_page_with_pdfs;
                }
            }

            if($page->parent != 0){
                $parent = $page->parent;
            }else{
                $parent = $page->id;
            }



            $meta_data['meta_title'] = $page->meta_title;
            $meta_data['meta_desc'] = $page->meta_desc;
            $meta_data['meta_key'] = $page->meta_key;

            if($page->id == 5){
                $page->contact_page = 1;
            }
            $invesment_page = 0;
            $invesment_sub_pages = '';
            if($page->id == 75){
                $invesment_sub_pages = DB::table('pages')
                    ->where('parent', $page->id)
                    ->where('hidden',0)
                    ->get();

                $invesment_page = 1;

            }




            if($page->id == 76){
                $page_projects = DB::table('sub_pages_with_pdfs')->where('project_category', '!=',0)
                    ->where('hidden', 0)
                    ->get();

                return view('frontend.page_for_project', compact('page_projects','all_news','page_news','meta_data','page','bradcramp','sidebar_pages1', 'lang','sidebar_pages','slug'));

            }
            if($page->custom_page == 1){

                $partners = DB::table('partners')->orderby('partner_order', "asc")
                    ->get();


                $new_collection_products = Product::where("new_collection", 1)->orderby("id", "desc")->limit(4)->get();
                $not_in_array = array();
                foreach ($new_collection_products as $new_collection_product){
                    $not_in_array[] = $new_collection_product->id;
                }

                $all_products = Product::whereNotIn('id', $not_in_array)->paginate(9);
                $productCats = ProductCat::all();


                $social_links = DB::table('home_pages')->where('id', 1)->first();

                $category_news = News::orderby("id","desc")->paginate(6);
                $the_bests = Product::where("the_best", '=', 1)->limit(4)->get();
                $welcome_text = DB::table('home_pages')->where('id', 1)->first();
                return view("frontend.pages.$page->slug", compact('welcome_text','the_bests','category_news','social_links','productCats','all_products','new_collection_products','coachCats','packages','partners','invesment_sub_pages','invesment_page','all_news','page_news','meta_data','page','bradcramp','sidebar_pages1', 'lang','sidebar_pages','slug'));
            }else{
                return view('frontend.page', compact('page_calendars','invesment_sub_pages','invesment_page','all_news','page_news','meta_data','page','bradcramp','sidebar_pages1', 'lang','sidebar_pages','slug'));
            }

        }else{
            return view('frontend.errors.404', compact( 'lang'));
        }
    }

    public function contact_us(Request $request){

        if(isset($request->send_ok) && $request->send_ok == "ok"){
            $mail_text = file_get_contents("https://printel.am/test.html");

            $user_name = $request->user_name;
            $user_mail = $request->user_mail;
            $user_message = $request->user_message;

            $to      = 'your.gmail.account@gmail.com';
            $subject = 'PRINTeL subject';
            $message = 'hello'. $mail_text;
            $headers = 'From: "My Business Name" <my.business@email.com>' . "\\r\
" .
                'Reply-To: website@printel.am' . "\\r\
" .
                'X-Mailer: PHP/' . phpversion(). "\\r\
".
                'Content-type: text/html; charset=iso-8859-1' . "\\r\
";

            if(mail("paruyrhovakimyan@gmail.com", 'PRINTeL subject', $message, $headers)){
                $request->session()->flash('status', 'Task was successful!');
            }


            /*$headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-Type: multipart/related; boundary=emailsectionseparator; type=text/html' . "\r\n";
            $headers .= "From: PRINTeL.am" . "\r\n" .
            $message = '<p>Name: '.$user_name.'</p>';
            $message .= '<p>E-mail: '.$user_mail.'</p>';
            $message .= '<p>Message: '.$user_message.'</p>';
            $message .= '<p>Message: '.$mail_text.'</p>';
            if(mail("paruyrhovakimyan@gmail.com", 'PRINTeL', $message, $headers)){
                $request->session()->flash('status', 'Task was successful!');
            }*/

            return redirect('/');
            /*PRINTeL@ysu.am*/
        }
    }

    public function sendMail(Request $request)
    {
        $redirect_url = url('/')."/page/hetadardz-kap";
        $name = $request->contact_name;
        $email = $request->contact_mail;
        $phone = $request->contact_phone;
        $text = $request->contact_text;
        if ($name != '' && $email != '' && $phone != '' && $text != '' && filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
            $headers .= 'To: energy.am <superdjut@yandex.ru>' . "\r\n";
            $headers .= 'From: ' . $name . ' <' . $email . '>' . "\r\n";
            $message = '<p>Name: ' . $name . '</p>';
            $message .= '<p>E-mail: ' . $email . '</p>';
            $message .= '<p>Phone: ' . $phone . '</p>';
            $message .= '<p>Message: ' . $text . '</p>';
            if (mail('paruyrhovakimyan@gmail.com', 'From Energy.am', $message, $headers)) {
                $request->session()->flash('status', 'Message sent!');
                return redirect($redirect_url);
            } else {
                $request->session()->flash('status', 'Something wrong!');
                return redirect($redirect_url);
            }
        }
    }

    public function getTeams($lang){
        $teams = Team::where("stick", "=", 1)->orderBy("team_order", "asc")->get();
        $result = '';
        foreach ($teams as $team){
            $result .= '<div class="col-md-3 stuff_memb">
                        <img class="stuff_img" src="'.url('/').'/uploads/partners/'.$team->image.'" alt="">
                        <div class="team_title_boc">
                            <h3>'.$team->{"title_$lang"}.'</h3>
                            <p>'.$team->{"team_position_$lang"}.'</p>
                        </div>
                        <div class="stuff_desc"> '.$team->{"short_desc_$lang"}.'</div>
                    </div>';
        }

        return $result;
    }

    public function getAllTeams($lang){
        $teams = Team::all();
        $result = '';
        foreach ($teams as $team){
            $result .= '<div class="col-md-3 stuff_memb">
                        <img class="stuff_img" src="'.url('/').'/uploads/partners/'.$team->image.'" alt="">
                        <div class="team_title_boc">
                            <h3>'.$team->{"title_$lang"}.'</h3>
                            <p>'.$team->{"team_position_$lang"}.'</p>
                        </div>
                        <div class="stuff_desc"> '.$team->{"short_desc_$lang"}.'</div>
                    </div>';
        }

        return $result;
    }

    public function getCareer($lang){
        $result = '';
        $careers = Career::where("hidden", '=', 0)->get();

        if(count($careers) > 0){

            $result .= ' <div class="caree">
            <div class="container">
                <h4 class="oru_partners_h3">'.app('App\Http\Controllers\GlossaryController')->getTranslate("career",$lang).'</h4>';

            foreach ($careers as $career){
                $result .= '<div class="job">
                    <i class="fa fa-plus job_fa" aria-hidden="true"></i>
                    <i class="fa fa-minus job_fa_min" aria-hidden="true"></i>
                    '.$career->{"title_$lang"}.'
                    </div>
                <div class="caree_content" style="display: none;">
                    <div class="contanier">
                        <div class="row">
                            <form action="'.url('/').'/career/order/'.$career->id.'" method="post"  enctype="multipart/form-data">
                            '.csrf_field().'
                                <div class="col-md-6 caree_content_left">
                                    <input class="contact_input" type="text" name="name_surname_'.$career->id.'" placeholder="'.app('App\Http\Controllers\GlossaryController')->getTranslate("name_surname",$lang).'" required>
                                    <input class="contact_input" type="email" name="career_mail_'.$career->id.'" placeholder="'.app('App\Http\Controllers\GlossaryController')->getTranslate("mail",$lang).'" required>
                                    <input type="file" name="career_file_'.$career->id.'" required>
                                </div>
                                <div class="col-md-6 caree_content_right">
                                    <textarea class="contact_text" name="career_desc_'.$career->id.'" cols="70" rows="5" placeholder="'.app('App\Http\Controllers\GlossaryController')->getTranslate("contact_message",$lang).'" required></textarea>
                                    <input type="hidden" name="career_id_'.$career->id.'" value="'.$career->id.'">
                                    <input class="contact_submit" type="submit" placeholder="" value="'.app('App\Http\Controllers\GlossaryController')->getTranslate("contact_send",$lang).'">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>';
            }

            $result .= '</div></div></div>';
        }

        return $result;



    }

    public function careerOrder($career_id, Request $request){

        $careerOrder = new CareerOrder();

        if(isset($request) && $request !=''){
            $name_surname = $request->{"name_surname_$career_id"};
            $career_mail = $request->{"career_mail_$career_id"};
            $career_desc = $request->{"career_desc_$career_id"};


            $field_name = "career_file_".$career_id;
            if ($request->hasFile($field_name)) {
                $destinationPath =  base_path() . '/public/uploads/career/';
                $file = $request->file($field_name);
                $file_name = $file->getClientOriginalName();
                $file->move($destinationPath , $file_name);
                $careerOrder->order_file= $file_name;
            }
            $careerOrder->name_surname = $name_surname;
            $careerOrder->email = $career_mail;
            $careerOrder->description = $career_desc;
            $careerOrder->career_id = $career_id;
            $careerOrder->save();
            $request->session()->flash('career_order_status', 'Ձեր հայտը ընդունվել է:');



            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
            $headers .= 'To: insport.am <info@insport.am>' . "\r\n";
            $headers .= 'From: ' . $name_surname . ' <' . $career_mail . '>' . "\r\n";
            $message = '<p>Name: ' . $name_surname . '</p>';
            $message .= '<p>E-mail: ' . $career_mail . '</p>';
            $message .= '<p>File: <a href="'.url('/').'/public/uploads/career/'.$file_name.'">File</a></p>';
            $message .= '<p>Message: ' . $career_desc . '</p>';
            mail('paruyrhovakimyan@gmail.com', 'inSport.am', $message, $headers);

        }else{
            $request->session()->flash('career_order_status', 'Task was faild!');
        }
        return redirect(url('/').'/page/mer-masin');

    }

    public function getCoachCat($cat, $lang){

        if($cat !=0){
            $cat_row = CoachCategory::find($cat);
            if($cat_row){
                return $cat_row->{"title_$lang"};
            }else{
                return "";
            }

        }


    }

    public function getCoach($id, $lang){
        if($id !=0){
            $cat_row = Coach::find($id);
            if($cat_row){
                return $cat_row->{"title_$lang"};
            }else{
                return "";
            }
        }


    }

    public function getCoachImg($id){
        if($id !=0){
            $cat_row = Coach::find($id);
            if($cat_row){
                if($cat_row->image !=''){
                    $image = '<div class="coach_img_popup"><img src="'.url('/').'/public/uploads/coach/'.$cat_row->image.'"></div>';
                }else{
                    $image = '';
                }
            }else{
                $image = '';
            }
        }else{
            $image = '';
        }
        return $image;


    }

    public function getCalendarInfo($cat_id,$coach_id,$lang){
        $cat_row = CoachCategory::find($cat_id);
        $coach_row = Coach::find($coach_id);
        if(!empty($cat_row)){
            $cat_row_title = $cat_row->{"title_$lang"};
            $cat_row_desc = $cat_row->{"description_$lang"};
        }else{
            $cat_row_title = '';
            $cat_row_desc = '';
        }
        if($cat_row || $coach_row){
            $result = '<div class="calendar_info">
            <div class="close_calendar_info"><i class="fa fa-times" aria-hidden="true"></i></div>';
            $result .= '<div class="calendar_info_left">
<div class="calendar_info_title">'.$cat_row_title.'</div>
<div class="calendar_info_desc">'.$cat_row_desc.'</div></div> ';

            if(!empty($coach_row) && $coach_row->image !=''){
                $result .= '<div class="calendar_info_img">
<img src="'.url('/').'/public/uploads/coach/'.$coach_row->image.'">
<span>'.$coach_row->{"title_$lang"}.'</span>
</div>';
            }

            $result .= '</div>';
        }else{
            $result = '';
        }
        return $result;
    }

    public  function singlePackage($id){
        $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
        $lang = $first_lang->short;
        $package = Package::find($id);
        if(isset($package) && !empty($package)){
            $meta_data['meta_title'] = $package->{"title_$lang"};
            $meta_data['meta_desc'] = $package->{"title_$lang"};
            $meta_data['meta_key'] = $package->{"title_$lang"};
            return view('frontend.package', compact( 'package','lang','meta_data'));
        } else{
            return view('frontend.errors.404', compact( 'lang'));
        }
    }

    public  function singlePackageLang($lang,$id){
        $package = Package::find($id);
        if(isset($package) && !empty($package)){
            $meta_data['meta_title'] = $package->{"title_$lang"};
            $meta_data['meta_desc'] = $package->{"title_$lang"};
            $meta_data['meta_key'] = $package->{"title_$lang"};
            return view('frontend.package', compact( 'package','lang','meta_data'));
        } else{
            return view('frontend.errors.404', compact( 'lang'));
        }
    }

    public function singleOrderPackage(Request $request, $id){
        if(isset($request->name_surname) && $request->name_surname !=''){
            $name = $request->name_surname;
            $email = $request->mail;
            $phone = $request->phone;
            $text = $request->message;
            if ($name != '' && $email != '' && $phone != '' && $text != '' && filter_var($email, FILTER_VALIDATE_EMAIL)) {

                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
                $headers .= 'To: insport.am <info@insport.am>' . "\r\n";
                $headers .= 'From: ' . $name . ' <' . $email . '>' . "\r\n";
                $message = '<p>Name: ' . $name . '</p>';
                $message .= '<p>E-mail: ' . $email . '</p>';
                $message .= '<p>Phone: ' . $phone . '</p>';
                $message .= '<p>Message: ' . $text . '</p>';
                if (mail('paruyrhovakimyan@gmail.com', 'inSport.am', $message, $headers)) {
                    $request->session()->flash('status', 'Message sent!');
                    return redirect(url('/')."/package/$id");
                } else {
                    $request->session()->flash('status', 'Something wrong!');
                    return redirect(url('/')."/package/$id");
                }
            }
        }
    }

    public function singleOrderPackageLang(Request $request, $id){
        if(isset($request->name_surname) && $request->name_surname !=''){
            $name = $request->name_surname;
            $email = $request->mail;
            $phone = $request->phone;
            $text = $request->message;
            if ($name != '' && $email != '' && $phone != '' && $text != '' && filter_var($email, FILTER_VALIDATE_EMAIL)) {

                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
                $headers .= 'To: insport.am <info@insport.am>' . "\r\n";
                $headers .= 'From: ' . $name . ' <' . $email . '>' . "\r\n";
                $message = '<p>Name: ' . $name . '</p>';
                $message .= '<p>E-mail: ' . $email . '</p>';
                $message .= '<p>Phone: ' . $phone . '</p>';
                $message .= '<p>Message: ' . $text . '</p>';
                if (mail('paruyrhovakimyan@gmail.com', 'inSport.am', $message, $headers)) {
                    $request->session()->flash('status', 'Message sent!');
                    return redirect(url('/')."/package/$id");
                } else {
                    $request->session()->flash('status', 'Something wrong!');
                    return redirect(url('/')."/package/$id");
                }
            }
        }
    }
}
