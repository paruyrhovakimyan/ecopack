<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompanyController extends Controller
{
    public function index($id){

        $company = DB::table('companies')->where('id', $id)->first();
        return view('frontend.company.company', compact('company'));
    }
}
