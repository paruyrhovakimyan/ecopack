<?php

namespace App\Http\Controllers;

use App\CartMeta;
use App\Page;
use App\Product;
use App\ProductCat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use App\Package;
use App\Services\MobileDetect;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($local_lang = NULL)
    {

        $detect = new MobileDetect;
       if($detect->isMobile()){
           $mobile_brows = 1;
       }else{
           $mobile_brows = 0;
       }

        if(isset($local_lang)){
            $lang = $local_lang;
        }else{
            $first_lang = DB::table('languages')->where('first', '=', 1)->where('hidden', '=', 0)->first();
            $lang = $first_lang->short;
        }


        $languages = DB::table('languages')->where('hidden', '=', 0)->get();
        $slider = DB::table('slider_items')
            ->where('hidden', '0')
            ->where('slider_id', '1')
            ->orderBy('item_order', 'asc')
            ->get();


        $welcome_text = DB::table('home_pages')->where('id', 1)->first();



        $artadranqner = Page::where('parent', 3)->get();


        $categories = ProductCat::where("parent_cat", '=', 0)->where('hidden', '=', 0)->get();

        $the_bests = Product::where("the_best", '=', 1)->where("hidden", '=', 0)->limit(4)->get();
        

        $new_collections = Product::where("new_collection", '=', 1)->where("hidden", '=', 0)->orderby('id', 'desc')->limit(4)->get();
        $sales_collections = Product::where("pr_new_price", '!=', 0)->where("hidden", '=', 0)->limit(4)->get();




        $newses_stick = DB::table('news')
            ->where('hidden','=', 0)
            ->orderBy('date1', 'desc')
            ->limit(3)
            ->get();

        $partners = DB::table('partners')->where("hidden", '=', 0)->orderby('id', "DESC")
            ->get();

        $meta_data['meta_title'] = $welcome_text->{"meta_title_".$lang};
        $meta_data['meta_desc'] = $welcome_text->{"meta_desc_".$lang};
        $meta_data['meta_key'] = $welcome_text->{"meta_key_".$lang};
        $meta_data['meta_image'] = url('/')."/public/img/logo.png";
        $meta_data['meta_url'] = url('/');


        $packages = Package::all();
        $products = Product::all();
        return view('frontend.home', compact('artadranqner','mobile_brows','sales_collections','new_collections','the_bests','categories','products','packages','newses_stick','languages', 'meta_data', 'partners', 'slider', 'welcome_text', 'lang'));
    }

    public function dataFormat($data, $lang)
    {
        $month = date("m", strtotime($data));

        if($lang == 'en'){
            $result = date("M d, Y", strtotime($data));
        }elseif ($lang == 'ru'){
            switch ($month){
                case "01":
                    $result = "Январь ".date("d, Y", strtotime($data));
                    break;
                case "02":
                    $result = "Февраль ".date("d, Y", strtotime($data));
                    break;
                case "03":
                    $result = "Март ".date("d, Y", strtotime($data));
                    break;
                case "04":
                    $result = "Апрель ".date("d, Y", strtotime($data));
                    break;
                case "05":
                    $result = "Май ".date("d, Y", strtotime($data));
                    break;
                case "06":
                    $result = "Июнь ".date("d, Y", strtotime($data));
                    break;
                case "07":
                    $result = "Июль ".date("d, Y", strtotime($data));
                    break;
                case "08":
                    $result = "Август ".date("d, Y", strtotime($data));
                    break;
                case "09":
                    $result = "Сентябрь ".date("d, Y", strtotime($data));
                    break;
                case "10":
                    $result = "Октябрь ".date("d, Y", strtotime($data));
                    break;
                case "11":
                    $result = "Ноябрь ".date("d, Y", strtotime($data));
                    break;
                case "12":
                    $result = "Декабрь ".date("d, Y", strtotime($data));
                    break;
            }
        }else{
            switch ($month){
                case "01":
                    $result = "Հունվար ".date("d, Y", strtotime($data));
                    break;
                case "02":
                    $result = "Փետրվար ".date("d, Y", strtotime($data));
                    break;
                case "03":
                    $result = "Մարտ ".date("d, Y", strtotime($data));
                    break;
                case "04":
                    $result = "Ապրիլ ".date("d, Y", strtotime($data));
                    break;
                case "05":
                    $result = "Մայիս ".date("d, Y", strtotime($data));
                    break;
                case "06":
                    $result = "Հունիս ".date("d, Y", strtotime($data));
                    break;
                case "07":
                    $result = "Հուլիս ".date("d, Y", strtotime($data));
                    break;
                case "08":
                    $result = "Օգոստոս ".date("d, Y", strtotime($data));
                    break;
                case "09":
                    $result = "Սեպտեմբեր ".date("d, Y", strtotime($data));
                    break;
                case "10":
                    $result = "Հոկտեմբեր ".date("d, Y", strtotime($data));
                    break;
                case "11":
                    $result = "Նոյեմբեր ".date("d, Y", strtotime($data));
                    break;
                case "12":
                    $result = "Դեկտեմբեր ".date("d, Y", strtotime($data));
                    break;
            }
        }
        return $result;
    }
    public function dataMonthFormat($data, $lang)
    {
        $month = date("m", strtotime($data));

        if($lang == 'en'){
            $result = date("M", strtotime($data));
        }elseif ($lang == 'ru'){
            switch ($month){
                case "01":
                    $result = "Янв";
                    break;
                case "02":
                    $result = "Фев";
                    break;
                case "03":
                    $result = "Мар";
                    break;
                case "04":
                    $result = "Апр";
                    break;
                case "05":
                    $result = "Май";
                    break;
                case "06":
                    $result = "Июнь";
                    break;
                case "07":
                    $result = "Июль";
                    break;
                case "08":
                    $result = "Авг";
                    break;
                case "09":
                    $result = "Сен";
                    break;
                case "10":
                    $result = "Окт";
                    break;
                case "11":
                    $result = "Ноя";
                    break;
                case "12":
                    $result = "Дек";
                    break;
            }
        }else{
            switch ($month){
                case "01":
                    $result = "Հուն";
                    break;
                case "02":
                    $result = "Փետ";
                    break;
                case "03":
                    $result = "Մար";
                    break;
                case "04":
                    $result = "Ապր";
                    break;
                case "05":
                    $result = "Մայ";
                    break;
                case "06":
                    $result = "Հուն";
                    break;
                case "07":
                    $result = "Հուլ";
                    break;
                case "08":
                    $result = "Օգ";
                    break;
                case "09":
                    $result = "Սեպ";
                    break;
                case "10":
                    $result = "Հոկ";
                    break;
                case "11":
                    $result = "Նոյ";
                    break;
                case "12":
                    $result = "Դեկ";
                    break;
            }
        }
        return $result;
    }

    public function getAction($action)
    {
        $lang = config('app.locale');
        $result = '';
        switch ($action) {
            case "partners": {

                $partners = DB::table('partners')->select("short_desc", "id", "partner_order", "image", "created_at", "hidden", "title_$lang as title", "link_$lang as link")->where('hidden', 0)
                    ->orderby('id', "DESC")
                    ->get();

                if (!empty($partners)) {
                    foreach ($partners as $partner) {
                        if($partner->short_desc !=''){
                            $short_desc = $partner->short_desc;
                        }else{
                            $short_desc = '';
                        }

                        $result .= '<div class="news_sub_page_pdf_blocks">
                        <div class="partner_sub_page_pdf_one">
                            <div class="image_sub_page">
                                <img src="' .url('/').'/uploads/partners/150/'. $partner->image . '"  alt="' . $partner->title . '" title="' . $partner->title . '">
                            </div>
                            <p class="partner_sub_one_title"> ' . $partner->title . '</p>
                            <div class="news_sub_page_pdf_list">
                            <div class="news_sub_page_pdf_desc">
                                '.$short_desc.'
                                <a target="_blank" href="'. $partner->link .'">'. $partner->link .'</a>
                            </div>
                        </div>
                        </div>
                        
                </div>';
                    }
                }
                break;
            }
        }
        return $result;
    }
    public function getLanguageBox($lang){
        $current_lang = DB::table('languages')->where('short', '=', $lang)->where('hidden', '=', 0)->first();
        $languages = DB::table('languages')->where('short', '!=', $lang)->where('hidden', '=', 0)->get();
        $result = '<div class="language_box">
                        <div class="current_language">
                            <p>'.$current_lang->title.' <i class="fa fa-angle-down" aria-hidden="true"></i></p>
                        </div>
                        <div class="hidden_languages">';
        foreach($languages as $language){
            $result .='<a href="'.app('App\Http\Controllers\LanguageController')->language_switcher($language->short).'">'.$language->title.'</a>';
        }


        $result .= '</div></div>';
        //$result .= '</div>';
        return $result;

    }

    public function getLanguageBox1($lang){
        $current_lang = DB::table('languages')->where('short', '=', $lang)->where('hidden', '=', 0)->first();
        $languages = DB::table('languages')->where('short', '!=', $lang)->where('hidden', '=', 0)->get();
        $result = '<div class="language_box">
                        <div class="current_language">
                            <p>'.$current_lang->title.' <i class="fa fa-angle-down" aria-hidden="true"></i></p>
                        </div>
                        <div class="hidden_languages">';
                    foreach($languages as $language){
                        $result .='<a href="'.app('App\Http\Controllers\LanguageController')->language_switcher($language->short).'">'.$language->title.'</a>';
                    }


        $result .= '</div></div>';
                    return $result;

    }

    public function getMobileLanguageBox($lang){

        $current_lang = DB::table('languages')->where('short', '=', $lang)->where('hidden', '=', 0)->first();
        $languages = DB::table('languages')->where('short', '!=', $lang)->where('hidden', '=', 0)->get();
        $result = '<div class="language_box">
                        <div class="current_language">
                            <p>'.$current_lang->title.'</p>
                        </div>
                        <div class="ohet_languages">';

        foreach($languages as $language){
            $result .='<a href="'.app('App\Http\Controllers\LanguageController')->language_switcher($language->short).'">'.$language->title.'</a>';
        }


        $result .= '</div></div>';
        return $result;

    }

    public function getCardBox($lang){
        $cart_id = (int)session('card_id');
        if(session('card_id')){
            $result_cart_meta_show = CartMeta::where("cart_id", '=',$cart_id)->get();

            $all_price = 0;

            $all_count = 0;

            foreach ($result_cart_meta_show as $row_cart_meta_show){
                $all_count += $row_cart_meta_show->product_count;
                $prods_id = (int)$row_cart_meta_show->product_id;
                $pr_info = Product::find($prods_id);
                $all_price += $pr_info->pr_price * $row_cart_meta_show->product_count;

            }
            $return = '<div class="cart__head_count"><b>'.$all_count.'</b></div>

    <div class="cart__head_price"><span>Զամբյուղ</span>

        <div class="head_all_price">

            <small>'.$all_count.'</small>

            '.$all_price .' AMD 

        </div>

    </div>';
        }else{
            $return ='  <div class="cart__head_count"><b>0</b></div><span>
			 <div class="cart__head_not"><span>Զամբյուղը դատարկ է</span>
             </div>';
        }



        return $return;
    }

    public function getCardContentBox($lang){
        $cart_id = (int)session('card_id');
        $result = '';
        if($cart_id){
            $result_cart_meta_show = CartMeta::where("cart_id", '=',$cart_id)->get();
            if($result_cart_meta_show){
                $result .= ' <div class="checkout__summary">
        <thead>
        <tr>
        </tr>
        </thead>';
                foreach ($result_cart_meta_show as $row_cart_meta_show){
                    $pr_id = $row_cart_meta_show->product_id;
                    $result_product_show = Product::find($pr_id);
                    $result .= ' <div class="pr_row del'.$pr_id.'">
            <div class="checkout_row_image"><img height="50" alt="'.$result_product_show->{'title_'.$lang}.'" title="'.$result_product_show->{'title_'.$lang}.'" src="'.url('/').'/public/uploads/product/thumbs/'.$result_product_show->image.'" />
                <strong>'.$result_product_show->{'title_'.$lang}.'</strong> </div>
           
            <div class="delete_row_pr"><a style="cursor: pointer" class="del_item" data-id="'.$pr_id.'"><i class="fa fa-times" aria-hidden="true"></i></a></div>
            <div class="checkout_row_info">
             <div class="cena_cart56"><small>քանակ </small> <span>'. $row_cart_meta_show->product_count.'</span> </div>
            <div class="cena_cart55"><small>Միավորի գինը</small> '.$row_cart_meta_show->pr_price.'
                <small>AMD</small>
            </div>
</div>
                    </div>';
                }
                $result .= '<div class="checkout_footer">
        <div><p>Ընդհանուր  <span class="checkout__total all_price_number">1 AMD</span></p></div>
        </div>
        </div><!-- /checkout__summary -->
        <button class="checkout__option checkout__option--silent checkout__cancel"><i class="fa fa-angle-left"></i>  Շարունակել գնումները</button>
        <a href="'.url('/').'/card"><button class="checkout__option checkout__option--loud">Զամբյուղ</button></a>';
            }

        }
        return $result;
    }

}
