<?php

namespace App\Http\Middleware;

use Closure;

class AccountAutorization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session('account')){

            return $next($request);
        }else{
            return redirect('/login');
        }
    }
}
