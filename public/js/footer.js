var base_url = 'http://prof.mycard.am/';

$(".jssort101").parent().addClass("Ssss");


$(document).ready(function(){
    $(".job").click(function(){
        $(this).next().slideToggle(1000);
        $(this).toggleClass("open_job");
        $(this).children().toggleClass("open_job_fa");
        $(this).children().next().toggleClass("job_fa_min_opne");
    });


    setTimeout(function() {
        $('.career_order_message').fadeOut('fast');
    }, 4000);
});

$(document).on("click","#kupit_knopka",function() {
    var taaga = $(this);
    var lang = $(this).attr("data-lang");
    var count = $(this).attr("data-count");
    var product = $(this).attr("data-id");
    var product_data = new FormData();
    product_data.append('product', product);
    product_data.append('lang', lang);
    product_data.append('count', count);
    product_data.append('rate', 'AMD');

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/add-to-card',
        data: product_data,
        type: 'POST',
        contentType: false, // The content type used when sending data to the server.
        cache: false, // To unable request pages to be cached
        processData: false,
        success: function (msg) {
            if(msg){
                $(".card_count p").html(msg['all']);
                $(".top-cart-content").html(msg['content']);
            }

        }
    });
});
$(document).on("click",".buy_prod",function() {
    var taaga = $(this);
    var lang = $(this).attr("data-lang");
    var count = $(".pr_count_in_cart1").val();
    var product = $(this).attr("data-id");
    var product_data = new FormData();
    product_data.append('product', product);
    product_data.append('lang', lang);
    product_data.append('count', count);
    product_data.append('rate', 'AMD');

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/add-to-card',
        data: product_data,
        type: 'POST',
        contentType: false, // The content type used when sending data to the server.
        cache: false, // To unable request pages to be cached
        processData: false,
        success: function (msg) {
            if(msg){
                $(".card_count p").html(msg['all']);
                $(".top-cart-content").html(msg['content']);
            }

        }
    });
});













$(document).on("click", ".cart_head", function () {
    $(".checkout__order").addClass("back_fon_win");
    $(".checkout").addClass("checkout--active");
});
$(document).on("click", ".checkout__cancel", function () {
    $(".checkout__order").removeClass("back_fon_win");
    $(".checkout").removeClass("checkout--active");
});

$(".delete_item").click(function(){
    $(".checkout__cancel").click();
    var this_action = $(this);
    var pr_id = $(this).attr("data-id");
    swal({
            title: "",
            text: "Դուք ցանկանու՞մ եք հեռացնել ապրանքը:",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#cd3566',
            confirmButtonText: 'Այո',
            cancelButtonText: 'Ոչ'
        },
        function(){
            var product_data = new FormData();
            product_data.append('product', pr_id);
            product_data.append('count', 0);
            product_data.append('rate', 'AMD');
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/add-to-card',
                data: product_data,
                type: 'POST',
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
                success: function (msg) {
                    location.reload();

                }
            });

        });
})

$(".continue").click(function(){
    var registration_or_guest=0;
    $(".registration_or_guest").each(function(){
        if($(this).is(":checked")){
            registration_or_guest = $(this).val();
        }
    })
    if(registration_or_guest=='guest'){
        document.location.href=base_url + "guest";
    }
    if(registration_or_guest=='registration'){
        document.location.href=base_url + "checkout-registration";
    }
});

$(".product_desc_tab").click(function () {
    $(".pr_info_tab").removeClass("active_tab");
    $(this).addClass("active_tab");
    $(".product_info_desc").addClass("hidden");
    $(".product_desc").removeClass("hidden");
});
$(".product_charecrers_tab").click(function () {
    $(".pr_info_tab").removeClass("active_tab");
    $(this).addClass("active_tab");
    $(".product_info_desc").addClass("hidden");
    $(".product_charecrers").removeClass("hidden");
});
$(".product_video_tab").click(function () {
    $(".pr_info_tab").removeClass("active_tab");
    $(this).addClass("active_tab");
    $(".product_info_desc").addClass("hidden");
    $(".product_video").removeClass("hidden");
});

$(".cart_minus1").click(function(){
    var count = parseInt($(this).next(".pr_count_in_cart1").val())-1;
    if(count<1){
        count = 1;
    }
    $(".pr_count_in_cart1").val(count);
})
$(".cart_plus1").click(function(){
    var count = parseInt($(this).prev(".pr_count_in_cart1").val())+1;
    $(".pr_count_in_cart1").val(count);
})
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57) ){
        return false;
    }
    return true;
}
$(".pr_count_in_cart1").keyup(function(){
    if($(this).val()<1){$(this).val("1");}
})


function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57) )
        return false;
    return true;
}
$(".pr_count_in_cart").keyup(function(e){
    $(".checkout__cancel").click();
    if(e.keyCode<37 || e.keyCode>40){
        $(".for_loading").show();
        var this_action = $(this);
        var count = parseInt($(this).val());
        var lang = $(this).attr("data-lang");
        var product = $(this).attr("data-id");
        if(count){}else{count=1;}
        var product_data = new FormData();
        product_data.append('product', product);
        product_data.append('lang', lang);
        product_data.append('count', count);
        product_data.append('rate', 'AMD');

        $.ajax({
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/updatecard',
            data: product_data,
            type: 'POST',
            contentType: false, // The content type used when sending data to the server.
            cache: false, // To unable request pages to be cached
            processData: false,
            success: function (msg) {
                if(msg){
                    $(".card_count p").html(msg['all']);
                    $(".top-cart-content").html(msg['content']);
                    $(".pr_count_in_cart" + product).val(new_count);

                }

            }
        });
    }
})



$(document).on("click",".update_card_btn",function() {
    location.reload();
});
$(document).on("click",".cart_minus",function() {
    var taaga = $(this);
    var lang = $(this).attr("data-lang");
    var product = $(this).attr("data-id");


    var count = parseInt($(".pr_count_in_cart" + product).val())-1;
    var new_count = 0;
    if(count<1){
        new_count=0;
        count = 1;
    }
    else{
        new_count=count;
    }


    var product_data = new FormData();
    product_data.append('product', product);
    product_data.append('lang', lang);
    product_data.append('count', new_count);
    product_data.append('rate', 'AMD');

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/updatecard',
        data: product_data,
        type: 'POST',
        contentType: false, // The content type used when sending data to the server.
        cache: false, // To unable request pages to be cached
        processData: false,
        success: function (msg) {
            if(msg){
                $(".card_count p").html(msg['all']);
                $(".top-cart-content").html(msg['content']);
                $(".pr_count_in_cart" + product).val(new_count);

            }

        }
    });
});
$(document).on("click",".cart_plus",function() {
    var taaga = $(this);
    var lang = $(this).attr("data-lang");
    var product = $(this).attr("data-id");


    var count = parseInt($(".pr_count_in_cart" + product).val())+1;

    var product_data = new FormData();
    product_data.append('product', product);
    product_data.append('lang', lang);
    product_data.append('count', count);
    product_data.append('rate', 'AMD');

    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/updatecard',
        data: product_data,
        type: 'POST',
        contentType: false, // The content type used when sending data to the server.
        cache: false, // To unable request pages to be cached
        processData: false,
        success: function (msg) {
            if(msg){
                $(".card_count p").html(msg['all']);
                $(".top-cart-content").html(msg['content']);
                $(".pr_count_in_cart" + product).val(count);

            }

        }
    });
});
$(document).ready(function(){
    $(".order_id").click(function(){

        if($(this).next().hasClass( "open_info" )){
            $(this).next().removeClass("open_info");
            $(this).next().slideUp(1000);
        }else{
            $(".own_one_order.open_info").slideToggle(1000);
            $(".own_one_order").removeClass("open_info");
            $(this).next().addClass("open_info");
            $(this).next().slideToggle(1000);
        }


        $(this).toggleClass("open_job");
        $(this).children().toggleClass("open_job_fa");
        $(this).children().next().toggleClass("job_fa_min_opne");
    });
});
