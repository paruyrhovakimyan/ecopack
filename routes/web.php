<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('admin', 'Admin\LoginController@index');
Route::post('admin', 'Admin\LoginController@login');

Route::group(['middleware'=> ['admin'], 'prefix'=>'admin'], function (){


    Route::get('/upload_content', 'Admin\DashboardController@upload_content');
    Route::post('/upload_content', 'Admin\DashboardController@upload_content');

    Route::get('/homepage', 'Admin\HomePageController@index')->name("home_page");
    Route::post('/homepage', 'Admin\HomePageController@save')->name("home_page");



    Route::post('/pages/addCalendarRow', 'Admin\PagesController@addCalendarRow');
    Route::post('/package_add', 'Admin\PackageController@packageAdd');
    Route::resource('/package', 'Admin\PackageController');
    Route::resource('/prcats', 'Admin\ProductCatController');
    Route::resource('/brands', 'Admin\BrandController');
    Route::resource('/prtype', 'Admin\ProductTypeController');
    Route::resource('/products', 'Admin\ProductController');
    Route::resource('/coachcat', 'Admin\CoachCategoryController');
    Route::resource('/coaches', 'Admin\CoachController');
    Route::resource('/teams', 'Admin\TeamController');
    Route::resource('/career', 'Admin\CareerController');
    Route::resource('/all', 'Admin\AllFunctionsController');
    Route::get('/logout', 'Admin\LoginController@logout');
    Route::get('/dashboard', 'Admin\DashboardController@index');

    Route::resource('/categories', 'Admin\CategoriesController');
    Route::post('/categories/changeHidden', 'Admin\CategoriesController@changeHidden');
    Route::resource('/project_categories', 'Admin\ProjectCategoriesController');
    Route::post('/project_categories/changeHidden', 'Admin\ProjectCategoriesController@changeHidden');

    Route::resource('/pages', 'Admin\PagesController');

    Route::resource('/news', 'Admin\NewsController');

    Route::resource('/articles', 'Admin\ArticlesController');
    Route::get('articles/add_pdf', 'Admin\ArticlesController@add_pdf');
    Route::post('articles/add_pdf', 'Admin\ArticlesController@add_pdf');

    Route::resource('/media', 'Admin\MediaController');

    Route::get('/about_us', 'Admin\AboutController@index')->name('about_us');
    Route::post('/about_us', 'Admin\AboutController@save');

    Route::post('/services/changeHidden', 'Admin\ServicesController@changeHidden');
    Route::resource('/services', 'Admin\ServicesController');
    Route::get('services/add_sub_pages', 'Admin\ServicesController@add_sub_pages');
    Route::post('services/add_sub_pages', 'Admin\ServicesController@add_sub_pages');


    Route::get('/contact', 'Admin\ContactController@index')->name('contact_page');
    Route::post('/contact', 'Admin\ContactController@save');

    Route::resource('/projects', 'Admin\ProjectsController');
    Route::get('/upload', 'Admin\PageGalleryController@upload');
    Route::post('/upload', 'Admin\PageGalleryController@upload');
    Route::post('/projects/changeHidden', 'Admin\ProjectsController@changeHidden');

    Route::resource('/partners', 'Admin\PartnersController');
    Route::get('/partners/upload', 'Admin\PartnersController@upload');
    Route::post('/partners/upload', 'Admin\PartnersController@upload');
    Route::post('/partners/changeHidden', 'Admin\PartnersController@changeHidden');

    Route::resource('/companies', 'Admin\CompaniesController');
    Route::get('/companies/upload', 'Admin\CompaniesController@upload');
    Route::post('/companies/upload', 'Admin\CompaniesController@upload');
    Route::post('/companies/changeHidden', 'Admin\CompaniesController@changeHidden');

    Route::resource('/menu', 'Admin\MenuController');
    Route::get('/menu/save_menu', 'Admin\MenuController@save_menu');
    Route::post('/menu/save_menu', 'Admin\MenuController@save_menu');
    Route::get('/menu/add_link_to_menu', 'Admin\MenuController@add_link_to_menu');
    Route::post('/menu/add_link_to_menu', 'Admin\MenuController@add_link_to_menu');
    Route::resource('/slider', 'Admin\SliderController');
    Route::post('/slider/changeHidden', 'Admin\SliderController@changeHidden');
    Route::post('/slider/slide_delete', 'Admin\SliderController@slide_delete');
    Route::get('/slide/edit/{token}', 'Admin\SliderController@edit_slide')->name("slide_edit");
    Route::get('/slide/create/{token}', 'Admin\SliderController@create')->name("slide_edit");
    Route::get('/slide/update', 'Admin\SliderController@update_slide');
    Route::post('/slide/update', 'Admin\SliderController@update_slide');
    Route::get('/comments', 'Admin\CommentsController@index');
    Route::post('/comments/changeHidden', 'Admin\CommentsController@changeHidden');
    Route::get('/delete_comments/{token}', 'Admin\CommentsController@destroy');
    Route::post('/delete_comments/{token}', 'Admin\CommentsController@destroy');
    Route::resource('/authors', 'Admin\AuthorsController');
    Route::get('authors/add_author', 'Admin\AuthorsController@add_author');
    Route::post('authors/add_author', 'Admin\AuthorsController@add_author');
    Route::get('/users', 'Admin\UsersController@index')->name("users");
    Route::get('/users/{token}', 'Admin\UsersController@show')->name("users");
    Route::get('/delete_user/{token}', 'Admin\UsersController@destroy');
    Route::post('/delete_user/{token}', 'Admin\UsersController@destroy');
    Route::get('/upload/content', 'Admin\DashboardController@upload');
    Route::post('/upload/content', 'Admin\DashboardController@upload');
    Route::resource('/glossary', 'Admin\GlossaryController');
    Route::get('/settings', 'Admin\SettingsController@index')->name("settings");
    Route::post('/settings', 'Admin\SettingsController@save');
    Route::get('/settings/{id}/save', 'Admin\SettingsController@update');
    Route::post('/settings/{id}/save', 'Admin\SettingsController@update');
    Route::get('/settings/delete/{id}', 'Admin\SettingsController@delete');
    Route::post('/settings/delete/{id}', 'Admin\SettingsController@delete');


    Route::get('/sub_pages/{parent}', 'Admin\SubPageController@index');
    Route::get('/sub_pages/{parent}/create', 'Admin\SubPageController@create');
    Route::post('/sub_pages/{parent}/create', 'Admin\SubPageController@store');
    Route::get('/sub_pages/{parent}/edit/{id}', 'Admin\SubPageController@edit');
    Route::get('/sub_pages/{id}/update', 'Admin\SubPageController@update');
    Route::post('/sub_pages/{id}/update', 'Admin\SubPageController@update');



    Route::get('/sub_pages_with_gallery/add/{id}', 'Admin\SubPageWithGalleryController@add_gallery');
    Route::post('/sub_pages_with_gallery/add/{id}', 'Admin\SubPageWithGalleryController@storeGallery');
    Route::get('/sub_pages_with_gallery_delete', 'Admin\SubPageWithGalleryController@gallery_delete');
    Route::post('/sub_pages_with_gallery_delete', 'Admin\SubPageWithGalleryController@gallery_delete');
    Route::get('/sub_pages_with_gallery/edit/{id}', 'Admin\SubPageWithGalleryController@edit_gallery');
    Route::post('/sub_pages_with_gallery/edit/{id}', 'Admin\SubPageWithGalleryController@updateGallery');



    Route::get('/sub_pdf/{parent}', 'Admin\PagePdfController@index');
    Route::get('/sub_pdf/{parent}/create', 'Admin\PagePdfController@create');
    Route::post('/sub_pdf/{parent}/create', 'Admin\PagePdfController@store');
    Route::get('/sub_pdf/{parent}/edit/{id}', 'Admin\PagePdfController@edit');
    Route::post('/sub_pdf/{parent}/edit/{id}', 'Admin\PagePdfController@update');
    Route::get('/page_pdf_delete', 'Admin\PagePdfController@delete_pdf');
    Route::post('/page_pdf_delete', 'Admin\PagePdfController@delete_pdf');
    Route::get('/news_pdf/{parent}', 'Admin\NewsPdfController@index');
    Route::get('/news_pdf/{parent}/create', 'Admin\NewsPdfController@create');
    Route::post('/news_pdf/{parent}/create', 'Admin\NewsPdfController@store');
    Route::get('/news_pdf/{parent}/edit/{id}', 'Admin\NewsPdfController@edit');
    Route::post('/news_pdf/{parent}/edit/{id}', 'Admin\NewsPdfController@update');
    Route::get('/news_pdf_delete', 'Admin\NewsPdfController@delete_pdf');
    Route::post('/news_pdf_delete', 'Admin\NewsPdfController@delete_pdf');
    Route::get('/news_sub_page_pdf/{parent}', 'Admin\NewsSubPageController@index')->name('news_sub_page_pdf');
    Route::get('/news_sub_page_pdf/{parent}/create', 'Admin\NewsSubPageController@create')->name('news_sub_page_pdf');
    Route::post('/news_sub_page_pdf/{parent}/create', 'Admin\NewsSubPageController@store')->name('news_sub_page_pdf');
    Route::get('/news_sub_page_pdf/{parent}/edit/{id}', 'Admin\NewsSubPageController@edit')->name('news_sub_page_pdf');
    Route::post('/news_sub_page_pdf/{parent}/edit/{id}', 'Admin\NewsSubPageController@update')->name('news_sub_page_pdf');
    Route::get('/news_sub_page_pdf_delete', 'Admin\NewsSubPageController@news_sub_page_pdf_delete')->name('news_sub_page_pdf');
    Route::post('/news_sub_page_pdf_delete', 'Admin\NewsSubPageController@news_sub_page_pdf_delete')->name('news_sub_page_pdf');

    Route::post('/news_sub_page_pdf/plus_pdf', 'Admin\NewsSubPageController@plus_pdf')->name('news_sub_page_pdf');

    Route::get('/news_sub_page_pdf/upload_pdf', 'Admin\NewsSubPageController@upload_pdf')->name('news_sub_page_pdf');
    Route::post('/news_sub_page_pdf/upload_pdf', 'Admin\NewsSubPageController@upload_pdf')->name('news_sub_page_pdf');

    Route::get('/news_sub_page_pdf/delete_sub_pdf', 'Admin\NewsSubPageController@delete_sub_pdf');
    Route::post('/news_sub_page_pdf/delete_sub_pdf', 'Admin\NewsSubPageController@delete_sub_pdf');
    Route::get('/news_sub_page_pdf/edit_sub_pdf_block', 'Admin\NewsSubPageController@edit_sub_pdf_block')->name('news_sub_page_pdf');
    Route::post('/news_sub_page_pdf/edit_sub_pdf_block', 'Admin\NewsSubPageController@edit_sub_pdf_block')->name('news_sub_page_pdf');
    Route::get('/news_sub_page_pdf/update_sub_pdf', 'Admin\NewsSubPageController@update_sub_pdf')->name('news_sub_page_pdf');
    Route::post('/news_sub_page_pdf/update_sub_pdf', 'Admin\NewsSubPageController@update_sub_pdf')->name('news_sub_page_pdf');

    Route::get('/sub_pages_with_pdf/{parent}', 'Admin\PageSubWithPdfController@index')->name('sub_pages_with_pdf');
    Route::get('/sub_pages_with_pdf/{parent}/create', 'Admin\PageSubWithPdfController@create')->name('sub_pages_with_pdf');
    Route::post('/sub_pages_with_pdf/{parent}/create', 'Admin\PageSubWithPdfController@store')->name('sub_pages_with_pdf');
    Route::get('/sub_pages_with_pdf/{parent}/edit/{id}', 'Admin\PageSubWithPdfController@edit')->name('sub_pages_with_pdf');
    Route::post('/sub_pages_with_pdf/{parent}/edit/{id}', 'Admin\PageSubWithPdfController@update')->name('sub_pages_with_pdf');
    Route::get('/sub_pages_with_pdf_delete', 'Admin\PageSubWithPdfController@news_sub_page_pdf_delete')->name('sub_pages_with_pdf');
    Route::post('/sub_pages_with_pdf_delete', 'Admin\PageSubWithPdfController@news_sub_page_pdf_delete')->name('sub_pages_with_pdf');

    Route::post('/sub_pages_with_pdf/plus_pdf', 'Admin\PageSubWithPdfController@plus_pdf')->name('sub_pages_with_pdf');

    Route::get('/sub_pages_with_pdf/upload_pdf', 'Admin\PageSubWithPdfController@upload_pdf')->name('sub_pages_with_pdf');
    Route::post('/sub_pages_with_pdf/upload_pdf', 'Admin\PageSubWithPdfController@upload_pdf')->name('sub_pages_with_pdf');

    Route::get('/sub_pages_with_pdf/delete_sub_pdf', 'Admin\PageSubWithPdfController@delete_sub_pdf');
    Route::post('/sub_pages_with_pdf/delete_sub_pdf', 'Admin\PageSubWithPdfController@delete_sub_pdf');
    Route::get('/sub_pages_with_pdf/edit_sub_pdf_block', 'Admin\PageSubWithPdfController@edit_sub_pdf_block')->name('sub_pages_with_pdf');
    Route::post('/sub_pages_with_pdf/edit_sub_pdf_block', 'Admin\PageSubWithPdfController@edit_sub_pdf_block')->name('sub_pages_with_pdf');
    Route::get('/sub_pages_with_pdf/update_sub_pdf', 'Admin\PageSubWithPdfController@update_sub_pdf')->name('sub_pages_with_pdf');
    Route::post('/sub_pages_with_pdf/update_sub_pdf', 'Admin\PageSubWithPdfController@update_sub_pdf')->name('sub_pages_with_pdf');


    Route::get('/page_gallery/add/{id}', 'Admin\PageGalleryController@add_gallery');
    Route::post('/page_gallery/add/{id}', 'Admin\PageGalleryController@storeGallery');
    Route::get('/page_gallery_delete', 'Admin\PageGalleryController@gallery_delete');
    Route::post('/page_gallery_delete', 'Admin\PageGalleryController@gallery_delete');
    Route::get('/page_gallery/edit/{id}', 'Admin\PageGalleryController@edit_gallery');
    Route::post('/page_gallery/edit/{id}', 'Admin\PageGalleryController@updateGallery');
    
    
    //Sub page gallery 
    Route::get('/sub_page_gallery/add/{id}', 'Admin\SubPageGalleryController@add_gallery');
    Route::post('/sub_page_gallery/add/{id}', 'Admin\SubPageGalleryController@storeGallery');
    Route::get('/sub_page_gallery_delete', 'Admin\SubPageGalleryController@gallery_delete');
    Route::post('/sub_page_gallery_delete', 'Admin\SubPageGalleryController@gallery_delete');
    Route::get('/sub_page_gallery/edit/{id}', 'Admin\SubPageGalleryController@edit_gallery');
    Route::post('/sub_page_gallery/edit/{id}', 'Admin\SubPageGalleryController@updateGallery');
    

    Route::get('/news_gallery/add/{id}', 'Admin\NewsGalleryController@add_gallery');
    Route::post('/news_gallery/add/{id}', 'Admin\NewsGalleryController@storeGallery');
    Route::get('/news_gallery_delete', 'Admin\NewsGalleryController@gallery_delete');
    Route::post('/news_gallery_delete', 'Admin\NewsGalleryController@gallery_delete');

    Route::get('/news_gallery/edit/{id}', 'Admin\NewsGalleryController@edit_gallery');
    Route::post('/news_gallery/edit/{id}', 'Admin\NewsGalleryController@updateGallery');



    Route::get('/admin_users', 'Admin\AdminUsersController@index')->name("admin_users");
    Route::get('/admin_users/create', 'Admin\AdminUsersController@create')->name("admin_users");
    Route::post('/admin_users/create', 'Admin\AdminUsersController@store');
    Route::get('/admin_users/edit/{id}', 'Admin\AdminUsersController@edit')->name("admin_users");
    Route::post('/admin_users/edit/{id}', 'Admin\AdminUsersController@update');
    Route::get('/admin_users_delete', 'Admin\AdminUsersController@delete');
    Route::post('/admin_users_delete', 'Admin\AdminUsersController@delete');


    Route::get('/hidden_check', 'Admin\DashboardController@hidden_check');
    Route::post('/hidden_check', 'Admin\DashboardController@hidden_check');
    Route::get('/checked_delete', 'Admin\DashboardController@checked_delete');
    Route::post('/checked_delete', 'Admin\DashboardController@checked_delete');

    Route::get('/orders', 'Admin\DashboardController@donateIrders');

});


Route::get('/package/{token}', 'PageController@singlePackage');
Route::post('/package/{token}', 'PageController@singleOrderPackage');
Route::get('/request/clubcard', 'RequestController@clubCard');
Route::post('/request/clubcard', 'RequestController@clubCard');
Route::post('/career/order/{token}', 'PageController@careerOrder');
Route::post('/donate', 'DonateController@index');
Route::get('/projects/{token}', 'ProjectController@index');
Route::get('/page/{token}', 'PageController@index');
Route::get('/send-mail', 'PageController@sendMail');
Route::post('/send-mail', 'PageController@sendMail');
Route::get('/page_pdf/{token}', 'PageWithPdfController@index');
Route::get('/', 'HomeController@index');
Route::get('/category/{slug}', 'CategoryController@index');
Route::get('/category/{cat}/{slug}', 'ArticleController@index');
Route::get('/subscribe', 'SubscribeController@index');
Route::post('/subscribe', 'SubscribeController@index');
Route::get('/contact_us', 'PageController@contact_us');
Route::post('/contact_us', 'PageController@contact_us');

Route::get('/private_area_login', 'PrivateAreaController@login');
Route::post('/private_area_login', 'PrivateAreaController@login');
Route::get('/search', 'SearchController@index');
Route::post('/search', 'SearchController@index');
Route::get('/donate-back', 'DonateController@back');
Route::get('/success', 'DonateController@success');
Route::get('/fail', 'DonateController@fail');
Route::get('/product/{token}', 'ProductController@index');
Route::post('/product/{token}', 'ProductController@productOrder');
Route::get('/products/{token}', 'ProductCatController@index');
Route::get('/login', 'AccountController@login');
Route::post('/sing-in', 'AccountController@singIn');
Route::get('/registration', 'AccountController@registration');
Route::post('/registration', 'AccountController@registrateUser');
Route::post('/add-to-card', 'ProductController@addToCard');
Route::post('/updatecard', 'ProductController@updateCard');
Route::get('/card', 'CardController@index');
Route::get('/checkout', 'CardController@checkout');
Route::post('/checkout-login', 'CardController@checkoutLogin');
Route::get('/guest', 'CardController@guest');
Route::post('/guest', 'CardController@guestRequest');
Route::get('/checkout-registration', 'CardController@checkoutRegistration');
Route::post('/checkout-registration', 'CardController@checkoutRegistrationRequest');
Route::get('/checkout-logedin', 'CardController@checkoutLogedin');
Route::get('/new-collection', 'ProductCatController@newCollection');
Route::get('/sales-collection', 'ProductCatController@salesCollection');

Route::get('/gallery/{slug}', 'GalleryController@gallery_view');

Route::get('account/change', 'AccountController@profile')->name('profile_edit');
Route::post('account/change', 'AccountController@changeProfile');
Route::get('account/password', 'AccountController@changePassword');
Route::post('account/change-password', 'AccountController@savePassword');
Route::get('account/logout', 'AccountController@logout')->name('profile_logout');

Route::group(['middleware'=> ['account'], 'prefix'=>'account'], function (){
    Route::get('/', 'AccountController@index')->name('profile_dashboard');
    Route::get('/profile', 'AccountController@profile')->name('profile_edit');
    Route::get('/logout', 'AccountController@logout')->name('profile_logout');
    Route::get('/change_password', 'AccountController@change_password')->name('profile_change_password');
});

Route::group(['middleware'=> ['locale'], 'prefix'=>'{lang?}'], function() {
    Route::get('/', 'HomeController@index');
    Route::get('page/{token}', 'PageController@indexLang');
    Route::get('/projects/{token}', 'ProjectController@indexLang');
    Route::get('/page_pdf/{token}', 'PageWithPdfController@indexLang');
    Route::get('/category/{slug}', 'CategoryController@indexLang');
    Route::get('/category/{cat}/{slug}', 'ArticleController@indexLang');
    Route::get('/search', 'SearchController@index');
    Route::post('/search', 'SearchController@index');
    Route::get('/donate-back', 'DonateController@back');
    Route::post('/donate', 'DonateController@indexLang');
    Route::get('/success', 'DonateController@successLang');
    Route::get('/fail', 'DonateController@failLang');
    Route::get('/product/{token}', 'ProductController@indexLang');
    Route::get('/package/{token}', 'PageController@singlePackageLang');
    Route::post('/package/{token}', 'PageController@singleOrderPackageLang');
    Route::get('/products/{token}', 'ProductCatController@indexLang');
    Route::get('/card', 'CardController@indexLang');
    Route::get('/new-collection', 'ProductCatController@newCollectionLang');
    Route::get('/sales-collection', 'ProductCatController@salesCollectionLang');
});



Route::get('/company/{token}', 'CompanyController@index');








