@extends('frontend.company.page')

@section('content')
    <div class="company_left">

        <div class="company_left_logo">
            @if(isset($page->image_logo) && $page->image_logo !='')
            <img src="{{ url('/') }}/public/uploads/page/{{ $page->image_logo }}">
                @endif
        </div>
        <div class="company_desc">
            <h2>{{ $page->title }}</h2>
            <p>{{ $page->short_desc }}</p>
            <p>
                <a id="mi-btn" class="company_more_a st-btn btn-bordered">More Information</a>
                <a class="st-btn btn-fill" href="https://goo.gl/nYZG8t">Get It Now!</a>
            </p>
        </div>
        <div class="st-footer">
            <ul class="social-icons">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
            </ul>
        </div>

    </div>
    <div class="company_right">
        <div class="company_larg_img">
        </div>

        <div class="company_more_info">
            <div class="company_more_desc">
                {!! $page->description  !!}
            </div>
            @if(!empty($page->parent_page_pdfs))
                @foreach($page->parent_page_pdfs as $page_pdfs)

                    <div class="one_pdf_block">
                        <img title="{{ $page_pdfs->title }}" class="pdf_icon" src="{{ url('/') }}/img/printel_icons_pdf.jpg" >
                        <div class="page_pdf_right">
                            <a title="{{ $page_pdfs->title }}" class="pdf_title" target="_blank" href="{{ url('/') }}/uploads/page/pdf_en/{{ $page_pdfs->pdf_file }}">{{ $page_pdfs->title }}</a>
                            <p class="pdf_desc">{{ $page_pdfs->pdf_desc }}</p>
                        </div>
                    </div>


                @endforeach

            @endif


            @if(!empty($page->page_with_pdfs))
                <div class="sub_page_with_pdf_blocks">
                    @foreach($page->page_with_pdfs as $news_sub_page_pdf)

                        <div class="news_sub_page_pdf_one">
                            <p>
                                <span class="sub_page_icon_plus ">
                                    <img title="" class="pdf_icon" src="{{ url('/') }}/img/printel_icons_doc.jpg" >
</span>
                                <span class="sub_page_icon_minus hidden ">
                                    <img title="" class="pdf_icon" src="{{ url('/') }}/img/printel_icons_open_doc.jpg" >
                                </span>
                                {{ $news_sub_page_pdf->title }}
                            </p>
                        </div>
                        <div class="news_sub_page_pdf_list">
                            @if(!empty($news_sub_page_pdf->description ))
                                <div class="description_sub_page">
                                    {!! $news_sub_page_pdf->description !!}
                                </div>
                            @endif
                            @if(!empty($news_sub_page_pdf->sub_page_with_pdf_files))
                                @foreach($news_sub_page_pdf->sub_page_with_pdf_files as $sub_page_pdf)
                                    <div class="one_pdf_block">
                                        <img title="{{ $sub_page_pdf->title }}" class="pdf_icon" src="{{ url('/') }}/img/printel_icons_pdf.jpg">
                                        <div class="page_pdf_right">
                                            <a title="{{ $sub_page_pdf->title }}" class="pdf_title" target="_blank" href="{{ url('/') }}/uploads/page/pdfs/{{ $sub_page_pdf->file }}">{{ $sub_page_pdf->title }}</a>
                                            @if($sub_page_pdf->short_desc != "")
                                                <p class="pdf_desc">{{ $sub_page_pdf->short_desc }}</p>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    @endforeach
                </div>


            @endif


            @if(!empty($page->parent_page_pdfs) && !empty($page->child_page_galleries))
                <hr class="pdf_hr_gallery">
            @endif

            @if(!empty($page->child_page_galleries))

                @foreach($page->child_page_galleries as $page_galleries)
                    <div class="page_gallery_block">
                        <img title="{{ $page_galleries->title }}" class="pdf_icon" src="{{ url('/') }}/public/img/printel_icons_gallery.jpg" >
                        <p class="page_gallery_title">{{ $page_galleries->title }}</p>
                        <div class="page_gallery_desc">
                            <p>{{ $page_galleries->short_desc }}</p>
                        </div>


                        <div class="page_gallery_images">


                            <div class="row magnific-gallery add_bottom_60 ">
                                @if(!empty($page_galleries->images))
                                    @foreach($page_galleries->images as $page_gallery_image)
                                        <div class="col-md-6 col-sm-6">
                                            <div class="img_wrapper_gallery">
                                                <div class="img_container_gallery">
                                                    <a href="{{ url('/') }}/public/uploads/gallery/{{ $page_gallery_image->image }}" title="Photo title">
                                                        <img alt="Image" class="img-responsive" src="{{ url('/') }}/public/uploads/gallery/150/{{ $page_gallery_image->image }}">
                                                        <i class="icon-resize-full-2"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>




                        </div>
                    </div>

                @endforeach
            @endif
            <a id="close-info"><i class="fa fa-window-close" aria-hidden="true"></i></a>
        </div>
    </div>

    <style>
        .company_larg_img{
            background-image: url('{{ url('/') }}/public/uploads/page/{{ $page->image_bg }}');
        }
        .company_left{
            background: {{ $page->bg_color }};
        }
    </style>
@endsection