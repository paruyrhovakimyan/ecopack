@extends('frontend.layouts.page')
@section('meta_title', $meta_data['meta_title'])
@section('meta_desc', $meta_data['meta_desc'])
@section('meta_key', $meta_data['meta_key'])
@section('content')
    @if($page->image_bg !='')
    <div class="page_banner">
        <img src="{{ url('/') }}/public/uploads/page/{{ $page->image_bg }}">
    </div>
    @endif
    <div class="container margin_100">
        <div class="@if(isset($page->contact_page) && $page->contact_page == 1) {{ "contact_page_left" }} @endif">
            <div class="  page_left_title">
                <h2>{{ $page->title }}</h2>
            </div>
            @if($page->short_desc !='')
            <div class="page_short_desc">
                <p>{{ $page->short_desc }}</p>
            </div>
            @endif
            <div class="page_description page_content @if(!empty($page->page_with_pdfs)) {{ "border_bottom_none" }} @endif">
                {!! $page->description !!}
            </div>


            @if(!empty($page_calendars))

                @foreach($page_calendars as $table_key => $page_calendar)
                    @if(isset($page->calendar_times[$table_key]) && !empty($page->calendar_times[$table_key]))

                        <div class="calendar_title">
                            <p>{{ $page_calendar->{"calendar_title_$lang"} }}</p>
                        </div>
                    <table class="calendar_table">
                        <tr>
                            <th width="9%" class="jamer_th">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("calendar_time",$lang) }}</th>
                            <th width="13%">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("calendar_monday",$lang) }}</th>
                            <th width="13%">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("calendar_tuesday",$lang) }}</th>
                            <th width="13%">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("calendar_wednesday",$lang) }}</th>
                            <th width="13%">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("calendar_thursday",$lang) }}</th>
                            <th width="13%">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("calendar_friday",$lang) }}</th>
                            <th width="13%">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("calendar_saturday",$lang) }}</th>
                            <th width="13%">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("calendar_sanday",$lang) }}</th>
                        </tr>

                        @foreach($page->calendar_times[$table_key] as $key => $page_calendar_time)
                            <tr>
                                <td class="jamer_td" width="150px">{{ $page_calendar_time }}</td>
                                <td class="">
                                    <span class="popup_open_info">{{ app('App\Http\Controllers\PageController')->getCoachCat($page->erku_coach_cats["$table_key"][$key],$lang) }}</span>
                                    <p class="hover_popup_img">
                                        {{ app('App\Http\Controllers\PageController')->getCoach($page->erku_coaches["$table_key"][$key],$lang) }}

                                    </p>
                                        {!! app('App\Http\Controllers\PageController')->getCoachImg($page->erku_coaches["$table_key"][$key])  !!}
                                        {!! app('App\Http\Controllers\PageController')->getCalendarInfo($page->erku_coach_cats["$table_key"][$key],$page->erku_coaches["$table_key"][$key],$lang)  !!}
                                </td>
                                <td class="">
                                    <span class="popup_open_info">{{ app('App\Http\Controllers\PageController')->getCoachCat($page->ereq_coach_cats["$table_key"][$key],$lang) }}</span>
                                    <p class="hover_popup_img">{{ app('App\Http\Controllers\PageController')->getCoach($page->ereq_coaches["$table_key"][$key],$lang) }}</p>

                                   {!! app('App\Http\Controllers\PageController')->getCoachImg($page->ereq_coaches["$table_key"][$key])  !!}
                                    {!! app('App\Http\Controllers\PageController')->getCalendarInfo($page->ereq_coach_cats["$table_key"][$key],$page->ereq_coaches["$table_key"][$key],$lang)  !!}
                                </td>
                                <td class="">
                                    <span class="popup_open_info">{{ app('App\Http\Controllers\PageController')->getCoachCat($page->choreq_coach_cats["$table_key"][$key],$lang) }}</span>
                                    <p class="hover_popup_img">{{ app('App\Http\Controllers\PageController')->getCoach($page->choreq_coaches["$table_key"][$key],$lang) }}</p>

                                        {!! app('App\Http\Controllers\PageController')->getCoachImg($page->choreq_coaches["$table_key"][$key])  !!}
                                    {!! app('App\Http\Controllers\PageController')->getCalendarInfo($page->choreq_coach_cats["$table_key"][$key],$page->choreq_coaches["$table_key"][$key],$lang)  !!}
                                </td>
                                <td class="">
                                    <span class="popup_open_info">{{ app('App\Http\Controllers\PageController')->getCoachCat($page->hing_coach_cats["$table_key"][$key],$lang) }}</span>
                                    <p class="hover_popup_img">{{ app('App\Http\Controllers\PageController')->getCoach($page->hing_coaches["$table_key"][$key],$lang) }}</p>
                                     {!! app('App\Http\Controllers\PageController')->getCoachImg($page->hing_coaches["$table_key"][$key])  !!}
                                    {!! app('App\Http\Controllers\PageController')->getCalendarInfo($page->hing_coach_cats["$table_key"][$key],$page->hing_coaches["$table_key"][$key],$lang)  !!}
                                </td>
                                <td class="">
                                    <span class="popup_open_info">{{ app('App\Http\Controllers\PageController')->getCoachCat($page->urbat_coach_cats["$table_key"][$key],$lang) }}</span>
                                    <p class="hover_popup_img">{{ app('App\Http\Controllers\PageController')->getCoach($page->urbat_coaches["$table_key"][$key],$lang) }}</p>
                                      {!! app('App\Http\Controllers\PageController')->getCoachImg($page->urbat_coaches["$table_key"][$key])  !!}
                                    {!! app('App\Http\Controllers\PageController')->getCalendarInfo($page->urbat_coach_cats["$table_key"][$key],$page->urbat_coaches["$table_key"][$key],$lang)  !!}
                                </td>
                                <td class="">
                                    <span class="popup_open_info">{{ app('App\Http\Controllers\PageController')->getCoachCat($page->shabat_coach_cats["$table_key"][$key],$lang) }}</span>
                                    <p class="hover_popup_img">{{ app('App\Http\Controllers\PageController')->getCoach($page->shabat_coaches["$table_key"][$key],$lang) }}</p>
                                      {!! app('App\Http\Controllers\PageController')->getCoachImg($page->shabat_coaches["$table_key"][$key])  !!}
                                    {!! app('App\Http\Controllers\PageController')->getCalendarInfo($page->shabat_coach_cats["$table_key"][$key],$page->shabat_coaches["$table_key"][$key],$lang)  !!}
                                </td>
                                <td class="">
                                    <span class="popup_open_info">{{ app('App\Http\Controllers\PageController')->getCoachCat($page->kiraki_coach_cats["$table_key"][$key],$lang) }}</span>
                                    <p class="hover_popup_img">{{ app('App\Http\Controllers\PageController')->getCoach($page->kiraki_coaches["$table_key"][$key],$lang) }}</p>
                                      {!! app('App\Http\Controllers\PageController')->getCoachImg($page->kiraki_coaches["$table_key"][$key])  !!}
                                    {!! app('App\Http\Controllers\PageController')->getCalendarInfo($page->kiraki_coach_cats["$table_key"][$key],$page->kiraki_coaches["$table_key"][$key],$lang)  !!}
                                </td>
                            </tr>
                        @endforeach

                    </table>
                    @endif
                @endforeach

            @endif


            @if(!empty($page->child_page_galleries))

                @foreach($page->child_page_galleries as $page_galleries)
                    <div class="page_gallery_block page_pdf_gallery_block">
                        <img title="{{ $page_galleries->title }}" class="pdf_icon" src="{{ url('/') }}/img/printel_icons_gallery.jpg" >
                        <p class="page_gallery_title">{{ $page_galleries->title }}</p>
                        <div class="page_gallery_desc">
                            <p>{{ $page_galleries->short_desc }}</p>
                        </div>


                        <div class="page_gallery_images">


                            <div class="row magnific-gallery add_bottom_60 ">
                                @if(!empty($page_galleries->images))
                                    @foreach($page_galleries->images as $page_gallery_image)
                                        <div class="col-md-4 col-sm-4">
                                            <div class="img_wrapper_gallery">
                                                <div class="img_container_gallery">
                                                    <a href="{{ url('/') }}/public/uploads/gallery/{{ $page_gallery_image->image }}" title="Photo title">
                                                        <img alt="Image" class="img-responsive" src="{{ url('/') }}/uploads/gallery/150/{{ $page_gallery_image->image }}">
                                                        <i class="icon-resize-full-2"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>




                        </div>
                    </div>

                @endforeach
            @endif

            @if(!empty($page->calendar_time))

                <table class="calendar_table">
                    <tr>
                        <th class="jamer_th">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("calendar_time",$lang) }}</th>
                        <th>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("calendar_monday",$lang) }}</th>
                        <th>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("calendar_tuesday",$lang) }}</th>
                        <th>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("calendar_wednesday",$lang) }}</th>
                        <th>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("calendar_thursday",$lang) }}</th>
                        <th>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("calendar_friday",$lang) }}</th>
                        <th>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("calendar_saturday",$lang) }}</th>
                    </tr>
                    @foreach($page->calendar_time as $key => $page_calendar_time)
                        <tr>
                            <td class="jamer_td" width="150px">{{ $page_calendar_time }}</td>
                            <td class="calendar_td{{ $page->erku_coachs[$key] }}">
                                <span>{{ app('App\Http\Controllers\PageController')->getCoachCat($page->erku_coach_cat[$key],$lang) }}</span>
                                <p>{{ app('App\Http\Controllers\PageController')->getCoach($page->erku_coachs[$key],$lang) }}</p>
                            </td>
                            <td class="calendar_td{{ $page->ereq_coachs[$key] }}">
                                <span>{{ app('App\Http\Controllers\PageController')->getCoachCat($page->ereq_coach_cat[$key],$lang) }}</span>
                                <p>{{ app('App\Http\Controllers\PageController')->getCoach($page->ereq_coachs[$key],$lang) }}</p>
                            </td>
                            <td class="calendar_td{{ $page->choreq_coachs[$key] }}">
                                <span>{{ app('App\Http\Controllers\PageController')->getCoachCat($page->choreq_coach_cat[$key],$lang) }}</span>
                                <p>{{ app('App\Http\Controllers\PageController')->getCoach($page->choreq_coachs[$key],$lang) }}</p>
                            </td>
                            <td class="calendar_td{{ $page->hing_coachs[$key] }}">
                                <span>{{ app('App\Http\Controllers\PageController')->getCoachCat($page->hing_coach_cat[$key],$lang) }}</span>
                                <p>{{ app('App\Http\Controllers\PageController')->getCoach($page->hing_coachs[$key],$lang) }}</p>
                            </td>
                            <td class="calendar_td{{ $page->urbat_coachs[$key] }}">
                                <span>{{ app('App\Http\Controllers\PageController')->getCoachCat($page->urbat_coach_cat[$key],$lang) }}</span>
                                <p>{{ app('App\Http\Controllers\PageController')->getCoach($page->urbat_coachs[$key],$lang) }}</p>
                            </td>
                            <td class="calendar_td{{ $page->shabat_coachs[$key] }}">
                                <span>{{ app('App\Http\Controllers\PageController')->getCoachCat($page->shabat_coach_cat[$key],$lang) }}</span>
                                <p>{{ app('App\Http\Controllers\PageController')->getCoach($page->shabat_coachs[$key],$lang) }}</p>
                            </td>
                        </tr>
                    @endforeach
                </table>
            @endif

            @if(isset($invesment_page) && $invesment_page == 1)
                <div class="page_investment">
                    @foreach($invesment_sub_pages as $investment)
                        <div class="invesment_box">
                            <div class="invesment_logo">
                                <img src="{{ url('/') }}/public/uploads/page/{{ $investment->image_logo }}" class="img-responsive" alt="image">
                            </div>
                            <div class="invesment_info">
                                <a href="{{ app('App\Http\Controllers\GlossaryController')->getTranslate("home_url",$lang) }}/page/{{ $investment->slug }}">{{ $investment->{"title_".$lang} }}</a>
                                <p>{{ $investment->{"short_desc_".$lang} }}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif


                @if(isset($page->contact_page) && $page->contact_page == 1)
                <div class="right sidebar">
                    <div class="contact_form">
                        <form class="send_form" action="{{ url('/') }}/send-mail" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("first_name",$lang) }}</label>
                                <input type="text" name="contact_name" value="" class="form-control contact_name">
                            </div>
                            <div class="form-group">
                                <label>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("mail",$lang) }}</label>
                                <input type="email" name="contact_mail" value="" class="form-control contact_mail">
                            </div>
                            <div class="form-group">
                                <label>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("phone",$lang) }}</label>
                                <input type="text" name="contact_phone" value="" class="form-control contact_phone">
                            </div>
                            <div class="form-group">
                                <label>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("contact_message",$lang) }}</label>
                                <textarea name="contact_text" class="form-control contact_text"></textarea>
                            </div>
                            <div class="form-group">
                                <a class="contact_btn pull-right btn btn-primary">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("contact_send",$lang) }}</a>
                            </div>
                            @if (session('status'))
                                <div class="senr_message">
                                    {{ session('status') }}
                                </div>
                            @endif
                        </form>
                    </div>
                </div>
                @endif




        </div>

    </div>
@endsection