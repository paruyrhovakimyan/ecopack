@extends('frontend.layouts.page')
<script src="{{ asset('/public/js/jssor.slider.min.js') }}"></script>
@section('content')
    <style>
        .jssorb051 .i {
            position: absolute;
            cursor: pointer;
        }

        .jssorb051 .i .b {
            fill: #fff;
            fill-opacity: 0.5;
            stroke: #000;
            stroke-width: 400;
            stroke-miterlimit: 10;
            stroke-opacity: 0.5;
        }

        .jssorb051 .i:hover .b {
            fill-opacity: .7;
        }

        .jssorb051 .iav .b {
            fill-opacity: 1;
        }

        .jssorb051 .i.idn {
            opacity: .3;
        }
    </style>

    <!--#region Arrow Navigator Skin -->
    <!-- Help: https://www.jssor.com/development/slider-with-arrow-navigator.html -->
    <style>
        .jssora051 {
            display: block;
            position: absolute;
            cursor: pointer;
        }

        .jssora051 .a {
            fill: none;
            stroke: #fff;
            stroke-width: 360;
            stroke-miterlimit: 10;
        }

        .jssora051:hover {
            opacity: .8;
        }

        .jssora051.jssora051dn {
            opacity: .5;
        }

        .jssora051.jssora051ds {
            opacity: .3;
            pointer-events: none;
        }
    </style>


    <div id="slider1_container"
         style="position: relative; margin: 0 auto;top: 0px; left: 0px; width: 1600px; height: 470px; overflow: hidden;">

        <!-- Loading Screen -->
        <div data-u="loading" class="jssorl-009-spin"
             style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;"
                 src="{{ url('/') }}/public/img/spin.svg"/>
        </div>

        <!-- Slides Container -->
        <div class="slider_content" data-u="slides" style="position: absolute; left: 0px; top: 0px; width: 1600px;
            height: 470px; overflow: hidden;">
            @foreach($slider as $slide)
                <div>
                    <img data-u="image" src="{{ url('/') }}/public/uploads/slider/{{ $slide->image }}" alt=""/>
                    @if($slide->{ "slide_title_".$lang} !='')

                        <div class="slider_text">
                            <h3>
                                {{ $slide->{ "slide_title_".$lang} }}
                            </h3>

                        </div>
                        <div class="slider_text1">
                            @if($slide->{ "slide_sub_title_".$lang} !='')
                                <p>{{ $slide->{ "slide_sub_title_".$lang} }}</p>
                            @endif
                        </div>
                        @if($slide->slider_icon !='')
                            <div class="slider_icon">
                                @if($slide->slide_contact !='')
                                <p>{{ $slide->slide_contact }}</p>
                                @endif
                                <img src="{{ url('/') }}/public/uploads/slider/{{ $slide->slider_icon }}">

                            </div>
                        @endif
                    @endif
                </div>
            @endforeach
        </div>

        <!--#region Arrow Navigator Skin Begin -->
        <!-- Help: https://www.jssor.com/development/slider-with-arrow-navigator.html -->
        <div data-u="arrowleft" class="jssora051" style="width:75px;height:75px;top:0px;left:25px;" data-autocenter="2"
             data-scale="0.75" data-scale-left="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora051" style="width:75px;height:75px;top:0px;right:25px;"
             data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
            </svg>
        </div>
        <!--#endregion Arrow Navigator Skin End -->


    </div>

<?php  /*
    <div class="home_categories">
        <div class="container">
            <div class="home_categories_title">
                <hr class="home_categories_title_hr">
                <p>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("products",$lang) }}</p>
            </div>
            <div class="home_categories_items">
                @foreach($categories as $category)

                    <div class="home_category_item">
                        <div class="home_category_item_img">
                            <a href="{{ url('/') }}/products/{{$category->id}}">
                                <img src="{{ url('/') }}/public/uploads/prcat/{{ $category->pr_cat_image }}">
                            </a>
                        </div>
                        <div class="home_category_item_title">
                            <a href="{{ url('/') }}/products/{{$category->id}}">{{ $category->{"title_".$lang} }}</a>
                        </div>


                    </div>

                @endforeach
            </div>
        </div>

    </div>
*/ ?>
    <div class="home_categories">
        <div class="container">
            <div class="home_categories_title">
                <p>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("products",$lang) }}</p>
            </div>
        </div>
        <div class="container">
            <div class="@if($mobile_brows == 1) {{ "mobile_sloder" }} @endif @if(count($categories) > 7) {{ "carousel_category" }} @endif">
                <div class="carousel-button-left"><a href="#"></a></div>
                <div class="carousel-button-right"><a href="#"></a></div>
                <div class="carousel-wrapper">
                    <div class="carousel-items">

                        @if(!empty($artadranqner))
                            @foreach($artadranqner as $category)
                                <div class="carousel-block">
                                    <div class="home_category_item">
                                        <div class="home_category_item_img">
                                            <a href="{{ url('/') }}/page/{{$category->slug}}">
                                                <img src="{{ url('/') }}/public/uploads/page/{{ $category->image }}">
                                            </a>
                                        </div>
                                        <div class="home_category_item_title">
                                            <a href="{{ url('/') }}/page/{{$category->slug}}">{{ $category->{"title_".$lang} }}</a>
                                        </div>


                                    </div>
                                </div>
                            @endforeach
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container con_container">
        <div class="con_1">
            <img class="cont_1_img" src="{{ url('/') }}/uploads/home_page/{{$welcome_text->image}}" alt="">
            <div class="con_1_con">
                <div class="cont_1_title">{{ $welcome_text->{"about_us_title_".$lang} }}</div>
                <div class="con_1_content">{!! $welcome_text->{"about_us_desc_".$lang} !!}</div>
            </div>
        </div>
        <div class="con_1 con_2_bg">
            <img class="cont_2_img" src="{{ url('/') }}/uploads/home_page/{{$welcome_text->block_2_img}}" alt="">
            <div class="con_1_con">
                <div class="cont_1_title">{{ $welcome_text->{"block_2_title_".$lang} }}</div>
                <div class="con_1_content">{!! $welcome_text->{"block_2_desc_".$lang} !!}</div>
            </div>
        </div>
        <div class="con_1 con_3_bg">
            <img class="cont_1_img" src="{{ url('/') }}/uploads/home_page/{{$welcome_text->block_3_img}}" alt="">
            <div class="con_1_con">
                <div class="cont_1_title">{{ $welcome_text->{"block_3_title_".$lang} }}</div>
                <div class="con_1_content">{!! $welcome_text->{"block_3_desc_".$lang} !!}</div>
            </div>
        </div>
    </div>

    <div class="events">
        <div class="container">
            <div class="events_main_title">
                <a href="{{ app('App\Http\Controllers\GlossaryController')->getTranslate("home_url",$lang) }}page/noroutyounner">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("news_title",$lang) }}</a>
                <hr class="home_categories_title_hr2">
            </div>
            <div class="row">

                @if(!empty($newses_stick))
                    @foreach($newses_stick as $news)
                        <div class="col-md-4">
                            <div class="home_news_img">
                                <img src="{{ url('/') }}/uploads/news/{{ $news->image }}" alt="">
                            </div>

                            <div class="events_title">
                                <p class="events_title_p"><a class="event_list_a" href="{{ app('App\Http\Controllers\CategoryController')->getPermalink($news->slug, $lang) }}">{{ $news->{"title_".$lang} }}</a></p>
                               </div>
                        </div>
                    @endforeach
                @endif
            </div>
{{--            <div class="more_items">--}}
{{--                <a href="{{ app('App\Http\Controllers\GlossaryController')->getTranslate("home_url",$lang) }}page/noroutyounner">ՏԵՍՆԵԼ ԱՎԵԼԻՆ</a>--}}
{{--            </div>--}}
        </div>
    </div>

{{--    <div class="oru_partners">--}}
{{--        <div class="container">--}}
{{--            <div class="partner_slider">--}}
{{--                <div class="partner_slider_title">--}}
{{--                    <p>գործընկերներ</p>--}}

{{--                </div>--}}
{{--                <div class="partner_slider_blok">--}}
{{--                    <div class="container">--}}
{{--                        <div class="@if(count($partners) > 6) {{ "carousel shadow" }} @endif">--}}
{{--                            <div class="carousel-button-left"><a href="#"></a></div>--}}
{{--                            <div class="carousel-button-right"><a href="#"></a></div>--}}
{{--                            <div class="carousel-wrapper">--}}
{{--                                <div class="carousel-items">--}}

{{--                                    @if(!empty($partners))--}}
{{--                                        @foreach($partners as $partner)--}}
{{--                                            <div class="carousel-block">--}}
{{--                                                <a target="_blank" href="{{ $partner->link }}">--}}
{{--                                                    <img title="{{ $partner->{"title_".$lang} }}" src="{{ url('/') }}/uploads/partners/150/{{ $partner->image }}"--}}
{{--                                                         alt="{{ $partner->{"title_".$lang} }}"/>--}}
{{--                                                </a>--}}

{{--                                            </div>--}}
{{--                                        @endforeach--}}
{{--                                    @endif--}}

{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}



{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    <script>
        jssor_slider1_init = function () {
            var options = {
                $FillMode: 2,                                       //[Optional] The way to fill image in slide, 0 stretch, 1 contain (keep aspect ratio and put all inside slide), 2 cover (keep aspect ratio and cover whole slide), 4 actual size, 5 contain for large image, actual size for small image, default value is 0
                $AutoPlay: 1,                                    //[Optional] Auto play or not, to enable slideshow, this option must be set to greater than 0. Default value is 0. 0: no auto play, 1: continuously, 2: stop at last slide, 4: stop on click, 8: stop on user navigation (by arrow/bullet/thumbnail/drag/arrow key navigation)
                $Idle: 4000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 1,                                   //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

                $ArrowKeyNavigation: 1,   			            //[Optional] Steps to go for each navigation request by pressing arrow key, default value is 1.
                $SlideEasing: $Jease$.$OutQuint,          //[Optional] Specifies easing for right to left animation, default value is $Jease$.$OutQuad
                $SlideDuration: 1200,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide, default value is 20
                //$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                $SlideHeight: 470,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
                $SlideSpacing: 0, 					                //[Optional] Space between each slide in pixels, default value is 0
                $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                $DragOrientation: 1,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $Cols is greater than 1, or parking position is not 0)


                $ArrowNavigatorOptions: {                       //[Optional] Options to specify and enable arrow navigator or not
                    $Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
                }
            };

            var jssor_slider1 = new $JssorSlider$('slider1_container', options);

            /*#region responsive code begin*/

            //you can remove responsive code if you don't want the slider scales while window resizing
            function ScaleSlider() {
                var bodyWidth = document.body.clientWidth;
                if (bodyWidth)
                    jssor_slider1.$ScaleWidth(Math.min(bodyWidth, 1920));
                else
                    $Jssor$.$Delay(ScaleSlider, 30);
            }

            ScaleSlider();
            $Jssor$.$AddEvent(window, "load", ScaleSlider);

            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>

    <script>
        jssor_slider1_init();
    </script>
@endsection

