@extends('frontend.layouts.page')
@section('meta_title', "ԳՆՈՒՄՆԵՐԻ ԶԱՄԲՅՈՒՂ")
@section('meta_desc', "ԳՆՈՒՄՆԵՐԻ ԶԱՄԲՅՈՒՂ")
@section('meta_key', "ԳՆՈՒՄՆԵՐԻ ԶԱՄԲՅՈՒՂ")
@section('content')
    <div class="container margin_100">
        <div class="page_left_title">
            <h2>Պատվերի ձևակերպում</h2>
        </div>
        <div class="page_description page_content">
            <div class="checkout_form">
                <div class="checkout_new_user">
                    <div class="new_user_title">Նոր գնորդ</div>
                    <input type="radio" name="registration_or_guest" value="guest" class="registration_or_guest" checked=""><label>Պատվերի ձևակերպում առանց գրանցվելու</label><br>
                    <input type="radio" name="registration_or_guest" value="registration" class="registration_or_guest"><label>Ձևակերպել պատվերը և գրանցվել</label><br>
                    <input type="button" class="continue" value="Շարունակել" name="continue_ordering">
                </div>
                <div class="checkout_login">
                    <div class="new_user_title">Գրանցված օգտվող</div>
                    <form class="polzovatel_form" action="{{ url('/') }}/checkout-login" method="post">
                        {{ csrf_field() }}
                        <label>Էլ. փոստ *</label><br><input type="email" name="email" class="user_email"><br>
                        <label>Գաղտնաբառը *</label><br><input type="password" name="password" class="user_pass"><br>
                        <input type="submit" name="login" class="login_ok" value="Մուտք գործել">
                    </form>
                </div>
               </div>
        </div>
    </div>
@endsection