@extends('frontend.layouts.page')
@section('meta_title', $meta_data['meta_title'])
@section('meta_desc', $meta_data['meta_desc'])
@section('meta_key', $meta_data['meta_key'])
@section('content')
    <div class="container margin_100">
        <div class="page_left_title">
            <h2>Պատվիրել {{ $package->{"title_$lang"} }}</h2>
            <div class="single_package_desc">
                @if($package->{"package_desc_$lang"} !='')
                    <div class="package_service">
                        <ul>
                            @foreach(json_decode($package->{"package_desc_$lang"}) as $key => $package_desc_am)
                                <li><p>{{ $package_desc_am }}</p></li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="order_package_form">

                <form action="{{ url('/') }}/package/{{ $package->id }}" method="post">
                    {{ csrf_field() }}
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("name_surname",$lang) }}</label>
                            <input type="text" name="name_surname" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("mail",$lang) }}</label>
                            <input type="email" name="mail" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("phone",$lang) }}</label>
                            <input type="text" name="phone" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("contact_message",$lang) }}</label>
                            <textarea name="message" class="form-control"></textarea>
                        </div>
                        <div class="">
                            <input class="package_order_submit" type="submit" placeholder="" value="{{ app('App\Http\Controllers\GlossaryController')->getTranslate("contact_send",$lang) }}">
                        </div>
                    </div>

                </form>
            </div>

        </div>
    </div>
@endsection