@extends('frontend.layouts.page')
@section('meta_title', "ԳՆՈՒՄՆԵՐԻ ԶԱՄԲՅՈՒՂ")
@section('meta_desc', "ԳՆՈՒՄՆԵՐԻ ԶԱՄԲՅՈՒՂ")
@section('meta_key', "ԳՆՈՒՄՆԵՐԻ ԶԱՄԲՅՈՒՂ")
@section('content')
    <div class="container margin_100">
        <div class="page_left_title">
            <h2>ԳՆՈՒՄՆԵՐԻ ԶԱՄԲՅՈՒՂ</h2>
        </div>
        <div class="page_description page_content">
            <div class="cart_table">
                @foreach($result_cart_meta_show as $row_cart_meta_show)
                    <div class="card_product_block">
                        <div class="cart_prod_img">
                            <img src="{{ url('/') }}/public/uploads/product/thumbs/{{ $row_cart_meta_show->meta_product->image }}">
                        </div>

                        <div class="cart_prod_title">
                            <p>{{ $row_cart_meta_show->meta_product->{'title_'.$lang} }}</p>
                            <span>{{ $row_cart_meta_show->meta_product->pr_price }} <img src="{{ url('/') }}/public/img/dram_1.png" alt=""></span>
                        </div>
                        <div class="cart_prod_count">
                            <a data-lang="{{ $lang }}"  data-id="{{ $row_cart_meta_show->product_id }}" style="cursor: pointer" class="cart_minus">-</a>
                            <input data-lang="{{ $lang }}"  data-id="{{ $row_cart_meta_show->product_id }}" type="text" onkeypress="return isNumberKey(event)" class="pr_count_in_cart pr_count_in_cart{{ $row_cart_meta_show->product_id }}" value="{{ $row_cart_meta_show->product_count }}" name="pr_count_in_cart">
                            <a data-lang="{{ $lang }}"  data-id="{{ $row_cart_meta_show->product_id }}" style="cursor: pointer" class="cart_plus">+</a>
                        </div>

                        <div class="cart_prod_all_price">
                            <p>{{ $row_cart_meta_show->product_count * $row_cart_meta_show->meta_product->pr_price }} <img src="{{ url('/') }}/public/img/dram_1.png" alt=""></p>
                            <a style="cursor: pointer" class="delete_item delbtn77" data-id="{{ $row_cart_meta_show->product_id }}">Հեռացնել Զամբյուղից</a>
                            <input type="hidden" name="pr_id" class="pr_id_del" value="{{ $row_cart_meta_show->product_id }}">
                        </div>
                    </div>

                @endforeach
                <div class="update_card">
                    <button class="update_card_btn">Update Card</button>
                </div>
            </div>
            <div class="card_checkout_block">
                <div class="all_price"> Ընդհանուր: <span class="all_price_number">{{ $all_price_end }}</span> AMD</div>
                <div class="korzina_bottom">
                    <a href="{{ url('/') }}"><div class="next_prod">Շարունակել գնումները     </div></a>
                    <a href="{{ url('/') }}/checkout"><div class="aformit">Պատվերի ձևակերպում</div></a>
                </div>
            </div>
        </div>
    </div>

@endsection