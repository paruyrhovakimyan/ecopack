@extends('frontend.layouts.page')
@section('meta_title', $meta_data['meta_title'])
@section('meta_desc', $meta_data['meta_desc'])
@section('meta_key', $meta_data['meta_key'])
@section('content')
    <div class="container single_block">
        <div class="home_categories_title">
            <p>{{ $gallery->{"title_$lang"} }}</p>
        </div>
        <div class="">
            <p class="left_head_text">{!! $gallery->{"short_desc_$lang"} !!}</p>
        </div>


    <div class="page_gallery_images">

        <div class="row magnific-gallery add_bottom_60 ">
                @foreach($gallery_images as $page_gallery_image)
                    <div class="col-md-3 col-sm-3">
                        <div class="img_wrapper_gallery">
                            <div class="img_container_gallery">
                                <a href="{{ url('/') }}/public/uploads/gallery/{{ $page_gallery_image->image }}" title="Photo title">
                                    <img alt="Image" class="img-responsive" src="{{ url('/') }}/uploads/gallery/150/{{ $page_gallery_image->image }}">
                                    <i class="icon-resize-full-2"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
        </div>

    </div>

    </div>
@endsection