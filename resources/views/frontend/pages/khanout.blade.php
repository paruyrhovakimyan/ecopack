@extends('frontend.layouts.page')
@section('meta_title', $meta_data['meta_title'])
@section('meta_desc', $meta_data['meta_desc'])
@section('meta_key', $meta_data['meta_key'])
@section('content')
    <div class="link">
        <div class="container">
            {!! $bradcramp !!}
        </div>
    </div>
    <div class="container margin_100">
        <div class="we_logo">
            <div class="container">
                <img class="we_logo_img" src="{{ url('/') }}/public/img/Object21.png" alt="">
                <h3 class="we_logo_h3">{{ $page->title }}</h3>
            </div>
        </div>
        <div class="clear"></div>


        @if($page->description !='')
            <div class="page_description custom_page_content @if(!empty($page->page_with_pdfs)) {{ "border_bottom_none" }} @endif">
                {!! $page->description !!}
            </div>
        @endif

        <div class="shop">
                <div class="container">
                    <div class="page_left_title">
                        <h4 class="oru_partners_h3 packages_h3">Վերջին ներմուծած ապրանքներ</h4>
                    </div>

                    <div class="row">

                        @foreach($new_collection_products as $new_collection_product)
                            <div class="col-md-3 shop_article">
                                <a href="{{ app('App\Http\Controllers\GlossaryController')->getTranslate("home_url",$lang) }}product/{{ $new_collection_product->id }}"><img class="shop_article_img" src="{{ url('/') }}/uploads/product/thumbs/{{$new_collection_product->image}}" alt=""></a>
                                <p class="shop_article_desc">{{ $new_collection_product->{"title_$lang"} }}</p>
                                <p class="shop_article_price">{{$new_collection_product->pr_price}} դրամ</p>
                            </div>
                        @endforeach

                    </div>
                </div>
            <div class="container shop_sec shop_article">
                <div class="row">
                    <div class="col-md-3 shop_left">
                        @foreach($productCats as $productCat)
                            <a class="shop_left_a" href="{{ app('App\Http\Controllers\GlossaryController')->getTranslate("home_url",$lang) }}products/{{ $productCat->id }}">{{ $productCat->{"title_$lang"} }}</a>

                        @endforeach
                    </div>
                    <div class="col-md-9 shop_right">
                        @foreach($all_products as $product)
                                <div class="col-md-4 product_item">
                                    <a href="{{ app('App\Http\Controllers\GlossaryController')->getTranslate("home_url",$lang) }}product/{{ $product->id }}"><img class="shop_article_img" src="{{ url('/') }}/uploads/product/thumbs/{{$product->image}}" alt=""></a>
                                    <p class="shop_article_desc">{{ $product->{"title_$lang"} }}</p>
                                    <p class="shop_article_price">{{$product->pr_price}} դրամ</p>
                                </div>
                        @endforeach
                            <div class="news_pag">
                                {{ $all_products->links() }}
                            </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection