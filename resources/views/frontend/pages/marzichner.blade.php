@extends('frontend.layouts.page')
@section('meta_title', $meta_data['meta_title'])
@section('meta_desc', $meta_data['meta_desc'])
@section('meta_key', $meta_data['meta_key'])
@section('content')
    <div class="link">
        <div class="container">
            {!! $bradcramp !!}
        </div>
    </div>
    <div class="container margin_100">
        <div class="we_logo">
            <div class="container">
                <img class="we_logo_img" src="{{ url('/') }}/public/img/Object21.png" alt="">
                <h3 class="we_logo_h3">{{ $page->title }}</h3>
            </div>
        </div>
        <div class="clear"></div>


        @if($page->description !='')
        <div class="page_description custom_page_content @if(!empty($page->page_with_pdfs)) {{ "border_bottom_none" }} @endif">
            {!! $page->description !!}
        </div>
        @endif

        <div class="stuff">

            @foreach($coachCats as $coachCat)
                @if(isset($coachCat->coaches) && count($coachCat->coaches) > 0)
                <div class="container">
                        <div class="page_left_title">
                            <h2>{{ $coachCat->{"title_$lang"} }}</h2>
                        </div>

                        <div class="coaches_list">
                            @if($coachCat->coaches)
                                @foreach($coachCat->coaches as $coach)
                                    <div class="col-md-4 coach_block">
                                        <img class="stuff_img" src="{{ url('/') }}/public/uploads/coach/{{ $coach->image }}" alt="">
                                        <div class="team_title_boc">
                                            <h3>{{ $coach->{"title_$lang"} }}</h3>
                                        </div>
                                        <div class="stuff_desc">
                                            {!! $coach->{"short_desc_$lang"}  !!}
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                </div>
                @endif
            @endforeach

            <div class="container">
                <div class="row">

                </div>
            </div>
        </div>
    </div>

@endsection