@extends('frontend.layouts.page')
@section('meta_title', $meta_data['meta_title'])
@section('meta_desc', $meta_data['meta_desc'])
@section('meta_key', $meta_data['meta_key'])
@section('content')
    <div class="link">
        <div class="container">
            {!! $bradcramp !!}
        </div>
    </div>
    <div class="container margin_100">

        <div class="page_left_title">
            <h2>{{ $page->title }}</h2>
        </div>
        @if($page->description !='')
        <div class="page_description custom_page_content @if(!empty($page->page_with_pdfs)) {{ "border_bottom_none" }} @endif">
            {!! $page->description !!}
        </div>

        @endif

        <div class="our_packages">
            @foreach($packages as $key => $package)


                <div style="height: {{ $package->height }}px" class="package_block">
                    <div class="package_img">
                        <img src="{{ url('/') }}/public/uploads/news/{{ $package->image }}">
                    </div>
                    <div class="package_title">
                        {{ $package->{"title_$lang"} }}
                    </div>


                    @if($package->{"package_desc_$lang"} !='')
                        <div class="package_service">
                            <ul>
                                @foreach(json_decode($package->{"package_desc_$lang"}) as $key => $package_desc_am)
                                    <li><p>{{ $package_desc_am }}</p></li>
                                @endforeach
                                <li class="package_order_li"><a href="{{ app('App\Http\Controllers\GlossaryController')->getTranslate("home_url",$lang) }}package/{{ $package->id }}">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("package_order",$lang) }}</a></li>
                            </ul>
                        </div>
                    @endif

                </div>
            @endforeach
        </div>


    </div>
@endsection