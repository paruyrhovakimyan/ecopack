@extends('frontend.layouts.page')
@section('meta_title', $meta_data['meta_title'])
@section('meta_desc', $meta_data['meta_desc'])
@section('meta_key', $meta_data['meta_key'])
@section('content')
    <div class="link">
        <div class="container">
            {!! $bradcramp !!}
        </div>
    </div>
    <div class="container margin_100">
        <div class="">
            <div class="page_left_title">
                <h2>{{ $page->title }}</h2>
            </div>
        </div>
        @if($page->description !='')
            <div class="page_description page_content">
                {!! $page->description !!}
            </div>
        @endif
        <div class="stuff">
            <div class="container">
                <div class="row">
                    {!!  app('App\Http\Controllers\PageController')->getAllTeams($lang)  !!}
                </div>
            </div>
        </div>
        <div class="container">
            <hr>
        </div>


    </div>
    @if (session('career_order_status'))
        <div class="career_order_message">
            {{ session('career_order_status') }}
        </div>
    @endif
@endsection