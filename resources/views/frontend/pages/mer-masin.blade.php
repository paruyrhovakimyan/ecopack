@extends('frontend.layouts.page')
@section('meta_title', $meta_data['meta_title'])
@section('meta_desc', $meta_data['meta_desc'])
@section('meta_key', $meta_data['meta_key'])
@section('content')
    <div class="container margin_100">


            @if($welcome_text->image !='')
            <div class="about_us_img">
                <img src="{{ url('/') }}/public/uploads/home_page/{{ $welcome_text->image }}" alt="" class="about_image">
            </div>
            @endif

        <div class="page_left_title">
            <h2>{{ $welcome_text->{"about_us_title_".$lang} }}</h2>
        </div>
        <div class="page_description">
            {!! $welcome_text->{"about_us_desc_".$lang} !!}
        </div>
    </div>

@endsection


