@extends('frontend.layouts.page')
@section('meta_title', $meta_data['meta_title'])
@section('meta_desc', $meta_data['meta_desc'])
@section('meta_key', $meta_data['meta_key'])
@section('content')
    <div class="link">
        <div class="container">
            {!! $bradcramp !!}
        </div>
    </div>

    <div class="container margin_100">
        <div class="page_left_title">
            <h2>{{ $page->title }}</h2>
        </div>

        <div class="page_description custom_page_content @if(!empty($page->page_with_pdfs)) {{ "border_bottom_none" }} @endif">
            {!! $page->description !!}
        </div>
        <div class="our_friends">
            @php $count = 1; @endphp
            @foreach($partners as $key => $partner)
                <div class="our_friend_box  @if($key >= (count($partners) - count($partners)%5) || (count($partners)%5 == 0 && $key >= (count($partners) - 5))) {{"no_border_bottom"}}@endif  @if($count%5 == 0) {{ "no_border_right" }} @endif">
                    <a target="_blank" href="{{ $partner->link }}">
                        <img title="{{ $partner->{"title_".$lang} }}" src="{{ url('/') }}/uploads/partners/150/{{ $partner->image }}"
                             alt="{{ $partner->{"title_".$lang} }}"/>
                    </a>
                </div>
                @php $count++; @endphp
            @endforeach
        </div>
    </div>

@endsection