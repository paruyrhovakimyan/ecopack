@extends('frontend.layouts.page')

@section('content')
    <div class="container single_block">
        <div class="category_news">
            <div class="home_categories_title">
                <p>ԷԿՈԼՈԳԻԱ</p>
            </div>
            @if(!empty($category_news))
                @foreach($category_news as $key => $news)
                    <div class="post @if($key%3 == 0) {{"cat_first_post"}} @elseif($key%3 == 1) {{"cat_second_post"}} @else {{"cat_last_post"}} @endif ">
                        <a href="{{ app('App\Http\Controllers\CategoryController')->getPermalink($news->slug, $lang) }}">
                            <img class="news_img" src="{{ url('/') }}/uploads/news/{{ $news->image }}" alt="">
                        </a>
                        <div class="post_right">
                            <div class="post_right_title">
                                <h1 class="news_h1"><a
                                            href="{{ app('App\Http\Controllers\CategoryController')->getPermalink($news->slug, $lang) }}">{{ $news->{"title_".$lang} }}</a>
                                </h1>
                            </div>
                        </div>
                    </div>
                    @if($key %3 == 2 && $key != count($category_news) - 1)
                        <div class="news_line"></div>
                        @endif
                @endforeach
                <div class="news_pag">
                    {{ $category_news->links() }}
                </div>
            @endif

        </div>
    </div>
@endsection