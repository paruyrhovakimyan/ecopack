@extends('frontend.layouts.page')
@section('meta_title', $meta_data['meta_title'])
@section('meta_desc', $meta_data['meta_desc'])
@section('meta_key', $meta_data['meta_key'])
@section('content')
    <div class="container margin_100">
        <div class="home_categories">
            <div class="container">
                <div class="home_categories_title">
                    <hr class="home_categories_title_hr">
                    <p>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("products",$lang) }}</p>
                </div>
            </div>
            <div class="container">
                <div class="@if($mobile_brows == 1) {{ "mobile_sloder" }} @endif @if(count($productCats) > 7) {{ "carousel_category" }} @endif">
                    <div class="carousel-button-left"><a href="#"></a></div>
                    <div class="carousel-button-right"><a href="#"></a></div>
                    <div class="carousel-wrapper">
                        <div class="carousel-items">

                            @if(!empty($productCats))
                                @foreach($productCats as $category)
                                    <div class="carousel-block">
                                        <div class="home_category_item">
                                            <div class="home_category_item_img">
                                                <a href="{{ url('/') }}/products/{{$category->id}}">
                                                    <img src="{{ url('/') }}/public/uploads/prcat/{{ $category->pr_cat_image }}">
                                                </a>
                                            </div>
                                            <div class="home_category_item_title">
                                                <a href="{{ url('/') }}/products/{{$category->id}}">{{ $category->{"title_".$lang} }}</a>
                                            </div>


                                        </div>
                                    </div>
                                @endforeach
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <div class="product_cat_items">
        <div class="container">
            <div class="shop_cat">
                <div class="shop_article">
                    <div class="shop_right">
                            <div class="shop_right_title">
                                @if(isset($productcat))
                                    <p>{{ $productcat->{"title_$lang"} }}</p>
                                @endif
                                @if(isset($productcat_custom))
                                        <p>{{ $productcat_custom }}</p>
                                    @endif
                            </div>
                            @foreach($all_products as $product)



                                <div class="item">
                                    <div class="item-inner">
                                        <div class="item_img">
                                            <img src="{{ url('/') }}/public/uploads/product/{{ $product->image }}" alt="">
                                            <div class="action">
                                                <a href="{{ url('/') }}/product/{{ $product->pr_id }}" title="Դիտել">
                                                    <i class="fa fa-search"></i>
                                                </a>

                                                <a style="cursor:pointer" id="kupit_knopka" title="Ավելացնել զամբյուղ" class="add_to_cart" data-count="1" data-lang="{{ $lang }}" pr-link="{{ url('/') }}/product/{{ $product->pr_id }}" data-id="{{ $product->pr_id }}">
                                                    <i class="fa fa-shopping-cart"></i>
                                                </a>
                                                <input type="hidden" name="pr_id" class="pr_id" value="{{ $product->pr_id }}">

                                            </div>
                                        </div>
                                        <div class="item-info">
                                            <div class="info-inner">
                                                @if($product->new_collection == 1 || $product->pr_new_price != 0)
                                                <div class="info_sale">
                                                    @if($product->new_collection == 1)
                                                    <p>ՆՈՐՈՒթՅՈՒՆ</p>
                                                    @endif
                                                    @if($product->pr_new_price != 0)
                                                        <span>-{{ round(($product->pr_price - $product->pr_new_price) / $product->pr_price * 100)."%" }}</span>
                                                    @endif
                                                </div>
                                                @endif

                                                    <div class="info-price">
                                                        <p>@if($product->pr_new_price != '0') {{ $product->pr_new_price }} @else {{ $product->pr_price }} @endif <img src="{{ url('/') }}/public/img/dram_f.png" alt=""> </p>
                                                        @if($product->pr_new_price != 0)
                                                            <span>{{ $product->pr_price }}</span>
                                                        @endif
                                                    </div>
                                                <div class="info-title">
                                                    <a href="{{ url('/') }}/product/{{ $product->pr_id }}">{{ $product->{"title_".$lang} }}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            @endforeach
                        <div class="col-md-12">
                            <div class="news_pag">
                                {{ $all_products->links() }}
                            </div>
                        </div>

                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection