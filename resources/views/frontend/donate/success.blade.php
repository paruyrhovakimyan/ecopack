@extends('frontend.layouts.page')
@section('meta_title', $meta_data['meta_title'])
@section('meta_desc', $meta_data['meta_desc'])
@section('meta_key', $meta_data['meta_key'])
@section('content')

<div class="donate_success">
    <div class="container margin_100">
        <div class="page_left_title">
            <h2>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("success_title",$lang) }} </h2>
        </div>
        <div class="success_content">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("success_text",$lang) }}</div>
    </div>

</div>
@endsection
