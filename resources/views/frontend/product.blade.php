@extends('frontend.layouts.page')
@section('meta_title', $meta_data['meta_title'])
@section('meta_desc', $meta_data['meta_desc'])
@section('meta_key', $meta_data['meta_key'])

@section('content')
    <script src="{{ asset('/public/js/jssor.slider.min.js') }}"></script>
    <div class="link">
        <div class="container">
            {!! $bradcramp !!}
        </div>
    </div>
    <div class="container margin_100">
        <div class="we_logo">
            <div class="container">
                <img class="we_logo_img" src="{{ url('/') }}/public/img/Object21.png" alt="">
                <h3 class="we_logo_h3">ԽԱՆՈՒԹ</h3>
            </div>
        </div>
        <div class="clear"></div>
        <div class="shop  @if(count($product->images) == 0) {{ "shop_single_image" }} @endif">
            <div class="product_images_slider">
    <script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_SlideshowTransitions = [
                {$Duration:1200,x:0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                {$Duration:1200,x:-0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                {$Duration:1200,x:-0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                {$Duration:1200,x:0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                {$Duration:1200,y:0.3,$During:{$Top:[0.3,0.7]},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                {$Duration:1200,y:-0.3,$SlideOut:true,$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                {$Duration:1200,y:-0.3,$During:{$Top:[0.3,0.7]},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                {$Duration:1200,y:0.3,$SlideOut:true,$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                {$Duration:1200,x:0.3,$Cols:2,$During:{$Left:[0.3,0.7]},$ChessMode:{$Column:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                {$Duration:1200,x:0.3,$Cols:2,$SlideOut:true,$ChessMode:{$Column:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                {$Duration:1200,y:0.3,$Rows:2,$During:{$Top:[0.3,0.7]},$ChessMode:{$Row:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                {$Duration:1200,y:0.3,$Rows:2,$SlideOut:true,$ChessMode:{$Row:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                {$Duration:1200,y:0.3,$Cols:2,$During:{$Top:[0.3,0.7]},$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                {$Duration:1200,y:-0.3,$Cols:2,$SlideOut:true,$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                {$Duration:1200,x:0.3,$Rows:2,$During:{$Left:[0.3,0.7]},$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                {$Duration:1200,x:-0.3,$Rows:2,$SlideOut:true,$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                {$Duration:1200,x:0.3,y:0.3,$Cols:2,$Rows:2,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$ChessMode:{$Column:3,$Row:12},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                {$Duration:1200,x:0.3,y:0.3,$Cols:2,$Rows:2,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$SlideOut:true,$ChessMode:{$Column:3,$Row:12},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                {$Duration:1200,$Delay:20,$Clip:3,$Assembly:260,$Easing:{$Clip:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                {$Duration:1200,$Delay:20,$Clip:3,$SlideOut:true,$Assembly:260,$Easing:{$Clip:$Jease$.$OutCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                {$Duration:1200,$Delay:20,$Clip:12,$Assembly:260,$Easing:{$Clip:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                {$Duration:1200,$Delay:20,$Clip:12,$SlideOut:true,$Assembly:260,$Easing:{$Clip:$Jease$.$OutCubic,$Opacity:$Jease$.$Linear},$Opacity:2}
            ];

            var jssor_1_options = {
                $AutoPlay: 0,
                $SlideshowOptions: {
                    $Class: $JssorSlideshowRunner$,
                    $Transitions: jssor_1_SlideshowTransitions,
                    $TransitionsOrder: 1
                },
                $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$
                },
                $ThumbnailNavigatorOptions: {
                    $Class: $JssorThumbnailNavigator$,
                    $SpacingX: 5,
                    $SpacingY: 5
                }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 450;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
    <style>
        /* jssor slider loading skin spin css */
        .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-009-spin {
            from {
                transform: rotate(0deg);
            }

            to {
                transform: rotate(360deg);
            }
        }


        .jssora106 {display:block;position:absolute;cursor:pointer;}
        .jssora106 .c {fill:#fff;opacity:.3;}
        .jssora106 .a {fill:none;stroke:#000;stroke-width:350;stroke-miterlimit:10;}
        .jssora106:hover .c {opacity:.5;}
        .jssora106:hover .a {opacity:.8;}
        .jssora106.jssora106dn .c {opacity:.2;}
        .jssora106.jssora106dn .a {opacity:1;}
        .jssora106.jssora106ds {opacity:.3;pointer-events:none;}

        .jssort101 .p {position: absolute;top:0;left:0;box-sizing:border-box;background:#000;}
        .jssort101 .p .cv {position:relative;top:0;left:0;width:100%;height:100%;border:2px solid #000;box-sizing:border-box;z-index:1;}
        .jssort101 .a {fill:none;stroke:#fff;stroke-width:400;stroke-miterlimit:10;visibility:hidden;}
        .jssort101 .p:hover .cv, .jssort101 .p.pdn .cv {border:none;border-color:transparent;}
        .jssort101 .p:hover{padding:2px;}
        .jssort101 .p:hover .cv {background-color:rgba(0,0,0,6);opacity:.35;}
        .jssort101 .p:hover.pdn{padding:0;}
        .jssort101 .p:hover.pdn .cv {border:2px solid #fff;background:none;opacity:.35;}
        .jssort101 .pav .cv {border-color:#fff;opacity:.35;}
        .jssort101 .pav .a, .jssort101 .p:hover .a {visibility:visible;}
        .jssort101 .t {position:absolute;top:0;left:0;width:100%;height:100%;border:none;opacity:.6;}
        .jssort101 .pav .t, .jssort101 .p:hover .t{opacity:1;}
        div#jssor_1 div {
            margin: 0 auto;
        }
    </style>
    <div id="jssor_1" class="@if(count($product->images) == 0) {{ "pr_single_image" }} @endif" style="position:relative;margin:0 auto;top:0px;left:0px;width:450px;height:450px;visibility:hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:#f7f7f7;">
            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="../svg/loading/static-svg/spin.svg" />
        </div>
        <div data-u="slides" style="margin:auto;cursor:default;position:relative;top:0px;right:0px;bottom:0px;left:0px;width:330px;height:444px;overflow:hidden;">
            <div>
                <img  data-u="image" src="{{ url('/') }}/public/uploads/product/{{ $product->image }}" />
                <img data-u="thumb" src="{{ url('/') }}/public/uploads/product/{{ $product->image }}" />
            </div>
            @if(!empty($product->images))
                @foreach($product->images as $pr_image)
                    <div>
                        <img width="330" height="444" data-u="image" src="{{ url('/') }}/public/uploads/gallery/{{ $pr_image->image }}" />
                        <img data-u="thumb" src="{{ url('/') }}/public/uploads/gallery/{{ $pr_image->image }}" />
                    </div>
                @endforeach

            @endif

        </div>
        <!-- Thumbnail Navigator -->
        <div data-u="thumbnavigator" class="jssort101" style="position:absolute;left:0 !important;bottom: -99px;width:454px;height:100px;background-color:#f7f7f7;" data-autocenter="1" data-scale-bottom="0.75">
            <div data-u="slides">
                <div data-u="prototype" class="p" style="width: 62px;height:84px;">
                    <div data-u="thumbnailtemplate" class="t"></div>
                    <svg viewBox="0 0 16000 16000" class="cv">
                        <circle class="a" cx="8000" cy="8000" r="3238.1"></circle>
                        <line class="a" x1="6190.5" y1="8000" x2="9809.5" y2="8000"></line>
                        <line class="a" x1="8000" y1="9809.5" x2="8000" y2="6190.5"></line>
                    </svg>
                </div>
            </div>
        </div>
        <!-- Arrow Navigator -->
        <div data-u="arrowleft" class="jssora106" style="width:55px;height:55px;top:162px;left:30px;" data-scale="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
                <polyline class="a" points="7930.4,5495.7 5426.1,8000 7930.4,10504.3 "></polyline>
                <line class="a" x1="10573.9" y1="8000" x2="5426.1" y2="8000"></line>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora106" style="width:55px;height:55px;top:162px;right:30px;" data-scale="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
                <polyline class="a" points="8069.6,5495.7 10573.9,8000 8069.6,10504.3 "></polyline>
                <line class="a" x1="5426.1" y1="8000" x2="10573.9" y2="8000"></line>
            </svg>
        </div>
    </div>
    <script type="text/javascript">jssor_1_slider_init();</script>
</div>
            <div class="product_single_info">
                <div class="shop_article_content_title">
                    <p class="shop_article_content_title_p">{{ $product->{"title_$lang"} }}</p>
                </div>

                <div class="single_product_tabs">

                    @if($product->{"short_desc_".$lang} !='')
                        <div class="pr_info_tab product_charecrers_tab active_tab">Հատկանիշ</div>
                    @endif
                    @if($product->{"short_desc_global_".$lang} !='')
                        <div class="pr_info_tab product_desc_tab">Նկարագրություն</div>
                    @endif
                    @if(isset($product->video) && $product->video !='')
                        <div class="pr_info_tab product_video_tab">Տեսանյութ</div>
                    @endif
                </div>

                @if($product->{"short_desc_".$lang} !='')
                    <div class="product_info_desc product_charecrers">{!! $product->{"short_desc_".$lang} !!}</div>
                @endif
                @if($product->{"short_desc_global_".$lang} !='')
                    <div class="product_info_desc product_desc hidden">{!! $product->{"short_desc_global_".$lang} !!}</div>
                @endif
                @if(isset($product->video) && $product->video !='')
                    <div class="product_info_desc product_video hidden">
                        <iframe src="{{ $product->video }}" frameborder="0"></iframe>
                    </div>
                @endif



                <div class="single_product_shopping_block">
                    <div class="single_price">
                        <p>@if($product->pr_new_price != 0 || $product->pr_new_price !='null' || $product->pr_new_price !='') {{ $product->pr_new_price }} @else {{ $product->pr_price }} @endif <img src="{{ url('/') }}/public/img/dram_single.png" alt=""></p>
                        @if($product->pr_new_price != 0)
                            <span>{{ $product->pr_price }}</span>
                        @endif
                    </div>
                    <div class="shop_count">
                        <div class="count_product">
                            <a style="cursor: pointer" class="cart_minus1"><i class="fa fa-minus" aria-hidden="true"></i></a>
                            <input data-id="{{ $product->id }}" type="text" onkeypress="return isNumberKey(event)" class="pr_count_in_cart1" value="1" name="pr_count_in_cart">
                            <a style="cursor: pointer" class="cart_plus1"><i class="fa fa-plus" aria-hidden="true"></i></a>
                        </div>
                        <div class="buy_prod_block">
                            <a data-lang="{{ $lang }}" data-id="{{ $product->id }}" class="buy_prod buy_cl button btn-cart" style="cursor: pointer"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Գնել</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="related_product">
            <div class="home_categories_title">
                <hr class="home_categories_title_hr">
                <p>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("related_product",$lang) }}</p>
            </div>
            <div class="related_product_items">
                <div class="container">
                        <div class="carousel_related">
                            <div class="carousel-button-left"><a href="#"></a></div>
                            <div class="carousel-button-right"><a href="#"></a></div>
                            <div class="carousel-wrapper">
                                <div class="carousel-items">


                                    @if(!empty($related_products))
                                        @php($product_rel_array = array())
                                        @foreach($related_products as $related_product)
                                            @if(!in_array($related_product->pr_id,$product_rel_array))
                                            <div class="carousel-block">
                                                <div class="item">
                                                    <div class="item-inner">
                                                        <div class="item_img">
                                                            <img src="{{ url('/') }}/public/uploads/product/{{ $related_product->image }}" alt="">
                                                            <div class="action">
                                                                <a href="{{ url('/') }}/product/{{ $related_product->pr_id }}" title="Դիտել">
                                                                    <i class="fa fa-search"></i>
                                                                </a>

                                                                <a style="cursor:pointer" id="kupit_knopka" title="Ավելացնել զամբյուղ" class="add_to_cart" data-count="1" data-lang="{{ $lang }}" pr-link="{{ url('/') }}/product/{{ $related_product->id }}" data-id="{{ $related_product->id }}">
                                                                    <i class="fa fa-shopping-cart"></i>
                                                                </a>
                                                                <input type="hidden" name="pr_id" class="pr_id" value="{{ $related_product->pr_id }}">

                                                            </div>
                                                        </div>
                                                        <div class="item-info">
                                                            <div class="info-inner">
                                                                @if($related_product->new_collection == 1 || $related_product->pr_old_price != 0)
                                                                    <div class="info_sale">
                                                                        @if($related_product->new_collection == 1)
                                                                            <p>ՆՈՐՈՒթՅՈՒՆ</p>
                                                                        @endif
                                                                            @if($related_product->pr_new_price != 0)
                                                                                <span>-{{ round(($related_product->pr_price - $related_product->pr_new_price) / $related_product->pr_price * 100)."%" }}</span>
                                                                            @endif



                                                                    </div>
                                                                @endif

                                                                <div class="info-price">
                                                                    <p>@if($related_product->pr_new_price != '0') {{ $related_product->pr_new_price }} @else {{ $related_product->pr_price }} @endif <img src="{{ url('/') }}/public/img/dram_f.png" alt=""></p>
                                                                    @if($related_product->pr_new_price != 0)
                                                                        <span>{{ $related_product->pr_price }}</span>
                                                                    @endif
                                                                </div>
                                                                <div class="info-title">
                                                                    <a href="{{ url('/') }}/product/{{ $related_product->id }}">{{ $related_product->{"title_".$lang} }}</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                                @php($product_rel_array[] = $related_product->pr_id)
                                            @endif
                                        @endforeach
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        @if(!empty($recomended_products) && count($recomended_products) > 0)
            <div class="related_product recomended_products">
            <div class="home_categories_title">
                <hr class="home_categories_title_hr">
                <p>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("recomended",$lang) }}</p>
            </div>
            <div class="related_product_items">
                <div class="container">
                        <div class="carousel_recomended">
                            <div class="carousel-button-left"><a href="#"></a></div>
                            <div class="carousel-button-right"><a href="#"></a></div>
                            <div class="carousel-wrapper">
                                <div class="carousel-items">


                                        @foreach($recomended_products as $recomended_product)
                                            <div class="carousel-block">
                                                <div class="item">
                                                    <div class="item-inner">
                                                        <div class="item_img">
                                                            <img src="{{ url('/') }}/public/uploads/product/{{ $recomended_product->image }}" alt="">
                                                            <div class="action">
                                                                <a href="{{ url('/') }}/product/{{ $recomended_product->id }}" title="Դիտել">
                                                                    <i class="fa fa-search"></i>
                                                                </a>

                                                                <a style="cursor:pointer" id="kupit_knopka" title="Ավելացնել զամբյուղ" class="add_to_cart" data-count="1" data-lang="{{ $lang }}" pr-link="{{ url('/') }}/product/{{ $recomended_product->id }}" data-id="{{ $recomended_product->id }}">
                                                                    <i class="fa fa-shopping-cart"></i>
                                                                </a>
                                                                <input type="hidden" name="pr_id" class="pr_id" value="{{ $recomended_product->id }}">

                                                            </div>
                                                        </div>
                                                        <div class="item-info">
                                                            <div class="info-inner">
                                                                @if($recomended_product->new_collection == 1 || $recomended_product->pr_old_price != 0)
                                                                    <div class="info_sale">
                                                                        @if($recomended_product->new_collection == 1)
                                                                            <p>ՆՈՐՈՒթՅՈՒՆ</p>
                                                                        @endif
                                                                            @if($recomended_product->pr_new_price != 0)
                                                                                <span>-{{ round(($recomended_product->pr_price - $recomended_product->pr_new_price) / $recomended_product->pr_price * 100)."%" }}</span>
                                                                            @endif
                                                                    </div>
                                                                @endif

                                                                <div class="info-price">
                                                                    <p>@if($recomended_product->pr_new_price != '0') {{ $recomended_product->pr_new_price }} @else {{ $recomended_product->pr_price }} @endif <img src="{{ url('/') }}/public/img/dram_f.png" alt=""></p>
                                                                    @if($recomended_product->pr_new_price != 0)
                                                                        <span>{{ $recomended_product->pr_price }}</span>
                                                                    @endif
                                                                </div>
                                                                <div class="info-title">
                                                                    <a href="{{ url('/') }}/product/{{ $recomended_product->id }}">{{ $recomended_product->{"title_".$lang} }}</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        @endforeach


                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        @endif
    </div>
@endsection