@extends('frontend.layouts.page')
@section('meta_title', $meta_data['meta_title'])
@section('meta_desc', $meta_data['meta_desc'])
@section('meta_key', $meta_data['meta_key'])
@section('content')
    <div class="link">
        <div class="container">
            {!! $bradcramp !!}
        </div>
    </div>
    <div class="container margin_100">
        <div class="left">
            <div class="page_left_title">
                <h2>{{ $page->title }}</h2>
                <hr class="title_bottom">
            </div>
            @if($page->short_desc !='')
                <div class="page_short_desc">
                    <p>{{ $page->short_desc }}</p>
                </div>
            @endif
            <div class="page_content @if(!empty($page->page_with_pdfs)) {{ "border_bottom_none" }} @endif">
                {!! $page->description !!}
            </div>


            <div class="projects_category_block">
                @foreach($page_projects as $serice)
                    <div class="services_box">
                        <div class="tour_container">
                            <div class="img_container">
                                @if(isset($serice->need_donate) && $serice->need_donate == 1)
                                    <img class="donate_icon" src="{{ url('/') }}/public/img/donate.png" >
                                @endif
                                <a href="{{ app('App\Http\Controllers\GlossaryController')->getTranslate("home_url",$lang) }}/page_pdf/{{ $serice->slug }}">

                                    <img src="{{ url('/') }}/public/uploads/page/{{ $serice->image_main }}" class="img-responsive" alt="image">
                                </a>
                                <div class="service_box_title">
                                    <a href="{{ app('App\Http\Controllers\GlossaryController')->getTranslate("home_url",$lang) }}/page_pdf/{{ $serice->slug }}">{{ $serice->{"title_".$lang} }}</a>
                                    <span>{{ Carbon\Carbon::parse($serice->created_at)->format('d M Y') }}</span>
                                    <p>{{ $serice->{"short_desc_".$lang} }}</p>
                                    <a class="read_more" href="{{ app('App\Http\Controllers\GlossaryController')->getTranslate("home_url",$lang) }}/page_pdf/{{ $serice->slug }}">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("read_more",$lang) }}</a>
                                </div>
                            </div>
                        </div>
                        <!-- End box tour -->
                    </div>
                @endforeach
            </div>


            @if(isset($invesment_page) && $invesment_page == 1)
                <div class="page_investment">
                    @foreach($invesment_sub_pages as $investment)
                        <div class="invesment_box">
                            <div class="invesment_logo">
                                <img src="{{ url('/') }}/public/uploads/page/{{ $investment->image_logo }}" class="img-responsive" alt="image">
                            </div>
                            <div class="invesment_info">
                                <a href="{{ app('App\Http\Controllers\GlossaryController')->getTranslate("home_url",$lang) }}/page/{{ $investment->slug }}">{{ $investment->{"title_".$lang} }}</a>
                                <p>{{ $investment->{"short_desc_".$lang} }}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif

            @if($page_news == 1)

                <div class="category_news">
                    @if(!empty($all_news))
                        @foreach($all_news as $news)
                            <div class="post">
                                <a href="{{ app('App\Http\Controllers\CategoryController')->getPermalink($news->slug, $lang) }}">
                                    <img class="news_img" src="{{ url('/') }}/uploads/news/{{ $news->image }}" alt="">
                                </a>
                                <div class="post_right">

                                    <h1 class="news_h1"><a href="{{ app('App\Http\Controllers\CategoryController')->getPermalink($news->slug, $lang) }}">{{ $news->title }}</a></h1>
                                    <p class="news_date">{{  date('d M Y', strtotime($news->date1)) }}</p>
                                    <p class="news_cont">
                                        {{ $news->short_desc }}
                                    </p>
                                    <a class="news_href" href="{{ app('App\Http\Controllers\CategoryController')->getPermalink($news->slug, $lang) }}">read more</a>
                                </div>
                            </div>
                        @endforeach
                        <div class="news_pag">
                            {{ $all_news->links() }}
                        </div>
                    @endif

                </div>

            @endif



            @if(!empty($page->parent_page_pdfs))
                @foreach($page->parent_page_pdfs as $page_pdfs)

                    <div class="one_pdf_block">
                        <img title="{{ $page_pdfs->title }}" class="pdf_icon" src="{{ url('/') }}/img/printel_icons_pdf.jpg" >
                        <div class="page_pdf_right">
                            <a title="{{ $page_pdfs->title }}" class="pdf_title" target="_blank" href="{{ url('/') }}/uploads/page/pdf_en/{{ $page_pdfs->pdf_file }}">{{ $page_pdfs->title }}</a>
                            <p class="pdf_desc">{{ $page_pdfs->pdf_desc }}</p>
                        </div>
                    </div>


                @endforeach

            @endif


            @if(!empty($page->page_with_pdfs))
                <div class="sub_page_with_pdf_blocks">
                    @foreach($page->page_with_pdfs as $news_sub_page_pdf)

                        <div class="news_sub_page_pdf_one">
                            <p>
                                <span class="sub_page_icon_plus ">
                                    <img title="" class="pdf_icon" src="{{ url('/') }}/img/printel_icons_doc.jpg" >
</span>
                                <span class="sub_page_icon_minus hidden ">
                                    <img title="" class="pdf_icon" src="{{ url('/') }}/img/printel_icons_open_doc.jpg" >
                                </span>
                                {{ $news_sub_page_pdf->title }}
                            </p>
                        </div>
                        <div class="news_sub_page_pdf_list">
                            @if(!empty($news_sub_page_pdf->description ))
                                <div class="description_sub_page">
                                    {!! $news_sub_page_pdf->description !!}
                                </div>
                            @endif
                            @if(!empty($news_sub_page_pdf->sub_page_with_pdf_files))
                                @foreach($news_sub_page_pdf->sub_page_with_pdf_files as $sub_page_pdf)
                                    <div class="one_pdf_block">
                                        <img title="{{ $sub_page_pdf->{"title_".$lang} }}" class="pdf_icon" src="{{ url('/') }}/img/printel_icons_pdf.jpg">
                                        <div class="page_pdf_right">
                                            <a title="{{ $sub_page_pdf->{"title_".$lang} }}" class="pdf_title" target="_blank" href="{{ url('/') }}/uploads/page/pdfs/{{ $sub_page_pdf->{"file_".$lang} }}">{{ $sub_page_pdf->{"title_".$lang} }}</a>
                                            @if($sub_page_pdf->{"short_desc_".$lang} != "")
                                                <p class="pdf_desc">{{ $sub_page_pdf->{"short_desc_".$lang} }}</p>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    @endforeach
                </div>


            @endif


            @if(!empty($page->parent_page_pdfs) && !empty($page->child_page_galleries))
                <hr class="pdf_hr_gallery">
            @endif

            @if(!empty($page->child_page_galleries))

                @foreach($page->child_page_galleries as $page_galleries)
                    <div class="page_gallery_block">
                        <img title="{{ $page_pdfs->title }}" class="pdf_icon" src="{{ url('/') }}/img/printel_icons_gallery.jpg" >
                        <p class="page_gallery_title">{{ $page_galleries->title }}</p>
                        <div class="page_gallery_desc">
                            <p>{{ $page_galleries->short_desc }}</p>
                        </div>


                        <div class="page_gallery_images">


                            <div class="row magnific-gallery add_bottom_60 ">
                                @if(!empty($page_galleries->images))
                                    @foreach($page_galleries->images as $page_gallery_image)
                                        <div class="col-md-4 col-sm-4">
                                            <div class="img_wrapper_gallery">
                                                <div class="img_container_gallery">
                                                    <a href="{{ url('/') }}/uploads/gallery/{{ $page_gallery_image->image }}" title="Photo title">
                                                        <img alt="Image" class="img-responsive" src="{{ url('/') }}/uploads/gallery/150/{{ $page_gallery_image->image }}">
                                                        <i class="icon-resize-full-2"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>




                        </div>
                    </div>

                @endforeach
            @endif



        </div>
        <div class="right sidebar">

            @if(isset($page->contact_page) && $page->contact_page == 1)
                <div class="contact_form">
                    <form class="send_form" action="{{ url('/') }}/send-mail" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("first_name",$lang) }}</label>
                            <input type="text" name="contact_name" value="" class="form-control contact_name">
                        </div>
                        <div class="form-group">
                            <label>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("mail",$lang) }}</label>
                            <input type="email" name="contact_mail" value="" class="form-control contact_mail">
                        </div>
                        <div class="form-group">
                            <label>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("phone",$lang) }}</label>
                            <input type="text" name="contact_phone" value="" class="form-control contact_phone">
                        </div>
                        <div class="form-group">
                            <label>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("contact_message",$lang) }}</label>
                            <textarea name="contact_text" class="form-control contact_text"></textarea>
                        </div>
                        <div class="form-group">
                            <a class="contact_btn pull-right btn btn-primary">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("contact_send",$lang) }}</a>
                        </div>
                        @if (session('status'))
                            <div class="senr_message">
                                {{ session('status') }}
                            </div>
                        @endif
                    </form>
                </div>

            @else
                <div class="news_links">
                    <ul class="news_ul">
                        @if(!empty($sidebar_pages))
                            @foreach($sidebar_pages as $sidebar_page)
                                <li class="news_li"><a
                                            class="news_a @if($slug == $sidebar_page->slug) {{ "sidebar_active_link" }} @endif"
                                            href="{{ $sidebar_page->link  }}">{{ $sidebar_page->title }}</a></li>
                            @endforeach
                        @endif
                        @if(!empty($sidebar_pages1))
                            @foreach($sidebar_pages1 as $sidebar_page1)
                                <li class="news_li"><a
                                            class="news_a @if($slug == $sidebar_page1->slug) {{ "sidebar_active_link" }} @endif"
                                            href="{{ $sidebar_page1->link  }}">{{ $sidebar_page1->title }}</a></li>
                            @endforeach
                        @endif
                    </ul>
                </div>
            @endif


        </div>
        <div class="main_title">

        </div>

    </div>
@endsection