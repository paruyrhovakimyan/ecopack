@extends('frontend.layouts.page')
@section('meta_title', "Search")
@section('meta_desc',"Search")
@section('meta_key', "Search")
@section('content')
    <div class="link">
        <div class="container">
            {!! $bredcramp !!}
        </div>
    </div>
    <div class="container margin_100">
        <div class="">
            <div class="page_left_title">
                <h2>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("search_result",$lang) }}</h2>
                <hr class="title_bottom">
            </div>
            @if($search_empty == 0)
            @if(count($services) > 0)
                @foreach($services as $service)
                    <div class="search_result_block">
                        <a href="{{ $service->permalink }}">{!! $service->result_title  !!}</a>
                    </div>
                @endforeach

            @endif
            @if(!empty($articles))
                @foreach($articles as $article)
                    <div class="search_result_block">
                        <a href="{{ $article->permalink }}">{!! $article->result_title  !!}</a>
                    </div>
                @endforeach

            @endif
                @else
                <div class="search_min_length">
                    <p>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("search_min_length",$lang) }}</p>
                </div>

            @endif
        </div>
    </div>
@endsection