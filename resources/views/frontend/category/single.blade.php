@extends('frontend.layouts.page')

@section('meta_title', $meta_data['meta_title'])
@section('meta_desc', $meta_data['meta_desc'])
@section('meta_key', $meta_data['meta_key'])
@section('meta_image', $meta_data['meta_image'])
@section('meta_url', $meta_data['meta_url'])
@section('content')

    <div class="container  single_block">

        <div class="home_categories_title">
            <p>Էկեոլոգիա</p>
        </div>
        <div class="single_post_left">

            @if($news->cat_id == 14)
                <div class="left_text">
                    @if(isset($news->video_link) && $news->video_link !='')
                        <div class="single_video">
                            <iframe width="100%" height="400" src="{{ $news->video_link }}"></iframe>
                        </div>
                    @endif
                </div>
            @else
                @if($news->id != 60)
                    <div class="left_img">
                        <img class="news_img" src="{{ url('/') }}/uploads/news/{{ $news->image }}" alt="">
                    </div>
                @endif
            @endif
            @if($news->id == 60)
                <div class="page_left_title">
                    <h2>{{ $news->title }}</h2>
                    <hr class="title_bottom">
                </div>
            @else
                <div class="left_head">
                    <p class="left_head_p">{{ $news->title }}</p>
                </div>
            @endif
            @if(!empty($news->short_desc))
                <div class="single_short_desc">
                    <p>{{ $news->short_desc }}</p>
                </div>
            @endif


            <div class="left_text">
                <div class="left_head_text">
                    {!! $news->description  !!}
                </div>
            </div>


            <div class="news_tag_block">
                {!! $news->tag_block !!}
            </div>
            @if(count($news->news_pdfs) > 0)
                <hr class="pdf_hr_gallery">
                @foreach($news->news_pdfs as $page_pdfs)

                    <div class="one_pdf_block">
                        <img title="{{ $page_pdfs->title }}" class="pdf_icon"
                             src="{{ url('/') }}/img/printel_icons_pdf.jpg">
                        <div class="page_pdf_right">
                            <a title="{{ $page_pdfs->title }}" class="pdf_title" target="_blank"
                               href="{{ url('/') }}/uploads/news/pdf_en/{{ $page_pdfs->pdf_file }}">{{ $page_pdfs->title }}</a>
                            @if($page_pdfs->pdf_desc !='')
                                <p class="pdf_desc">{{ $page_pdfs->pdf_desc }}</p>
                            @endif
                        </div>
                    </div>


                @endforeach

            @endif



            @if(count($news->news_sub_page_pdf) > 0)

                <hr>
                <div class="news_sub_page_pdf_blocks">
                    @foreach($news->news_sub_page_pdf as $news_sub_page_pdf)

                        <div class="news_sub_page_pdf_one">
                            <p>
                                <span class="sub_page_icon_plus ">
                                    <img title="" class="pdf_icon" src="{{ url('/') }}/img/printel_icons_doc.jpg">
</span>
                                <span class="sub_page_icon_minus hidden ">
                                    <img title="" class="pdf_icon" src="{{ url('/') }}/img/printel_icons_open_doc.jpg">
                                </span>
                                {{ $news_sub_page_pdf->title }}
                            </p>
                        </div>
                        <div class="news_sub_page_pdf_list">
                            @if(!empty($news_sub_page_pdf->description ))
                                <div class="description_sub_page">
                                    {!! $news_sub_page_pdf->description !!}
                                </div>
                            @endif
                            @if(!empty($news_sub_page_pdf->pdfs))
                                @foreach($news_sub_page_pdf->pdfs as $sub_page_pdf)
                                    <div class="one_pdf_block">
                                        <img title="{{ $sub_page_pdf->title }}" class="pdf_icon"
                                             src="{{ url('/') }}/img/printel_icons_pdf.jpg">
                                        <div class="page_pdf_right">
                                            <a title="{{ $sub_page_pdf->title }}" class="pdf_title" target="_blank"
                                               href="{{ url('/') }}/uploads/news/pdfs/{{ $sub_page_pdf->file }}">{{ $sub_page_pdf->title }}</a>
                                            @if($sub_page_pdf->short_desc != "")
                                                <p class="pdf_desc">{{ $sub_page_pdf->short_desc }}</p>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    @endforeach
                </div>


            @endif


            @if(!empty($news->news_galleries))
                <hr class="pdf_hr_gallery">
                @foreach($news->news_galleries as $page_galleries)
                    <div class="page_gallery_block">
                        <div class="page_gallery_block_top">
                            <img title="" class="pdf_icon" src="{{ url('/') }}/img/printel_icons_gallery.jpg">

                            <p class="page_gallery_title">{{ $page_galleries->{"title_".$lang} }}</p>
                            @if($page_galleries->{"short_desc_".$lang} !='')
                                <div class="page_gallery_desc">
                                    <p>{{ $page_galleries->{"short_desc_".$lang} }}</p>
                                </div>
                            @endif
                        </div>


                        <div class="page_gallery_images">


                            <div class="row magnific-gallery ">
                                @if(!empty($page_galleries->images))
                                    @foreach($page_galleries->images as $page_gallery_image)
                                        <div class="col-md-4 col-sm-4">
                                            <div class="img_wrapper_gallery">
                                                <div class="img_container_gallery">
                                                    <a href="{{ url('/') }}/uploads/gallery/{{ $page_gallery_image->image }}"
                                                       title="{{ $news->title }}">
                                                        <img alt="Image" class="img-responsive"
                                                             src="{{ url('/') }}/uploads/gallery/150/{{ $page_gallery_image->image }}">
                                                        <i class="icon-resize-full-2"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>


                        </div>
                    </div>

                @endforeach
            @endif


            <div class="share_buttons">
                <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                <script src="//yastatic.net/share2/share.js"></script>
                <div class="ya-share2" data-services="facebook,twitter,linkedin"></div>
            </div>


        </div>

        <div class="single_post_right">
            @foreach($other_posts as $news)
                <div class="single_right_post">
                    <div class="single_right_post_image">
                        <img src="{{ url('/') }}/public/uploads/news/{{ $news->image }}" alt="">
                    </div>
                    <div class="single_right_title">
                        <a href="{{ app('App\Http\Controllers\CategoryController')->getPermalink($news->slug, $lang) }}">{{ $news->title }}</a>
                    </div>
                </div>

            @endforeach
        </div>

    </div>
@endsection