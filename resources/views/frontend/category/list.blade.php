@extends('frontend.layouts.page')

@section('meta_title', $meta_data['meta_title'])
@section('meta_desc', $meta_data['meta_desc'])
@section('meta_key', $meta_data['meta_key'])
@section('content')
    <div class="link">
        <div class="container">
            {!! $bradcramp !!}
        </div>
    </div>
    <div class="container margin_100 single_block">

        <div class="we_logo">
            <div class="container">
                <img class="we_logo_img" src="{{ url('/') }}/public/img/Object21.png" alt="">
                <h3 class="we_logo_h3">{{ $category_info->title }}</h3>
            </div>
        </div>
        <div class="clear"></div>

        <div class="left">
            <div class="page_left_title">
                <h2>{{ $category_info->title }}</h2>
            </div>
            <div class="category_news">
                @if(!empty($category_news))
                    @foreach($category_news as $news)


                        <div class="post">
                            <a href="{{ app('App\Http\Controllers\CategoryController')->getPermalink($news->slug, $lang) }}">
                                <img class="news_img" src="{{ url('/') }}/uploads/news/{{ $news->image }}" alt="">
                            </a>
                            <div class="post_right">
                                @if($news->cat_id !=23 )
                                <div class="event_list_exp">

                                        <p class="event_list_p_m">{{ app('App\Http\Controllers\HomeController')->dataMonthFormat($news->date2, $lang) }}</p>

                                    <p class="event_list_p_d">{{  date('d', strtotime($news->date2)) }}</p>
                                </div>
                                @endif
                                <div class="post_right_title">
                                    <h1 class="news_h1"><a href="{{ app('App\Http\Controllers\CategoryController')->getPermalink($news->slug, $lang) }}">{{ $news->title }}</a></h1>
                                    <p class="news_date">{{ app('App\Http\Controllers\HomeController')->dataFormat($news->date1, $lang) }}</p>

                                </div>
                                <p class="news_cont">
                                    {{ $news->short_desc }}
                                </p>
                                <a class="news_href" href="{{ app('App\Http\Controllers\CategoryController')->getPermalink($news->slug, $lang) }}">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("read_more",$lang) }}</a>
                            </div>
                        </div>
                    @endforeach
                        <div class="news_pag">
                            {{ $category_news->links() }}
                        </div>
                @endif

            </div>
        </div>
        <div class="right sidebar">
            <div class="caree1">
                    <div class="right_sidebar_search_box">
                        <form action="{{ url('/') }}/search" method="post" class="right_search_form">
                            {{ csrf_field() }}
                            <input autocomplete="off" placeholder="Որոնում" type="text" name="stext" value="" class="right_search_form_input">
                            <input type="submit" class="search_submit">
                        </form>
                    </div>

                    <div class="events_right_list">
                        {{ app('App\Http\Controllers\GlossaryController')->getTranslate("dasakargum",$lang) }}</div>
                    <div class="caree_content" style="display: block">
                        <div class="contanier">
                            <div class="row">
                                <ul>
                                    @foreach ($sidebar_pages as $sidebar_page)

                                        <li class="events_right_list_li @if($slug == $sidebar_page->slug) {{ "active_sidebar_cat" }} @endif"><a class="events_right_list_a" href="{{ app('App\Http\Controllers\GlossaryController')->getTranslate("home_url",$lang) }}category/{{ $sidebar_page->slug }}"> > {{ $sidebar_page->title }}</a></li>

                                    @endforeach

                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="events_right_news">
                        ՆՈՐՈՒԹՅՈՒՆՆԵՐ</div>
                    <div class="caree_content">
                        <div class="contanier">
                            @foreach($sidebar_pag_posts as $sidebar_pag_post)

                                <div class="event">
                                    <a href="{{ app('App\Http\Controllers\CategoryController')->getPermalink($sidebar_pag_post->slug, $lang) }}">
                                        <img class="event_img" src="{{ url('/') }}/uploads/news/{{ $sidebar_pag_post->image }}" alt="">
                                    </a>
                                <div class="event_title">{{ $sidebar_pag_post->title }}</div>
                            </div>

                            @endforeach
                        </div>
                    </div>
            </div>
        </div>

    </div>
@endsection

