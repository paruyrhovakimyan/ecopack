@extends('frontend.layouts.page')

@section('content')
    <div class="link">
        <div class="container">
            {!! $bradcramp !!}
        </div>
    </div>
    <div class="container margin_100">
        <div class="message">
            <div class="container">
                <div class="msg_left">
                    <div class="msg_form">
                        <div class="msg_text">
                            <p class="msg_p">{{ $contact_info->title }}</p>
                        </div>
                        <form action="{{ url('/') }}/contact_us" method="post">
                            {{ csrf_field() }}
                            <div class="form-group pull-left my_group">
                                <input type="text" name="user_name" class="my_form_inp" id="usr" placeholder="{{ $contact_info->name_contact }}" required>
                            </div>
                            <div class="form-group pull-right my_group">
                                <input type="text" name="user_mail" class="my_form_inp my_form_inp2" id="pwd" placeholder="{{ $contact_info->email_contact }}" required>
                            </div>
                            <div class="form-group my_group_form">
                                <textarea class="my_msg" name="user_message" rows="5" id="comment" placeholder="{{ $contact_info->message_contact }}" required></textarea>
                            </div>
                            <input type="hidden" name="send_ok" value="ok">
                            <input class="my_search my_submit" type="submit" value="{{ $contact_info->send_label }}">
                        </form>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="msg_right">
                    <p class="msg_info">{{ $contact_info->address_label }}</p>
                    <p class="msg_tel">
                        <i class="fa fa-phone transformed my_fa" aria-hidden="true"></i>
                        <span>{{ $contact_info->phone_right }}</span></p>
                    <p class="msg_mail">
                        <i class="fa fa-envelope my_fa" aria-hidden="true"></i>
                        <span>{{ $contact_info->mail_right }}</span>
                    </p>
                    <p class="msg_cord">
                        <i class="fa fa-map-marker my_fa" aria-hidden="true"></i>
                        <span>{{ $contact_info->address }}</span>
                    </p>
                    <div class="social">
                        <p class="soc_media">{{ $contact_info->social_media_title }}</p>
                        <a href="{{ $contact_info->facebook }}" target="_blank"><i class="fa fa-facebook-official fa_icon"></i></a>
                        <a href="{{ $contact_info->youtube }}" target="_blank"><img class="img_logo3" src="{{ url('/') }}/img/youtube.jpg" alt=""></a>
                        <a href="{{ $contact_info->gplus }}" target="_blank"><i
                                    class="fa fa-google-plus-official gp_icon"></i></a>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection