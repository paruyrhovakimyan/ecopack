@extends('frontend.layouts.page')
@section('meta_title', $meta_data['meta_title'])
@section('meta_desc', $meta_data['meta_desc'])
@section('meta_key', $meta_data['meta_key'])
@section('content')
    <div class="link">
        <div class="container">
            {!! $bradcramp !!}
        </div>
    </div>
    <div class="container margin_100">
        <div class="left">
            <div class="page_left_title">
                <h2>{{ $current_project_categories->{"title_".$lang} }}</h2>
                <hr class="title_bottom">
            </div>
           <div class="projects_category_block">
               @foreach($pages as $serice)
                    <div class="services_box">
                        <div class="tour_container">
                            <div class="img_container">
                                @if(isset($serice->need_donate) && $serice->need_donate == 1)
                                    <img class="donate_icon" src="{{ url('/') }}/public/img/donate.png" >
                                @endif
                                <a href="{{ app('App\Http\Controllers\GlossaryController')->getTranslate("home_url",$lang) }}/page_pdf/{{ $serice->slug }}">

                                    <img src="{{ url('/') }}/public/uploads/page/{{ $serice->image_main }}" class="img-responsive" alt="image">
                                </a>
                                <div class="service_box_title">
                                    <a href="{{ app('App\Http\Controllers\GlossaryController')->getTranslate("home_url",$lang) }}/page_pdf/{{ $serice->slug }}">{{ $serice->{"title_".$lang} }}</a>
                                    <span>{{ Carbon\Carbon::parse($serice->created_at)->format('d M Y') }}</span>
                                    <p>{{ $serice->{"short_desc_".$lang} }}</p>
                                    <a class="read_more" href="{{ app('App\Http\Controllers\GlossaryController')->getTranslate("home_url",$lang) }}/page_pdf/{{ $serice->slug }}">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("read_more",$lang) }}</a>
                                </div>
                            </div>
                        </div>
                        <!-- End box tour -->
                    </div>
               @endforeach
           </div>
        </div>
        <div class="right sidebar">
            <div class="news_links">
                <ul class="news_ul">
                    @if(!empty($sidebar_pages))
                        @foreach($sidebar_pages as $sidebar_page)
                            <li class="news_li"><a
                                        class="news_a @if($slug == $sidebar_page->slug) {{ "sidebar_active_link" }} @endif"
                                        href="{{ $sidebar_page->link  }}">{{ $sidebar_page->title }}</a></li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
        <div class="main_title">

        </div>

    </div>
@endsection