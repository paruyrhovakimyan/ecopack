@extends('frontend.layouts.page')
@section('meta_title', $meta_data['meta_title'])
@section('meta_desc', $meta_data['meta_desc'])
@section('meta_key', $meta_data['meta_key'])
@section('meta_image', $meta_data['meta_image'])
@section('meta_url', $meta_data['meta_url'])

@section('content')
    <link href="{{ asset('/public/css/shop.css') }}" rel="stylesheet">
    @if(isset($banner_page->image_bg) && $banner_page->image_bg !='')
    <div class="page_pdf_banner">
        <div class="page_pdf_banner_img">
            <img src="{{ url('/') }}/public/uploads/page/{{ $banner_page->image_bg }}">
        </div>
    </div>
    @endif
    
    <div class="link">
        <div class="container">
            {!! $bradcramp !!}
        </div>
    </div>
    <div class="container margin_100">
        <div class="">
            <div class="page_left_title">
                <h2>{{ $page->title }}</h2>
                
            </div>
            @if($page->image_main != '')
                <div class="page_pdf_image">
                    <img src="{{ url('/') }}/public/uploads/page/{{ $page->image_main }}">
                </div>
            @endif
            @if($page->parent_id == 76)
                @if($page->date1 && $page->date2)
                    <div class="date_block">
                        <span>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("date_range",$lang) }}</span>
                        <b>{{$page->date1. " - ".$page->date2 }}</b>
                    </div>
                @endif
            @endif
            @if($page->short_desc !='')
                <div class="page_short_desc">
                    <p>{{ $page->short_desc }}</p>
                </div>
            @endif
            <div class="page_content">{!! $page->description !!}</div>


            @if(count($page->parent_page_pdfs) > 0)
                @foreach($page->parent_page_pdfs as $page_pdfs)

                    <div class="one_pdf_block">
                        <img title="{{ $page_pdfs->{"title_".$lang} }}" class="pdf_icon" src="{{ url('/') }}/img/printel_icons_pdf.jpg" >
                        <div class="page_pdf_right">
                            <a title="{{ $page_pdfs->{"title_".$lang} }}" class="pdf_title" target="_blank" href="{{ url('/') }}/uploads/page/pdfs/{{ $page_pdfs->{"file_".$lang} }}">{{ $page_pdfs->{"title_".$lang} }}</a>
                            <p class="pdf_desc">{{ $page_pdfs->{"short_desc_".$lang} }}</p>
                        </div>
                    </div>
                @endforeach
            @endif




            @if((count($page->sub_folders) > 0))
                <div class="sub_page_with_pdf_blocks">
                    @foreach($page->sub_folders as $news_sub_page_pdf)

                        <div class="news_sub_page_pdf_one">
                            <p>
                                <span class="sub_page_icon_plus ">
                                    <img title="" class="pdf_icon" src="{{ url('/') }}/img/printel_icons_doc.jpg" >
                                </span>
                                <span class="sub_page_icon_minus hidden ">
                                    <img title="" class="pdf_icon" src="{{ url('/') }}/img/printel_icons_open_doc.jpg" >
                                </span>
                                {{ $news_sub_page_pdf->title }}
                            </p>
                        </div>
                        <div class="news_sub_page_pdf_list">
                            @if(!empty($news_sub_page_pdf->description ))
                                <div class="description_sub_page">
                                    {!! $news_sub_page_pdf->description !!}
                                </div>
                            @endif
                            @if(!empty($news_sub_page_pdf->files))
                                @foreach($news_sub_page_pdf->files as $sub_page_pdf)
                                    <div class="one_pdf_block">
                                        <img title="{{ $sub_page_pdf->{"title_".$lang} }}" class="pdf_icon" src="{{ url('/') }}/img/printel_icons_pdf.jpg">
                                        <div class="page_pdf_right">
                                            <a title="{{ $sub_page_pdf->{"title_".$lang} }}" class="pdf_title" target="_blank" href="{{ url('/') }}/uploads/page/pdfs/{{ $sub_page_pdf->{"file_".$lang} }}">{{ $sub_page_pdf->{"title_".$lang} }}</a>
                                            @if($sub_page_pdf->{"short_desc_".$lang} != "")
                                                <p class="pdf_desc">{{ $sub_page_pdf->{"short_desc_".$lang} }}</p>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    @endforeach
                </div>
            @endif
            @if(!empty($page->sub_gallery_folders))

                @foreach($page->sub_gallery_folders as $page_galleries)
                    <div class="page_gallery_block page_pdf_gallery_block">
                        <img title="{{ $page_galleries->title }}" class="pdf_icon" src="{{ url('/') }}/img/printel_icons_gallery.jpg" >
                        <p class="page_gallery_title">{{ $page_galleries->title }}</p>
                        <div class="page_gallery_desc">
                            <p>{{ $page_galleries->short_desc }}</p>
                        </div>


                        <div class="page_gallery_images">


                            <div class="row magnific-gallery add_bottom_60 ">
                                @if(!empty($page_galleries->images))
                                    @foreach($page_galleries->images as $page_gallery_image)
                                        <div class="col-md-4 col-sm-4">
                                            <div class="img_wrapper_gallery">
                                                <div class="img_container_gallery">
                                                    <a href="{{ url('/') }}/uploads/gallery/{{ $page_gallery_image->image }}" title="Photo title">
                                                        <img alt="Image" class="img-responsive" src="{{ url('/') }}/uploads/gallery/150/{{ $page_gallery_image->image }}">
                                                        <i class="icon-resize-full-2"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>




                        </div>
                    </div>

                @endforeach
            @endif

            @if(count($page->parent_page_pdfs) > 0 && count($page->gallery) > 0)
                <hr class="pdf_hr_gallery">
            @endif
            @if(count($page->gallery) > 0)



                <div class="page_gallery_block">
                        <div class="page_gallery_images">
                            <div class="row magnific-gallery add_bottom_60 ">
                                @foreach($page->gallery as $page_gallery_image)

                                    <div class="col-md-4 col-sm-4">
                                        <div class="img_wrapper_gallery">
                                            <div class="img_container_gallery">
                                                <a href="{{ url('/') }}/uploads/gallery/{{ $page_gallery_image->image }}" title="Photo title">
                                                    <img alt="Image" class="img-responsive" src="{{ url('/') }}/uploads/gallery/150/{{ $page_gallery_image->image }}">
                                                    <i class="icon-resize-full-2"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>




                        </div>
                    </div>
            @endif

        </div>
        {{--<div class="right sidebar">
            <div class="news_links">
                <ul class="news_ul">
                    @if(!empty($sidebar_pages))
                        @foreach($sidebar_pages as $sidebar_page)
                            <li class="news_li"><a
                                        class="news_a @if($slug == $sidebar_page->slug) {{ "sidebar_active_link" }} @endif"
                                        href="{{ $sidebar_page->link  }}">{{ $sidebar_page->title }}</a></li>
                        @endforeach
                    @endif
                    @if(!empty($sidebar_pages1))
                        @foreach($sidebar_pages1 as $sidebar_page1)
                            <li class="news_li"><a
                                        class="news_a @if($slug == $sidebar_page1->slug) {{ "sidebar_active_link" }} @endif"
                                        href="{{ $sidebar_page1->link  }}">{{ $sidebar_page1->title }}</a></li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
        <div class="main_title">

        </div>--}}

    </div>
@endsection


