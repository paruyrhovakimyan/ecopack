<html><head>
    @php
        $url = $_SERVER['REQUEST_URI'];
       $url_array = explode("/",$url);
    if($url_array[1]=='public'){
        if($url_array[2]=='ru' || $url_array[2]=='en'){
        $lang = $url_array[2];
        }else{
        $lang = "am";
        }
    }else{
        if($url_array[1]=='ru' || $url_array[1]=='en'){
        $lang = $url_array[1];
        }else{
        $lang = "am";
        }
    }
    @endphp
    <title>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("error_page",$lang) }}</title>
    <style type="text/css">
        h2,p{ margin:0; padding:0;}
        html, body{ margin:0; padding:0; background:#fff; font-family:'MoshVerdana_Bold' }

        @font-face {
            font-family: 'MoshVerdana';
            src: url("https://arch.mycard.am/css/font/MoshVerdana.ttf") format("truetype");
        }
        @font-face {
            font-family: 'MoshVerdana_Bold';
            src: url("https://arch.mycard.am/css/font/MoshVerdana_Bold.ttf") format("truetype");
        }
        @font-face {
            font-family: 'MoshVerdana_Bold_Italic';
            src: url("https://arch.mycard.am/css/font/MoshVerdana_Bold_Italic.ttf") format("truetype");
        }
        @font-face {
            font-family: 'MoshVerdana_Italic';
            src: url("https://arch.mycard.am/css/font/MoshVerdana_Italic.ttf") format("truetype");
        }

        #container404 { max-width:960px; height:500px; margin:0 auto; position:relative; text-align:center;}
        #container404 .logo{width:100%; text-align:center; margin:40px 0 20px;}
        #container404 .contentArea{width:100%; margin:0 auto 50px;}
        #container404 .contentArea h2{
            font-size: 25px;
            color: #000;
            text-align: center;
            margin-bottom: 20px;
        }
        #container404 .contentArea p{color:#666; font-size:20px; text-align:center;}
        #container404 a.gotohome {
            padding: 12px 25px 12px 15px;
            color: #fff;
            background: #666;
            display: inline-block;
            text-decoration: none;
            font-size: 20px;
        }

        #container404 a.gotohome:hover {color:#000; }
        @media screen and (max-width:600px) {
            #container404 .contentArea{top:5%}
            #container404 .contentArea h2 {font-size:40px;}
            #container404 .contentArea p{font-size:15px;}
            #container404 a.gotohome {top:40%;}
        }
        @media screen and (max-width:480px) {
            #container404 .contentArea{top:5%}
            #container404 .contentArea h2 {font-size:40px;}
            #container404 .contentArea p{font-size:15px;}
            #container404 a.gotohome {top:30%;}
        }

    </style>
</head>
<body>

<div id="container404">
    <!--<div class="logo"><img src="http://dlb.am/LOGO.png" alt="logo" /></div>-->
    <div class="">
        <img src="{{ url('/') }}/public/img/arch_404.png" alt="logo">
    </div>
    <div class="contentArea">
        <h2><span style="color:#31302E">404-</span>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("page_not_found",$lang) }}</h2>
        <p>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("page_not_found_text",$lang) }}</p>
        <p>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("page_not_found_text2",$lang) }}</p>
    </div>
    <a href="@if($lang == "am") {{ url('/') }} @else {{ url('/')."/".$lang }} @endif" class="gotohome">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("back_to_home",$lang) }}</a> </div>

</body></html>