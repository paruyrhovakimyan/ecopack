@extends('frontend.layouts.page')
@section('content')
    <main>
        <section id="hero" class="login">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                        <div id="login">
                            <div class="text-center "><h3 class="registration_title">Registration</h3></div>
                            <hr>
                            <form class="author_register_form" action="{{ url('/') }}/registration" method="post">
                                {{ csrf_field() }}
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" value="{{ old('name') }}" class="author_name form-control" name="name"  placeholder="name">
                                    </div>
                                    <div class="form-group">
                                        <label>Surname</label>
                                        <input type="text" value="{{ old('surname') }}" class="author_surname form-control" name="surname" placeholder="surname">
                                    </div>
                                    <div class="form-group">
                                        <label>Gender</label>
                                        <select name="sex"  class="author_sex form-control">
                                            <option value="">--Select Gender--</option>
                                            <option @if(old('sex') == 'male') {{ "selected" }} @endif value="male">Male</option>
                                            <option @if(old('sex') == 'female') {{ "selected" }} @endif value="female">Female</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input value="{{ old('email') }}" type="email" name="email" class="author_mail form-control" placeholder="Email">
                                    </div>


                                    <div class="form-group">
                                        <label>Password</label>
                                        <input value="{{ old('password') }}" type="password" name="password" class="author_pass form-control" id="password1" placeholder="Password">
                                    </div>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                    <div class="form-group">
                                        <label>Confirm password</label>
                                        <input value="{{ old('password_confirmation') }}" type="password" name="password_confirmation" class="author_confirm_pass form-control" id="password2" placeholder="Confirm password">
                                    </div>
                                </div>
                                <input type="hidden" name="register_add" value="ok">
                                <div class="col-md-6  col-md-offset-3">
                                    <div class="btn_full author_create_btn">Create an account</div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main><!-- End main -->
@endsection
