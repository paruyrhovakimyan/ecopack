@extends('frontend.layouts.page')
@section('content')
    <main>
        <section id="account_section">
            <div class="container">
                <div class="account_left_menu">
                    <div class="name_user">
                        <span class="username">{{ $user_info->name." ".$user_info->surname }}</span></div>
                    <div class="account_settings">
                        <a href="{{ app('App\Http\Controllers\GlossaryController')->getTranslate("home_url",$lang) }}account"
                           title="Պատվերների արխիվ" class="account_menu_item1 current_item ">Պատվերների արխիվ </a>
                        <a href="{{ app('App\Http\Controllers\GlossaryController')->getTranslate("home_url",$lang) }}account/change"
                           title="Փոխել տվյալները" class="account_menu_item2 ">Փոխել տվյալները </a>
                        <a href="{{ app('App\Http\Controllers\GlossaryController')->getTranslate("home_url",$lang) }}account/password"
                           title="Փոխել գաղտնաբառը " class="account_menu_item3 ">Փոխել գաղտնաբառը </a>
                        <a href="{{ app('App\Http\Controllers\GlossaryController')->getTranslate("home_url",$lang) }}account/logout"
                           title="Ելք" class="account_menu_item5 ">Ելք</a>
                    </div>
                </div>
                <div class="account_right_content">
                    <div class="account_contain">
                        <div class="account-content-title-wrap">
                            @foreach($user_orders as $user_order)
                                <div class="order_id order_bg">
                                    <i class="fa fa-plus job_fa" aria-hidden="true"></i>
                                    <i class="fa fa-minus job_fa_min" aria-hidden="true"></i>
                                    <b>Patver</b>
                                    <span>{{ "N.".$user_order->id }}</span>
                                </div>
                                <div class="own_one_order" id="order{{ $user_order->id }}">
                                    <div class="tvyalner">
                                        <div class="tvyalner_left">

                                            <div class="order_info_line"><b>Պատվերի հրապարակման ամսաթիվը </b> <span>{{ $user_order->created_at }}</span>
                                            </div>
                                            <div class="order_info_line"><b>Ստացող:</b> <span>{{ $user_order->order_name }}</span></div>
                                            <div class="order_info_line"><b>հեռախոս:</b> <span>{{ $user_order->order_phone  }}</span></div>
                                        </div>
                                        <div class="tvyalner_right">
                                            <div class="order_info_line"><b>Էլ. փոստ </b><span>{{ $user_order->order_email  }}</span></div>
                                            <div class="order_info_line"><b>Հասցե:</b> <span>{{ $user_order->order_adress  }}</span></div>
                                            <div class="order_info_line"><b>Վճարման տարբերակը: </b> <span>Կանխիկ</span></div>
                                            <div class="order_info_line"><b>Կարգավիճակ:</b> <span>@if($user_order->order_status == 0) {{ "Ընթացքի մեջ է" }} @else {{ "Ավարտված" }} @endif</span></div>
                                        </div>
                                    </div>
                                    <table style="border-collapse: collapse;text-align: center;" border="1"
                                           class="cart_table_own">
                                        <tbody>
                                        <tr>
                                            <th>Նկար</th>
                                            <th>Ապրանքի անվանումը</th>
                                            <th>Քանակ</th>
                                            <th>Միավորի գինը</th>
                                            <th>Ընդհանուր</th>
                                        </tr>

                                        @foreach($user_order->metas as $user_order_meta)
                                            <tr>
                                                <td class="cart_pr_img">
                                                    <div class="cart_prod_img">
                                                        <img src="{{ url('/') }}/public/uploads/product/thumbs/{{ $user_order_meta->meta_product->image }}">
                                                    </div>
                                                </td>
                                                <td><p>{{ $user_order_meta->meta_product->{'title_'.$lang} }}</p></td>
                                                <td>{{ $user_order_meta->product_count }}</td>
                                                <td class="pr_one_price">
                                                    @if($user_order_meta->meta_product->new_price != 0)
                                                        {{ $user_order_meta->meta_product->new_price }}
                                                    @else
                                                        {{ $user_order_meta->meta_product->pr_price }}
                                                    @endif
                                                </td>
                                                <td class="pr_all_price">
                                                    @if($user_order_meta->meta_product->new_price != 0)
                                                        {{ $user_order_meta->meta_product->new_price * $user_order_meta->product_count }}
                                                    @else
                                                        {{ $user_order_meta->meta_product->pr_price * $user_order_meta->product_count }}
                                                    @endif
                                                </td>
                                            </tr>

                                        @endforeach
                                        </tbody>
                                    </table>
                                    <div class="all_price">Ընդհանուր գումարը: <span
                                                class="all_price_number">{{ $user_order->all_price }}</span> AMD
                                    </div>
                                </div>

                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main><!-- End main -->
@endsection
