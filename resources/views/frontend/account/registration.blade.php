@extends('frontend.layouts.page')
@section('content')
<main>
    <div class="login_container">
        <div id="registration">
            <div class="text-center "><h3 class="registration_title">Գրանցում</h3>
            </div>
            <hr>
            <form class="author_register_form" action="{{ url('/') }}/registration" method="post">
                {{ csrf_field() }}
                <div class="col-md-12 col-sm-12">
                    <div class="form-group">
                        <label>Անուն *</label>
                        <input type="text" value="" class="author_name form-control" name="name" placeholder="Անուն" required>
                    </div>
                    <div class="form-group">
                        <label>Ազգանուն *</label>
                        <input type="text" value="" class="author_name form-control" name="surname" placeholder="Ազգանուն" required>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <div class="form-group">
                        <label>Էլ. հասցե *</label>
                        <input value="" type="email" name="email" class="author_mail form-control" placeholder="Էլ. հասցե" required>
                    </div>
                    <div class="form-group">
                        <label>Հեռախոս *</label>
                        <input value="" type="text" name="phone" class="author_mail form-control" placeholder="Հեռախոս" required>
                    </div>


                    <div class="form-group">
                        <label>Գաղտնաբառ
                            *</label>
                        <input value="" type="password" name="password" class="author_pass form-control" id="password1" placeholder="Գաղտնաբառ" required>
                    </div>
                    <div class="form-group">
                        <label>Կրկնել գաղտնաբառը
                            *</label>
                        <input value="" type="password" name="password_confirmation" class="author_confirm_pass form-control" id="password2" placeholder="Կրկնել գաղտնաբառը" required>
                    </div>
                    <div class="form-group">
                        <label class="reg_term_lab myCheckbox">
                            <input type="checkbox" name="registration_terms1" value="1" class="reg_terms" required>
                            <span></span>Ես հաստատում եմ, որ իմ 16 տարեկանը լրացած է:            </label>
                    </div>
                </div>
                <input type="hidden" name="register_add" value="ok">
                <div class="col-md-8  col-md-offset-2">
                    <input type="submit" class="btn_full author_create_btn" value="Գրանցվել">
                </div>

            </form>
        </div>
    </div>

</main>

@endsection
