@extends('frontend.layouts.page')
@section('content')
<main>
    <div class="login_container">
        <div id="login">
            <div class="text-center "><h3 class="registration_title">Մուտք</h3>
            </div>
            <hr>
            <form class="author_register_form" action="{{ url('/') }}/sing-in" method="post">
                {{ csrf_field() }}
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Էլ. հասցե *</label>
                        <input value="" type="email" name="email" class="author_mail form-control" placeholder="Էլ. հասցե">
                    </div>


                    <div class="form-group">
                        <label>Գաղտնաբառ
                            *</label>
                        <input value="" type="password" name="password" class="author_pass form-control" id="password1" placeholder="Գաղտնաբառ">
                    </div>
                </div>
                <input type="hidden" name="sing_in_add" value="ok">
                <div class="col-md-12">
                    <input type="submit" class="btn_full sing_in" value="Մուտք">
                </div>
            </form>
        </div>
    </div>

</main>

@endsection
