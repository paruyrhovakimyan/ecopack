@extends('frontend.layouts.page')
@section('content')
    <main>
        <section id="account_section">
            <div class="container">
                <div class="account_left_menu">
                    <div class="name_user">
                        <span class="username">{{ $user_info->name." ".$user_info->surname }}</span></div>
                    <div class="account_settings">
                        <a href="{{ app('App\Http\Controllers\GlossaryController')->getTranslate("home_url",$lang) }}account" title="Պատվերների արխիվ" class="account_menu_item1 current_item ">Պատվերների արխիվ </a>
                        <a href="{{ app('App\Http\Controllers\GlossaryController')->getTranslate("home_url",$lang) }}account/change" title="Փոխել տվյալները" class="account_menu_item2 ">Փոխել տվյալները </a>
                        <a href="{{ app('App\Http\Controllers\GlossaryController')->getTranslate("home_url",$lang) }}account/password" title="Փոխել գաղտնաբառը " class="account_menu_item3 ">Փոխել գաղտնաբառը </a>
                        <a href="{{ app('App\Http\Controllers\GlossaryController')->getTranslate("home_url",$lang) }}account/logout" title="Ելք" class="account_menu_item5 ">Ելք</a>
                    </div>
                </div>
                <div class="account_right_content">
                    <div class="edit_profile_container">
                        <form action="{{ url('/') }}/account/change" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="account-content-title-wrap">
                                <ul class="author_fields">
                                    <li>
                                        <div class="author-field-head">Անուն *</div>
                                        <div class="author-tail clearfix">
                                            <input type="text" name="name" class="form-control"
                                                   value="{{ $user_info->name }}">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="author-field-head">Ազգանուն*</div>
                                        <div class="author-tail clearfix">
                                            <input type="text" name="surname" class="form-control"
                                                   value="{{ $user_info->surname }}">
                                        </div>

                                    </li>
                                    <li>
                                        <div class="author-field-head">Հեռախոս *</div>
                                        <div class="author-tail clearfix">
                                            <input type="text" name="phone" class="form-control"
                                                   value="{{ $user_info->phone }}">
                                        </div>

                                    </li>
                                    <li>
                                        <input type="hidden" name="lang" value="{{ $lang }}">
                                        <input type="submit" class="author-edit-profile-submit" value="Փոխել">
                                    </li>
                                </ul>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </section>
    </main><!-- End main -->
@endsection
