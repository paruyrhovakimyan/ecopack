<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>
<html class="ie ie9"> <![endif]-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('meta_title')</title>
    <meta property="og:title" content="@yield('meta_title')"/>
    <meta property="og:image" content="@yield('meta_image')"/>
    <meta property="og:url" content="@yield('meta_url')"/>
    <meta property="og:site_name"
          content=""/>
    <meta name="description" content="@yield('meta_desc')">
    <meta name="keywords" content="@yield('meta_key')">
    <meta name="author" content="">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-118780637-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-118780637-1');
    </script>
    <!-- Favicons-->
    <link rel="shortcut icon" href="{{ url('/') }}/public/img/logo.png" type="image/x-icon">
    
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <!-- Google web fonts -->
    <link href="https://fonts.googleapis.com/css?family=Gochi+Hand|Lato:300,400|Montserrat:400,400i,700,700i"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('/public/assets/bootstrap/dist/css/bootstrap.css?v=1.1.3') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/public/sweetalert/lib/sweet-alert.css?v=1.1.3') }}">

    <!-- BASE CSS -->
    <link href="{{ asset('/public/css/base.css?v=1.1.3') }}" rel="stylesheet">
    <link href="{{ asset('/public/css/menu.css?v=1.1.3') }}" rel="stylesheet">
    <link href="{{ asset('/public/css/owl/owl.carousel.css?v=1.1.3') }}" rel="stylesheet">
    <link href="{{ asset('/public/css/owl/owl.theme.css?v=1.1.3') }}" rel="stylesheet">

    <link href="{{ asset('/public/css/layerslider/css/layerslider.css?v=1.1.3') }}" rel="stylesheet">

    <!-- REVOLUTION LAYERS STYLES -->
    <style>
        .tp-caption.NotGeneric-Title,
        .NotGeneric-Title {
            color: rgba(255, 255, 255, 1.00);
            font-size: 32px;
            line-height: 70px;
            font-weight: 800;
            font-style: normal;
            text-decoration: none;
            background-color: transparent;
            border-color: transparent;
            border-style: none;
            border-width: 0px;
            border-radius: 0 0 0 0px
        }

        .tp-caption.NotGeneric-SubTitle,
        .NotGeneric-SubTitle {
            color: rgba(255, 255, 255, 1.00);
            font-size: 13px;
            line-height: 20px;
            font-weight: 500;
            font-style: normal;
            text-decoration: none;
            background-color: transparent;
            border-color: transparent;
            border-style: none;
            border-width: 0px;
            border-radius: 0 0 0 0px;
            letter-spacing: 4px
        }

        .tp-caption.NotGeneric-Icon,
        .NotGeneric-Icon {
            color: rgba(255, 255, 255, 1.00);
            font-size: 30px;
            line-height: 30px;
            font-weight: 400;
            font-style: normal;
            text-decoration: none;
            background-color: rgba(0, 0, 0, 0);
            border-color: rgba(255, 255, 255, 0);
            border-style: solid;
            border-width: 0px;
            border-radius: 0px 0px 0px 0px;
            letter-spacing: 3px
        }

        .tp-caption.NotGeneric-Button,
        .NotGeneric-Button {
            color: rgba(255, 255, 255, 1.00);
            font-size: 14px;
            line-height: 14px;
            font-weight: 500;
            font-style: normal;
            text-decoration: none;
            background-color: rgba(0, 0, 0, 0);
            border-color: rgba(255, 255, 255, 0.50);
            border-style: solid;
            border-width: 1px;
            border-radius: 0px 0px 0px 0px;
            letter-spacing: 3px
        }

        .tp-caption.NotGeneric-Button:hover,
        .NotGeneric-Button:hover {
            color: rgba(255, 255, 255, 1.00);
            text-decoration: none;
            background-color: transparent;
            border-color: rgba(255, 255, 255, 1.00);
            border-style: solid;
            border-width: 1px;
            border-radius: 0px 0px 0px 0px;
            cursor: pointer
        }
    </style>

</head>

<body>

<div id="preloader">
    <div class="sk-spinner sk-spinner-wave">
        <div class="sk-rect1"></div>
        <div class="sk-rect2"></div>
        <div class="sk-rect3"></div>
        <div class="sk-rect4"></div>
        <div class="sk-rect5"></div>
    </div>
</div>
<!-- End Preload -->

<!-- Mobile menu overlay mask -->

<!-- Header================================================== -->

<div class="mobile_header">
    <div class="mobile_logo">
        <div class="mobile_lang">
            <div class="top_header_lang">
                {!! app('App\Http\Controllers\HomeController')->getLanguageBox($lang)  !!}
            </div>
            <div class="mobile_account">
                <div class="top_header_account">
                    @if(session('account'))
                        <a href="{{ url('/') }}/account"><i class="fa fa-user" aria-hidden="true"></i></a>
                    @else
                        <a href="{{ url('/') }}/login"><i class="fa fa-user" aria-hidden="true"></i></a>
                    @endif
                </div>
            </div>

        </div>
        <div class="mobile_card">
            <div class="top_header_card">
                <div class="card_icon">
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                </div>
                <div class="card_count"><p>{{ app('App\Http\Controllers\CardController')->getCount() }}</p></div>

                <div class="top-cart-content">
                    {!! app('App\Http\Controllers\CardController')->getCardContent($lang)  !!}

                </div>
            </div>
        </div>

    </div>
    <div class="mobile_header_top_right">
        <div class="menu_button">
            <hr>
            <hr>
            <hr>
        </div>
    </div>
    <div class="mobile_menu">
        {!! app('App\Http\Controllers\MenuController')->getTopMenu($lang) !!}
    </div>

</div>

<div class="top_header">
    <div class="container">
        <div class="top_header_phone">
            <p>{{ app('App\Http\Controllers\GlossaryController')->getTranslate("phone_number",$lang) }}</p>
        </div>
        <div class="top_header_right">
            <div class="top_header_search">
                <form action="{{ url('/') }}/search" method="post">
                    {{ csrf_field() }}
                    <input type="text" name="search_text" class="search_text">
                    <button type="submit" id="edit-submit" name="op" value="Поиск" class="submit_siarch"><i class="fa fa-search" aria-hidden="true"></i></button>
                </form>
            </div>
            <div class="top_header_lang">
                {!! app('App\Http\Controllers\HomeController')->getLanguageBox($lang)  !!}
            </div>
        </div>
    </div>
</div>
<nav class="navbar navbar-expand-lg navbar-dark my_nav">
    <div class="container">
        <div class="header_logo">
            <a href="{{ app('App\Http\Controllers\GlossaryController')->getTranslate("home_url",$lang) }}">
                <img src="{{ url('/') }}/public/img/logo.png" alt="">
            </a>
        </div>
        <div class="collapse navbar-collapse" id="navbarColor01">
            {!! app('App\Http\Controllers\MenuController')->getTopMenu($lang) !!}
        </div>
        <div class="footer_soc">
            <a href=""><i class="fa fa-facebook footer_fa" aria-hidden="true"></i></a>
            <a href=""><i class="fa fa-twitter footer_fa" aria-hidden="true"></i></a>
            <a href=""><i class="fa fa-instagram footer_fa" aria-hidden="true"></i></a>
        </div>
    </div>
</nav>


<main>
    @yield('content')
</main>
<!-- End main -->

<div class="footer">
    <div class="container">
        <div class="footer_container">
            <div class="footer_left">
                <p class="footer_tel_col" style="font-weight: bold;">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("pashton1",$lang) }}</p>
                <p class="footer_tel" style="font-weight: bold;">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("anun1",$lang) }}</p>
                <p class="footer_tel">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("phone_number1",$lang) }}</p>
                <p class="footer_tel_col">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("email1",$lang) }}</p>
            </div>
            <div class="footer_left2">
                <p class="footer_tel_col" style="font-weight: bold;">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("pashton2",$lang) }}</p>
                <p class="footer_tel" style="font-weight: bold;">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("anun2",$lang) }}</p>
                <p class="footer_tel">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("phone_number2",$lang) }}</p>
                <p class="footer_tel_col">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("email2",$lang) }}</p>
            </div>
            <div class="footer_left3">
                <p class="footer_tel_col" style="font-weight: bold;">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("pashton3",$lang) }}</p>
                <p class="footer_tel" style="font-weight: bold;">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("anun3",$lang) }}</p>
                <p class="footer_tel">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("phone_number3",$lang) }}</p>
                <p class="footer_tel_col">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("email3",$lang) }}</p>
            </div>
            <div class="footer_right">
                <p class="footer_soc_green">
                    <a href=""><i class="fa fa-facebook footer_fa_green" aria-hidden="true"></i></a>
                    <a href=""><i class="fa fa-twitter footer_fa_green" aria-hidden="true"></i></a>
                    <a href=""><i class="fa fa-instagram footer_fa_green" aria-hidden="true"></i></a>
                </p>
            </div>
        </div>
        <p class="footer_rights">
            Copyright© 2019, <span class="footer_name">{{ app('App\Http\Controllers\GlossaryController')->getTranslate("copy_right",$lang) }}</span>
        </p>
    </div>
</div>


<div id="toTop"></div><!-- Back to top button -->


@if (session('status'))
    <div class="flash-message">
        <div class="alert alert-success">
            Your message was sent successfully. Thanks.
        </div>
    </div>
@endif


<!-- Common scripts -->
<script src="{{ asset('/public/js/jquery-2.2.4.min.js') }}"></script>
<script src="{{ asset('/public/js/common_scripts_min.js') }}"></script>
<script src="{{ asset('/public/js/functions.js') }}"></script>
<script src="{{ asset('/public/sweetalert/lib/sweet-alert.js') }}"></script>

<script src="{{ asset('/public/css/layerslider/js/greensock.js') }}"></script>
<script src="{{ asset('/public/css/layerslider/js/layerslider.transitions.js') }}"></script>
<script src="{{ asset('/public/css/layerslider/js/layerslider.kreaturamedia.jquery.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        'use strict';
        $('#layerslider').layerSlider({
            autoStart: true,
            responsive: true,
            responsiveUnder: 1170,
            layersContainer: 1170,
            skinsPath: 'css/layerslider/skins/'
        });
    });
</script>


<script type="text/javascript">

    $('.flash-message').show(0).delay(2000).hide(1000);

    $(".my_cony").click(function () {
        $(".popup_contact").toggle();
    });
    $(".close_contact").click(function () {
        $(".popup_contact").toggle();
    });

    $(".private_area_login ").click(function () {
        var usename = $(".private_area_username").val();
        var password = $(".private_area_password").val();
        $.ajax({
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/private_area_login',
            data: {"usename": usename, "password": password},
            success: function (msg) {
                if (msg == 'false') {
                    $(".private_area_error").html("Invalid login or password");
                } else {
                    $(".private_area_content").append(msg);
                    $(".private_area_block").removeClass("hidden");


                }

            }
        });
    });
    $(".close_private_area").click(function () {
        $(".private_area_block").addClass("hidden");
        $(".private_box").remove();
    });

    $(".news_sub_page_pdf_one").click(function () {

        if ($(this).next(".news_sub_page_pdf_list").hasClass("active_page_list")) {
            $(this).removeClass("active_title");
            $('.sub_page_icon_plus').removeClass("hidden");
            $('.sub_page_icon_minus').addClass("hidden");
            $(this).next(".news_sub_page_pdf_list").removeClass("active_page_list");
        } else {
            $(".news_sub_page_pdf_list").removeClass("active_page_list");
            $(this).next(".news_sub_page_pdf_list").addClass("active_page_list");
            $('.sub_page_icon_plus').removeClass("hidden");
            $('.sub_page_icon_minus').addClass("hidden");
            $(".news_sub_page_pdf_one").removeClass("active_title");
            $(this).addClass("active_title");
            $(this).children().children('.sub_page_icon_plus').addClass("hidden");
            $(this).children().children('.sub_page_icon_minus').removeClass("hidden")
        }

    });

    $(".partner_sub_one_title").click(function () {

        if ($(this).next(".news_sub_page_pdf_list").hasClass("active_page_list")) {
            $(this).parent(".partner_sub_page_pdf_one").removeClass("active_title");
            $('.sub_page_icon_plus').removeClass("hidden");
            $('.sub_page_icon_minus').addClass("hidden");
            $(this).next(".news_sub_page_pdf_list").removeClass("active_page_list");
        } else {
            $(".news_sub_page_pdf_list").removeClass("active_page_list");
            $(this).next(".news_sub_page_pdf_list").addClass("active_page_list");
            $('.sub_page_icon_plus').removeClass("hidden");
            $('.sub_page_icon_minus').addClass("hidden");
            $(".partner_sub_page_pdf_one").removeClass("active_title");
            $(this).parent(".partner_sub_page_pdf_one").addClass("active_title");
            $(this).children().children('.sub_page_icon_plus').addClass("hidden");
            $(this).children().children('.sub_page_icon_minus').removeClass("hidden")
        }

    });


    $(".current_language").click(function () {
        $(".hidden_languages").slideToggle(200);
    });


    /*ready*/
    function displ() {
        jQuery(".subscribe").css({"display": "block"});
    }

    function hide() {
        jQuery(".subscribe").css({"display": "none"});
    }

    jQuery(document).on('click', ".carousel-button-right", function () {
        var carusel = jQuery(this).parents('.carousel');
        right_carusel(carusel);
        return false;
    });
    jQuery(document).on('click', ".carousel-button-left", function () {
        var carusel = jQuery(this).parents('.carousel');
        left_carusel(carusel);
        return false;
    });

    function left_carusel(carusel) {
        var block_width = $(carusel).find('.carousel-block').outerWidth();
        jQuery(carusel).find(".carousel-items .carousel-block").eq(-1).clone().prependTo($(carusel).find(".carousel-items"));
        jQuery(carusel).find(".carousel-items").css({"left": "-" + block_width + "px"});
        jQuery(carusel).find(".carousel-items .carousel-block").eq(-1).remove();
        jQuery(carusel).find(".carousel-items").animate({left: "0px"}, 200);

    }

    function right_carusel(carusel) {
        var block_width = $(carusel).find('.carousel-block').outerWidth();
        jQuery(carusel).find(".carousel-items").animate({left: "-" + block_width + "px"}, 200, function () {
            jQuery(carusel).find(".carousel-items .carousel-block").eq(0).clone().appendTo(jQuery(carusel).find(".carousel-items"));
            jQuery(carusel).find(".carousel-items .carousel-block").eq(0).remove();
            jQuery(carusel).find(".carousel-items").css({"left": "0px"});
        });
    }

    jQuery(function () {
        auto_right('.carousel');
        auto_right_related('.carousel_related');
        auto_right_recomended('.carousel_recomended');
        auto_right_category('.carousel_category');
        auto_right_category('.mobile_sloder');
        auto_right_category('.the_best_carusel');
    });

    function auto_right(carusel) {
        setInterval(function () {
            if (!jQuery(carusel).is('.hover'))
                right_carusel(carusel);
        }, 3000)
    }
    function auto_right_related(carusel) {
        setInterval(function () {
            if (!jQuery(carusel).is('.hover'))
                right_carusel(carusel);
        }, 5000)
    }
    function auto_right_recomended(carusel) {
        setInterval(function () {
            if (!jQuery(carusel).is('.hover'))
                right_carusel(carusel);
        }, 5500)
    }
    function auto_right_category(carusel) {
        setInterval(function () {
            if (!jQuery(carusel).is('.hover'))
                right_carusel(carusel);
        }, 6500)
    }

    jQuery(document).on('mouseenter', '.carousel', function () {
        jQuery(this).addClass('hover')
    });
    jQuery(document).on('mouseleave', '.carousel', function () {
        jQuery(this).removeClass('hover')
    })
</script>
<script type="text/javascript">
    function codeAddress() {
        alert('ok');
    }

</script>
<script type="text/javascript"
        src="{{ asset('/public/js/footer.js') }}"></script>

<script>
    function email_check(email_tr) {
        var re = /\S+@\S+\.\S+/;
        return re.test(email_tr);
    }

    $(document).on("click", ".contact_btn", function () {
        var test = true;
        if ($(".contact_name").val() == '') {
            $(".contact_name").css("border-color", "red");
            test = false;
        }
        else {
            $(".contact_name").css("border-color", "#444");
        }
        if ($(".contact_mail").val() == '' || !email_check($(".contact_mail").val())) {
            $(".contact_mail").css("border-color", "red");
            test = false;
        }
        else {
            $(".contact_mail").css("border-color", "#444");
        }
        if ($(".contact_phone").val() == '') {
            $(".contact_phone").css("border-color", "red");
            test = false;
        }
        else {
            $(".contact_phone").css("border-color", "#444");
        }
        if ($(".contact_text").val() == '') {
            $(".contact_text").css("border-color", "red");
            test = false;
        }
        else {
            $(".contact_text").css("border-color", "#444");
        }
        if (test) {
            $(".send_form").submit();
        }
        return false;
    })
    $(document).on("click", ".search_box", function () {
        $(".header_right_top").toggleClass("activr_search");
    })
</script>
<script>

    $(document).on("click", ".donate_block_title", function () {
        $(".donate_form").slideToggle(200);
    });
    $(document).on("click", ".donate_btn", function () {
        var donate = true;
        if ($(".first_name").val() == '') {
            $(".first_name").css("border-color", "red");
            donate = false;
        }
        else {
            $(".first_name").css("border-color", "#ccc");
        }
        if ($(".amount").val() == '' || $(".amount").val() == 0) {
            $(".amount").css("border-color", "red");
            donate = false;
        }
        else {
            $(".amount").css("border-color", "#ccc");
        }
        if ($(".last_name").val() == '') {
            $(".last_name").css("border-color", "red");
            donate = false;
        }
        else {
            $(".last_name").css("border-color", "#ccc");
        }
        if ($(".phone").val() == '') {
            $(".phone").css("border-color", "red");
            donate = false;
        }
        else {
            $(".phone").css("border-color", "#ccc");
        }
        if ($(".address").val() == '') {
            $(".address").css("border-color", "red");
            donate = false;
        }
        else {
            $(".address").css("border-color", "#ccc");
        }
        if ($(".email").val() == '' || !email_check($(".email").val())) {
            $(".email").css("border-color", "red");
            donate = false;
        }
        else {
            $(".email").css("border-color", "#ccc");
        }

        if (donate) {
            $(".donate_forma").submit();
        }

    });


    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    $("#phone_number").on("input", function (e) {
        e.target.value = e.target.value.replace(/[^0-9]/g, '')
    });
    $(".close_order_block").click(function () {
        $(".product_order").css("display","none");
    })

    $(".shop_article_content_button").click(function () {
        $(".product_order").css("display","block");
    })
$(document).ready(function () {
    $(document).on("click",".popup_open_info", function () {
        $(".calendar_info").css("display","none");
        $(this).parent().children(".calendar_info").toggle();
    });
    $(document).on("click",".close_calendar_info", function () {
        $(".calendar_info").css("display","none");
    })
    $( ".hover_popup_img" )
        .mouseover(function() {
            $(this).next(".coach_img_popup").css("display","inline-block");
        })
        .mouseout(function() {
            $(this).next(".coach_img_popup").css("display","none");
        });
})
    $(".menu_button").click(function () {
        $(".mobile_menu").toggle(200);
    })
</script>
</body>

</html>

