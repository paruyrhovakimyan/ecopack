@extends('frontend.layouts.page')
@section('meta_title', "ԳՆՈՒՄՆԵՐԻ ԶԱՄԲՅՈՒՂ")
@section('meta_desc', "ԳՆՈՒՄՆԵՐԻ ԶԱՄԲՅՈՒՂ")
@section('meta_key', "ԳՆՈՒՄՆԵՐԻ ԶԱՄԲՅՈՒՂ")
@section('content')
    <div class="container margin_100">
        <form class="checkout_guest" action="{{ url('/') }}/guest" method="post">
            {{ csrf_field() }}
            <div class="checkout_titlep">
                <p>Պատվերի ձևակերպում</p>
            </div>
            <div class="andznakan_tvyal">
                <h3>Անձնական տվյալները</h3>
                <label>Անուն,Ազգանուն *</label><input type="text" class="order_name" value="{{ $user_info->name }}" name="order_name" required>
                <label>Էլ. փոստ *</label><input type="email" class="order_email" value="{{ $user_info->email }}" name="order_email" required><br>
                <label>Հեռախոս *</label><input type="text" class="order_telefon" value="{{ $user_info->phone }}" name="order_phone" required><br>
            </div>
            <div class="araqman_hasce">
                <h3>Առաքման հասցե</h3>
                <label>Հասցե * </label><input type="text" class="address" name="address" required><br>
                <div class="order_shenq">
                    <label>Շենք`</label><input type="text" name="order_street">
                </div>
                <div class="order_shenq">
                    <label>Մուտք՝</label><input type="text" name="order_mutq">
                </div>
                <div class="order_shenq">
                    <label>Բնակարան`</label><input type="text" name="order_home">
                </div>
                <div class="order_shenq">
                    <label>Հարկ`</label><input type="text" name="order_hark">
                </div>
                <label>Նշումներ</label><textarea class="post_textarea" name="order_comment"></textarea><br>

            </div>


            <br>
            <label class="variant_oplati">Վճարման տարբերակը *  </label><br>
            <div class="pay_type">
                <label>Կանխիկ </label>
            </div>
            <div class="cart_all">

                <div class="order_products_count">
                    <span>Պատվերի ապրանքները:</span>
                    <p>{{ $all_count_end }}</p>
                </div>
                <div class="delivery_cost"></div>
                <div class="all_price"> Ընդհանուր գումարը: <span class="all_price_number">{{ $all_price_end }}</span> AMD
                </div>
            </div>
            <input type="hidden" name="confirm_order" value="checkaut">
            <input type="submit" name="confirm_order1" class="confirm_order" value="Պատվերի ձևակերպում">
        </form>
    </div>


@endsection