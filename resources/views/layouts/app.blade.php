$title_en = $request->title_en;
$subtitle_en = $request->subtitle_en;
$firstname_en = $request->firstname_en;
$name_contact_en = $request->name_contact_en;
$lastname_en = $request->lastname_en;
$lastname_contact_en = $request->lastname_contact_en;
$mail_en = $request->mail_en_en;
$email_contact_en = $request->email_contact_en;
$phone_en = $request->phone_en;
$phone_contact_en = $request->phone_contact_en;
$message_en = $request->message_en;
$message_contact_en = $request->message_contact_en;
$captcha_en = $request->captcha_en;
$send_label_en = $request->send_label_en;
$address_label_en = $request->address_label_en;
$address_en = $request->address_en;
$help_label_en = $request->help_label_en;
$help_text_en = $request->help_text_en;
$phone_right_en = $request->phone_right_en;
$mail_right_en = $request->mail_right_en;
$need_right_en = $request->need_right_en;
$need_help_right_en = $request->need_help_right_en;
$help_phone_right_en = $request->help_phone_right_en;
$help_work_right_en = $request->help_work_right_en;
DB::table('contact_page_en')
->where('id', 1)
->update(
[
'title_en' => $title_en,
'subtitle' => $subtitle_en,
'firstname' => $firstname_en,
'name_contact' => $name_contact_en,
'lastname' => $lastname_en,
'lastname_contact' => $lastname_contact_en,
'mail' => $mail_en,
'email_contact' => $email_contact_en,
'phone' => $phone_en,
'phone_contact' => $phone_contact_en,
'message' => $message_en,
'message_contact' => $message_contact_en,
'captcha' => $captcha_en,
'send_label' => $send_label_en,
'address_label' => $address_label_en,
'address' => $address_en,
'help_label' => $help_label_en,
'help_text' => $help_text_en,
'phone_right' => $phone_right_en,
'mail_right' => $mail_right_en,
'need_right' => $need_right_en,
'need_help_right' => $need_help_right_en,
'help_phone_right' => $help_phone_right_en,
'help_work_right' => $help_work_right_en,
]
);