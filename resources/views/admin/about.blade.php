@extends('admin.layouts.page')

@section('content')

    <div class="container">
        <div class="row edit_category">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3>About Us</h3></div>
                </div>
            </div>
        </div>
        <div class="form_block">
            <ul class="languages_tabs">
                <li class="arm_tab active_tab">
                    <p><span>Armenian</span></p>
                </li>
                <li class="rus_tab">

                    <p><span>RUSSIAN</span></p>
                </li>
                <li class="eng_tab">
                    <p><span>ENGLISH</span></p>
                </li>
            </ul>
            <div class="form_box">
                <div class="panel panel-default">
                    <div class="about_form">
                        <form class="form-horizontal" method="POST" action="{{ route('about_us') }}">
                            {{ csrf_field() }}
                            <div class="form-group arm_block">
                                <label for="Page Title">Armenian Title</label>
                                <input id="about_title" type="text" class="form-control" name="about_title" value="{{ $about_info->title }}" required autofocus>
                            </div>

                            <div style="display: none" class="form-group rus_block">
                                <label for="title">Russian Title</label>
                                <input id="about_title" type="text" class="form-control" name="title_ru" value="{{ $about_info->title_ru  }}" autofocus>
                            </div>
                            <div style="display: none" class="form-group eng_block">
                                <label for="title">English Title</label>
                                <input id="about_title" type="text" class="form-control" name="title_en" value="{{ $about_info->title_en  }}" autofocus>
                            </div>

                            <div class="form-group arm_block">
                                <label for="Description">Armenian Description</label>
                                <textarea class="form-control description" name="desc">{{ $about_info->description }}</textarea>
                            </div>

                            <div style="display: none" class="form-group rus_block ">
                                <label for="Description">Russian Description</label>
                                <textarea class="form-control description" name="desc_ru">{{ $about_info->description_ru  }}</textarea>
                            </div>
                            <div style="display: none" class="form-group eng_block">
                                <label for="Description">English Description</label>
                                <textarea class="form-control description" name="desc_en">{{ $about_info->description_en  }}</textarea>
                            </div>


                            <div class="row">
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <h4> META HEADERS </h4>
                                </div>
                                <div class="col-md-6 col-sm-6 pull-left">

                                    <div class="form-group arm_block">
                                        <label>Armenian Meta Title:</label>
                                        <input type="text" placeholder='Meta Title' class="form-control"
                                               value='{{ $about_info->meta_title }}' id='meta_title' name='meta_title'/>
                                    </div>

                                    <div style="display: none" class="form-group rus_block">
                                        <label>Russian Meta Title:</label>
                                        <input type="text" placeholder='Meta Title' class="form-control"
                                               value='{{ $about_info->meta_title_ru  }}' id='meta_title' name='meta_title_ru'/>
                                    </div>
                                    <div style="display: none" class="form-group eng_block">
                                        <label>English Meta Title:</label>
                                        <input type="text" placeholder='Meta Title' class="form-control"
                                               value='{{ $about_info->meta_title_en  }}' id='meta_title' name='meta_title_en'/>
                                    </div>


                                </div>
                                <div class="col-md-6 col-sm-6 pull-left">
                                    <div class="form-group arm_block">
                                        <label>Armenian Meta Keyword:</label>
                                        <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key' class="form-control"
                                               value="{{ $about_info->meta_key }}"/>
                                    </div>

                                    <div style="display: none" class="form-group rus_block">
                                        <label>Russian Meta Keyword:</label>
                                        <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_ru' class="form-control"
                                               value="{{ $about_info->meta_key_ru  }}"/>
                                    </div>
                                    <div style="display: none" class="form-group eng_block">
                                        <label>English Meta Keyword:</label>
                                        <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_en' class="form-control"
                                               value="{{ $about_info->meta_key_en  }}"/>
                                    </div>

                                </div>
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <div class="form-group arm_block">
                                        <label>Armenian Meta Description</label>
                                        <textarea id="meta_desc" class="form-control" name="meta_desc">{{ $about_info->meta_desc }}</textarea>
                                    </div>
                                    <div style="display: none" class="form-group rus_block">
                                        <label>Russian Meta Description</label>
                                        <textarea id="meta_desc" class="form-control" name="meta_desc_ru">{{ $about_info->meta_desc_ru  }}</textarea>
                                    </div>
                                    <div style="display: none" class="form-group eng_block">
                                        <label>English Meta Description</label>
                                        <textarea id="meta_desc" class="form-control" name="meta_desc_en">{{ $about_info->meta_desc_en  }}</textarea>
                                    </div>

                                </div>
                            </div>

                            <input type="hidden" name="save_about" value="ok">
                            <button type="submit" class="btn_1 green pull-right">
                                Save
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
