@extends('admin.layouts.page')

@section('content')

    <div class="container">
        <div class="form_box">
                <div class="page_title_box">
                    {!! $page_bredcrump  !!}
                </div>
            @if( count($languages) > 1)
                <ul class="languages_tabs">
                    @foreach($languages as $language)
                        <li data-tab="{{ $language->short }}"
                            class="lang_tab {{ $language->short."_tab" }} @if($language->first == 1) {{"active_tab"}} @endif">
                            <p><span>{{ $language->title }}</span></p>
                        </li>
                    @endforeach
                </ul>
            @endif
                <div class="about_form">
                    <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/sub_pdf/{{ $parent_id }}/edit/{{ $page_pdf->id }}" enctype='multipart/form-data'>
                        {{ csrf_field() }}
                        <div class="col-md-8 page_left">
                            @foreach($languages as $language)
                                <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                    <label for="title">{{ $language->title }} Title</label>
                                    <input id="about_title" type="text" class="form-control" name="pdf_name_{{ $language->short }}" value="{{ $page_pdf->{"pdf_name_".$language->short} }}"
                                           autofocus>
                                </div>

                                <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                    <label for="Description">{{ $language->title }} Short Description</label>
                                    <textarea class="form-control short_desc" name="pdf_text_{{ $language->short }}">{{ $page_pdf->{"pdf_text_".$language->short} }}</textarea>
                                </div>
                            @endforeach
                                @foreach($languages as $language)
                                    <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                        <div class="">
                                            <label>Upload {{ $language->title }} PDF</label>
                                        </div>
                                        <div class="pdf_file_title">
                                            <input type="text" name="old_pdf_title" readonly class="new_pdf_file_{{ $language->short }} new_pdf_file old_pdf_title"
                                                   value="{{ $page_pdf->{"pdf_file_".$language->short} }}">
                                        </div>
                                        <div class="file-upload btn btn_1 green" style="float:left;">
                                            <span>Choose file</span>
                                            <input data-lang="new_pdf_file_{{ $language->short }}" onchange="readURLPdf(this)" type="file" name="pdf_file_{{ $language->short }}"
                                                   class="image upload article_pdf_file">
                                        </div>
                                    </div>

                                @endforeach
                            <div class="col-md-12">
                                <input type="hidden" name="edit_pdf" value="ok">
                                <button type="submit" class="btn_1 green pull-right">
                                    Save
                                </button>
                            </div>
                        </div>


                    </form>
                </div>
            </div>



    </div>
@endsection
