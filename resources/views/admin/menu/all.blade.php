@extends('admin.layouts.page')
<link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/jquery.switch.css') }}" rel="stylesheet">
@section('content')

    <div class="container services_block">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Menus</h3>
                        <div class="select_menu">
                            <span>Select a menu to edit:</span>
                            <select name="menus" class="menus">
                                <option value="0">-- Select menu --</option>
                                @foreach($menus as $menu)
                                    <option value="{{ $menu->id }}">{{ $menu->title }}</option>
                                @endforeach
                            </select>
                            <span>or <a class="btn_1 green" href="{{ url('/') }}/admin/menu/create">create a new menu</a></span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
