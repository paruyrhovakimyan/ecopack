@extends('admin.layouts.page')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Menus</h3>
                        <div class="select_menu">
                            <span>Select a menu to edit:</span>
                            <select name="menus" class="menus">
                                <option value="0">-- Select menu --</option>
                                @foreach($menus as $menu)
                                    <option value="{{ $menu->id }}">{{ $menu->title }}</option>
                                @endforeach
                            </select>
                            <span>or <a class="btn_1 green" href="{{ url('/') }}/admin/menu/create">create a new menu</a></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="menu_box">
                            <div class="menu_edit_section">
                                <div class="menu_items_left ">
                                    <div class="menu_add_pages disabled">
                                        <div class="menu_add_pages_title ">
                                            Էջեր
                                        </div>
                                        <div class="menu_add_pages_content">
                                            @foreach($pages as $page)
                                                <div class="one_row"><input type="checkbox" name="page_add"
                                                                            class="page_add"
                                                                            value="{{ $page->id }}"><label>{{ $page->title }}</label>
                                                </div>
                                            @endforeach
                                        </div>
                                        <input type="button" name="add_to_menu_pages" class="add_to_menu_pages"
                                               value="Add to Menu">
                                    </div>
                                    <div class="menu_add_pages disabled">
                                        <div class="menu_add_pages_title">Բաժիններ</div>
                                        <div class="menu_add_pages_content">
                                            @foreach($categories as $category)
                                                <div class="one_row"><input type="checkbox" name="page_add"
                                                                            class="page_add"
                                                                            value="{{ $category->id }}"><label>{{ $category->title }}</label>
                                                </div>
                                            @endforeach
                                        </div>
                                        <input type="button" name="add_to_menu_cats" class="add_to_menu_cats" value="Add to Menu">
                                    </div>
                                    <div class="menu_add_pages disabled" style="margin-bottom: 20px;">
                                        <div class="menu_add_pages_title">Link</div>
                                        <div class="menu_add_pages_content menu_add_pages_content_hidden"
                                             style="text-align: center">
                                            <label>Menu item name in Arminian:</label>
                                            <input type="text" name="link_name" class="link_name"
                                                   style="margin-bottom: 10px"><br>
                                            <label>Menu item name in Russian:</label>
                                            <input type="text" name="link_name_ru" class="link_name_ru"
                                                   style="margin-bottom: 10px"><br>
                                            <label>Menu item name in English:</label>
                                            <input type="text" name="link_name_en" class="link_name_en"
                                                   style="margin-bottom: 10px"><br>
                                            <label>Menu item link in Arminian:</label>
                                            <input type="text" name="link_href" class="link_href"
                                                   style="width: 98%"><br>
                                            <label>Menu item link in Russian:</label>
                                            <input type="text" name="link_href_ru" class="link_href_ru"
                                                   style="width: 98%"><br>
                                            <label>Menu item link in English:</label>
                                            <input type="text" name="link_href_en" class="link_href_en"
                                                   style="width: 98%"><br>
                                        </div>
                                        <input type="button" name="add_to_menu_link" class="add_to_menu_link"
                                               value="Add to Menu">
                                    </div>
                                </div>

                                    <div class="menu_right_area">
                                        <div class="new_menu_title">
                                            <form action="{{ url('/') }}/admin/menu" method="post">
                                                {{ csrf_field() }}
                                                <label>Menu Name</label>
                                                <input type="text" class="form-control new_menu_title_input" name="menu_title" value="">
                                                <input type="submit" class="btn btn-primary pull-right" value="Create Menu">
                                                <input type="hidden" name="create_menu" value="ok">
                                            </form>
                                        </div>
                                        <div class="menu_right_area_body">
                                            <p>Give your menu a name, then click Create Menu.</p>
                                        </div>
                                    </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
