@extends('admin.layouts.page')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Menus</h3>
                        <div class="select_menu">
                            <span>Select a menu to edit:</span>
                            <select name="menus" class="menus">
                                @foreach($menus as $menu)
                                    <option @if($current_menu->id ==$menu->id ) {{ "selected" }} @endif value="{{ $menu->id }}">{{ $menu->title }}</option>
                                @endforeach
                            </select>
                            <span>or <a class="btn_1 green"
                                        href="{{ url('/') }}/admin/menu/create">create a new menu</a></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="menu_box">
                            <div class="menu_edit_section">
                                <div class="menu_items_left ">
                                    <div class="menu_add_pages active_open">
                                        <div class="menu_add_pages_title ">
                                            Էջեր
                                        </div>
                                        <div class="menu_add_pages_content">
                                            @foreach($pages as $page)
                                                <div class="one_row"><input type="checkbox" name="page_add"
                                                                            class="page_add"
                                                                            value="{{ $page->id }}"><label>{{ $page->title }}</label>
                                                </div>
                                            @endforeach
                                        </div>
                                        <input type="button" name="add_to_menu_pages" class="add_to_menu_pages"
                                               value="Add to Menu">
                                    </div>
                                    <div class="menu_add_pages">
                                        <div class="menu_add_pages_title">Բաժիններ</div>
                                        <div class="menu_add_pages_content">
                                            @foreach($categories as $category)
                                                <div class="one_row"><input type="checkbox" name="page_add"
                                                                            class="page_add"
                                                                            value="{{ $category->id }}"><label>{{ $category->title }}</label>
                                                </div>
                                            @endforeach
                                        </div>
                                        <input type="button" name="add_to_menu_cats" class="add_to_menu_cats" value="Add to Menu">
                                    </div>
                                    <div class="menu_add_pages" style="margin-bottom: 20px;">
                                        <div class="menu_add_pages_title">Link</div>
                                        <div class="menu_add_pages_content menu_add_pages_content_hidden"
                                             style="text-align: center">

                                            @if($settings->lang_am == 1)
                                                <label>Menu item name in Arminian:</label>
                                                <input type="text" name="link_name" class="link_name"
                                                       style="margin-bottom: 10px"><br>
                                                <label>Menu item link in Arminian:</label>
                                                <input type="text" name="link_href" class="link_href"
                                                       style="width: 98%"><br>
                                            @endif
                                            @if($settings->lang_ru == 1)
                                                    <label>Menu item name in Russian:</label>
                                                    <input type="text" name="link_name_ru" class="link_name_ru"
                                                           style="margin-bottom: 10px"><br>
                                                    <label>Menu item link in Russian:</label>
                                                    <input type="text" name="link_href_ru" class="link_href_ru"
                                                           style="width: 98%"><br>
                                            @endif
                                            @if($settings->lang_en == 1)
                                                    <label>Menu item name in English:</label>
                                                    <input type="text" name="link_name_en" class="link_name_en"
                                                           style="margin-bottom: 10px"><br>
                                                    <label>Menu item link in English:</label>
                                                    <input type="text" name="link_href_en" class="link_href_en"
                                                           style="width: 98%"><br>
                                            @endif







                                        </div>
                                        <input type="button" name="add_to_menu_link" class="add_to_menu_link"
                                               value="Add to Menu">
                                    </div>
                                </div>

                                <div class="menu_right_area">
                                    <div class="new_menu_title">
                                        <label>Menu Name</label>
                                        <input type="text" class="form-control new_menu_title_input" name="menu_title"
                                               value="{{ $current_menu->title }}">
                                        <input type="hidden" class="current_menu_id" name="current_menu_id"
                                               value="{{ $current_menu->id }}">
                                        <input type="button" name="save_menu"
                                               class="btn save_menu btn-primary pull-right" value="Save Menu">
                                    </div>
                                    <div class="menu_right_area_body">
                                        <div class="menu_structure">
                                            <p>Menu Structure</p>
                                            <span class="menu_item_null">Add menu items from the column on the left.</span>
                                            <span class="menu_item_not_null hidden">Drag each item into the order you prefer. Click the arrow on the right of the item to reveal additional configuration options.</span>
                                        </div>
                                        <div class="gridster menu_items_box ready">
                                            <ul>
                                                @foreach($menu_parent_items as $row_item)

                                                    <li data-row="{{$row_item->item_order}}" data-col="1" data-sizex="8"
                                                        data-sizey="1" class="gs-w"
                                                        data-id_m="{{$row_item->item_id}}">{{$row_item->name}}<a
                                                                onclick="remove_from_menu(this)"><i
                                                                    class="icon-cancel-2"></i></a></li>

                                                    @foreach($row_item->child as $row_childName)
                                                        @if(isset($row_childName['name']))
                                                            <li data-row="{{$row_childName['order']}}" data-col="2"
                                                                data-sizex="8" data-sizey="1" class="gs-w childe"
                                                                data-id_m="{{$row_childName['id']}}">
                                                                {{$row_childName['name']}}<a
                                                                        onclick="remove_from_menu(this)"><i
                                                                            class="icon-cancel-2"></i></a></li>


                                                            @if(isset($row_childName['child2']))
                                                                @foreach($row_childName['child2'] as $row_childName2)
                                                                    @if(isset($row_childName2['name']))
                                                                        <li data-row="{{$row_childName2['order']}}"
                                                                            data-col="3" data-sizex="8" data-sizey="1"
                                                                            class="gs-w childe"
                                                                            data-id_m="{{$row_childName2['id']}}">
                                                                            {{$row_childName2['name']}}<a
                                                                                    onclick="remove_from_menu(this)"><i
                                                                                        class="icon-cancel-2"></i></a>
                                                                        </li>

                                                                    @endif
                                                                    @if(isset($row_childName2['child3']))
                                                                        @foreach($row_childName2['child3'] as $row_childName3)
                                                                            @if(isset($row_childName3['name']))
                                                                                <li data-row="{{$row_childName3['order']}}"
                                                                                    data-col="4" data-sizex="8"
                                                                                    data-sizey="1" class="gs-w childe"
                                                                                    data-id_m="{{$row_childName3['id']}}">
                                                                                    {{$row_childName3['name']}}<a
                                                                                            onclick="remove_from_menu(this)"><i
                                                                                                class="icon-cancel-2"></i></a>
                                                                                </li>

                                                                            @endif
                                                                            @if(isset($row_childName3['child4']))
                                                                                @foreach($row_childName3['child4'] as $row_childName4)
                                                                                    @if(isset($row_childName4['name']))
                                                                                        <li data-row="{{$row_childName4['order']}}"
                                                                                            data-col="5" data-sizex="8"
                                                                                            data-sizey="1"
                                                                                            class="gs-w childe"
                                                                                            data-id_m="{{$row_childName4['id']}}">
                                                                                            {{$row_childName4['name']}}
                                                                                            <a
                                                                                                    onclick="remove_from_menu(this)"><i
                                                                                                        class="icon-cancel-2"></i></a>
                                                                                        </li>

                                                                                    @endif
                                                                                    @if(isset($row_childName4['child5']))
                                                                                        @foreach($row_childName4['child5'] as $row_childName5)
                                                                                            @if(isset($row_childName5['name']))
                                                                                                <li data-row="{{$row_childName5['order']}}"
                                                                                                    data-col="6"
                                                                                                    data-sizex="8"
                                                                                                    data-sizey="1"
                                                                                                    class="gs-w childe"
                                                                                                    data-id_m="{{$row_childName5['id']}}">
                                                                                                    {{$row_childName5['name']}}
                                                                                                    <a onclick="remove_from_menu(this)"><i
                                                                                                                class="icon-cancel-2"></i></a>
                                                                                                </li>

                                                                                            @endif
                                                                                            @if(isset($row_childName5['child6']))
                                                                                                @foreach($row_childName5['child6'] as $row_childName6)
                                                                                                    @if(isset($row_childName6['name']))
                                                                                                        <li data-row="{{$row_childName6['order']}}"
                                                                                                            data-col="7"
                                                                                                            data-sizex="8"
                                                                                                            data-sizey="1"
                                                                                                            class="gs-w childe"
                                                                                                            data-id_m="{{$row_childName6['id']}}">
                                                                                                            {{$row_childName6['name']}}
                                                                                                            <a onclick="remove_from_menu(this)"><i
                                                                                                                        class="icon-cancel-2"></i></a>
                                                                                                        </li>

                                                                                                    @endif
                                                                                                @endforeach
                                                                                            @endif
                                                                                        @endforeach
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif

                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            @endif

                                                        @endif
                                                    @endforeach

                                                @endforeach

                                            </ul>
                                        </div>
                                    </div>
                                    <div class="new_menu_title">
                                        <a data-id="{{ $current_menu->id }}" class="menu_delete btn_delete btn-danger">Delete</a>
                                        <input type="button" name="save_menu"
                                               class="btn save_menu btn-primary pull-right" value="Save Menu">
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

