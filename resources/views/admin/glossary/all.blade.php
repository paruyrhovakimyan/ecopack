@extends('admin.layouts.page')

@section('content')
    <div class="container services_block">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Add glossary</h3>
                    </div>
                    <div class="col--md-12">
                        <div class="about_form">
                            <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/glossary">
                                {{ csrf_field() }}
                                <div class="glossary_col">
                                    <div class="form-group">
                                        <label>Short Code</label>
                                        @if(isset($edit_glssary->short_code) && $edit_glssary->short_code !='')
                                        <input type="text" class="form-control" value="{{ $edit_glssary->short_code }}" name="short_code" required>
                                            @else
                                            <input type="text" class="form-control" name="short_code" required>
                                        @endif
                                    </div>
                                </div>

                                @foreach($languages as $language)
                                    <div class="glossary_col">
                                        <div class="form-group">
                                            <label>{{ $language->title }}</label>
                                            @if(isset($edit_glssary->{"glossary_".$language->short} ) && $edit_glssary->{"glossary_".$language->short} !='')
                                                <input type="text" class="form-control" value="{{ $edit_glssary->{"glossary_".$language->short} }}" name="glossary_{{$language->short}}">
                                            @else
                                                <input type="text" class="form-control" name="glossary_{{$language->short}}">
                                            @endif

                                        </div>
                                    </div>
                                @endforeach
                                <div class="glossary_col">
                                    <div class="form-group">
                                        <input type="hidden" name="save_glossary" value="ok">
                                        @if(isset($edit_glssary->id) && $edit_glssary->id !='')
                                            <input type="hidden" name="glossary_id" value="{{ $edit_glssary->id }}">
                                        @else
                                            <input type="hidden" name="glossary_id" value="0">
                                        @endif
                                        <input type="submit" class="btn_1 green pull-right add_glossary_submit form-control" name="add_glossary_submit" value="Save">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <table class="glossary_table">
                            <tr>
                                <th>Short code</th>

                                @foreach($languages as $language)
                                    <th>{{ $language->title }}</th>
                                @endforeach

                                <th></th>
                            </tr>
                            @foreach($all_glossary as $glossary)
                                <tr>
                                    <td>{{$glossary->short_code}}</td>
                                    @foreach($languages as $language)
                                        <td>{{$glossary->{"glossary_".$language->short} }}</td>
                                    @endforeach
                                    <td><a href="{{ url('/') }}/admin/glossary?id={{$glossary->id}}" class="edit_glossary btn_2">Edit</a></td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection