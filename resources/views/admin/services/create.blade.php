@extends('admin.layouts.page')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>New Service</h3>
                    </div>
                </div>
            </div>
        </div>

        <div class="form_block">
            <ul class="languages_tabs">
                @if($settings->lang_am == 1)
                    <li class="arm_tab @if(config('app.locale') == 'am') {{"active_tab"}} @endif">
                        <p><span>Armenian</span></p>
                    </li>
                @endif
                @if($settings->lang_ru == 1)
                    <li class="rus_tab @if(config('app.locale') == 'ru') {{"active_tab"}} @endif">
                        <p><span>RUSSIAN</span></p>
                    </li>
                @endif
                @if($settings->lang_en == 1)
                    <li class="eng_tab @if(config('app.locale') == 'en') {{"active_tab"}} @endif">
                        <p><span>ENGLISH</span></p>
                    </li>
                @endif
            </ul>
            <div class="form_box">
                <div class="panel panel-default">
                    <div class="about_form">
                        <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/services" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                                <label for="title">Armenian Title</label>
                                <input id="about_title" type="text" class="form-control" name="title" value="" required autofocus>
                            </div>
                            <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block">
                                <label for="title">Russian Title</label>
                                <input id="about_title" type="text" class="form-control" name="title_ru" value="" autofocus>
                            </div>
                            <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                <label for="title">English Title</label>
                                <input id="about_title" type="text" class="form-control" name="title_en" value="" autofocus>
                            </div>
                            <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                                <label for="Description">Armenian Short Description</label>
                                <textarea class="form-control short_desc" name="short_desc"></textarea>
                            </div>
                            <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block">
                                <label for="Description">Russian Short Description</label>
                                <textarea class="form-control short_desc" name="short_desc_ru"></textarea>
                            </div>
                            <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                <label for="Description">English Short Description</label>
                                <textarea class="form-control short_desc" name="short_desc_en"></textarea>
                            </div>
                            <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                                <label for="Description">Armenian Description</label>
                                <textarea class="form-control description" name="desc"></textarea>
                            </div>
                            <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block ">
                                <label for="Description">Russian Description</label>
                                <textarea class="form-control description" name="desc_ru"></textarea>
                            </div>
                            <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                <label for="Description">English Description</label>
                                <textarea class="form-control description" name="desc_en"></textarea>
                            </div>

                            <div class="service_date">
                                <label>Date</label>
                                <input id="datapicker2" name="service_date" type="text" value="{{ date("d-m-Y") }}" class="author-combobox-wrap form-control">
                            </div>

                            <div class="row">


                            <div class="form-group">
                                <div class="company_logo">
                                    <div class="col-md-12 col-sm-12 pull-left title_present">
                                        <div class="choose_img_title">
                                            <h4>Service banner <small>(width:1400px, height:470px)</small></h4>
                                        </div>
                                        <div class="avatar_img">
                                            <div class="file-upload btn btn_1 green">
                                                <span>Choose image</span>
                                                <input type="file" name="image" id="uploadBtnLogo" class="image upload"
                                                       onchange="readURLLogo(this)">
                                            </div>
                                        </div>
                                        <div class="service-preview-img profile-image-preview_logo">
                                            <div class="ct-media--left">
                                                <a>
                                                    <img id="uploadPreviewLogo" src="">
                                                </a>
                                            </div>
                                            <div class="ct-media--content">
                                                <span></span>
                                                <a class="cross" onclick="deleteimgLogo();" style="cursor:pointer;">
                                                    <i class="fa fa-trash-o"></i> Delete
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>




                            <div class="row">
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <h4> META HEADERS </h4>
                                </div>
                                <div class="col-md-6 col-sm-6 pull-left">

                                    <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                                        <label>Armenian Meta Title:</label>
                                        <input type="text" placeholder='Meta Title' class="form-control"
                                               value='' id='meta_title' name='meta_title'/>
                                    </div>
                                    <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block">
                                        <label>Russian Meta Title:</label>
                                        <input type="text" placeholder='Meta Title' class="form-control"
                                               value='' id='meta_title' name='meta_title_ru'/>
                                    </div>
                                    <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                        <label>English Meta Title:</label>
                                        <input type="text" placeholder='Meta Title' class="form-control"
                                               value='' id='meta_title' name='meta_title_en'/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 pull-left">
                                    <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                                        <label>Armenian Meta Keyword:</label>
                                        <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key' class="form-control"
                                               value=""/>
                                    </div>
                                    <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block">
                                        <label>Russian Meta Keyword:</label>
                                        <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_ru' class="form-control"
                                               value=""/>
                                    </div>
                                    <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                        <label>English Meta Keyword:</label>
                                        <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_en' class="form-control"
                                               value=""/>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                                        <label>Armenian Meta Description</label>
                                        <textarea id="meta_desc" class="form-control" name="meta_desc"></textarea>
                                    </div>
                                    <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block">
                                        <label>Russian Meta Description</label>
                                        <textarea id="meta_desc" class="form-control" name="meta_desc_ru"></textarea>
                                    </div>
                                    <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                        <label>English Meta Description</label>
                                        <textarea id="meta_desc" class="form-control" name="meta_desc_en"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group order_block">
                                <label for="service_order">Order</label>
                                <input id="service_order" type="text" class="form-control" name="service_order" value="1">
                            </div>

                            <div class="form-group">

                                <div class="service_sub_block">

                                </div>
                                <div class="">
                                    <h4>Service Sub Pages</h4>
                                </div>
                                <div class="btn-primary service_sub_plus">+</div>
                            </div>

                            <input type="hidden" name="add_service" value="ok">
                            <button type="submit" class="btn_1 green pull-right">
                                Save
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
