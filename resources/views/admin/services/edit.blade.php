@extends('admin.layouts.page')

@if(isset($data['edit_successful']))
    <div class="edit_successful">
        <p>The Service updated</p>
    </div>
@endif

@section('content')

    <div class="container">
        <div class="row edit_category">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Edit Service</h3>
                        <a class="btn_1 green pull-right" href="{{ route('services.index') }}">Back to Service</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="form_block">
            <ul class="languages_tabs">
                @if($settings->lang_am == 1)
                    <li class="arm_tab @if(config('app.locale') == 'am') {{"active_tab"}} @endif">
                        <p><span>Armenian</span></p>
                    </li>
                @endif
                @if($settings->lang_ru == 1)
                    <li class="rus_tab @if(config('app.locale') == 'ru') {{"active_tab"}} @endif">
                        <p><span>RUSSIAN</span></p>
                    </li>
                @endif
                @if($settings->lang_en == 1)
                    <li class="eng_tab @if(config('app.locale') == 'en') {{"active_tab"}} @endif">
                        <p><span>ENGLISH</span></p>
                    </li>
                @endif
            </ul>
            <div class="form_box">
                <div class="panel panel-default">
                    <div class="about_form">
                        <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/services/{{ $service->id }}" enctype="multipart/form-data">
                            {{method_field('PUT')}}
                            {{ csrf_field() }}
                            <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                                <label for="title">Armenian Title</label>
                                <input id="about_title" type="text" class="form-control" name="title" value="{{ $service->title_am  }}" required autofocus>
                            </div>

                            <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block">
                                <label for="title">Russian Title</label>
                                <input id="about_title" type="text" class="form-control" name="title_ru" value="{{ $service->title_ru  }}" autofocus>
                            </div>
                            <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                <label for="title">English Title</label>
                                <input id="about_title" type="text" class="form-control" name="title_en" value="{{ $service->title_en  }}" autofocus>
                            </div>


                            <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                                <label for="Description">Armenian Short Description</label>
                                <textarea class="form-control short_desc" name="short_desc">{{ $service->short_desc_am  }}</textarea>
                            </div>

                            <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block">
                                <label for="Description">Russian Short Description</label>
                                <textarea class="form-control short_desc" name="short_desc_ru">{{ $service->short_desc_ru  }}</textarea>
                            </div>
                            <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                <label for="Description">English Short Description</label>
                                <textarea class="form-control short_desc" name="short_desc_en">{{ $service->short_desc_en  }}</textarea>
                            </div>



                            <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                                <label for="Description">Armenian Description</label>
                                <textarea class="form-control description" name="desc">{{ $service->description_am  }}</textarea>
                            </div>


                            <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block ">
                                <label for="Description">Russian Description</label>
                                <textarea class="form-control description" name="desc_ru">{{ $service->description_ru  }}</textarea>
                            </div>
                            <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                <label for="Description">English Description</label>
                                <textarea class="form-control description" name="desc_en">{{ $service->description_en  }}</textarea>
                            </div>


                            <div class="service_date">
                                <label>Date</label>
                                <input id="datapicker2" name="service_date" type="text" value="{{ $service->date }}" class="author-combobox-wrap form-control">
                            </div>








                            <div class="row">
                                <div class="company_logo">
                                    <div class="col-md-12 col-sm-12 pull-left title_present">
                                        <div class="">
                                            <h4>Service banner <small>(width:1400px, height:470px)</small></h4>
                                        </div>
                                        <div class="avatar_img">
                                            <div class="file-upload btn btn_1 green">
                                                <span>Choose image</span>
                                                <input type="file" name="image" id="uploadBtnLogo" class="image upload"
                                                       onchange="readURLEditLogo(this)">
                                            </div>
                                        </div>
                                        <div class="service-preview-img profile-image-preview_logo @if($service->banner !='') {{ "active" }} @endif">
                                            <div class="ct-media--left">
                                                <a>
                                                    <img id="uploadPreviewLogo" src="{{ url('') }}/uploads/services/thumbs/{{ $service->banner }}">
                                                </a>
                                            </div>
                                            <input type="hidden" name="old_image" class="old_image_logo" value="{{ $service->banner }}">
                                            <div class="ct-media--content">
                                                <span></span>
                                                <a class="cross" onclick="deleteimgeditLogo();" style="cursor:pointer;">
                                                    <i class="fa fa-trash-o"></i> Delete
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <h4> META HEADERS </h4>
                                </div>
                                <div class="col-md-6 col-sm-6 pull-left">

                                    <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                                        <label>Armenian Meta Title:</label>
                                        <input type="text" placeholder='Meta Title' class="form-control"
                                               value='{{ $service->meta_title_am  }}' id='meta_title' name='meta_title'/>
                                    </div>


                                    <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block">
                                        <label>Russian Meta Title:</label>
                                        <input type="text" placeholder='Meta Title' class="form-control"
                                               value='{{ $service->meta_title_ru  }}' id='meta_title' name='meta_title_ru'/>
                                    </div>
                                    <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                        <label>English Meta Title:</label>
                                        <input type="text" placeholder='Meta Title' class="form-control"
                                               value='{{ $service->meta_title_en  }}' id='meta_title' name='meta_title_en'/>
                                    </div>

                                </div>
                                <div class="col-md-6 col-sm-6 pull-left">
                                    <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                                        <label>Armenian Meta Keyword:</label>
                                        <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key' class="form-control"
                                               value="{{ $service->meta_key_am  }}"/>
                                    </div>

                                    <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block">
                                        <label>Russian Meta Keyword:</label>
                                        <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_ru' class="form-control"
                                               value="{{ $service->meta_key_ru  }}"/>
                                    </div>
                                    <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                        <label>English Meta Keyword:</label>
                                        <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_en' class="form-control"
                                               value="{{ $service->meta_key_en  }}"/>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                                        <label>Armenian Meta Description</label>
                                        <textarea id="meta_desc" class="form-control" name="meta_desc">{{ $service->meta_desc_am  }}</textarea>
                                    </div>

                                    <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block">
                                        <label>Russian Meta Description</label>
                                        <textarea id="meta_desc" class="form-control" name="meta_desc_ru">{{ $service->meta_desc_ru  }}</textarea>
                                    </div>
                                    <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                        <label>English Meta Description</label>
                                        <textarea id="meta_desc" class="form-control" name="meta_desc_en">{{ $service->meta_desc_en  }}</textarea>
                                    </div>


                                </div>
                            </div>
                            <div class="form-group order_block">
                                <label for="service_order">Order</label>
                                <input id="service_order" type="text" class="form-control" name="service_order" value="{{ $service->service_order }}">
                            </div>
                            <div class="form-group">
                                <label>Slug</label>
                                <input type="text" class="form-control slug_input" name="slug" value="{{ $service->slug }}">
                            </div>

                            <div class="form-group">

                                <div class="service_sub_block">
                                    @foreach($service_sub_pages as $service_sub_page)
                                        <div class="services_sub_page">
                                            <div onclick="remove_from_authors(this)" class="remove_sub_page"><i class="fa fa-trash-o"></i></div>
                                            <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                                                <label for="title">Armenian Title</label>
                                                <input id="about_title" type="text" value="{{ $service_sub_page->title_am }}" class="form-control" name="sub_page_title[]" value="" required autofocus>
                                            </div>
                                            <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block">
                                                <label for="title">Russian Title</label>
                                                <input id="about_title" type="text" {{ $service_sub_page->title_ru }} class="form-control" name="sub_page_title_ru[]" value="" autofocus>
                                            </div>
                                            <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                                <label for="title">English Title</label>
                                                <input id="about_title" type="text" {{ $service_sub_page->title_en }} class="form-control" name="sub_page_title_en[]" value="" autofocus>
                                            </div>
                                            <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                                                <label for="Description">Armenian Description</label>
                                                <textarea class="form-control description" name="sub_page_desc[]">{{ $service_sub_page->description_am }}</textarea>
                                            </div>
                                            <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block ">
                                                <label for="Description">Russian Description</label>
                                                <textarea class="form-control description" name="sub_page_desc_ru[]">{{ $service_sub_page->description_ru }}</textarea>
                                            </div>
                                            <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                                <label for="Description">English Description</label>
                                                <textarea class="form-control description" name="sub_page_desc_en[]">{{ $service_sub_page->description_en }}</textarea>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="">
                                    <h4>Service Sub Pages</h4>
                                </div>
                                <div class="btn-primary service_sub_plus">+</div>
                            </div>


                            <input type="hidden" name="edit_service" value="ok">
                            <button type="submit" class="btn_1 green pull-right">
                                Save
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
