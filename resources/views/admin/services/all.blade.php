@extends('admin.layouts.page')
<link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/jquery.switch.css') }}" rel="stylesheet">
@section('content')

    <div class="container services_block">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Services</h3>
                        <a class="btn_1 green pull-right" href="{{ url('/') }}/admin/services/create">Add Service</a>
                    </div>
                    <div class="about_form">
                        <form class="form-horizontal" method="POST" action="{{ route('about_us') }}">
                            {{ csrf_field() }}
                        </form>


                        <section id="section-1" class="content-current">
                            @foreach($services as $service)
                                <div class="strip_booking">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2">
                                            <label class="switch-light switch-ios pull-right">
                                                <input data-id="{{ $service->id }}" type="checkbox" class="option_hidden option_hidden{{$service->id}}" @if($service->hidden == 0){{ "checked" }}@endif
                                                       value="@if($service->hidden == 0){{ "1" }}@else{{ "0" }}@endif">

                                                <span>
							                        <span>Hide</span>
													<span>Show</span>
													</span>
                                                <a></a>
                                            </label>
                                        </div>
                                        <div class="col-md-6 col-sm-5">
                                            <h3 class="">{{ $service->title }}</h3>
                                        </div>
                                        <div class="col-md-2 col-sm-3">
                                            <ul class="info_booking">
                                                <li><strong>Service id</strong> {{ $service->id }}</li>
                                                <li><strong>Service order</strong> {{ $service->service_order }}</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <div class="booking_buttons">
                                                <a href="{{ url('/') }}/admin/services/{{ $service->id }}/edit" class="btn_2">Edit</a>
                                                <a data-id="{{ $service->id }}" class="service_delete btn_delete btn-danger">Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End row -->
                                </div>
                            @endforeach
                        </section>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
