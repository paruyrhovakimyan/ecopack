@extends('admin.layouts.page')

@if(isset($data['edit_successful']))
    <div class="edit_successful">
        <p>The Service updated</p>
    </div>
@endif

@section('content')

    <div class="container">
        <div class="form_box">
            <div class="page_title_box">
                <h4>{{ $category->{"title_".$default_lang} }}</h4>
            </div>
            @if( count($languages) > 1)
                <ul class="languages_tabs">
                    @foreach($languages as $language)
                        <li data-tab="{{ $language->short }}" class="lang_tab {{ $language->short."_tab" }} @if($language->first == 1) {{"active_tab"}} @endif">
                            <p><span>{{ $language->title }}</span></p>
                        </li>
                    @endforeach
                </ul>
            @endif
                    <div class="about_form">
                        
                        <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/prcats/{{ $category->id }}" enctype="multipart/form-data">
                        {{method_field('PUT')}}
                        {{ csrf_field() }}
                        <!--Armenian-->
                            <div class="col-md-8 page_left">


                                @foreach($languages as $language)
                                    <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                        <label for="title">{{ $language->title }} Title</label>
                                        <input id="about_title" type="text" class="form-control" name="title_{{ $language->short }}" value="{{ $category->{"title_".$language->short} }}"
                                               autofocus>
                                    </div>
                                    <div  class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                        <label for="Description">{{ $language->title }} Description</label>
                                        <textarea class="form-control description" name="short_desc_{{ $language->short }}">{{ $category->{"description_".$language->short} }}</textarea>
                                    </div>

                                @endforeach

                                    <div class="form-group">
                                        <label for="category">Parent category</label>
                                        <select name="parent_cat" id="" class="form-control">
                                            <option value="0">No parent</option>
                                            @foreach($categories as $category_all)
                                                <option @if($category_all->id == $category->parent_cat) {{ "selected" }} @endif value="{{ $category_all->id }}" style="font-weight:bold !important">{{ $category_all->{"title_".$default_lang} }}</option>
                                                @foreach($category_all->childs as $child_cat )
                                                    <option @if($child_cat->id == $category->parent_cat) {{ "selected" }} @endif value="{{ $child_cat->id }}"> - {{ $child_cat->{"title_".$default_lang} }}</option>
                                                @endforeach
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="row page_image">
                                        <div class="company_logo">
                                            <div class="">
                                                <label for="Description">Image</label>
                                            </div>
                                            <div class="avatar_img">
                                                <div class="file-upload btn btn_1 green">
                                                    <span>Choose image</span>
                                                    <input type="file" name="image" id="uploadBtnLogo" class="image upload"
                                                           onchange="readURLLogo(this)">
                                                </div>
                                            </div>
                                            <div class="profile-image-preview_logo @if($category->pr_cat_image !='') {{ "active" }} @endif">
                                                <div class="ct-media--left">
                                                    <a>
                                                        <img id="uploadPreviewLogo" src="{{ url('/')}}/public/uploads/prcat/{{ $category->pr_cat_image }}">
                                                    </a>
                                                </div>
                                                <input type="hidden" name="old_image" class="old_image" value="{{ $category->pr_cat_image }}">
                                                <div class="ct-media--content">
                                                    <span></span>
                                                    <a class="cross" onclick="deleteimgLogo();" style="cursor:pointer;">
                                                        <i class="fa fa-trash-o"></i> Delete
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <div class="col-md-12">
                                    <input type="hidden" name="edit_category" value="ok">
                                    <button type="submit" class="btn_1 green pull-right">
                                        Save
                                    </button>
                                </div>
                            </div>

                            <!--English-->


                        </form>
                    </div>
            </div>
    </div>
@endsection
