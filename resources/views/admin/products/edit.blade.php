@extends('admin.layouts.page')

@if(isset($data['edit_successful']))
    <div class="edit_successful">
        <p>The Service updated</p>
    </div>
@endif

@section('content')

    <div class="container">
        <div class="form_box">
            <div class="page_title_box">
                <h4>{{ $product->{"title_".$default_lang} }}</h4>
            </div>
            @if( count($languages) > 1)
                <ul class="languages_tabs">
                    @foreach($languages as $language)
                        <li data-tab="{{ $language->short }}" class="lang_tab {{ $language->short."_tab" }} @if($language->first == 1) {{"active_tab"}} @endif">
                            <p><span>{{ $language->title }}</span></p>
                        </li>
                    @endforeach
                </ul>
            @endif
            <div class="about_form">

                <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/products/{{ $product->id }}" enctype='multipart/form-data'>
                {{method_field('PUT')}}
                {{ csrf_field() }}
                <!--Armenian-->
                    <div class="col-md-8 page_left">


                        @foreach($languages as $language)
                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="title">{{ $language->title }} Title</label>
                                <input id="about_title" type="text" class="form-control" name="title_{{ $language->short }}" value="{{ $product->{"title_".$language->short} }}"
                                       autofocus>
                            </div>
                            <div  class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="Description">{{ $language->title }} Character</label>
                                <textarea class="form-control description_pr" name="short_desc_{{ $language->short }}">{{ $product->{"short_desc_".$language->short} }}</textarea>
                            </div>
                            <div  class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="Description">{{ $language->title }} Description</label>
                                <textarea class="form-control description_pr" name="short_desc_global_{{ $language->short }}">{{ $product->{"short_desc_global_".$language->short} }}</textarea>
                            </div>
                        @endforeach
                            <div class="form-group">
                                <label for="title">Video</label>
                                <input id="about_title" type="text" class="form-control" name="video" value="{{ $product->video }}"
                                       autofocus>
                            </div>

                            <div class="article_cat
                            author_fields form-group">
                                <label>Category</label>
                                <div class="recomed_cats">
                                    @foreach($categories as $category_all)
                                        <div class="check_block_parent">
                                            <div class="check_block">
                                               <span><b>{{ $category_all->{"title_".$default_lang} }}</b></span>
                                            </div>
                                            @foreach($category_all->childs as $child_cat )
                                                <div class="check_block_child">
                                                    <input @if(in_array($child_cat->id, $cat_array)) {{ "checked" }} @endif type="checkbox" value="{{ $child_cat->id }}" name="product_cat_id[]">
                                                    <span>{{ $child_cat->{"title_".$default_lang} }}</span>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                </div>

                            </div>


                        {{--<div class="article_cat  author_fields form-group">
                            <label>Category</label>
                            <select class="author-combobox-wrap article_author_name form-control" name="product_cat_id"
                                    id="cat_id" required>
                                <option value="0">--Select category--</option>
                                @foreach($categories as $category_all)
                                    <option @if($category_all->id == $product->product_cat_id) {{ "selected" }} @endif value="{{ $category_all->id }}" style="font-weight:bold !important">{{ $category_all->{"title_".$language->short} }}</option>
                                    @foreach($category_all->childs as $child_cat )
                                        <option @if($child_cat->id == $product->product_cat_id) {{ "selected" }} @endif value="{{ $child_cat->id }}"> - {{ $child_cat->{"title_".$language->short} }}</option>
                                    @endforeach
                                @endforeach
                            </select>
                        </div>--}}
                        <div class="article_cat author_fields form-group hidden_field ">
                            <label>Brand</label>
                            <select class="author-combobox-wrap article_author_name form-control" name="pr_brand"
                                    id="cat_id" required>
                                <option value="0">--Select Brand--</option>
                                @foreach($brands as $brand)
                                    <option @if($brand->id == $product->pr_brand) {{ "selected" }} @endif value="{{ $brand->id }}" >{{ $brand->{"title_".$language->short} }}</option>
                                @endforeach
                            </select>
                        </div>


                            <div class="article_cat recomed_cats_group author_fields form-group">
                                <label>Recomended Category</label>
                                <div class="recomed_cats">
                                    @foreach($categories as $category_all)
                                        <div class="check_block_parent">
                                        <div class="check_block">
                                            <input @if(in_array($category_all->id, (array)json_decode($product->recomended))) {{ "checked" }} @endif type="checkbox" value="{{ $category_all->id }}" name="recomeded_cat[]">
                                            <span><b>{{ $category_all->{"title_".$default_lang} }}</b></span>
                                        </div>
                                        @foreach($category_all->childs as $child_cat )
                                            <div class="check_block_child">
                                                <input @if(in_array($child_cat->id, (array)json_decode($product->recomended))) {{ "checked" }} @endif type="checkbox" value="{{ $child_cat->id }}" name="recomeded_cat[]">
                                                <span>{{ $child_cat->{"title_".$default_lang} }}</span>
                                            </div>
                                        @endforeach
                                        </div>
                                    @endforeach
                                </div>

                            </div>

                        <div class="article_cat author_fields form-group hidden_field ">
                            <label>Box type</label>
                            <select class="author-combobox-wrap article_author_name form-control" name="pr_brand"
                                    id="cat_id" required>
                                <option value="0">--Select Type--</option>
                                @foreach($ProductTypes as $prtype)
                                    <option @if($prtype->id == $prtype->pr_brand) {{ "selected" }} @endif value="{{ $prtype->id }}" >{{ $prtype->{"title_".$default_lang} }}</option>
                                @endforeach
                            </select>
                        </div>

                            <div class="article_cat author_fields form-group">
                                <label for="Description">Price</label>
                                <input id="about_title" type="text" class="author-combobox-wrap article_author_name form-control" name="pr_price" value="{{ $product->pr_price }}"
                                       autofocus>
                            </div>

                            <div class="article_cat author_fields form-group">
                                <label for="Description">New Price</label>
                                <input id="about_title" type="text" class="author-combobox-wrap article_author_name form-control" name="pr_new_price" value="{{ $product->pr_new_price }}"
                                       autofocus>
                            </div>
                            <div class="article_cat author_fields form-group">
                                <label for="Description">Product Code</label>
                                <input id="about_title" type="text" class="author-combobox-wrap article_author_name form-control" name="pr_code" value="{{ $product->pr_code }}"
                                       autofocus>
                            </div>
                            <div class="article_cat author_fields form-group">
                                <label for="Description">In Stock</label>
                                <div class="check_pr">
                                    <input @if($product->in_stock == 1) {{ "checked" }} @endif type="checkbox" name="in_stock" value="1">
                                </div>
                            </div>
                            <div class="article_cat author_fields form-group">
                                <label>New Collection</label>
                                <div class="check_pr">
                                    <input @if($product->new_collection == 1) {{ "checked" }} @endif type="checkbox" name="new_collection" value="1">
                                </div>
                            </div>
                            <div class="article_cat author_fields form-group">
                                <label>The Best</label>
                                <div class="check_pr">
                                    <input @if($product->the_best == 1) {{ "checked" }} @endif type="checkbox" name="the_best" value="1">
                                </div>
                            </div>



                        <div class="row page_image">
                            <div class="company_logo">
                                <div class="">
                                    <label>Image (width: 420px, heght:565px)</label>
                                </div>
                                <div class="avatar_img">
                                    <div class="file-upload btn btn_1 green">
                                        <span>Choose image</span>
                                        <input type="file" name="image" id="uploadBtnLogo" class="image upload"
                                               onchange="readURLEditLogo(this)">
                                    </div>
                                </div>
                                <div class="profile-image-preview_logo @if($product->image !='') {{ "active" }} @endif">
                                    <div class="ct-media--left">
                                        <a>
                                            <img id="uploadPreviewLogo" src="/uploads/product/{{ $product->image }}">
                                        </a>
                                    </div>
                                    <input type="hidden" name="old_image" class="old_image_logo" value="{{ $product->image }}">
                                    <div class="ct-media--content">
                                        <span></span>
                                        <a class="cross" onclick="deleteimgeditLogo();" style="cursor:pointer;">
                                            <i class="fa fa-trash-o"></i> Delete
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <label>Upload multiple picts (width: 420px, heght:565px)</label>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group ">

                                        <div class="ct-u-marginBottom30">
                                            <div id="uploadzone" dir="ltr"
                                                 class="dropzone  @if(count($projectImages) != '0') {{ $dropzoneDefaultHidden }} @endif">
                                                <div class="dz-default dz-message"><i class="fa fa-camera"></i>
                                                    <!--<img src="/../assets/images/fileuploader-dragdrop-icon.png">-->
                                                    <div class="btn_1 green"><span>Upload file</span></div>
                                                    <h3 class="fileuploader-input-caption"><span>Drop files here or click to upload.</span></h3></div>
                                                @if(count($projectImages) > 0)
                                                    @foreach($projectImages as $property_image)
                                                        <div class="dz-preview dz-processing dz-image-preview dz-success dz-complete">
                                                            <div class="dz-image"><img data-dz-thumbnail=""
                                                                                       alt="{{ $property_image->image }}"
                                                                                       src="{{ url('/') }}/public/uploads/gallery/150/{{ $property_image->image }}">
                                                            </div>
                                                            <div class="dz-details">
                                                                <div class="dz-filename">
                                                                    <span data-dz-name="">@if(isset($property_image->image)){{ $property_image->image }} @endif</span>
                                                                </div>
                                                                <input type="hidden" data-dz-name1="" name="filename[]"
                                                                       value="{{ $property_image->image }}"
                                                                       class="dz-filename-after">
                                                            </div>
                                                            <div class="dz-progress">
                                                        <span class="dz-upload" data-dz-uploadprogress=""
                                                              style="width: 100%;"></span>
                                                            </div>
                                                            <div class="dz-error-message">
                                                                <span data-dz-errormessage=""></span>
                                                            </div>
                                                            <div class="dz-success-mark">
                                                                <svg width="54px" height="54px" viewBox="0 0 54 54"
                                                                     version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                     xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                                                                    <title>Check</title>
                                                                    <defs></defs>
                                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                                                       fill-rule="evenodd" sketch:type="MSPage">
                                                                        <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z"
                                                                              id="Oval-2" stroke-opacity="0.198794158"
                                                                              stroke="#747474" fill-opacity="0.816519475"
                                                                              fill="#FFFFFF" sketch:type="MSShapeGroup"></path>
                                                                    </g>
                                                                </svg>
                                                            </div>
                                                            <div class="dz-error-mark">
                                                                <svg width="54px" height="54px" viewBox="0 0 54 54"
                                                                     version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                     xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                                                                    <title>Error</title>
                                                                    <defs></defs>
                                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                                                       fill-rule="evenodd" sketch:type="MSPage">
                                                                        <g id="Check-+-Oval-2" sketch:type="MSLayerGroup"
                                                                           stroke="#747474" stroke-opacity="0.198794158"
                                                                           fill="#FFFFFF" fill-opacity="0.816519475">
                                                                            <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z"
                                                                                  id="Oval-2" sketch:type="MSShapeGroup"></path>
                                                                        </g>
                                                                    </g>
                                                                </svg>
                                                            </div>
                                                            <a class="dz-remove old_img_remove" data-dz-remove="">Remove
                                                                file</a>
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 hidden_field">
                                <label>Product Character</label>
                                <div class="news_sub_page_pdf_block">
                                    <div class="btn add_charecter">
                                        <i class="icon-plus"></i>
                                    </div>
                                    <div class="product_charecters">
                                        @foreach($languages as $language)
                                            <div class="form-group char_block lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                                <label for="title">{{ $language->title }} Charecter Title</label>
                                                <label for="Description">{{ $language->title }} Charecter Description</label>
                                            </div>
                                        @endforeach


                                        @if(isset($product->keys_am) && !empty($product->keys_am))
                                            @foreach($product->keys_am as $key => $key_am)
                                                    <div class="charecter_group">
                                                        <div class="form-group char_block lang_field lang_field_am">
                                                            <input id="about_title" type="text" class="form-control" placeholder="Օրինակ: Գույն" name="charecter_title_am[]" value="{{ $key_am }}"
                                                                   autofocus>
                                                            <input id="about_title" type="text" class="form-control" placeholder="Օրինակ: Սպիտակ" name="charecter_desc_am[]" value="{{ $product->vals_am[$key] }}"
                                                                   autofocus>
                                                        </div>
                                                        <div class="form-group char_block lang_field lang_field_ru @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                                            <input id="about_title" type="text" class="form-control" placeholder="Наптимер: Цвет" name="charecter_title_ru[]" value="{{ $product->keys_ru[$key] }}"
                                                                   autofocus>
                                                            <input id="about_title" type="text" class="form-control" placeholder="Наптимер: Белое" name="charecter_desc_ru[]" value="{{ $product->vals_ru[$key] }}"
                                                                   autofocus>
                                                        </div>
                                                        <div class="form-group char_block lang_field lang_field_en @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                                            <input id="about_title" type="text" class="form-control" placeholder="Exaple: Color" name="charecter_title_en[]" value="{{ $product->keys_en[$key] }}"
                                                                   autofocus>
                                                            <input id="about_title" type="text" class="form-control" placeholder="Exaple: White " name="charecter_desc_en[]" value="{{ $product->vals_en[$key] }}"
                                                                   autofocus>
                                                        </div>
                                                        <a class="remove_charecter_group"><i class="icon-minus"></i></a>
                                                    </div>

                                                @endforeach

                                        @endif

                                        </div>
                                </div>
                            </div>
                        <div class="col-md-12">
                            <input type="hidden" name="edit_category" value="ok">
                            <button type="submit" class="btn_1 green pull-right">
                                Save
                            </button>
                        </div>
                    </div>

                    <!--English-->


                </form>
            </div>
        </div>
    </div>
@endsection
