@extends('admin.layouts.page')

@section('content')

    <div class="container">
        <div class="form_box">
            <div class="page_title_box">
                <h4>Add Product</h4>
            </div>
            @if( count($languages) > 1)
                <ul class="languages_tabs">
                    @foreach($languages as $language)
                        <li data-tab="{{ $language->short }}"
                            class="lang_tab {{ $language->short."_tab" }} @if($language->first == 1) {{"active_tab"}} @endif">
                            <p><span>{{ $language->title }}</span></p>
                        </li>
                    @endforeach
                </ul>
            @endif
            <div class="about_form">

                <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/products" enctype='multipart/form-data'>
                    {{ csrf_field() }}
                    <div class="col-md-8 page_left">

                        @foreach($languages as $language)
                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="title">{{ $language->title }} Title</label>
                                <input id="about_title" type="text" class="form-control" name="title_{{ $language->short }}" value=""
                                       autofocus>
                            </div>

                            <div  class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="Description">{{ $language->title }} Character</label>
                                <textarea class="form-control description_pr" name="short_desc_{{ $language->short }}"></textarea>
                            </div>
                            <div  class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="Description">{{ $language->title }} Description</label>
                                <textarea class="form-control description_pr" name="short_desc_global_{{ $language->short }}"></textarea>
                            </div>

                        @endforeach
                            <div class="form-group">
                                <label for="title">Video</label>
                                <input id="about_title" type="text" class="form-control" name="video" value=""
                                       autofocus>
                            </div>
                            <div class="article_cat author_fields form-group">
                                <label>Category</label>
                                <div class="recomed_cats">
                                    @foreach($categories as $category_all)
                                        <div class="check_block_parent">
                                            <div class="check_block">
                                                <span><b>{{ $category_all->{"title_".$default_lang} }}</b></span>
                                            </div>
                                            @foreach($category_all->childs as $child_cat )
                                                <div class="check_block_child">
                                                    <input type="checkbox" value="{{ $child_cat->id }}" name="product_cat_id[]">
                                                    <span>{{ $child_cat->{"title_".$default_lang} }}</span>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                </div>

                            </div>
                            <div class="article_cat author_fields form-group hidden_field">
                                <label>Brand</label>
                                <select class="author-combobox-wrap article_author_name form-control" name="pr_brand"
                                        id="cat_id" required>
                                    <option value="0">--Select Brand--</option>
                                    @foreach($brands as $brand)
                                        <option value="{{ $brand->id }}" >{{ $brand->{"title_".$language->short} }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="article_cat recomed_cats_group author_fields form-group">
                                <label>Recomended Category</label>
                                <div class="recomed_cats">
                                    @foreach($categories as $category_all)
                                        <div class="check_block_parent">
                                            <div class="check_block">
                                                <input type="checkbox" value="{{ $category_all->id }}" name="recomeded_cat[]">
                                                <span><b>{{ $category_all->{"title_".$default_lang} }}</b></span>
                                            </div>
                                            @foreach($category_all->childs as $child_cat )
                                                <div class="check_block_child">
                                                    <input type="checkbox" value="{{ $child_cat->id }}" name="recomeded_cat[]">
                                                    <span>{{ $child_cat->{"title_".$default_lang} }}</span>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                </div>

                            </div>


                            <div class="article_cat author_fields form-group hidden_field ">
                                <label>Box type</label>
                                <select class="author-combobox-wrap article_author_name form-control" name="pr_brand"
                                        id="cat_id" required>
                                    <option value="0">--Select Type--</option>
                                    @foreach($ProductTypes as $prtype)
                                        <option  value="{{ $prtype->id }}" >{{ $prtype->{"title_".$default_lang} }}</option>
                                    @endforeach
                                </select>
                            </div>



                            <div class="article_cat author_fields form-group ">
                                <label for="Description">Price</label>
                                <input id="about_title" type="text" class="author-combobox-wrap article_author_name form-control" name="pr_price" value=""
                                       autofocus>
                            </div>
                            <div class="article_cat author_fields form-group hidden_field">
                                <label for="Description">Old Price</label>
                                <input id="about_title" type="text" class="author-combobox-wrap article_author_name form-control" name="pr_old_price" value=""
                                       autofocus>
                            </div>
                            <div class="article_cat author_fields form-group">
                                <label for="Description">New Price</label>
                                <input id="about_title" type="text" class="author-combobox-wrap article_author_name form-control" name="pr_new_price" value=""
                                       autofocus>
                            </div>
                            <div class="article_cat author_fields form-group">
                                <label for="Description">Product Code</label>
                                <input id="about_title" type="text" class="author-combobox-wrap article_author_name form-control" name="pr_code" value=""
                                       autofocus>
                            </div>
                            <div class="article_cat author_fields form-group">
                                <label for="Description">In Stock</label>
                                <div class="check_pr">
                                    <input checked type="checkbox" name="in_stock" value="1">
                                </div>
                            </div>
                            <div class="article_cat author_fields form-group">
                                <label>New Collection</label>
                                <div class="check_pr">
                                    <input type="checkbox" name="new_collection" value="1">
                                </div>
                            </div>
                            <div class="article_cat author_fields form-group">
                                <label>The Best</label>
                                <div class="check_pr">
                                    <input type="checkbox" name="the_best" value="1">
                                </div>
                            </div>

                        <div class="row page_image">
                            <div class="company_logo">
                                <div class="">
                                    <label for="Description">Image (width: 420px, heght:565px)</label>
                                </div>
                                <div class="avatar_img">
                                    <div class="file-upload btn btn_1 green">
                                        <span>Choose image</span>
                                        <input type="file" name="image" id="uploadBtnLogo" class="image upload"
                                               onchange="readURLLogo(this)">
                                    </div>
                                </div>
                                <div class="profile-image-preview_logo">
                                    <div class="ct-media--left">
                                        <a>
                                            <img id="uploadPreviewLogo" src="">
                                        </a>
                                    </div>
                                    <div class="ct-media--content">
                                        <span></span>
                                        <a class="cross" onclick="deleteimgLogo();" style="cursor:pointer;">
                                            <i class="fa fa-trash-o"></i> Delete
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <div class="form-group gallery_box">
                                <label>Upload multiple picts (width: 420px, heght:565px)</label>
                                <div id="uploadzone" class="dropzone" dir="ltr"></div>
                            </div>


                            <div class="col-md-12 hidden_field">
                                <label>Product Character</label>
                                <div class="news_sub_page_pdf_block">
                                    <div class="btn add_charecter">
                                        <i class="icon-plus"></i>
                                    </div>
                                    <div class="product_charecters">
                                        @foreach($languages as $language)
                                            <div class="form-group char_block lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                                <label for="title">{{ $language->title }} Charecter Title</label>
                                                <label for="Description">{{ $language->title }} Charecter Description</label>
                                            </div>
                                        @endforeach


                                            <div class="charecter_group">
                                                <div class="form-group char_block lang_field lang_field_am">
                                                    <input id="about_title" type="text" class="form-control" placeholder="Օրինակ: Գույն" name="charecter_title_am[]" value=""
                                                           autofocus>
                                                    <input id="about_title" type="text" class="form-control" placeholder="Օրինակ: Սպիտակ" name="charecter_desc_am[]" value=""
                                                           autofocus>
                                                </div>
                                                <div class="form-group char_block lang_field lang_field_ru @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                                    <input id="about_title" type="text" class="form-control" placeholder="Наптимер: Цвет" name="charecter_title_ru[]" value=""
                                                           autofocus>
                                                    <input id="about_title" type="text" class="form-control" placeholder="Наптимер: Белое" name="charecter_desc_ru[]" value=""
                                                           autofocus>
                                                </div>
                                                <div class="form-group char_block lang_field lang_field_en @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                                    <input id="about_title" type="text" class="form-control" placeholder="Exaple: Color" name="charecter_title_en[]" value=""
                                                           autofocus>
                                                    <input id="about_title" type="text" class="form-control" placeholder="Exaple: White " name="charecter_desc_en[]" value=""
                                                           autofocus>
                                                </div>
                                                <a class="remove_charecter_group"><i class="icon-minus"></i></a>
                                            </div>

                                    </div>
                                </div>
                            </div>
                        <input type="hidden" name="add_product" value="ok">
                        <div class="col-md-12">
                            <button type="submit" class="btn_1 green pull-right">
                                Save
                            </button>
                        </div>
                    </div>




                </form>
            </div>
        </div>

    </div>
@endsection
