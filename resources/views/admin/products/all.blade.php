@extends('admin.layouts.page')
<link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/jquery.switch.css') }}" rel="stylesheet">
@section('content')

    <div class="container services_block">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading pr_panel_heading">
                        <h3 class="col-md-2">Products</h3>
                        <div class="product_search col-md-4">
                            <form action="" method="get">
                                <input value="@if(isset($_GET['search_pr'])) {{ $_GET['search_pr'] }} @endif" type="text" name="search_pr" class="form-control">
                                <input type="submit" value="search" class="pr_search_submit">
                            </form>
                        </div>
                        <div class="product_search col-md-4">
                            <form action="" method="get">
                                <select name="pr_cat" class="form-control">
                                    <option value="0">--Choose Category--</option>
                                    @foreach($categories as $category)
                                        <option @if(isset($_GET['pr_cat']) && $_GET['pr_cat'] == $category->id) {{ "selected" }} @endif value="{{ $category->id }}">{{ $category->title_am }}</option>

                                        @foreach($category->childs as $child_cat )
                                            <option @if(isset($_GET['pr_cat']) && $_GET['pr_cat'] == $child_cat->id) {{ "selected" }} @endif value="{{ $child_cat->id }}"> --{{ $child_cat->title_am }}</option>
                                        @endforeach
                                    @endforeach
                                </select>
                                <input type="submit" value="search" class="pr_search_submit">
                            </form>
                        </div>
                        <a class="btn_1 green pull-right" href="{{ url('/') }}/admin/products/create">Add product</a>
                    </div>
                    <div class="about_form">
                        <form class="form-horizontal" method="POST" action="http://foc.loc/admin/about_us">
                            <input type="hidden" name="_token" value="U1AwYoBVfuvUYPcIFGJM9WGm3t1o6xR1K7vLExuD">
                        </form>
                        <section id="section-1" class="content-current">
                            @foreach($products as $news)
                                <div class="strip_booking @if($not_filter == 1) {{ "strip_project".$news->id }} @else {{ "strip_project".$news->pr_id }} @endif">
                                    <div class="row">
                                        <div class="project_img_box col-md-2 col-sm-2">
                                            <div class="page_hidden">
                                                @if($news->hidden == 0)
                                                    <a alt="Show" title="Show" class="visible" data-hidden="1" data-id="{{ $news->id }}" data-type="product">
                                                        <i class="icon-eye"></i>
                                                    </a>

                                                @else
                                                    <a alt="Hide" title="Hide" class="visible" data-hidden="0" data-id="{{ $news->id }}" data-type="product">
                                                        <i class="icon-eye-off"></i>
                                                    </a>
                                                @endif
                                            </div>
                                            @if($news->image !='')
                                           <img src="{{ url('/') }}/uploads/product/thumbs/{{$news->image}}" >
                                                @else
                                                <div class="date">
                                                    <span class="month">{{ date("M", strtotime($news->created_at)) }}</span>
                                                    <span class="day"><strong>{{ date("d", strtotime($news->created_at)) }}</strong>{{ date("D", strtotime($news->created_at)) }}</span>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="col-md-7 col-sm-7">
                                            <h3 class="">{{ $news->{"title_$default_lang"} }}</h3>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <p class="">{{ $news->pr_price }} դրամ</p>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <div class="booking_buttons">
                                                @if($not_filter == 1)
                                                    <a style="margin-bottom: 5px" href="{{ url('/') }}/admin/products/{{ $news->id }}/edit" class="btn_2">Edit</a>
                                                    <a data-id="{{ $news->id }}" class="product_delete btn_delete btn-danger">Delete</a>
                                                @else
                                                    <a style="margin-bottom: 5px" href="{{ url('/') }}/admin/products/{{ $news->pr_id }}/edit" class="btn_2">Edit</a>
                                                    <a data-id="{{ $news->pr_id }}" class="product_delete btn_delete btn-danger">Delete</a>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    <!-- End row -->
                                </div>
                            @endforeach
                            @if(isset($_GET["pr_cat"]))
                                    {{ $products->appends(['pr_cat' => $_GET["pr_cat"]])->links() }}
                            @else
                                    {{ $products->links() }}
                            @endif


                        </section>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
