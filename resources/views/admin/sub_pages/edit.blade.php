@extends('admin.layouts.page')

@if(isset($data['edit_successful']))
    <div class="edit_successful">
        <p>The page updated</p>
    </div>
@endif

@section('content')

    <div class="container">
        <div class="row edit_category">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Edit Sub page </h3>
                        <a class="btn_1 green pull-right" href="{{ url('/') }}/admin/sub_pages/3">Back to Pages</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="form_block">
            <ul class="languages_tabs">
                @if($settings->lang_am == 1)
                    <li class="arm_tab @if(config('app.locale') == 'am') {{"active_tab"}} @endif">
                        <p><span>Armenian</span></p>
                    </li>
                @endif
                @if($settings->lang_ru == 1)
                    <li class="rus_tab @if(config('app.locale') == 'ru') {{"active_tab"}} @endif">
                        <p><span>RUSSIAN</span></p>
                    </li>
                @endif
                @if($settings->lang_en == 1)
                    <li class="eng_tab @if(config('app.locale') == 'en') {{"active_tab"}} @endif">
                        <p><span>ENGLISH</span></p>
                    </li>
                @endif
            </ul>
            <div class="form_box">
                <div class="panel panel-default">

                    <div class="about_form">
                        <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/sub_pages/{{ $page->id }}/update">
                            {{ csrf_field() }}
                            <!--Armenian-->
                                <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block {{ $errors->has('title') ? ' has-error' : '' }}">
                                    <label for="title">Armenian Title</label>
                                    <input id="about_title" type="text" class="form-control" name="title" value="{{ $page->title_am }}"
                                           autofocus>
                                </div>
                                <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif"
                                     class="form-group arm_block">
                                    <label for="Description">Armenian Short Description</label>
                                    <textarea class="form-control short_desc" name="short_desc">{{ $page->short_desc_am }}</textarea>
                                </div>
                                <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                                    <label for="Description">Armenian Description</label>
                                    <textarea class="form-control description" name="desc">{{ $page->description_am }}</textarea>
                                </div>
                                <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="row arm_block">
                                    <div class="col-md-12 col-sm-12 pull-left">
                                        <h4> META HEADERS </h4>
                                    </div>
                                    <div class="col-md-6 col-sm-6 pull-left">

                                        <div class="form-group">
                                            <label>Armenian Meta Title:</label>
                                            <input type="text" placeholder='Meta Title' class="form-control"
                                                   value='{{ $page->meta_title_am }}' id='meta_title' name='meta_title'/>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 pull-left">
                                        <div class="form-group">
                                            <label>Armenian Meta Keyword:</label>
                                            <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key'
                                                   class="form-control"
                                                   value="{{ $page->meta_key_am }}"/>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 pull-left">
                                        <div class="form-group">
                                            <label>Armenian Meta Description</label>
                                            <textarea id="meta_desc" class="form-control" name="meta_desc">{{ $page->meta_desc_am }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <!--Armenian-->

                                <!--Russian-->
                                <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block {{ $errors->has('title_ru') ? ' has-error' : '' }}">
                                    <label for="title">Russian Title</label>
                                    <input id="about_title" type="text" class="form-control" name="title_ru" value="{{ $page->title_ru }}"
                                           autofocus>

                                </div>
                                <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block">
                                    <label for="Description">Russian Short Description</label>
                                    <textarea class="form-control short_desc" name="short_desc_ru">{{ $page->short_desc_ru }}</textarea>
                                </div>
                                <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block">
                                    <label for="Description">Russian Description</label>
                                    <textarea class="form-control description" name="desc_ru">{{ $page->description_ru }}</textarea>
                                </div>
                                <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="row rus_block">
                                    <div class="col-md-12 col-sm-12 pull-left">
                                        <h4> META HEADERS </h4>
                                    </div>
                                    <div class="col-md-6 col-sm-6 pull-left">

                                        <div class="form-group">
                                            <label>Russian Meta Title:</label>
                                            <input type="text" placeholder='Meta Title' class="form-control"
                                                   value='{{ $page->meta_title_ru }}' id='meta_title' name='meta_title_ru'/>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 pull-left">
                                        <div class="form-group">
                                            <label>Russian Meta Keyword:</label>
                                            <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_ru'
                                                   class="form-control"
                                                   value="{{ $page->meta_key_ru }}"/>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 pull-left">
                                        <div class="form-group">
                                            <label>Russian Meta Description</label>
                                            <textarea id="meta_desc" class="form-control" name="meta_desc_ru">{{ $page->meta_desc_ru }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <!--Russian-->

                                <!--English-->
                                <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                    <label for="title">English Title</label>
                                    <input id="about_title" type="text" class="form-control" name="title_en" value="{{ $page->title_en }}"
                                           autofocus>
                                </div>
                                <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                    <label for="Description">English Short Description</label>
                                    <textarea class="form-control short_desc" name="short_desc_en">{{ $page->short_desc_en }}</textarea>
                                </div>
                                <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                    <label for="Description">English Description</label>
                                    <textarea class="form-control description" name="desc_en">{{ $page->description_en }}</textarea>
                                </div>
                                <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="row eng_block">
                                    <div class="col-md-12 col-sm-12 pull-left">
                                        <h4> META HEADERS </h4>
                                    </div>
                                    <div class="col-md-6 col-sm-6 pull-left">

                                        <div class="form-group">
                                            <label>English Meta Title:</label>
                                            <input type="text" placeholder='Meta Title' class="form-control"
                                                   value='{{ $page->meta_title_en }}' id='meta_title' name='meta_title_en'/>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 pull-left">
                                        <div class="form-group">
                                            <label>English Meta Keyword:</label>
                                            <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_en'
                                                   class="form-control"
                                                   value="{{ $page->meta_key_en }}"/>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 pull-left">
                                        <div class="form-group">
                                            <label>English Meta Description</label>
                                            <textarea id="meta_desc" class="form-control" name="meta_desc_en">{{ $page->meta_desc_en }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <!--English-->

                                <div class="form-group">
                                    <label>Slug</label>
                                    <input type="text" class="form-control slug_input" name="slug" value="{{ $page->slug }}">
                                </div>
                            <input type="hidden" name="edit_page" value="ok">
                            <input type="hidden" name="parent_id" value="{{$parent_id}}">
                            <button type="submit" class="btn_1 green pull-right">
                                Save
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
