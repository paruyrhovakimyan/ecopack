@extends('admin.layouts.page')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3>New Sub Page</h3></div>
                </div>
            </div>
        </div>
        <div class="form_block">
            <ul class="languages_tabs">
                @if($settings->lang_am == 1)
                    <li class="arm_tab @if(config('app.locale') == 'am') {{"active_tab"}} @endif">
                        <p><span>Armenian</span></p>
                    </li>
                @endif
                @if($settings->lang_ru == 1)
                    <li class="rus_tab @if(config('app.locale') == 'ru') {{"active_tab"}} @endif">
                        <p><span>RUSSIAN</span></p>
                    </li>
                @endif
                @if($settings->lang_en == 1)
                    <li class="eng_tab @if(config('app.locale') == 'en') {{"active_tab"}} @endif">
                        <p><span>ENGLISH</span></p>
                    </li>
                @endif
            </ul>
            <div class="form_box">
                <div class="panel panel-default">
                    <div class="about_form">
                        <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/sub_pages/{{ $parent_id }}/create">
                        {{ csrf_field() }}

                            <div class="col-md-8">
                                <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif"
                                     class="form-group arm_block">
                                    <label for="title">Armenian Title</label>
                                    <input id="about_title" type="text" class="form-control" name="title" value=""
                                           autofocus>
                                </div>
                                <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif"
                                     class="form-group arm_block">
                                    <label for="Description">Armenian Short Description</label>
                                    <textarea class="form-control short_desc" name="short_desc"></textarea>
                                </div>
                                <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif"
                                     class="form-group arm_block">
                                    <label for="Description">Armenian Description</label>
                                    <textarea class="form-control description" name="desc"></textarea>
                                </div>
                                <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif"
                                     class="row arm_block">
                                    <div class="col-md-12 col-sm-12 pull-left">
                                        <h4> META HEADERS </h4>
                                    </div>
                                    <div class="col-md-6 col-sm-6 pull-left">

                                        <div class="form-group">
                                            <label>Armenian Meta Title:</label>
                                            <input type="text" placeholder='Meta Title' class="form-control"
                                                   value='' id='meta_title' name='meta_title'/>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 pull-left">
                                        <div class="form-group">
                                            <label>Armenian Meta Keyword:</label>
                                            <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key'
                                                   class="form-control"
                                                   value=""/>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 pull-left">
                                        <div class="form-group">
                                            <label>Armenian Meta Description</label>
                                            <textarea id="meta_desc" class="form-control" name="meta_desc"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <!--Armenian-->



                                <!--English-->
                                <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif"
                                     class="form-group eng_block">
                                    <label for="title">English Title</label>
                                    <input id="about_title" type="text" class="form-control" name="title_en" value=""
                                           autofocus>
                                </div>
                                <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                    <label for="Description">English Short Description</label>
                                    <textarea class="form-control short_desc" name="short_desc_en"></textarea>
                                </div>
                                <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif"
                                     class="form-group eng_block {{ $errors->has('description') ? ' has-error' : '' }}">
                                    <label for="Description">English Description</label>
                                    <textarea class="form-control description" name="desc_en"></textarea>
                                </div>
                                <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif"
                                     class="row eng_block">
                                    <div class="col-md-12 col-sm-12 pull-left">
                                        <h4> META HEADERS </h4>
                                    </div>
                                    <div class="col-md-6 col-sm-6 pull-left">

                                        <div class="form-group">
                                            <label>English Meta Title:</label>
                                            <input type="text" placeholder='Meta Title' class="form-control"
                                                   value='' id='meta_title' name='meta_title_en'/>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 pull-left">
                                        <div class="form-group">
                                            <label>English Meta Keyword:</label>
                                            <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_en'
                                                   class="form-control"
                                                   value=""/>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 pull-left">
                                        <div class="form-group">
                                            <label>English Meta Description</label>
                                            <textarea id="meta_desc" class="form-control" name="meta_desc_en"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <!--Armenian-->

                            <!--English-->

                            <input type="hidden" name="add_page" value="ok">
                            <button type="submit" class="btn_1 green pull-right">
                                Save
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
