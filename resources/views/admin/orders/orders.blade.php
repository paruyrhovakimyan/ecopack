@extends('admin.layouts.page')
@section('content')

    <div class="container services_block">
        <div class="row">
            <div class="col-md-12">
                <div class="page_title_box parent_page_title" style="margin-top: 0;">
                    <div class="col-md-8">
                        <h4>Ներդրումներ</h4>
                    </div>
                    <div class="col-md-4">

                    </div>
                </div>
                <table class="order_donate_table">
                    <tr>
                        <th>Անուն Ազգանուն</th>
                        <th>Ընկերությունը</th>
                        <th>Էլ. հասցե</th>
                        <th>Հեռախոս</th>
                        <th>Հասցե</th>
                        <th>Գումար ( USD )</th>
                        <th>Ծրագիր</th>
                        <th>Նշումներ</th>
                    </tr>
                    @foreach($orders as $order)
                        <tr>
                            <td>{{ $order->first_name." ".$order->last_name }}</td>
                            <td>{{ $order->company }}</td>
                            <td>{{ $order->email }}</td>
                            <td>{{ $order->phone }}</td>
                            <td>{{ $order->address }}</td>
                            <td>{{ $order->amount }}</td>
                            <td>{{ $order->project_title }}</td>
                            <td>{{ $order->order_note }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>

@endsection