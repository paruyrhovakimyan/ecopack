@extends('admin.layouts.page')

@if(isset($data['edit_successful']))
    <div class="edit_successful">
        <p>The Service updated</p>
    </div>
@endif

@section('content')

    <div class="container">
        <div class="form_box">
            <div class="page_title_box">
                <h4>{{ $coach->{"title_".$default_lang} }}</h4>
            </div>
            @if( count($languages) > 1)
                <ul class="languages_tabs">
                    @foreach($languages as $language)
                        <li data-tab="{{ $language->short }}" class="lang_tab {{ $language->short."_tab" }} @if($language->first == 1) {{"active_tab"}} @endif">
                            <p><span>{{ $language->title }}</span></p>
                        </li>
                    @endforeach
                </ul>
            @endif
                    <div class="about_form">
                        
                        <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/coaches/{{ $coach->id }}" enctype='multipart/form-data'>
                        {{method_field('PUT')}}
                        {{ csrf_field() }}
                        <!--Armenian-->
                            <div class="col-md-8 page_left">


                                @foreach($languages as $language)
                                    <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                        <label for="title">{{ $language->title }} Title</label>
                                        <input id="about_title" type="text" class="form-control" name="title_{{ $language->short }}" value="{{ $coach->{"title_".$language->short} }}"
                                               autofocus>
                                    </div>
                                    <div  class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                        <label for="Description">{{ $language->title }} Description</label>
                                        <textarea class="form-control description" name="short_desc_{{ $language->short }}">{{ $coach->{"short_desc_".$language->short} }}</textarea>
                                    </div>

                                @endforeach
                                    <div class="article_cat author_fields form-group">
                                        <label>Category</label>
                                        <div class="coach_cat_check_box">
                                            @foreach($categories as $category)
                                                <div class="coach_cat_check_box3">
                                                    <input @if(in_array($category->id, json_decode($coach->coach_category_id))) {{ "checked" }} @endif type="checkbox" name="coach_category_id[]" value="{{ $category->id }}">
                                                    <small>{{ $category->{"title_$default_lang"} }}</small>
                                                </div>
                                            @endforeach

                                        </div>
                                    </div>
                                    <div class="row page_image">
                                        <div class="company_logo">
                                            <div class="">
                                                <label>Image (width: 350px, heght:280px)</label>
                                            </div>
                                            <div class="avatar_img">
                                                <div class="file-upload btn btn_1 green">
                                                    <span>Choose image</span>
                                                    <input type="file" name="image" id="uploadBtnLogo" class="image upload"
                                                           onchange="readURLEditLogo(this)">
                                                </div>
                                            </div>
                                            <div class="profile-image-preview_logo @if($coach->image !='') {{ "active" }} @endif">
                                                <div class="ct-media--left">
                                                    <a>
                                                        <img id="uploadPreviewLogo" src="/uploads/coach/{{ $coach->image }}">
                                                    </a>
                                                </div>
                                                <input type="hidden" name="old_image" class="old_image_logo" value="{{ $coach->image }}">
                                                <div class="ct-media--content">
                                                    <span></span>
                                                    <a class="cross" onclick="deleteimgeditLogo();" style="cursor:pointer;">
                                                        <i class="fa fa-trash-o"></i> Delete
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div  class="form-group ">
                                        <label for="Description">Order</label>
                                        <input type="text" class="form-control" name="coach_order" value="{{ $coach->coach_order }}">
                                    </div>
                                <div class="col-md-12">
                                    <input type="hidden" name="edit_category" value="ok">
                                    <button type="submit" class="btn_1 green pull-right">
                                        Save
                                    </button>
                                </div>
                            </div>

                            <!--English-->


                        </form>
                    </div>
            </div>
    </div>
@endsection
