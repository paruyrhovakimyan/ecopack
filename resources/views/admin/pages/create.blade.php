@extends('admin.layouts.page')
<link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/jquery.switch.css') }}" rel="stylesheet">
@section('content')

    <div class="container">

            <div class="form_box">
                @if(!empty($page_bredcrump))
                    <div class="page_title_box">
                        {!! $page_bredcrump  !!}
                    </div>
                @endif
                @if( count($languages) > 1)
                    <ul class="languages_tabs">
                        @foreach($languages as $language)
                            <li data-tab="{{ $language->short }}" class="lang_tab {{ $language->short."_tab" }} @if($language->first == 1) {{"active_tab"}} @endif">
                                <p><span>{{ $language->title }}</span></p>
                            </li>
                        @endforeach
                    </ul>
                @endif
                <div class="about_form">

                    <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/pages" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <!--Armenian-->

                        @if(isset($parent_id) && $parent_id !='')
                            <div class="col-md-8 page_left">

                                @foreach($languages as $language)
                                    <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                        <label for="title">{{ $language->title }} Title</label>
                                        <input id="about_title" type="text" class="form-control" name="title_{{ $language->short }}" value=""
                                               autofocus>
                                    </div>

                                    <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                        <label for="Description">{{ $language->title }} Short Description</label>
                                        <textarea class="form-control short_desc" name="short_desc_{{ $language->short }}"></textarea>
                                    </div>
                                    <div  class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                        <label for="Description">{{ $language->title }} Description</label>
                                        <textarea class="form-control description" name="description_{{ $language->short }}"></textarea>
                                    </div>

                                @endforeach
                                    <div class="form-group show_in_home">
                                        <label class="for_class">Show in home</label>
                                        <label class="switch-light switch-ios pull-left">
                                            <input type="checkbox" name="show_in_home" class="option_hidden" checked="" value="1">
                                            <span>
                                                        <span>Hide</span>
                                                        <span>Show</span>
                                                        </span>
                                            <a></a>
                                        </label>
                                    </div>
                                @if($page_settings->config_data1 == 1)
                                    <div  class="form-group">
                                        <label for="Description">Data</label>
                                        <input type="text" class="form-control" name="data1" value="">
                                    </div>
                                @endif
                                @if($page_settings->config_data2 == 1)
                                    <div  class="form-group">
                                        <label for="Description">Data 2</label>
                                        <input type="text" class="form-control" name="data2" value="">
                                    </div>
                                @endif
                                @if($page_settings->config_cat == 1)
                                        <div class="article_cat author_fields form-group">
                                            <label>Category</label>
                                            <select class="author-combobox-wrap article_author_name form-control" name="cat_id"
                                                    id="cat_id" required>
                                                <option value="">--Select category--</option>
                                                @foreach($categories as $category)
                                                    <option value="{{ $category->id }}">{{ $category->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                @endif
                                @if($page_settings->config_tag == 1)
                                    <div sclass="form-group">
                                        <label for="Description">Tags</label>
                                        <textarea class="form-control short_desc" name="tag_{{ config('app.locale') }}"></textarea>
                                    </div>
                                @endif
                                @if($page_settings->config_image == 1)
                                    <div class="row page_image">
                                        <div class="company_logo">
                                            <div class="">
                                                <label for="Description">Image</label>
                                            </div>
                                            <div class="avatar_img">
                                                <div class="file-upload btn btn_1 green">
                                                    <span>Choose image</span>
                                                    <input type="file" name="image" id="uploadBtnLogo" class="image upload"
                                                           onchange="readURLLogo(this)">
                                                </div>
                                            </div>
                                            <div class="profile-image-preview_logo">
                                                    <div class="ct-media--left">
                                                        <a>
                                                            <img id="uploadPreviewLogo" src="">
                                                        </a>
                                                    </div>
                                                    <div class="ct-media--content">
                                                        <span></span>
                                                        <a class="cross" onclick="deleteimgLogo();" style="cursor:pointer;">
                                                            <i class="fa fa-trash-o"></i> Delete
                                                        </a>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                @endif
                                    <div class="row page_image">
                                        <div class="company_logo">
                                            <div class="">
                                                <label for="Description">Banner image (width:1600px, heght:800px)</label>
                                            </div>
                                            <div class="avatar_img">
                                                <div class="file-upload btn btn_1 green">
                                                    <span>Choose image</span>
                                                    <input type="file" name="image_bg" id="uploadBtnBackImage" class="image upload"
                                                           onchange="readURLBackImage(this)">
                                                </div>
                                            </div>
                                            <div class="profile-image-preview_back_image">
                                                <div class="ct-media--left">
                                                    <a>
                                                        <img id="uploadPreviewBackImage" src="">
                                                    </a>
                                                </div>
                                                <div class="ct-media--content">
                                                    <span></span>
                                                    <a class="cross" onclick="deleteimgBackImage();" style="cursor:pointer;">
                                                        <i class="fa fa-trash-o"></i> Delete
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>



                                @if($page_settings->config_logo == 1)
                                    <div class="row page_image">
                                        <div class="company_logo">
                                            <div class="">
                                                <label for="Description">Logo</label>
                                            </div>
                                            <div class="avatar_img">
                                                <div class="file-upload btn btn_1 green">
                                                    <span>Choose Image</span>
                                                    <input type="file" name="image_logo" id="uploadBtn1" class="image upload" onchange="readURL(this)">
                                                </div>
                                            </div>
                                            <div class="profile-image-preview">
                                                <div class="ct-media--left">
                                                    <a>
                                                        <img id="uploadPreview" src="">
                                                    </a>
                                                </div>
                                                <div class="ct-media--content">
                                                    <span></span>
                                                    <a class="cross" onclick="deleteimg();" style="cursor:pointer;">
                                                        <i class="fa fa-trash-o"></i> Delete
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                <div class="form-group meta_block">
                                    <div class="open_close_meta">
                                        <p>Meta <i class="icon-arrow-combo"></i></p>
                                    </div>
                                    <div class="meta_box">
                                        <div class="col-md-6 col-sm-6 pull-left">
                                            @foreach($languages as $language)
                                                <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                                    <label>{{ $language->title }} Meta Title:</label>
                                                    <input type="text" placeholder='Meta Title' class="form-control"
                                                           value='' id='meta_title' name='meta_title_{{ $language->short }}'/>
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="col-md-6 col-sm-6 pull-left">
                                            @foreach($languages as $language)
                                                <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                                    <label>{{ $language->title }} Meta Keyword:</label>
                                                    <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_{{ $language->short }}'
                                                           class="form-control"
                                                           value=""/>
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="col-md-12 col-sm-12 pull-left">
                                            @foreach($languages as $language)
                                                <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                                    <label>{{ $language->title }} Meta Description</label>
                                                    <textarea id="meta_desc" class="form-control" name="meta_desc_{{ $language->short }}"></textarea>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-4 page_right">
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <h4> Additional</h4>
                                        </div>
                                      {{--  <div class="additional_block col-md-12">
                                            <input type="checkbox" name="pdf_uploder" value="1">
                                            <label><i class="icon-doc-add"></i> Pdf uploader</label>
                                        </div>--}}
                                        <div class="additional_block col-md-12">
                                            <input type="checkbox" name="page_with_pdf" value="1">
                                            <label><i class="icon-folder-empty"></i> Sub page</label>
                                        </div>
                                        <div class="additional_block col-md-12">
                                            <input type="checkbox" name="calendar_config" value="1">
                                            <label><i class="icon-folder-empty"></i> Calendar</label>
                                        </div>
                                        {{--<div class="additional_block col-md-12">

                                            <input type="checkbox" name="gallery" value="1">
                                            <label><i class="icon-picture-1"></i> Gallery</label>
                                        </div>--}}
                                    </div>

                                </div>
                            </div>

                        @else
                            <div class="col-md-8 page_left">

                                @foreach($languages as $language)
                                    <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                        <label for="title">{{ $language->title }} Title</label>
                                        <input id="about_title" type="text" class="form-control" name="title_{{ $language->short }}" value=""
                                               autofocus>
                                    </div>

                                    <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                        <label for="Description">{{ $language->title }} Short Description</label>
                                        <textarea class="form-control short_desc" name="short_desc_{{ $language->short }}"></textarea>
                                    </div>
                                    <div  class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                        <label for="Description">{{ $language->title }} Description</label>
                                        <textarea class="form-control description" name="description_{{ $language->short }}"></textarea>
                                    </div>

                                @endforeach

                                    <div class="row page_image">
                                        <div class="company_logo">
                                            <div class="">
                                                <label for="Description">Banner image (width:1600px, heght:800px)</label>
                                            </div>
                                            <div class="avatar_img">
                                                <div class="file-upload btn btn_1 green">
                                                    <span>Choose image</span>
                                                    <input type="file" name="image_bg" id="uploadBtnBackImage" class="image upload"
                                                           onchange="readURLBackImage(this)">
                                                </div>
                                            </div>
                                            <div class="profile-image-preview_back_image">
                                                <div class="ct-media--left">
                                                    <a>
                                                        <img id="uploadPreviewBackImage" src="">
                                                    </a>
                                                </div>
                                                <div class="ct-media--content">
                                                    <span></span>
                                                    <a class="cross" onclick="deleteimgBackImage();" style="cursor:pointer;">
                                                        <i class="fa fa-trash-o"></i> Delete
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                <div class="form-group meta_block">
                                    <div class="open_close_meta">
                                        <p>Meta <i class="icon-arrow-combo"></i></p>
                                    </div>
                                    <div class="meta_box">
                                        <div class="col-md-6 col-sm-6 pull-left">
                                            @foreach($languages as $language)
                                                <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                                <label>{{ $language->title }} Meta Title:</label>
                                                <input type="text" placeholder='Meta Title' class="form-control"
                                                       value='' id='meta_title' name='meta_title_{{ $language->short }}'/>
                                            </div>
                                            @endforeach
                                        </div>
                                        <div class="col-md-6 col-sm-6 pull-left">
                                            @foreach($languages as $language)
                                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                                <label>{{ $language->title }} Meta Keyword:</label>
                                                <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_{{ $language->short }}'
                                                       class="form-control"
                                                       value=""/>
                                            </div>
                                            @endforeach
                                        </div>
                                        <div class="col-md-12 col-sm-12 pull-left">
                                            @foreach($languages as $language)
                                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                                <label>{{ $language->title }} Meta Description</label>
                                                <textarea id="meta_desc" class="form-control" name="meta_desc_{{ $language->short }}"></textarea>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-4 page_right">
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <h4> Additional</h4>
                                        </div>
                                        <div class="additional_block col-md-12">
                                            <input type="checkbox" name="sub_pages" value="1">
                                            <label><i class="icon-docs"></i> Sub pages</label>
                                        </div>
                                        <div class="additional_block col-md-12">
                                            <input type="checkbox" name="sub_pages" value="1">
                                            <label><i class="icon-docs"></i> Sub pages with gallery</label>
                                        </div>
                                        <div class="additional_block col-md-12">
                                            <input type="checkbox" name="config_project" value="1">
                                            <label><i class="icon-docs"></i> Sub Project pages</label>
                                        </div>
                                        <div class="additional_block col-md-12">
                                            <input type="checkbox" name="page_with_pdf" value="1">
                                            <label><i class="icon-folder-empty"></i> Page | pdf</label>
                                        </div>
                                        <div class="additional_block col-md-12">
                                            <input type="checkbox" name="pdf_uploder" value="1">
                                            <label><i class="icon-doc-add"></i> Pdf uploader</label>
                                        </div>
                                        <div class="additional_block col-md-12">
                                            <input  type="checkbox" name="gallery" value="1">
                                            <label><i class="icon-picture-1"></i> Gallery</label>
                                        </div>
                                        <div class="additional_block col-md-12">
                                            <input  type="checkbox" name="config_data1" value="1">
                                            <label><i class="icon-calendar"></i> Data 1</label>
                                        </div>
                                        <div class="additional_block col-md-12">
                                            <input  type="checkbox" name="config_data2" value="1">
                                            <label><i class="icon-calendar"></i> Data 2</label>
                                        </div>
                                        <div class="additional_block col-md-12">
                                            <input  type="checkbox" name="config_cat" value="1">
                                            <label><i class="icon-th-list-3"></i> Categories</label>
                                        </div>
                                        <div class="additional_block col-md-12">
                                            <input  type="checkbox" name="config_tag" value="1">
                                            <label><i class="icon-tags"></i> Tags</label>
                                        </div>
                                        <div class="additional_block col-md-12">
                                            <input  type="checkbox" name="config_image" value="1">
                                            <label><i class="icon-camera"></i> Image</label>
                                        </div>
                                        <div class="additional_block col-md-12">
                                            <input  type="checkbox" name="config_bg_image" value="1">
                                            <label><i class="icon-camera"></i>Background image</label>
                                        </div>
                                        <div class="additional_block col-md-12">
                                            <input  type="checkbox" name="config_logo" value="1">
                                            <label><i class="icon-camera"></i>Logo</label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        @endif
                        <input type="hidden" name="add_page" value="ok">
                        <input type="hidden" name="parent" value="@if(isset($parent_id) && $parent_id !=''){{$parent_id}}@else {{ "0" }}@endif">
                        <div class="col-md-12">
                            <button type="submit" class="btn_1 save_btn green pull-right">
                                Save
                            </button>
                        </div>

                    </form>
                </div>
            </div>
    </div>
@endsection
