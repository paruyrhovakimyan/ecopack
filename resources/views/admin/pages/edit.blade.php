@extends('admin.layouts.page')
<link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/jquery.switch.css') }}" rel="stylesheet">
@if(isset($data['edit_successful']))
    <div class="edit_successful">
        <p>The page updated</p>
    </div>
@endif

@section('content')

    <div class="container edit_container">

            <div class="form_box">
                <div class="page_title_box">
                    <h4>@if(!empty($page->parent_title)) {!! $page->parent_title !!} | @endif {{ $page->{"title_".$default_lang} }}</h4>
                </div>
                @if( count($languages) > 1)
                <ul class="languages_tabs">
                    @foreach($languages as $language)
                        <li data-tab="{{ $language->short }}" class="lang_tab {{ $language->short."_tab" }} @if($language->first == 1) {{"active_tab"}} @endif">
                            <p><span>{{ $language->title }}</span></p>
                        </li>
                    @endforeach
                </ul>
                @endif
                <div class="about_form">

                        <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/pages/{{ $page->id }}" enctype="multipart/form-data">
                        {{method_field('PUT')}}
                        {{ csrf_field() }}
                        <!--Armenian-->

                                <div class="col-md-9 page_left">
                                    @foreach($languages as $language)
                                        <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                            <label for="title">{{ $language->title }} Title</label>
                                            <input id="about_title" type="text" class="form-control" name="title_{{ $language->short }}" value="{{ $page->{"title_".$language->short} }}"
                                                   autofocus>
                                        </div>

                                        <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                            <label for="Description">{{ $language->title }} Short Description</label>
                                            <textarea class="form-control short_desc" name="short_desc_{{ $language->short }}">{{ $page->{"short_desc_".$language->short} }}</textarea>
                                        </div>
                                        <div  class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                            <label for="Description">{{ $language->title }} Description</label>
                                            <textarea class="form-control description" name="description_{{ $language->short }}">{{ $page->{"description_".$language->short} }}</textarea>
                                        </div>

                                    @endforeach

                                        @if(isset($page->parent) && $page->parent != 0)
                                            <div class="form-group show_in_home">
                                                <label class="for_class">Show in home</label>
                                                <label class="switch-light switch-ios pull-left">
                                                    <input type="checkbox" name="show_in_home" class="option_hidden" @if($page->show_in_home == 1){{ "checked" }} @endif value="1">
                                                    <span>
                                                        <span>Hide</span>
                                                        <span>Show</span>
                                                        </span>
                                                    <a></a>
                                                </label>
                                            </div>
                                        @endif
                                    @if(!empty($page_settings) && isset($page->parent) && $page->parent != 0)
                                            @if(isset($page_settings->config_image) && $page_settings->config_image == 1)
                                                <div class="row page_image">
                                                    <div class="company_logo">
                                                        <div class="">
                                                            <label for="Description">Image</label>
                                                        </div>
                                                        <div class="avatar_img">
                                                            <div class="file-upload btn btn_1 green">
                                                                <span>Choose image</span>
                                                                <input type="file" name="image" id="uploadBtnLogo" class="image upload"
                                                                       onchange="readURLLogo(this)">
                                                            </div>
                                                        </div>
                                                        <div class="profile-image-preview_logo @if($page->image !='') {{ "active" }} @endif">
                                                            <div class="ct-media--left">
                                                                <a>
                                                                    <img id="uploadPreviewLogo" src="{{ url('/')}}/public/uploads/page/{{ $page->image }}">
                                                                </a>
                                                            </div>
                                                            <input type="hidden" name="old_image" class="old_image" value="{{ $page->image }}">
                                                            <div class="ct-media--content">
                                                                <span></span>
                                                                <a class="cross" onclick="deleteimgLogo();" style="cursor:pointer;">
                                                                    <i class="fa fa-trash-o"></i> Delete
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif



                                            @if(isset($page_settings->config_logo) && $page_settings->config_logo == 1)
                                                <div class="row page_image">
                                                    <div class="company_logo">
                                                        <div class="">
                                                            <label for="Description">Logo (207px x 191px)</label>
                                                        </div>
                                                        <div class="avatar_img">
                                                            <div class="file-upload btn btn_1 green">
                                                                <span>Choose Image</span>
                                                                <input type="file" name="image_logo" id="uploadBtn1" class="image upload" onchange="readURL(this)">
                                                            </div>
                                                        </div>
                                                        <div class="profile-image-preview @if($page->image_logo !='') {{ "active" }} @endif">
                                                            <div class="ct-media--left">
                                                                <a>
                                                                    <img id="uploadPreview" src="@if($page->image_logo !='') {{ url('/') }}/public/uploads/page/{{ $page->image_logo }} @endif">
                                                                </a>
                                                            </div>
                                                            <input type="hidden" name="old_image_logo" class="old_image" value="{{ $page->image_logo }}">
                                                            <div class="ct-media--content">
                                                                <span></span>
                                                                <a class="cross" onclick="deleteimg();" style="cursor:pointer;">
                                                                    <i class="fa fa-trash-o"></i> Delete
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                    @endif
                                        <div class="row page_image">
                                            <div class="company_logo">
                                                <div class="">
                                                    <label for="Description">Banner image (width:1600px, heght:800px)</label>
                                                </div>
                                                <div class="avatar_img">
                                                    <div class="file-upload btn btn_1 green">
                                                        <span>Choose image</span>
                                                        <input type="file" name="image_bg" id="uploadBtnBackImage" class="image upload"
                                                               onchange="readURLBackImage(this)">
                                                    </div>
                                                </div>
                                                <div class="profile-image-preview_back_image @if($page->image_bg !='') {{ "active" }} @endif">
                                                    <div class="ct-media--left">
                                                        <a>
                                                            <img id="uploadPreviewBackImage"  src="@if($page->image_bg !='') {{ url('/')}}/public/uploads/page/{{ $page->image_bg }} @endif">
                                                        </a>
                                                    </div>
                                                    <input type="hidden" name="old_image_bg" class="old_image" value="{{ $page->image_bg }}">
                                                    <div class="ct-media--content">
                                                        <span></span>
                                                        <a class="cross" onclick="deleteimgBackImage();" style="cursor:pointer;">
                                                            <i class="fa fa-trash-o"></i> Delete
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    <div class="form-group">
                                        <label>Slug</label>
                                        <input type="text" class="form-control slug_input" name="slug" value="{{ $page->slug }}">


                                        <label class="switch-light switch-ios pull-right">
                                            <input data-id="{{ $page->id }}" type="checkbox" name="menu_item_hidden"
                                                   class="option_hidden{{$page->id}}"
                                                   @if($page->hidden == 0){{ "checked" }}@endif
                                                   value="@if($page->hidden == 0){{ "1" }}@else{{ "0" }}@endif">

                                            <span>
							                        <span>Hide</span>
													<span>Show</span>
													</span>
                                            <a></a>
                                        </label>

                                    </div>
                                        @if($page->calendar_config == 1)
                                            <div class="calendars_block">


                                            <div class="form-group">
                                                <label>Add Calendar</label>
                                                <div class="new_calendar_block">
                                                    <div data-calendar="{{ count($page_calendars) }}" class="btn new_calendar_block_add">
                                                        <i class="icon-plus"></i>
                                                    </div>
                                                    <div class="page_pdf_plus_block">
                                                    </div>
                                                </div>

                                            </div>

                                                @if(!empty($page_calendars))
                                                    @foreach($page_calendars as $table_key => $page_calendar)
                                                        @if(isset($page->calendar_times[$table_key]) && !empty($page->calendar_times[$table_key]))
                                                        <div class="form-group">
                                                            <label>Calendar {{ $table_key + 1 }}</label>
                                                        </div>
                                                        @foreach($languages as $language)
                                                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                                                <label>{{ $language->title }} Calendar Title:</label>
                                                                <input type="text" placeholder='Calendar Title' class="form-control"
                                                                       value='{{ $page_calendar->{"calendar_title_".$language->short} }}' id='meta_title' name='calendar_title_{{ $language->short }}[]'/>
                                                            </div>
                                                        @endforeach
                                                        <table class="calendar_table calendar_table_{{$table_key}}">
                                                            <tr>
                                                                <th class="jamer_td">Ժամերը</th>
                                                                <th>Երկ.</th>
                                                                <th>Երեք.</th>
                                                                <th>Չորեք.</th>
                                                                <th>Հինգ.</th>
                                                                <th>Ուրբ.</th>
                                                                <th>Շաբ.</th>
                                                                <th>Կիր.</th>
                                                                <th class="delete_calendar_tr_td"></th>
                                                            </tr>
                                                            @foreach($page->calendar_times[$table_key] as $key => $calendar_times)

                                                                <tr>
                                                                    <td class="jamer_td"><input type="text" name="jamer_{{ $table_key }}[]" value="{{$calendar_times}}" class="form-control"></td>
                                                                    <td data-day="erku" data-index="{{ $key }}" data-table="{{ $table_key }}" class="edit_column_info add_column_info_erku_{{ $key }}_{{ $table_key }}">
                                                                        <small class="calendar_small erku_caochcat_info_{{ $key }}_{{ $table_key }}">{{ app('App\Http\Controllers\Admin\PagesController')->getCoachCat($page->erku_coach_cats[$table_key][$key],"am")  }}</small>
                                                                        <small class="calendar_small erku_caoch_info_{{ $key }}_{{ $table_key }}">{{ app('App\Http\Controllers\Admin\PagesController')->getCoach($page->erku_coaches[$table_key][$key],"am") }}</small>
                                                                        <input type="hidden" name="erku_caochcat_{{ $table_key }}[]" class="erku_caochcat_{{ $key }}_{{ $table_key }}" value="{{ $page->erku_coach_cats[$table_key][$key] }}">
                                                                        <input type="hidden" name="erku_caoch_{{ $table_key }}[]" class="erku_caoch_{{ $key }}_{{ $table_key }}" value="{{ $page->erku_coaches[$table_key][$key] }}">
                                                                    </td>
                                                                    <td data-day="ereq" data-index="{{ $key }}" data-table="{{ $table_key }}" class="edit_column_info add_column_info_ereq_{{ $key }}_{{ $table_key }}">

                                                                        <small class="calendar_small ereq_caochcat_info_{{ $key }}_{{ $table_key }}">{{ app('App\Http\Controllers\Admin\PagesController')->getCoachCat($page->ereq_coach_cats[$table_key][$key],"am")  }}</small>
                                                                        <small class="calendar_small ereq_caoch_info_{{ $key }}_{{ $table_key }}">{{ app('App\Http\Controllers\Admin\PagesController')->getCoach($page->ereq_coaches[$table_key][$key],"am") }}</small>
                                                                        <input type="hidden" name="ereq_caochcat_{{ $table_key }}[]" class="ereq_caochcat_{{ $key }}_{{ $table_key }}" value="{{ $page->ereq_coach_cats[$table_key][$key] }}">
                                                                        <input type="hidden" name="ereq_caoch_{{ $table_key }}[]" class="ereq_caoch_{{ $key }}_{{ $table_key }}" value="{{ $page->ereq_coaches[$table_key][$key] }}">
                                                                    </td>
                                                                    <td data-day="choreq" data-index="{{ $key }}" data-table="{{ $table_key }}" class="edit_column_info add_column_info_choreq_{{ $key }}_{{ $table_key }}">
                                                                        <small class="calendar_small choreq_caochcat_info_{{ $key }}_{{ $table_key }}">{{ app('App\Http\Controllers\Admin\PagesController')->getCoachCat($page->choreq_coach_cats[$table_key][$key],"am")  }}</small>
                                                                        <small class="calendar_small choreq_caoch_info_{{ $key }}_{{ $table_key }}">{{ app('App\Http\Controllers\Admin\PagesController')->getCoach($page->choreq_coaches[$table_key][$key],"am") }}</small>
                                                                        <input type="hidden" name="choreq_caochcat_{{ $table_key }}[]" class="choreq_caochcat_{{ $key }}_{{ $table_key }}" value="{{ $page->choreq_coach_cats[$table_key][$key] }}">
                                                                        <input type="hidden" name="choreq_caoch_{{ $table_key }}[]" class="choreq_caoch_{{ $key }}_{{ $table_key }}" value="{{ $page->choreq_coaches[$table_key][$key] }}">
                                                                    </td>
                                                                    <td  data-day="hing" data-index="{{ $key }}" data-table="{{ $table_key }}" class="edit_column_info add_column_info_hing_{{ $key }}_{{ $table_key }}">
                                                                        <small class="calendar_small hing_caochcat_info_{{ $key }}_{{ $table_key }}">{{ app('App\Http\Controllers\Admin\PagesController')->getCoachCat($page->hing_coach_cats[$table_key][$key],"am")  }}</small>
                                                                        <small class="calendar_small hing_caoch_info_{{ $key }}_{{ $table_key }}">{{ app('App\Http\Controllers\Admin\PagesController')->getCoach($page->hing_coaches[$table_key][$key],"am") }}</small>
                                                                        <input type="hidden" name="hing_caochcat_{{ $table_key }}[]" class="hing_caochcat_{{ $key }}_{{ $table_key }}" value="{{ $page->hing_coach_cats[$table_key][$key] }}">
                                                                        <input type="hidden" name="hing_caoch_{{ $table_key }}[]" class="hing_caoch_{{ $key }}_{{ $table_key }}" value="{{ $page->hing_coaches[$table_key][$key] }}">

                                                                    </td>
                                                                    <td data-day="urbat" data-index="{{ $key }}" data-table="{{ $table_key }}" class="edit_column_info add_column_info_urbat_{{ $key }}_{{ $table_key }}">
                                                                        <small class="calendar_small urbat_caochcat_info_{{ $key }}_{{ $table_key }}">{{ app('App\Http\Controllers\Admin\PagesController')->getCoachCat($page->urbat_coach_cats[$table_key][$key],"am")  }}</small>
                                                                        <small class="calendar_small urbat_caoch_info_{{ $key }}_{{ $table_key }}">{{ app('App\Http\Controllers\Admin\PagesController')->getCoach($page->urbat_coaches[$table_key][$key],"am") }}</small>
                                                                        <input type="hidden" name="urbat_caochcat_{{ $table_key }}[]" class="urbat_caochcat_{{ $key }}_{{ $table_key }}" value="{{ $page->urbat_coach_cats[$table_key][$key] }}">
                                                                        <input type="hidden" name="urbat_caoch_{{ $table_key }}[]" class="urbat_caoch_{{ $key }}_{{ $table_key }}" value="{{ $page->urbat_coaches[$table_key][$key] }}">
                                                                    </td>
                                                                    <td  data-day="shabat" data-index="{{ $key }}" data-table="{{ $table_key }}" class="edit_column_info add_column_info_shabat_{{ $key }}_{{ $table_key }}">
                                                                        <small class="calendar_small shabat_caochcat_info_{{ $key }}_{{ $table_key }}">{{ app('App\Http\Controllers\Admin\PagesController')->getCoachCat($page->shabat_coach_cats[$table_key][$key],"am")  }}</small>
                                                                        <small class="calendar_small shabat_caoch_info_{{ $key }}_{{ $table_key }}">{{ app('App\Http\Controllers\Admin\PagesController')->getCoach($page->shabat_coaches[$table_key][$key],"am") }}</small>
                                                                        <input type="hidden" name="shabat_caochcat_{{ $table_key }}[]" class="shabat_caochcat_{{ $key }}_{{ $table_key }}" value="{{ $page->shabat_coach_cats[$table_key][$key] }}">
                                                                        <input type="hidden" name="shabat_caoch_{{ $table_key }}[]" class="shabat_caoch_{{ $key }}_{{ $table_key }}" value="{{ $page->shabat_coaches[$table_key][$key] }}">
                                                                    </td>
                                                                    <td  data-day="kiraki" data-index="{{ $key }}" data-table="{{ $table_key }}" class="edit_column_info add_column_info_kiraki_{{ $key }}_{{ $table_key }}">
                                                                        <small class="calendar_small kiraki_caochcat_info_{{ $key }}_{{ $table_key }}">{{ app('App\Http\Controllers\Admin\PagesController')->getCoachCat($page->kiraki_coach_cats[$table_key][$key],"am")  }}</small>
                                                                        <small class="calendar_small kiraki_caoch_info_{{ $key }}_{{ $table_key }}">{{ app('App\Http\Controllers\Admin\PagesController')->getCoach($page->kiraki_coaches[$table_key][$key],"am") }}</small>
                                                                        <input type="hidden" name="kiraki_caochcat_{{ $table_key }}[]" class="kiraki_caochcat_{{ $key }}_{{ $table_key }}" value="{{ $page->kiraki_coach_cats[$table_key][$key] }}">
                                                                        <input type="hidden" name="kiraki_caoch_{{ $table_key }}[]" class="kiraki_caoch_{{ $key }}_{{ $table_key }}" value="{{ $page->kiraki_coaches[$table_key][$key] }}">
                                                                    </td>
                                                                    <td class="delete_calendar_tr_td"><a class="delete_calendar_tr"><i class="fa fa-times" aria-hidden="true"></i></a></td>
                                                                </tr>
                                                            @endforeach
                                                        </table>
                                                        <div data-rowcount="{{ count($page->calendar_times[$table_key]) }}" data-table="{{ $table_key }}" class="add_calendar_row add_calendar_row_{{$table_key}}" colspan="7">Ավելացնել</div>

                                                        @endif
                                                    @endforeach

                                                @endif

                                                <input type="hidden" name="calendar_table_counts" value="{{ count($page_calendars) }}" class="calendar_table_counts">
                                            </div>
                                        @endif


                                    <div class="form-group meta_block">
                                        <div class="open_close_meta">
                                            <p>Meta <i class="icon-arrow-combo"></i></p>
                                        </div>
                                        <div class="meta_box">
                                            <div class="col-md-6 col-sm-6 pull-left">
                                                @foreach($languages as $language)
                                                    <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                                        <label>{{ $language->title }} Meta Title:</label>
                                                        <input type="text" placeholder='Meta Title' class="form-control"
                                                               value='{{ $page->{"meta_title_".$language->short} }}' id='meta_title' name='meta_title_{{ $language->short }}'/>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <div class="col-md-6 col-sm-6 pull-left">
                                                @foreach($languages as $language)
                                                    <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                                        <label>{{ $language->title }} Meta Keyword:</label>
                                                        <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_{{ $language->short }}'
                                                               class="form-control"
                                                               value="{{ $page->{"meta_key_".$language->short} }}"/>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <div class="col-md-12 col-sm-12 pull-left">
                                                @foreach($languages as $language)
                                                    <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                                        <label>{{ $language->title }} Meta Description</label>
                                                        <textarea id="meta_desc" class="form-control" name="meta_desc_{{ $language->short }}">{{ $page->{"meta_desc_".$language->short} }}</textarea>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>

                                    </div>
                                    <input type="hidden" name="edit_page" value="ok">
                                    <button type="submit" class="btn_1 save_btn green pull-right">
                                        Save
                                    </button>
                                </div>
                            @if($page->id != 21)
                                <div class="col-md-3 page_right">
                                    @if(isset($page->parent) && $page->parent == 0)
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <h4> Additional</h4>
                                            </div>
                                            <div class="additional_block col-md-12">

                                                <input @if($page->sub_pages == 1) {{ "checked" }}@endif type="checkbox" name="sub_pages" value="1">
                                                <label><i class="icon-docs"></i> Sub pages</label>
                                            </div>

                                            <div class="additional_block col-md-12">
                                                <input @if($page->sub_pages_with_gallery == 1) {{ "checked" }}@endif type="checkbox" name="sub_pages" value="1">
                                                <label><i class="icon-docs"></i> Sub pages with gallery</label>
                                            </div>

                                            <div class="additional_block col-md-12">
                                                <input type="checkbox" @if($page->config_project == 1) {{ "checked" }}@endif name="config_project" value="1">
                                                <label><i class="icon-docs"></i> Sub Project pages</label>
                                            </div>

                                            <div class="additional_block col-md-12">

                                                <input @if($page->config_data1 == 1) {{ "checked" }}@endif type="checkbox" name="config_data1" value="1">
                                                <label><i class="icon-calendar"></i> Data 1</label>
                                            </div>
                                            <div class="additional_block col-md-12">

                                                <input @if($page->config_data2 == 1) {{ "checked" }}@endif type="checkbox" name="config_data2" value="1">
                                                <label> <i class="icon-calendar"></i> Data 2</label>
                                            </div>
                                            <div class="additional_block col-md-12">

                                                <input @if($page->config_cat == 1) {{ "checked" }}@endif type="checkbox" name="config_cat" value="1">
                                                <label><i class="icon-th-list-3"></i> Categories</label>
                                            </div>
                                            <div class="additional_block col-md-12">

                                                <input @if($page->config_tag == 1) {{ "checked" }}@endif type="checkbox" name="config_tag" value="1">
                                                <label><i class="icon-tags"></i> Tags</label>
                                            </div>
                                            <div class="additional_block col-md-12">
                                                <input @if($page->config_image == 1) {{ "checked" }}@endif type="checkbox" name="config_image" value="1">
                                                <label><i class="icon-camera"></i> Image</label>
                                            </div>
                                            <div class="additional_block col-md-12">
                                                <input @if($page->config_bg_image == 1) {{ "checked" }}@endif type="checkbox" name="config_bg_image" value="1">
                                                <label><i class="icon-camera"></i>Background image</label>
                                            </div>
                                            <div class="additional_block col-md-12">
                                                <input @if($page->config_logo == 1) {{ "checked" }}@endif type="checkbox" name="config_logo" value="1">
                                                <label><i class="icon-camera"></i>Logo</label>
                                            </div>


                                        </div>

                                    </div>
                                    @else
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <h4> Additional</h4>
                                                </div>
                                              {{--  <div class="additional_block col-md-12">
                                                    <input @if($page->pdf_uploder == 1) {{ "checked" }}@endif type="checkbox" name="pdf_uploder" value="1">
                                                    <label><i class="icon-doc-add"></i> Pdf uploader</label>
                                                </div>--}}
                                                <div class="additional_block col-md-12">
                                                    <input @if($page->page_with_pdf == 1) {{ "checked" }}@endif type="checkbox" name="page_with_pdf" value="1">
                                                    <label><i class="icon-folder-empty"></i> Sub page</label>
                                                </div>
                                              {{--  <div class="additional_block col-md-12">
                                                    <input @if($page->calendar_config == 1) {{ "checked" }}@endif type="checkbox" name="calendar_config" value="1">
                                                    <label><i class="icon-folder-empty"></i> Calendar</label>
                                                </div>--}}
                                                <div class="additional_block col-md-12">

                                                    <input @if($page->gallery == 1) {{ "checked" }}@endif type="checkbox" name="gallery" value="1">
                                                    <label><i class="icon-picture-1"></i> Gallery</label>
                                                </div>
                                            </div>

                                        </div>
                                    @endif
                                </div>
                            @endif



                        </form>
                    </div>
            </div>


    </div>

    <div class="choose_calendar_field">
        <a class="close_choose_calendar_field"><i class="fa fa-times" aria-hidden="true"></i></a>
        <div class="choose_calendar_field_box">
            <label>Ընտրել տեսակը</label>
            <select name="shabat_caochcat[]" class="form-control choose_caochcat_pop">
                <option value="0">-Կատեգորիա-</option>
                @foreach($coachcategories as $coachcategory)
                    <option data-cocat="{{ $coachcategory->title_am }}" value="{{ $coachcategory->id }}">{{ $coachcategory->title_am }}</option>
                @endforeach
            </select>
        </div>
        <div class="choose_calendar_field_box">
            <label>Ընտրել մարզիչը</label>
            <select name="shabat_caoch" class="form-control choose_caoch_pop">
                <option value="0">-Մարզիչ-</option>
                @foreach($coaches as $coach)
                    <option data-coname="{{ $coach->title_am }}" value="{{ $coach->id }}">{{ $coach->title_am }}</option>
                @endforeach
            </select>
        </div>
        <div class="choose_calendar_field_box">
            <a data-day="" data-index="" data-table="" class="save_calendar_field">Պահպանել</a>
        </div>


    </div>
@endsection
