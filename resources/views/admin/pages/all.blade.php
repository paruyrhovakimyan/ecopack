@extends('admin.layouts.page')
<link href="{{ asset('public/css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('public/css/admin/jquery.switch.css') }}" rel="stylesheet">
@section('content')

    <div class="container services_block">
        <div class="col-md-8 page_show_content">
            <div class="page_title_box parent_page_title">
                <h4>Pages</h4>
                <div class="page_actions_block">
                    <a href="{{ url('/') }}/admin/pages/create" class="sub_page_add btn_2">
                        <i class="icon-docs"></i>
                        Add
                    </a>
                </div>
            </div>
                <div class="child_pages">
                    @foreach($pages as $page)
                        <div class="page_title_box child_page_title">
                            <p>{{ $page->title }}</p>
                            <div class="page_actions_block">
                                <a href="{{ url('/') }}/admin/pages/{{ $page->id }}/edit" class="parent_page_edit btn_2">
                                    <i class="icon-edit-3"></i>
                                    Edit
                                </a>
                                <a data-id="{{ $page->id }}" class="page_del_btn page_delete btn_delete btn-danger">
                                    <i class="icon-trash"></i>
                                    Delete
                                </a>
                            </div>

                        </div>
                    @endforeach
                </div>
            </div>
        </div>




    </div>
@endsection
