@extends('admin.layouts.page')
<link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/jquery.switch.css') }}" rel="stylesheet">
@section('content')

    <div class="container">
        <div class="form_box">
            <div class="page_title_box ">
                <h4 class="eng_block"><a href="{{ url('/') }}/admin/slider/1/edit">TOP BANNER</a> | Add Slide</h4>
            </div>

            @if( count($languages) > 1)
                <ul class="languages_tabs">
                    @foreach($languages as $language)
                        <li data-tab="{{ $language->short }}"
                            class="lang_tab {{ $language->short."_tab" }} @if($language->first == 1) {{"active_tab"}} @endif">
                            <p><span>{{ $language->title }}</span></p>
                        </li>
                    @endforeach
                </ul>
            @endif
            <div class="about_form">

                <form class="form-horizontal" method="POST"
                      action="{{ url('/') }}/admin/slider/{{ $slider_id }}" enctype='multipart/form-data'>
                    {{method_field('PUT')}}
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12 col-sm-12 pull-left add_slide_box">

                            <div class="add_slide_left">
                                <div class="add_slide_left_content">
                                    @foreach($languages as $language)
                                        <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                            <label for="title">{{ $language->title }} Title</label>
                                            <input id="about_title" type="text" class="form-control" name="slide_title_{{ $language->short }}" value=""
                                                   autofocus>
                                        </div>
                                        <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                            <label for="title">{{ $language->title }} Slide sub title <span class="slider_sub_title_length_{{$language->short}}">(300 chars)</span></label>
                                            <textarea data-lang="{{ $language->short }}" id="about_title" type="text" class="check_chars form-control" name="slide_sub_title_{{ $language->short }}"></textarea>

                                        </div>

                                    @endforeach
                                        <div class="form-group slide_link_box">
                                            <label for="title">Slide link</label>
                                            <input id="about_title" type="text" class="form-control" name="slide_link"
                                                   value="" autofocus>
                                        </div>
                                        <div class="form-group slide_link_box">
                                            <label for="title">Slide Contact Phone</label>
                                            <input id="about_title" type="text" class="form-control" name="slide_contact"
                                                   value="" autofocus>
                                        </div>
                                    <div class="form-group  slide_new_window">
                                        <label class="slide_new_window_title pull-left"
                                               style="margin-right: 15px;margin-top: 5px" for="title">New
                                            window</label>
                                        <label class="switch-light switch-ios pull-left">
                                            <input type="checkbox" name="new_blank" id="option_1" value="1">
                                            <span>
							                        <span>No</span>
													<span>Yes</span>
                                            </span>
                                            <a></a>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label for="service_order">Order</label>
                                        <input id="service_order" type="text" class="form-control" name="item_order"
                                               value="1">
                                    </div>
                                </div>
                            </div>

                            <div class="add_slide_right pull-right">
                                <div class="slide_effects_title">
                                    Image Gallery Custom Options
                                </div>
                                <div class="slide_effects_content">
                                    <div class="form-group">
                                        <label for="service_order">Slide Effect</label>
                                        <select class="slide_effect" name="slide_effect">
                                            <option value="fade">Fade</option>
                                            <option value="rendom">Random</option>
                                            <option value="zoomin">Zoom in</option>
                                            <option value="zoomout">Zoom out</option>
                                            <option value="fadetoleftfadefromright">Left to Right</option>
                                            <option value="fadetorightfadefromleft">Right to Left</option>
                                            <option value="fadetotopfadefrombottom">Top to Bottom</option>
                                            <option value="fadetobottomfadefromtop">Bottom to Top</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Change speed</label>
                                        <input type="text" class="change_speed" name="change_speed" value="2000">
                                    </div>
                                    <div class="form-group">
                                        <label>Title Effect</label>
                                        <select class="title_effect" name="title_effect">
                                            <option value="1">From left</option>
                                            <option value="2">From Right</option>
                                            <option value="3">From Bottom</option>
                                            <option value="4">From Top</option>
                                            <option value="5">Paralax</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Title paralax</label>
                                        <label class="switch-light switch-ios pull-left">
                                            <input type="checkbox" name="title_paralax" id="option_1" value="1">
                                            <span>
							                        <span>No</span>
													<span>Yes</span>
                                            </span>
                                            <a></a>
                                        </label>
                                    </div>
                                </div>


                                <input type="hidden" name="edit_slider" value="ok">

                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 pull-left">
                            <div class="add_slide_left_img slid_icon_image">
                                <label>Slider Icon (width:74px, heght:74px)</label>
                                <div class="form-group">
                                    <div class="avatar_img">
                                        <div class="file-upload btn btn_1 green">
                                            <span>Choose Image</span>
                                            <input type="file" name="image_logo" id="uploadBtnLogo" class="image upload" onchange="readURLLogo(this)">
                                        </div>
                                    </div>


                                </div>
                                <div class="profile-image-preview_logo ">
                                    <div class="ct-media--left">
                                        <a>
                                            <img id="uploadPreviewLogo" src="">
                                        </a>
                                    </div>
                                    <div class="ct-media--content">
                                        <span></span>
                                        <a class="cross" onclick="deleteimgLogo();" style="cursor:pointer;">
                                            <i class="fa fa-trash-o"></i> Delete
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 pull-left">

                            <div class="add_slide_left_img">
                                <label>Image (width:1600px, heght:800px)</label>
                                <div class="form-group">

                                    <div class="avatar_img">
                                        <div class="file-upload btn btn_1 green">
                                            <span>Choose Image</span>
                                            <input type="file" name="image" id="uploadBtn1" class="image upload"
                                                   onchange="readURL(this)">
                                        </div>
                                    </div>
                                </div>
                                <div class="profile-image-preview">
                                    <div class="ct-media--left">
                                        <a>
                                            <img id="uploadPreview" src="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="save_slider">
                        <button type="submit" class="btn_1 green slide_save_btn">
                            Save
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
