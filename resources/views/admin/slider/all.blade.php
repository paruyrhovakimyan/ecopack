@extends('admin.layouts.page')
<link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/jquery.switch.css') }}" rel="stylesheet">
@section('content')

    <div class="container services_block">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Sliders</h3>
                        <a class="btn_1 add_slider_button green pull-right">Add Slider</a>
                        <div class="add_silder hidden">
                            <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/slider" enctype='multipart/form-data'>
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input id="about_title" type="text" class="form-control" name="title" value="" required
                                           autofocus>
                                    <button type="submit" class="btn_1 green pull-right">
                                        Save
                                    </button>
                                </div>
                                <input type="hidden" name="add_slider" value="ok">

                            </form>
                        </div>
                    </div>
                    <div class="about_form">
                        <form class="form-horizontal" method="POST" action="http://foc.loc/admin/about_us">
                            <input type="hidden" name="_token" value="U1AwYoBVfuvUYPcIFGJM9WGm3t1o6xR1K7vLExuD">
                        </form>
                        <section id="section-1" class="content-current">
                            @foreach($sliders as $slider)
                                <div class="strip_booking strip_project{{ $slider->id }}">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <h3 class="">{{ $slider->title }}</h3>
                                        </div>
                                        <div class="col-md-2 col-sm-3">
                                            <ul class="info_booking">
                                                <li><strong>Slider id</strong> {{ $slider->id }}</li>
                                                <li><strong>Slides count</strong> {{ $slider->slides_count }}</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <div class="booking_buttons">
                                                <a href="{{ url('/') }}/admin/slider/{{ $slider->id }}/edit" class="btn_2">Edit</a>
                                                <a data-id="{{ $slider->id }}" class="company_delete btn_delete btn-danger">Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End row -->
                                </div>
                            @endforeach
                        </section>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
