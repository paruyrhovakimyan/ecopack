@extends('admin.layouts.page')
<link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/jquery.switch.css') }}" rel="stylesheet">
@if(isset($data['edit_successful']))
    <div class="edit_successful">
        <p>The Service updated</p>
    </div>
@endif

@section('content')

    <div class="container">
        <div class="row form_box">
            <div class="col-md-8 page_show_content panel-default slider_form_block">
                <div class="page_title_box ">
                    <h4 class="eng_block">Slides</h4>
                    <div class="page_actions_block">
                        <a href="{{ url('/') }}/admin/slide/create/{{ $slider->id }}" class="sub_page_add btn_2">
                            <i class="icon-docs"></i>
                            Add
                        </a>
                    </div>
                </div>
                    <div class="">
                        <form class="checked_form" action="{{ url('/') }}/admin/checked_delete" method="post">
                            {{ csrf_field() }}
                            <div class="select_all_block">
                                <input type="checkbox" class="checked_all" name="checked_all" value="1">
                                <label>Check all</label>
                                <button class="checked_delete" type="button" disabled><i class="icon-trash"></i></button>
                            </div>

                        <section id="section-1" class="content-current">
                            @foreach($slides as $slide)
                                <div class="child_page_title strip_booking strip_project{{ $slide->id }}">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2">
                                            <div class="check_one">
                                                <input type="checkbox" class="checked_one_input" name="checked_one_input[]" value="{{ $slide->id }}">
                                                <input type="checkbox" class="hidden check_type" name="check_type[]" value="slide" >
                                            </div>
                                            <div class="page_hidden">
                                                @if($slide->hidden == 0)
                                                    <a alt="Show" title="Show" class="visible" data-hidden="1" data-id="{{ $slide->id }}" data-type="slide">
                                                        <i class="icon-eye"></i>
                                                    </a>

                                                @else
                                                    <a alt="Hide" title="Hide" class="visible" data-hidden="0" data-id="{{ $slide->id }}" data-type="slide">
                                                        <i class="icon-eye-off"></i>
                                                    </a>
                                                @endif
                                            </div>

                                        </div>
                                        <div class="slider_image_box col-md-4 col-sm-4">
                                            <img src="{{ url('/') }}/uploads/slider/150/{{ $slide->{"image"} }}">
                                        </div>
                                        <div class="col-md-6 col-sm-6 ">
                                            <p class="">{{ $slide->{"slide_title_".$default_lang} }}</p>
                                            <div class="page_actions_block">
                                                <a href="{{url('/') }}/admin/slide/edit/{{ $slide->id }}" class="slide_edit btn_2">
                                                    <i class="icon-edit-3"></i>
                                                    Edit
                                                </a>
                                                <a data-id="{{ $slide->id }}" class="slide_delete btn_delete btn-danger">
                                                    <i class="icon-trash"></i>
                                                    Delete
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End row -->
                                </div>
                            @endforeach
                        </section>
                        </form>
                    </div>
            </div>



        </div>
    </div>
@endsection
