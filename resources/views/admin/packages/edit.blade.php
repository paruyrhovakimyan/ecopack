@extends('admin.layouts.page')

@if(isset($data['edit_successful']))
    <div class="edit_successful">
        <p>The Service updated</p>
    </div>
@endif

@section('content')
    <div class="container">
        <div class="form_box">
        <div class="page_title_box">
            <h4><a href="https://printel.mycard.am/admin/package">Packages</a> | {{ $package->{"title_".$default_lang} }}</h4>
        </div>
        @if( count($languages) > 1)
            <ul class="languages_tabs">
                @foreach($languages as $language)
                    <li data-tab="{{ $language->short }}" class="lang_tab {{ $language->short."_tab" }} @if($language->first == 1) {{"active_tab"}} @endif">
                        <p><span>{{ $language->title }}</span></p>
                    </li>
                @endforeach
            </ul>
        @endif

        <div class="about_form">
            <div class="col-md-8">

                <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/package/{{ $package->id }}" enctype='multipart/form-data'>
                    {{method_field('PUT')}}
                    {{ csrf_field() }}
                    @foreach($languages as $language)
                        <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                            <label for="title">{{ $language->title }} Title</label>
                            <input id="about_title" type="text" class="form-control" name="title_{{ $language->short }}" value="{{ $package->{"title_".$language->short} }}"
                                   autofocus>
                        </div>

                        <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                            <label for="Description">{{ $language->title }} Short Description</label>
                            <textarea class="form-control description short_desc" name="short_desc_{{ $language->short }}">{{ $package->{"short_desc_".$language->short} }}</textarea>
                        </div>
                    @endforeach


                    <div class="form-group order_block">
                        <label for="service_order">Order</label>
                        <input id="service_order" type="text" class="form-control" name="package_order" value="{{ $package->package_order }}">
                    </div>

                    <div class="row page_image">
                        <div class="company_logo">
                            <div class="">
                                <label for="Description">Image (width: 346px, heght:224px)</label>
                            </div>
                            <div class="avatar_img">
                                <div class="file-upload btn btn_1 green">
                                    <span>Choose image</span>
                                    <input type="file" name="image" id="uploadBtnLogo" class="image upload"
                                           onchange="readURLLogo(this)">
                                </div>
                            </div>
                            <div class="profile-image-preview_logo @if($package->image !='') {{ "active" }} @endif">
                                <div class="ct-media--left">
                                    <a>
                                        <img id="uploadPreviewLogo" src="{{ url('/') }}/uploads/news/{{ $package->image }}">
                                    </a>
                                </div>
                                <input type="hidden" name="old_image" class="old_image_logo" value="{{ $package->image }}">
                                <div class="ct-media--content">
                                    <span></span>
                                    <a class="cross" onclick="deleteimgLogo();" style="cursor:pointer;">
                                        <i class="fa fa-trash-o"></i> Delete
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Add service</label>
                        <div class="news_sub_page_pdf_block">
                            <div class="btn package_service_add">
                                <i class="icon-plus"></i>
                            </div>
                            <div class="page_pdf_plus_block">

                                @foreach($languages as $language)

                                    @if($package->{"package_desc_$language->short"} !='')
                                        @foreach(json_decode($package->{"package_desc_$language->short"}) as $key => $package_desc_am)
                                            <div class="form-group old_package_desc old_package_desc{{$key}}  lang_field lang_field_{{$language->short }} @if($language->first == 1)  {{"active_field" }} @else {{"hidden_field"}}@endif">
                                                <label for="title">{{$language->title}}  Title</label>
                                                <input id="about_title" type="text" class="form-control" name="package_desc_{{$language->short}}[]" value="{{ $package_desc_am }}"
                                                       autofocus>
                                                <div data-index = {{ $key }} class="remove_package">X</div>
                                            </div>
                                        @endforeach

                                    @endif

                                @endforeach

                            </div>
                        </div>

                    </div>



                    <input type="hidden" name="edit_package" value="ok">
                    <button type="submit" class="btn_1 green pull-right">
                        Save
                    </button>

                </form>
            </div>
        </div>
        </div>
    </div>
@endsection
