@extends('admin.layouts.page')

@section('content')

    <div class="container">
        <div class="form_box">
            <div class="page_title_box">
                <h4><a href="https://printel.mycard.am/admin/partners">Partners</a> | Add Partners</h4>
            </div>
            @if( count($languages) > 1)
                <ul class="languages_tabs">
                    @foreach($languages as $language)
                        <li data-tab="{{ $language->short }}" class="lang_tab {{ $language->short."_tab" }} @if($language->first == 1) {{"active_tab"}} @endif">
                            <p><span>{{ $language->title }}</span></p>
                        </li>
                    @endforeach
                </ul>
            @endif
            <div class="about_form">
                <div class="col-md-8">

                    <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/package" enctype='multipart/form-data'>

                        {{ csrf_field() }}

                        @foreach($languages as $language)
                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="title">{{ $language->title }} Title</label>
                                <input id="about_title" type="text" class="form-control" name="title_{{ $language->short }}" value=""
                                       autofocus>
                            </div>

                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="Description">{{ $language->title }} Short Description</label>
                                <textarea class="form-control description short_desc" name="short_desc_{{ $language->short }}"></textarea>
                            </div>
                        @endforeach
                        <div class="row page_image">
                            <div class="company_logo">
                                <div class="">
                                    <label for="Description">Image (width: 346px, heght:224px)</label>
                                </div>
                                <div class="avatar_img">
                                    <div class="file-upload btn btn_1 green">
                                        <span>Choose image</span>
                                        <input type="file" name="image" id="uploadBtnLogo" class="image upload"
                                               onchange="readURLLogo(this)">
                                    </div>
                                </div>
                                <div class="profile-image-preview_logo ">
                                    <div class="ct-media--left">
                                        <a>
                                            <img id="uploadPreviewLogo" src="">
                                        </a>
                                    </div>
                                    <input type="hidden" name="old_image" class="old_image_logo" value="">
                                    <div class="ct-media--content">
                                        <span></span>
                                        <a class="cross" onclick="deleteimgLogo();" style="cursor:pointer;">
                                            <i class="fa fa-trash-o"></i> Delete
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group order_block">
                            <label for="service_order">Order</label>
                            <input id="service_order" type="text" class="form-control" name="package_order" value="1">
                        </div>
                        <div class="form-group">
                            <label>Add service</label>
                            <div class="news_sub_page_pdf_block">
                                <div class="btn package_service_add">
                                    <i class="icon-plus"></i>
                                </div>
                                <div class="page_pdf_plus_block">

                                </div>
                            </div>

                        </div>

                        <input type="hidden" name="add_package" value="ok">
                        <button type="submit" class="btn_1 green pull-right">
                            Save
                        </button>

                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
