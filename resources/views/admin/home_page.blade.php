@extends('admin.layouts.page')

@section('content')
    <div class="container">
        <div class="form_box">
            <div class="page_title_box">
                <h4>HOME META HEADER </h4>
            </div>
            @if( count($languages) > 1)
                <ul class="languages_tabs">
                    @foreach($languages as $language)
                        <li data-tab="{{ $language->short }}"
                            class="lang_tab {{ $language->short."_tab" }} @if($language->first == 1) {{"active_tab"}} @endif">
                            <p><span>{{ $language->title }}</span></p>
                        </li>
                    @endforeach
                </ul>
            @endif
            <div class="about_form">
                <form action="{{ url('/') }}/admin/homepage" method="post" enctype='multipart/form-data'>
                    {{ csrf_field() }}
                    <div class="col-md-8">

                        <div class="col-md-12 col-sm-12 pull-left">
                            <div class="image_text form-group">
                                <label>Facebook</label>
                                <input type="text" name="fb_link" class="form-control " value="{{ $home_settings->fb_link }}">
                            </div>
                            <div class="image_text form-group">
                                <label>Youtube</label>
                                <input type="text" name="youtube_link" class="form-control " value="{{ $home_settings->youtube_link }}">
                            </div>
                            <div class="image_text form-group">
                                <label>Twitter</label>
                                <input type="text" name="tw_link" class="form-control " value="{{ $home_settings->tw_link }}">
                            </div>
                            <div class="image_text form-group">
                                <label>G+</label>
                                <input type="text" name="gplus" class="form-control " value="{{ $home_settings->gplus }}">
                            </div>
                            <div class="image_text form-group">
                                <label>Instagram</label>
                                <input type="text" name="instagram" class="form-control " value="{{ $home_settings->instagram }}">
                            </div>
                            <div class="image_text form-group">
                                <label>Linkedin</label>
                                <input type="text" name="linkedin" class="form-control " value="{{ $home_settings->linkedin }}">
                            </div>
                        </div>

                        @foreach($languages as $language)
                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="title">{{ $language->title }} About us Title</label>
                                <input id="about_title" type="text" class="form-control" name="about_us_title_{{ $language->short }}" value="{{ $home_settings->{"about_us_title_".$language->short} }}"
                                       autofocus>
                            </div>

                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="Description">{{ $language->title }} About us Description</label>
                                <textarea class="form-control description short_desc" name="about_us_desc_{{ $language->short }}">{{ $home_settings->{"about_us_desc_".$language->short} }}</textarea>
                            </div>

                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="block_2_title">{{ $language->title }} Block 2 Title</label>
                                <input id="block_2_title" type="text" class="form-control" name="block_2_title_{{ $language->short }}" value="{{ $home_settings->{"block_2_title_".$language->short} }}"
                                       autofocus>
                            </div>
                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="Block 2">{{ $language->title }} Block 2 Description</label>
                                <textarea class="form-control description short_desc" name="block_2_desc_{{ $language->short }}">{{ $home_settings->{"block_2_desc_".$language->short} }}</textarea>
                            </div>

                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="block_3_title">{{ $language->title }} Block 3 Title</label>
                                <input id="block_3_title" type="text" class="form-control" name="block_3_title_{{ $language->short }}" value="{{ $home_settings->{"block_3_title_".$language->short} }}"
                                       autofocus>
                            </div>
                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="Block 3">{{ $language->title }} Block 3 Description</label>
                                <textarea class="form-control description short_desc" name="block_3_desc_{{ $language->short }}">{{ $home_settings->{"block_3_desc_".$language->short} }}</textarea>
                            </div>

                        @endforeach
                        <div class="row page_image">
                            <div class="company_logo">
                                <div class="">
                                    <label for="Description">Image (width: 1140px, heght:570px)</label>
                                </div>
                                <div class="avatar_img">
                                    <div class="file-upload btn btn_1 green">
                                        <span>Choose image</span>
                                        <input type="file" name="image" id="uploadBtnLogo" class="image upload"
                                               onchange="readURLLogo(this)">
                                    </div>
                                </div>
                                <div class="profile-image-preview_logo  @if($home_settings->image !='') {{ "active" }} @endif ">
                                    <div class="ct-media--left">
                                        <a>
                                            <img id="uploadPreviewLogo" src="{{ url('/') }}/uploads/home_page/{{ $home_settings->image }}">
                                        </a>
                                    </div>
                                    <input type="hidden" name="old_image" class="old_image_logo" value="{{ $home_settings->image }}">
                                    <div class="ct-media--content">
                                        <span></span>
                                        <a class="cross" onclick="deleteimgeditLogo();" style="cursor:pointer;">
                                            <i class="fa fa-trash-o"></i> Delete
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row page_image">
                            <div class="company_logo">
                                <div class="">
                                    <label for="Description">Block 2 image (width: 1140px, heght:570px)</label>
                                </div>
                                <div class="avatar_img">
                                    <div class="file-upload btn btn_1 green">
                                        <span>Choose image</span>
                                        <input type="file" name="block_2_img" id="uploadBtnLogo" class="image_logo2 upload"
                                               onchange="readURLLogo2(this)">
                                    </div>
                                </div>
                                <div class="profile-image-preview_logo2  @if($home_settings->block_2_img !='') {{ "active" }} @endif ">
                                    <div class="ct-media--left">
                                        <a>
                                            <img id="uploadPreviewLogo2" src="{{ url('/') }}/uploads/home_page/{{ $home_settings->block_2_img }}">
                                        </a>
                                    </div>
                                    <input type="hidden" name="old_image2" class="old_image_logo2" value="{{ $home_settings->block_2_img }}">
                                    <div class="ct-media--content">
                                        <span></span>
                                        <a class="cross" onclick="deleteimgeditLogo2();" style="cursor:pointer;">
                                            <i class="fa fa-trash-o"></i> Delete
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row page_image">
                            <div class="company_logo">
                                <div class="">
                                    <label for="Description">Block 3 image (width: 1140px, heght:570px)</label>
                                </div>
                                <div class="avatar_img">
                                    <div class="file-upload btn btn_1 green">
                                        <span>Choose image</span>
                                        <input type="file" name="block_3_img" id="uploadBtnLogo" class="image_logo3 upload"
                                               onchange="readURLLogo3(this)">
                                    </div>
                                </div>
                                <div class="profile-image-preview_logo3  @if($home_settings->block_3_img !='') {{ "active" }} @endif ">
                                    <div class="ct-media--left">
                                        <a>
                                            <img id="uploadPreviewLogo3" src="{{ url('/') }}/uploads/home_page/{{ $home_settings->block_3_img }}">
                                        </a>
                                    </div>
                                    <input type="hidden" name="old_image3" class="old_image_logo3" value="{{ $home_settings->block_3_img }}">
                                    <div class="ct-media--content">
                                        <span></span>
                                        <a class="cross" onclick="deleteimgeditLogo3();" style="cursor:pointer;">
                                            <i class="fa fa-trash-o"></i> Delete
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group meta_block">
                            <div class="open_close_meta">
                                <p>Meta <i class="icon-arrow-combo"></i></p>
                            </div>
                            <div class="meta_box">
                                <div class="col-md-6 col-sm-6 pull-left">
                                    @foreach($languages as $language)
                                        <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                            <label>{{ $language->title }} Meta Title:</label>
                                            <input type="text" placeholder='Meta Title' class="form-control"
                                                   value='{{ $home_settings->{"meta_title_".$language->short} }}' id='meta_title' name='meta_title_{{ $language->short }}'/>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="col-md-6 col-sm-6 pull-left">
                                    @foreach($languages as $language)
                                        <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                            <label>{{ $language->title }} Meta Keyword:</label>
                                            <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_{{ $language->short }}'
                                                   class="form-control"
                                                   value="{{ $home_settings->{"meta_key_".$language->short} }}"/>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="col-md-12 col-sm-12 pull-left">
                                    @foreach($languages as $language)
                                        <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                            <label>{{ $language->title }} Meta Description</label>
                                            <textarea id="meta_desc" class="form-control" name="meta_desc_{{ $language->short }}">{{ $home_settings->{"meta_desc_".$language->short} }}</textarea>
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6 col-sm-6 col-sm-offset-3 col-md-offset-3">
                            <input type="hidden" name="save_home_meta" value="ok">
                            <input type="submit" class="welcome_page_save btn btn_1 green" value="Save">
                        </div>
                    </div>
                </form>
            </div>
        </div>



    </div>

@endsection