@extends('admin.layouts.page')
<link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/jquery.switch.css') }}" rel="stylesheet">
@section('content')

    <div class="container services_block">
        <div class="row">
            <div class="col-md-8">
                    <div class="about_form">
                        <div class="page_title_box parent_page_title">
                            <h4>Newsletter users</h4>
                        </div>
                        <section id="section-1" class="content-current">
                            <form class="checked_form" action="{{ url('/') }}/admin/checked_delete" method="post">
                                {{ csrf_field() }}
                                <div class="select_all_block">
                                    <input type="checkbox" class="checked_all" name="checked_all" value="1">
                                    <label>Check all</label>
                                    <button class="checked_delete" type="button" disabled><i class="icon-trash"></i></button>
                                </div>

                            @foreach($users as $user)
                                <div class="strip_booking strip_project{{ $user->id }}">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2">
                                            <div class="check_one" style="margin-top: 0">
                                                <input type="checkbox" class="checked_one_input" name="checked_one_input[]" value="{{ $user->id }}">
                                                <input type="checkbox" class="hidden check_type" name="check_type[]" value="users" >
                                            </div>
                                        </div>
                                        <div class="newsletter_users col-md-2 col-sm-2">
                                            <h3 class="">{{ $user->name }}</h3>
                                        </div>
                                        <div class="newsletter_users col-md-6 col-sm-6">
                                            @if(!empty($user->email))
                                            <p><span>Email: </span>{{ $user->email }}</p>
                                            @endif
                                            @if(!empty($user->country))
                                            <p><span>Country: </span>{{ $user->country }}</p>
                                            @endif
                                            @if(!empty($user->institution))
                                            <p><span>Institution: </span>{{ $user->institution }}</p>
                                                @endif
                                        </div>
                                        <div class="newsletter_users col-md-2 col-sm-2">
                                            <div class="page_actions_block">
                                                <a data-id="{{ $user->id }}" class="user_delete btn_delete btn-danger">
                                                    <i class="icon-trash"></i>
                                                    Delete
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End row -->
                                </div>
                            @endforeach
                            </form>
                        </section>

                    </div>
            </div>
        </div>
    </div>
@endsection
