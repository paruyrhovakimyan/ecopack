@extends('admin.layouts.page')
<link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/jquery.switch.css') }}" rel="stylesheet">
@section('content')

    <div class="container services_block">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Projects</h3>
                        <a class="btn_1 green pull-right" href="{{ url('/') }}/admin/projects/create">Add Project</a>
                    </div>
                    <div class="about_form">
                        <form class="form-horizontal" method="POST" action="http://foc.loc/admin/about_us">
                            <input type="hidden" name="_token" value="U1AwYoBVfuvUYPcIFGJM9WGm3t1o6xR1K7vLExuD">
                        </form>
                        <section id="section-1" class="content-current">
                            @foreach($projects as $project)
                                <div class="strip_booking strip_project{{ $project->id }}">
                                    <div class="row">
                                        <div class="project_img_box col-md-2 col-sm-2">
                                            @if($project->image !='')
                                           <img src="{{ url('/') }}/uploads/projects/thumbs/{{$project->image}}" >
                                                @else
                                                <div class="date">
                                                    <span class="month">{{ date("M", strtotime($project->created_at)) }}</span>
                                                    <span class="day"><strong>{{ date("d", strtotime($project->created_at)) }}</strong>{{ date("D", strtotime($project->created_at)) }}</span>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <label class="switch-light switch-ios pull-right">
                                                <input data-id="{{ $project->id }}" type="checkbox" class="project_hidden option_hidden{{$project->id}}" @if($project->hidden == 0){{ "checked" }}@endif
                                                value="@if($project->hidden == 0){{ "1" }}@else{{ "0" }}@endif">

                                                <span>
							                        <span>Hide</span>
													<span>Show</span>
													</span>
                                                <a></a>
                                            </label>
                                            <h3 class="">{{ $project->title }}</h3>
                                        </div>
                                        <div class="col-md-2 col-sm-3">
                                            <ul class="info_booking">
                                                <li><strong>Project id</strong> {{ $project->id }}</li>
                                                <li><strong>Project order</strong> {{ $project->project_order }}</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <div class="booking_buttons">
                                                <a href="{{ url('/') }}/admin/projects/{{ $project->id }}/edit" class="btn_2">Edit</a>
                                                <a data-id="{{ $project->id }}" class="project_delete btn_delete btn-danger">Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End row -->
                                </div>
                            @endforeach
                        </section>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
