@extends('admin.layouts.page')

@if(isset($data['edit_successful']))
    <div class="edit_successful">
        <p>The Service updated</p>
    </div>
@endif

@section('content')

    <div class="container">
        <div class="row edit_category">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span>Edit Service: </span><h3>{{ $project->title  }}</h3>
                        <a class="btn_1 green pull-right" href="{{ route('projects.index') }}">Back to Projects</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="form_block">
            <ul class="languages_tabs">
                @if($settings->lang_am == 1)
                    <li class="arm_tab @if(config('app.locale') == 'am') {{"active_tab"}} @endif">
                        <p><span>Armenian</span></p>
                    </li>
                @endif
                @if($settings->lang_ru == 1)
                    <li class="rus_tab @if(config('app.locale') == 'ru') {{"active_tab"}} @endif">
                        <p><span>RUSSIAN</span></p>
                    </li>
                @endif
                @if($settings->lang_en == 1)
                    <li class="eng_tab @if(config('app.locale') == 'en') {{"active_tab"}} @endif">
                        <p><span>ENGLISH</span></p>
                    </li>
                @endif
            </ul>
            <div class="form_box">
                <div class="panel panel-default">
                    <div class="about_form">
                        <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/projects/{{ $project->id }}">
                            {{method_field('PUT')}}
                            {{ csrf_field() }}
                            <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                                <label for="title">Armenian Title</label>
                                <input id="about_title" type="text" class="form-control" name="title" value="{{ $project->title  }}" required autofocus>

                            </div>
                            <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                                <label for="Description">Armenian Short Description</label>
                                <textarea class="form-control short_desc" name="short_desc">{{ $project->short_desc  }}</textarea>
                            </div>
                            <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                                <label for="Description">Armenian Description</label>
                                <textarea class="form-control description" name="desc">{{ $project->description  }}</textarea>
                            </div>


                            <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block">
                                <label for="title">Russian Title</label>
                                <input id="about_title" type="text" class="form-control" name="title_ru" value="{{ $project->title_ru  }}" autofocus>
                            </div>
                            <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block">
                                <label for="Description">Russian Short Description</label>
                                <textarea class="form-control short_desc" name="short_desc_ru">{{ $project->short_desc_ru  }}</textarea>
                            </div>
                            <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block">
                                <label for="Description">Russian Description</label>
                                <textarea class="form-control description" name="desc_ru">{{ $project->description_ru  }}</textarea>
                            </div>
                            <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                <label for="title">English Title</label>
                                <input id="about_title" type="text" class="form-control" name="title_en" value="{{ $project->title_en  }}" autofocus>
                            </div>
                            <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                <label for="Description">English Short Description</label>
                                <textarea class="form-control short_desc" name="short_desc_en">{{ $project->short_desc_en  }}</textarea>
                            </div>
                            <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                <label for="Description">English Description</label>
                                <textarea class="form-control description" name="desc_en">{{ $project->description_en  }}</textarea>
                            </div>


                            <div class="row">
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <h4> META HEADERS </h4>
                                </div>
                                <div class="col-md-6 col-sm-6 pull-left">

                                    <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                                        <label>Armenian Meta Title:</label>
                                        <input type="text" placeholder='Meta Title' class="form-control"
                                               value='{{ $project->meta_title  }}' id='meta_title' name='meta_title'/>
                                    </div>
                                    <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block">
                                        <label>Russian Meta Title:</label>
                                        <input type="text" placeholder='Meta Title' class="form-control"
                                               value='{{ $project->meta_title_ru  }}' id='meta_title' name='meta_title_ru'/>
                                    </div>
                                    <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                        <label>English Meta Title:</label>
                                        <input type="text" placeholder='Meta Title' class="form-control"
                                               value='{{ $project->meta_title_en  }}' id='meta_title' name='meta_title_en'/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 pull-left">
                                    <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                                        <label>Armenian Meta Keyword:</label>
                                        <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key' class="form-control"
                                               value="{{ $project->meta_key  }}"/>
                                    </div>
                                    <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block">
                                        <label>Russian Meta Keyword:</label>
                                        <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_ru' class="form-control"
                                               value="{{ $project->meta_key_ru  }}"/>
                                    </div>
                                    <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                        <label>English Meta Keyword:</label>
                                        <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_en' class="form-control"
                                               value="{{ $project->meta_key_en  }}"/>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                                        <label>Armenian Meta Description</label>
                                        <textarea id="meta_desc" class="form-control" name="meta_desc">{{ $project->meta_desc  }}</textarea>
                                    </div>
                                    <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block">
                                        <label>Russian Meta Description</label>
                                        <textarea id="meta_desc" class="form-control" name="meta_desc_ru">{{ $project->meta_desc_ru  }}</textarea>
                                    </div>
                                    <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                        <label>English Meta Description</label>
                                        <textarea id="meta_desc" class="form-control" name="meta_desc_en">{{ $project->meta_desc_en  }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group order_block">
                                <label for="service_order">Order</label>
                                <input id="service_order" type="text" class="form-control" name="order" value="{{ $project->project_order }}">
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <h4>Upload photo</h4>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group ">

                                        <div class="ct-u-marginBottom30">
                                            <div id="uploadzone" dir="ltr"
                                                 class="dropzone  @if(count($projectImages) != '0') {{ $dropzoneDefaultHidden }} @endif">
                                                <div class="dz-default dz-message"><i class="fa fa-camera"></i>
                                                    <!--<img src="/../assets/images/fileuploader-dragdrop-icon.png">-->
                                                    <div class="btn_1 green"><span>Upload file</span></div>
                                                    <h3 class="fileuploader-input-caption"><span>Drop files here or click to upload.</span></h3></div>
                                                @if(count($projectImages) > 0)
                                                    @foreach($projectImages as $property_image)
                                                        <div class="dz-preview dz-processing dz-image-preview dz-success dz-complete">
                                                            <div class="dz-image"><img data-dz-thumbnail=""
                                                                                       alt="{{ $property_image->image }}"
                                                                                       src="{{ url('/') }}/uploads/projects/150/{{ $property_image->image }}">
                                                            </div>
                                                            <div class="dz-details">
                                                                <div class="dz-filename">
                                                                    <span data-dz-name="">@if(isset($property_image->image)){{ $property_image->image }} @endif</span>
                                                                </div>
                                                                <input type="hidden" data-dz-name1="" name="filename[]"
                                                                       value="{{ $property_image->image }}"
                                                                       class="dz-filename-after">
                                                            </div>
                                                            <div class="dz-progress">
                                                        <span class="dz-upload" data-dz-uploadprogress=""
                                                              style="width: 100%;"></span>
                                                            </div>
                                                            <div class="dz-error-message">
                                                                <span data-dz-errormessage=""></span>
                                                            </div>
                                                            <div class="dz-success-mark">
                                                                <svg width="54px" height="54px" viewBox="0 0 54 54"
                                                                     version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                     xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                                                                    <title>Check</title>
                                                                    <defs></defs>
                                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                                                       fill-rule="evenodd" sketch:type="MSPage">
                                                                        <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z"
                                                                              id="Oval-2" stroke-opacity="0.198794158"
                                                                              stroke="#747474" fill-opacity="0.816519475"
                                                                              fill="#FFFFFF" sketch:type="MSShapeGroup"></path>
                                                                    </g>
                                                                </svg>
                                                            </div>
                                                            <div class="dz-error-mark">
                                                                <svg width="54px" height="54px" viewBox="0 0 54 54"
                                                                     version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                     xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                                                                    <title>Error</title>
                                                                    <defs></defs>
                                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                                                       fill-rule="evenodd" sketch:type="MSPage">
                                                                        <g id="Check-+-Oval-2" sketch:type="MSLayerGroup"
                                                                           stroke="#747474" stroke-opacity="0.198794158"
                                                                           fill="#FFFFFF" fill-opacity="0.816519475">
                                                                            <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z"
                                                                                  id="Oval-2" sketch:type="MSShapeGroup"></path>
                                                                        </g>
                                                                    </g>
                                                                </svg>
                                                            </div>
                                                            <a class="dz-remove old_img_remove" data-dz-remove="">Remove
                                                                file</a>
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>




                            <input type="hidden" name="edit_project" value="ok">
                            <button type="submit" class="btn_1 green pull-right">
                                Save
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
