@extends('admin.layouts.page')

@section('content')

    <div class="container">
            <div class="form_box">
                <div class="about_form">
                    <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/private_area/create">
                    {{ csrf_field() }}
                    <!--Armenian-->

                        <div class="col-md-8">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input id="about_title" type="text" class="form-control" name="title" value=""
                                           autofocus>
                                </div>
                             {{--   <div class="form-group">
                                    <label for="Description">Short Description</label>
                                    <textarea class="form-control short_desc" name="short_desc_{{ config('app.locale') }}"></textarea>
                                </div>--}}
                                <div  class="form-group">
                                    <label for="Description">Description</label>
                                    <textarea class="form-control description" name="description"></textarea>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label for="Description">Login</label>
                                        <input type="text" class="form-control description" name="login" required>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="Description">Password</label>
                                        <input type="text" class="form-control" name="password" required>
                                    </div>

                                </div>
                            <input type="hidden" name="add_private_area" value="ok">
                            <div class="col-md-12">
                                <button type="submit" class="btn_1 save_btn green pull-right">
                                    Save
                                </button>
                            </div>
                            </div>








                    </form>
                </div>
            </div>

    </div>
@endsection
