@extends('admin.layouts.page')

@section('content')

    <div class="container">
        <div class="about_form">
            <div class="col-md-8">
            <div class="page_title_box parent_page_title">
                <h4>Private Area | {{ $private_area->title }}</h4>
            </div>
            <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/private_area/{{ $private_area->id }}/edit">
            {{ csrf_field() }}
            <!--Armenian-->
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input id="about_title" type="text" class="form-control" name="title" value="{{ $private_area->title }}"
                               autofocus>
                    </div>
                    {{--   <div class="form-group">
                           <label for="Description">Short Description</label>
                           <textarea class="form-control short_desc" name="short_desc_{{ config('app.locale') }}"></textarea>
                       </div>--}}
                    <div class="form-group">
                        <label for="Description">Description</label>
                        <textarea class="form-control description" name="description">{{ $private_area->description }}</textarea>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <label for="Description">Login</label>
                            <input type="text" class="form-control description" name="login" value="{{ $private_area->login }}" required>
                        </div>
                        <div class="col-md-6">
                            <label for="Description">Password</label>
                            <input type="text" class="form-control" name="password" value="{{ $private_area->password }}" required>
                        </div>

                    </div>
                    <input type="hidden" name="edit_private_area" value="ok">
                    <div class="col-md-12">
                        <button type="submit" class="btn_1 save_btn green pull-right">
                            Save
                        </button>
                    </div>


            </form>
            </div>
        </div>
    </div>
@endsection
