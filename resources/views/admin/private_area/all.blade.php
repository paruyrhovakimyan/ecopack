@extends('admin.layouts.page')
<link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/jquery.switch.css') }}" rel="stylesheet">
@section('content')
    <div class="container services_block">
        <div class="col-md-8 page_show_content">
            <div class="page_title_box parent_page_title">
                <h4>Private Area</h4>
                <div class="page_actions_block">
                    <a href="{{ url('/') }}/admin/private_area/create" class="sub_page_add btn_2">
                        <i class="icon-docs"></i>
                        Add
                    </a>
                </div>
            </div>
            <div class="private_areas">
                <form class="checked_form" action="{{ url('/') }}/admin/checked_delete" method="post">
                    {{ csrf_field() }}
                    <div class="select_all_block" style="width: 100%">
                        <input type="checkbox" class="checked_all" name="checked_all" value="1">
                        <label>Check all</label>
                        <button class="checked_delete" type="button" disabled><i class="icon-trash"></i></button>
                    </div>
                @foreach($private_areas as $private_area)

                    <div class="private_area_block">
                        <div class="page_title_box child_page_title">
                            <div class="check_one">
                                <input type="checkbox" class="checked_one_input" name="checked_one_input[]" value="{{ $private_area->id }}">
                                <input type="checkbox" class="hidden check_type" name="check_type[]" value="private_area" >
                            </div>
                            <div class="page_hidden">
                                @if($private_area->hidden == 0)
                                    <a alt="Show" title="Show" class="visible" data-hidden="1" data-id="{{ $private_area->id }}" data-type="private_area">
                                        <i class="icon-eye"></i>
                                    </a>

                                @else
                                    <a alt="Hide" title="Hide" class="visible" data-hidden="0" data-id="{{ $private_area->id }}" data-type="private_area">
                                        <i class="icon-eye-off"></i>
                                    </a>
                                @endif
                            </div>
                            <p>{{ $private_area->title }}</p>
                            <div class="page_actions_block">
                                <a href="{{ url('/') }}/admin/private_area/add_pdf/{{ $private_area->id }}" class="page_pdf_btn btn_2"><i class="icon-doc-add"></i> Pdf</a>
                                <a href="{{ url('/') }}/admin/private_area/{{ $private_area->id }}/edit" class="parent_page_edit btn_2">
                                    <i class="icon-edit-3"></i>
                                    Edit
                                </a>
                                @if(!isset($private_area->files) || empty($private_area->files))
                                    <a data-id="{{ $private_area->id }}" class="page_del_btn private_area_del btn_delete btn-danger">
                                        <i class="icon-trash"></i>
                                        Delete
                                    </a>
                                @endif
                            </div>

                        </div>

                            @if(isset($private_area->files) && !empty($private_area->files))
                            <div class="private_area_child">
                                @foreach($private_area->files as $child_page_pdf)
                                            <div class="page_title_box child_page_title">
                                                <div class="check_one">
                                                    <input type="checkbox" class="checked_one_input" name="checked_one_input[]" value="{{ $child_page_pdf->id }}">
                                                    <input type="checkbox" class="hidden check_type" name="check_type[]" value="private_area_pdf" >
                                                </div>
                                                <div class="page_hidden">
                                                    @if($child_page_pdf->hidden == 0)
                                                        <a alt="Show" title="Show" class="visible" data-hidden="1" data-id="{{ $child_page_pdf->id }}" data-type="private_area_pdf">
                                                            <i class="icon-eye"></i>
                                                        </a>

                                                    @else
                                                        <a alt="Hide" title="Hide" class="visible" data-hidden="0" data-id="{{ $child_page_pdf->id }}" data-type="private_area_pdf">
                                                            <i class="icon-eye-off"></i>
                                                        </a>
                                                    @endif
                                                </div>
                                                <p>{{ $child_page_pdf->title }}</p>
                                                <div class="page_actions_block">
                                                    <a href="{{ url('/') }}/admin/private_area/edit_pdf/{{ $child_page_pdf->id  }}" class="parent_page_edit btn_2">
                                                        <i class="icon-edit-3"></i>Edit
                                                    </a>
                                                    <a data-id="{{ $child_page_pdf->id }}" class="page_del_btn delete_private_area_pdf btn_delete btn-danger">
                                                        <i class="icon-trash"></i>Delete
                                                    </a>
                                                </div>
                                            </div>
                                        @endforeach
                            </div>
                            @endif

                    </div>

                @endforeach
                </form>
            </div>
        </div>
    </div>
@endsection
