@extends('admin.layouts.page')

@section('content')

    <div class="container services_block">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Settings</h3>
                    </div>
                    <div class="about_form">
                        @if(isset($edit_language->id))
                            <form class="" action="{{ url('/') }}/admin/settings/{{ $edit_language->id }}/save" method="post">
                                {{ csrf_field() }}

                                <div class="form-group form_short">
                                    <label>Language name</label>
                                    <input type="text" class="short_input form-control" name="lang_name" value="{{ $edit_language->title }}" >
                                </div>
                                <div class="form-group form_short">
                                    <label>Short code</label>
                                    <input type="text" class="short_input form-control" name="short_code" value="{{ $edit_language->short }}" >
                                </div>
                                <div class="form-group">
                                    <input type="submit" name="save_settings" value="Save" class="lang_submit btn_1 green">
                                </div>
                            </form>
                         @else
                            <form class="" action="{{ url('/') }}/admin/settings" method="post">
                                {{ csrf_field() }}

                                <div class="form-group form_short">
                                    <label>Language name</label>
                                    <input type="text" class="short_input form-control" name="lang_name" value="" >
                                </div>
                                <div class="form-group form_short">
                                    <label>Short code</label>
                                    <input type="text" class="short_input form-control" name="short_code" value="" >
                                </div>
                                <div class="form-group">
                                    <input type="submit" name="save_settings" value="Save" class="lang_submit btn_1 green">
                                </div>
                            </form>
                        @endif


                    </div>
                    <div class="lamguages_block">
                        <table class="languages_table">
                            <tr>
                                <th>Language</th>
                                <th>Short</th>
                                <th>Status</th>
                                <th>Default</th>
                                <th>Action</th>
                            </tr>
                            @foreach($languages as $language)
                                <tr>
                                    <td>
                                        @if($language->first != 1)
                                        <div class="page_hidden">
                                            @if($language->hidden == 0)
                                                <a alt="Show" title="Show" class="visible" data-hidden="1" data-id="{{ $language->id }}" data-type="language">
                                                    <i class="icon-eye"></i>
                                                </a>

                                            @else
                                                <a alt="Hide" title="Hide" class="visible" data-hidden="0" data-id="{{ $language->id }}" data-type="language">
                                                    <i class="icon-eye-off"></i>
                                                </a>
                                            @endif
                                        </div>
                                        @endif
                                        {{ $language->title }}
                                    </td>
                                    <td>{{ $language->short }}</td>
                                    <td> @if($language->status == 1){{ "active" }} @else {{ "pasive" }} @endif</td>
                                    <td> @if($language->first == 1) <i class="fa fa-check" aria-hidden="true"></i> @else {{ "-" }} @endif</td>
                                    <td>
                                        <div class="booking_buttons">
                                            <a href="{{ url('/') }}/admin/settings?lang={{ $language->id }}" class="language_edit">Edit</a>
                                            @if($language->first != 1)
                                            <a data-id="{{ $language->id }}" class="language_delete btn_delete btn-danger">Delete</a>
                                                @endif
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
