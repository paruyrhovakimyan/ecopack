@extends('admin.layouts.page')

@section('content')

    <div class="container">
        <div class="form_box">
            @if( count($languages) > 1)
                <ul class="languages_tabs">
                    @foreach($languages as $language)
                        <li data-tab="{{ $language->short }}"
                            class="lang_tab {{ $language->short."_tab" }} @if($language->first == 1) {{"active_tab"}} @endif">
                            <p><span>{{ $language->title }}</span></p>
                        </li>
                    @endforeach
                </ul>
            @endif
            <div class="about_form">
                <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/news"
                      enctype='multipart/form-data'>
                    {{ csrf_field() }}

                    <div class="col-md-8 page_left">

                        @foreach($languages as $language)
                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="title">{{ $language->title }} Title</label>
                                <input id="about_title" type="text" class="form-control" name="title_{{ $language->short }}" value=""
                                       autofocus>
                            </div>

                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="Description">{{ $language->title }} Short Description</label>
                                <textarea class="form-control short_desc" name="short_desc_{{ $language->short }}"></textarea>
                            </div>
                            <div  class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="Description">{{ $language->title }} Description</label>
                                <textarea class="form-control description" name="description_{{ $language->short }}"></textarea>
                            </div>

                        @endforeach
                            <div  class="form-group">
                                <div class="check_one">
                                    <input type="checkbox" name="stick_home" value="1">
                                    Stick to Home
                                </div>
                            </div>
                        @if($page_settings->config_data1 == 1)
                            <div class="data1_field form-group">
                                <label for="Description">Data</label>
                                <input type="text" id="datapicker2" class="author-combobox-wrap form-control"
                                       name="date1" value="">
                            </div>
                        @endif
                        @if($page_settings->config_data2 == 1)
                            <div class="data1_field form-group">
                                <label for="Description">Data 2</label>
                                <input type="text" id="datapicker3" class="author-combobox-wrap form-control" name="date2"  value="">
                            </div>
                        @endif
{{--                        @if($page_settings->config_cat == 1)--}}
{{--                            <div class="article_cat author_fields form-group">--}}
{{--                                <label>Category</label>--}}
{{--                                <select class="author-combobox-wrap article_author_name form-control" name="cat_id"--}}
{{--                                        id="cat_id" required>--}}
{{--                                    <option value="">--Select category--</option>--}}
{{--                                    @foreach($categories as $category)--}}
{{--                                        <option value="{{ $category->id }}">{{ $category->title }}</option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}
{{--                            </div>--}}
{{--                        @endif--}}
                            <div sclass="form-group">
                                <label for="Description">Video</label>
                                <input id="about_title" type="text" class="form-control" name="video" value=""
                                       autofocus>
                            </div>
                        @if($page_settings->config_tag == 1)
                                @foreach($languages as $language)
                                    <div sclass="form-group">
                                        <label for="Description">{{ $language->title }}  Tags</label>
                                        <textarea class="form-control short_desc" name="tag_{{ $language->short }}"></textarea>
                                    </div>
                                @endforeach

                        @endif
                        @if($page_settings->config_image == 1)
                            <div class="row page_image">
                                <div class="company_logo">
                                    <div class="">
                                        <label for="Description">Image (width: 800px, heght:400px)</label>
                                    </div>
                                    <div class="avatar_img">
                                        <div class="file-upload btn btn_1 green">
                                            <span>Choose image</span>
                                            <input type="file" name="image" id="uploadBtnLogo" class="image upload"
                                                   onchange="readURLLogo(this)">
                                        </div>
                                    </div>
                                    <div class="profile-image-preview_logo">
                                        <div class="ct-media--left">
                                            <a>
                                                <img id="uploadPreviewLogo" src="">
                                            </a>
                                        </div>
                                        <div class="ct-media--content">
                                            <span></span>
                                            <a class="cross" onclick="deleteimgLogo();" style="cursor:pointer;">
                                                <i class="fa fa-trash-o"></i> Delete
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                            <div class="form-group meta_block">
                                <div class="open_close_meta">
                                    <p>Meta <i class="icon-arrow-combo"></i></p>
                                </div>
                                <div class="meta_box">
                                    <div class="col-md-6 col-sm-6 pull-left">
                                        @foreach($languages as $language)
                                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                                <label>{{ $language->title }} Meta Title:</label>
                                                <input type="text" placeholder='Meta Title' class="form-control"
                                                       value='' id='meta_title' name='meta_title_{{ $language->short }}'/>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-6 col-sm-6 pull-left">
                                        @foreach($languages as $language)
                                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                                <label>{{ $language->title }} Meta Keyword:</label>
                                                <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_{{ $language->short }}'
                                                       class="form-control"
                                                       value=""/>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-12 col-sm-12 pull-left">
                                        @foreach($languages as $language)
                                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                                <label>{{ $language->title }} Meta Description</label>
                                                <textarea id="meta_desc" class="form-control" name="meta_desc_{{ $language->short }}"></textarea>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>

                            </div>
                        <input type="hidden" name="add_news" value="ok">
                        <input type="hidden" name="parent_id" value="{{ $parent_id }}">
                        <button type="submit" class="btn_1 green pull-right">
                            Save
                        </button>
                    </div>
                    <div class="col-md-4 page_right">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <h4> Additional</h4>
                                </div>
                                <div class="additional_block col-md-12">
                                    <input type="checkbox" name="pdf_config" value="1">
                                    <label><i class="icon-doc-add"></i> Pdf uploader</label>
                                </div>
                                <div class="additional_block col-md-12">
                                    <input type="checkbox" name="gallery_config" value="1">
                                    <label><i class="icon-picture-1"></i> Gallery</label>
                                </div>
                                <div class="additional_block col-md-12">
                                    <input type="checkbox" name="page_pdf" value="1">
                                    <label><i class="icon-folder-empty"></i> Page | pdf</label>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
