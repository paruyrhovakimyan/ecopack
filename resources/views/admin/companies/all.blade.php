@extends('admin.layouts.page')
<link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/jquery.switch.css') }}" rel="stylesheet">
@section('content')

    <div class="container services_block">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Companies</h3>
                        <a class="btn_1 green pull-right" href="{{ url('/') }}/admin/companies/create">Add Company</a>
                    </div>
                    <div class="about_form">
                        <form class="form-horizontal" method="POST" action="http://foc.loc/admin/about_us">
                            <input type="hidden" name="_token" value="U1AwYoBVfuvUYPcIFGJM9WGm3t1o6xR1K7vLExuD">
                        </form>
                        <section id="section-1" class="content-current">
                            @foreach($companies as $company)
                                <div class="strip_booking strip_project{{ $company->id }}">
                                    <div class="row">
                                        <div class="project_img_box col-md-2 col-sm-2">
                                            @if($company->image !='')
                                           <img src="{{ url('/') }}/uploads/companies/thumbs/{{$company->image}}" >
                                                @else
                                                <div class="date">
                                                    <span class="month">{{ date("M", strtotime($company->created_at)) }}</span>
                                                    <span class="day"><strong>{{ date("d", strtotime($company->created_at)) }}</strong>{{ date("D", strtotime($company->created_at)) }}</span>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <label class="switch-light switch-ios pull-right">
                                                <input data-id="{{ $company->id }}" type="checkbox" class="company_hidden option_hidden{{$company->id}}" @if($company->hidden == 0){{ "checked" }}@endif
                                                value="@if($company->hidden == 0){{ "1" }}@else{{ "0" }}@endif">

                                                <span>
							                        <span>Hide</span>
													<span>Show</span>
													</span>
                                                <a></a>
                                            </label>
                                            <h3 class="">{{ $company->title }}</h3>
                                        </div>
                                        <div class="col-md-2 col-sm-3">
                                            <ul class="info_booking">
                                                <li><strong>Company id</strong> {{ $company->id }}</li>
                                                <li><strong>Company order</strong> {{ $company->company_order }}</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <div class="booking_buttons">
                                                <a href="{{ url('/') }}/admin/companies/{{ $company->id }}/edit" class="btn_2">Edit</a>
                                                <a data-id="{{ $company->id }}" class="company_delete btn_delete btn-danger">Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End row -->
                                </div>
                            @endforeach
                        </section>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
