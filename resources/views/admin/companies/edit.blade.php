@extends('admin.layouts.page')

@if(isset($data['edit_successful']))
    <div class="edit_successful">
        <p>The Service updated</p>
    </div>
@endif

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading edit_title">
                        <span>Edit Company: </span><h3>{{ $company->title  }}</h3>
                        <a class="btn_1 green pull-right" href="{{ route('companies.index') }}">Back to Companies</a>
                    </div>
                    <div class="about_form">
                        <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/companies/{{ $company->id }}" enctype='multipart/form-data'>
                            {{method_field('PUT')}}
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input id="about_title" type="text" class="form-control" name="title" value="{{ $company->title  }}" required autofocus>
                            </div>
                            <div class="form-group">
                                <label for="title">Subtitle</label>
                                <input id="about_title" type="text" class="form-control" name="subtitle" value="{{ $company->subtitle  }}">

                            </div>
                            <div class="form-group">
                                <label for="Description">Description</label>
                                <textarea class="form-control description" name="desc">{{ $company->description  }}</textarea>
                            </div>
                            <div class="col-md-6 col-sm-6 pull-left">
                                <div class="form-group">
                                    <label for="title">Video</label>
                                    <input id="about_title" type="text" class="form-control" name="video" value="{{ $company->video  }}">
                                </div>
                                <div class="form-group">
                                    <label for="title">Facebook</label>
                                    <input id="about_title" type="text" class="form-control" name="fb_link" value="{{ $company->fb_link  }}">
                                </div>
                                <div class="form-group">
                                    <label for="title">Twitter</label>
                                    <input id="about_title" type="text" class="form-control" name="tw_link" value="{{ $company->tw_link  }}">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 pull-left">
                                <div class="form-group">
                                    <label for="title">Instagram</label>
                                    <input id="about_title" type="text" class="form-control" name="in_link" value="{{ $company->in_link  }}">
                                </div>
                                <div class="form-group">
                                    <label for="title">G+</label>
                                    <input id="about_title" type="text" class="form-control" name="gl_link" value="{{ $company->gl_link  }}">
                                </div>
                            </div>



                            <div class="row">
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <h4> META HEADERS </h4>
                                </div>
                                <div class="col-md-6 col-sm-6 pull-left">

                                    <div class="form-group arm_block">
                                        <label>Meta Title:</label>
                                        <input type="text" placeholder='Meta Title' class="form-control"
                                               value='{{ $company->meta_title  }}' id='meta_title' name='meta_title'/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 pull-left">
                                    <div class="form-group arm_block">
                                        <label>Meta Keyword:</label>
                                        <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key' class="form-control"
                                               value="{{ $company->meta_key  }}"/>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <div class="form-group arm_block">
                                        <label>Meta Description</label>
                                        <textarea id="meta_desc" class="form-control" name="meta_desc">{{ $company->meta_desc  }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group order_block">
                                <label for="service_order">Order</label>
                                <input id="service_order" type="text" class="form-control" name="order" value="{{ $company->company_order }}">
                            </div>

                            <div class="row">
                                <div class="company_logo">
                                    <div class="col-md-5 col-sm-5 pull-left title_present">
                                        <div class="">
                                            <h4>Company logo</h4>
                                        </div>
                                        <div class="avatar_img">
                                            <div class="file-upload btn btn_1 green">
                                                <span>Choose logo</span>
                                                <input type="file" name="image_logo" id="uploadBtnLogo" class="image upload"
                                                       onchange="readURLEditLogo(this)">
                                            </div>
                                        </div>
                                        <div class="profile-image-preview_logo @if($company->bacground_image !='') {{ "active" }} @endif">
                                            <div class="ct-media--left">
                                                <a>
                                                    <img id="uploadPreviewLogo" src="/uploads/companies/logo/{{ $company->logo }}">
                                                </a>
                                            </div>
                                            <input type="hidden" name="old_image_logo" class="old_image_logo" value="{{ $company->logo }}">
                                            <div class="ct-media--content">
                                                <span></span>
                                                <a class="cross" onclick="deleteimgeditLogo();" style="cursor:pointer;">
                                                    <i class="fa fa-trash-o"></i> Delete
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="company_background">
                                    <div class="col-md-5 col-sm-5 pull-left title_present">
                                        <div class="">
                                            <h4>Choose background image</h4>
                                        </div>
                                        <div class="avatar_img">
                                            <div class="file-upload btn btn_1 green">
                                                <span>Choose image</span>
                                                <input type="file" name="image_bg" id="uploadBtnBg" class="image upload"
                                                       onchange="readURLEditBg(this)">
                                            </div>
                                        </div>
                                        <div class="profile-image-preview_bg @if($company->bacground_image !='') {{ "active" }} @endif">
                                            <div class="ct-media--left">
                                                <a>
                                                    <img id="uploadPreviewBg" src="/uploads/companies/background/{{ $company->bacground_image }}">
                                                </a>
                                                <input type="hidden" class="old_image_bg" name="old_image_bg" value="{{ $company->bacground_image }}">
                                            </div>
                                            <div class="ct-media--content">
                                                <span></span>
                                                <a class="cross" onclick="deleteimgeditBg();" style="cursor:pointer;">
                                                    <i class="fa fa-trash-o"></i> Delete
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="bg_control_group">
                                        <label class="control-label" for="foregroundColor">Background Color</label>
                                        <input type="color" name="back_color" class="back_color"
                                               value="{{ $company->bg_color }}">
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-5 pull-left title_present">
                                    <div class="">
                                        <h4>Company Image</h4>
                                    </div>
                                    <div class="avatar_img">
                                        <div class="file-upload btn btn_1 green">
                                            <span>Choose Image</span>
                                            <input type="file" name="image" id="uploadBtn1" class="image upload"
                                                   onchange="readURLEdit(this)">
                                        </div>
                                    </div>
                                    <div class="profile-image-preview @if($company->image !='') {{ "active" }} @endif">
                                        <div class="ct-media--left">
                                            <a>
                                                <img id="uploadPreview" src="/uploads/companies/150/{{ $company->image }}">
                                            </a>
                                        </div>
                                        <div class="ct-media--content">
                                            <span></span>
                                            <a class="cross" onclick="deleteimgedit();" style="cursor:pointer;">
                                                <i class="fa fa-trash-o"></i> Delete
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="old_image" value="{{ $company->image }}">
                               <div class="col-md-5 col-sm-5 pull-left title_present">
                                    <div class="">
                                        <h4>Image Position</h4>
                                    </div>
                                    <input type="hidden" class="img_position_type" name="img_position" value="{{ $company->image_position }}">
                                    <div data-type="left" class="position_left img_text_position @if($company->image_position == "left") {{ "avtive_position" }} @endif ">
                                        <span class="position_left_img">
                                            <i class="icon-picture"></i>
                                        </span>
                                        <span class="position_left_text">
                                            <i class="icon-text-width"></i>
                                        </span>
                                    </div>
                                    <div data-type="right" class="position_right img_text_position @if($company->image_position == "right") {{ "avtive_position" }} @endif">
                                        <span class="position_left_img">
                                            <i class="icon-text-width"></i>
                                        </span>
                                        <span class="position_left_text">
                                            <i class="icon-picture"></i>

                                        </span>
                                    </div>
                                    <div data-type="bottom" class="position_bottom img_text_position @if($company->image_position == "bottom") {{ "avtive_position" }} @endif">
                                        <span class="position_bottom_text">
                                            <i class="icon-text-width"></i>
                                        </span>
                                        <span class="position_bottom_img">
                                            <i class="icon-picture"></i>
                                        </span>
                                    </div>
                                    <div data-type="top" class="position_top img_text_position @if($company->image_position == "top") {{ "avtive_position" }} @endif">
                                         <span class="position_bottom_text">
                                          <i class="icon-picture"></i>
                                        </span>
                                        <span class="position_bottom_img">
                                              <i class="icon-text-width"></i>

                                        </span>
                                    </div>

                                </div>
                            </div>


                            <hr>

                            <div class="row">
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <h4>Upload photo</h4>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group ">

                                        <div class="ct-u-marginBottom30">
                                            <div id="uploadzone" dir="ltr"
                                                 class="dropzone  @if(count($companyImages) != '0') {{ $companyImages }} @endif">
                                                <div class="dz-default dz-message"><i class="fa fa-camera"></i>
                                                    <!--<img src="/../assets/images/fileuploader-dragdrop-icon.png">-->
                                                    <div class="btn_1 green"><span>Upload file</span></div>
                                                    <h3 class="fileuploader-input-caption"><span>Drop files here or click to upload.</span></h3></div>
                                                @if(count($companyImages) > 0)
                                                    @foreach($companyImages as $company_image)
                                                        <div class="dz-preview dz-processing dz-image-preview dz-success dz-complete">
                                                            <div class="dz-image"><img data-dz-thumbnail=""
                                                                                       alt="{{ $company_image->image }}"
                                                                                       src="/uploads/gallery/150/{{ $company_image->image }}">
                                                            </div>
                                                            <div class="dz-details">
                                                                <div class="dz-filename">
                                                                    <span data-dz-name="">@if(isset($company_image->image)){{ $company_image->image }} @endif</span>
                                                                </div>
                                                                <input type="hidden" data-dz-name1="" name="filename[]"
                                                                       value="{{ $company_image->image }}"
                                                                       class="dz-filename-after">
                                                            </div>
                                                            <div class="dz-progress">
                                                        <span class="dz-upload" data-dz-uploadprogress=""
                                                              style="width: 100%;"></span>
                                                            </div>
                                                            <div class="dz-error-message">
                                                                <span data-dz-errormessage=""></span>
                                                            </div>
                                                            <div class="dz-success-mark">
                                                                <svg width="54px" height="54px" viewBox="0 0 54 54"
                                                                     version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                     xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                                                                    <title>Check</title>
                                                                    <defs></defs>
                                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                                                       fill-rule="evenodd" sketch:type="MSPage">
                                                                        <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z"
                                                                              id="Oval-2" stroke-opacity="0.198794158"
                                                                              stroke="#747474" fill-opacity="0.816519475"
                                                                              fill="#FFFFFF" sketch:type="MSShapeGroup"></path>
                                                                    </g>
                                                                </svg>
                                                            </div>
                                                            <div class="dz-error-mark">
                                                                <svg width="54px" height="54px" viewBox="0 0 54 54"
                                                                     version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                     xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                                                                    <title>Error</title>
                                                                    <defs></defs>
                                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                                                       fill-rule="evenodd" sketch:type="MSPage">
                                                                        <g id="Check-+-Oval-2" sketch:type="MSLayerGroup"
                                                                           stroke="#747474" stroke-opacity="0.198794158"
                                                                           fill="#FFFFFF" fill-opacity="0.816519475">
                                                                            <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z"
                                                                                  id="Oval-2" sketch:type="MSShapeGroup"></path>
                                                                        </g>
                                                                    </g>
                                                                </svg>
                                                            </div>
                                                            <a class="dz-remove old_img_remove" data-dz-remove="">Remove
                                                                file</a>
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>




                            <input type="hidden" name="edit_company" value="ok">
                            <button type="submit" class="btn_1 green pull-right">
                                Save
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
