@extends('admin.layouts.page')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3>New Company</h3></div>
                    <div class="about_form">
                        <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/companies"
                              enctype='multipart/form-data'>
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input id="about_title" type="text" class="form-control" name="title" value="" required
                                       autofocus>

                            </div>
                            <div class="form-group">
                                <label for="title">Subtitle</label>
                                <input id="about_title" type="text" class="form-control" name="subtitle" value="">

                            </div>
                            <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                                <label for="Description">Description</label>
                                <textarea class="form-control description" name="desc"></textarea>
                            </div>
                            <div class="col-md-6 col-sm-6 pull-left">
                                <div class="form-group">
                                    <label for="title">Video</label>
                                    <input id="about_title" type="text" class="form-control" name="video" value="">
                                </div>
                                <div class="form-group">
                                    <label for="title">Facebook</label>
                                    <input id="about_title" type="text" class="form-control" name="fb_link" value="">
                                </div>
                                <div class="form-group">
                                    <label for="title">Twitter</label>
                                    <input id="about_title" type="text" class="form-control" name="tw_link" value="">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 pull-left">
                                <div class="form-group">
                                    <label for="title">Instagram</label>
                                    <input id="about_title" type="text" class="form-control" name="in_link" value="">
                                </div>
                                <div class="form-group">
                                    <label for="title">G+</label>
                                    <input id="about_title" type="text" class="form-control" name="gl_link" value="">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <h4> META HEADERS </h4>
                                </div>
                                <div class="col-md-6 col-sm-6 pull-left">

                                    <div class="form-group arm_block">
                                        <label>Meta Title:</label>
                                        <input type="text" placeholder='Meta Title' class="form-control"
                                               value='' id='meta_title' name='meta_title'/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 pull-left">
                                    <div class="form-group arm_block">
                                        <label>Meta Keyword:</label>
                                        <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key'
                                               class="form-control"
                                               value=""/>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <div class="form-group arm_block">
                                        <label>Meta Description</label>
                                        <textarea id="meta_desc" class="form-control" name="meta_desc"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group order_block">
                                <label for="service_order">Order</label>
                                <input id="service_order" type="text" class="form-control" name="order" value="1">
                            </div>


                            <div class="row">
                                <div class="company_logo">
                                    <div class="col-md-5 col-sm-5 pull-left title_present">
                                        <div class="">
                                            <h4>Company logo</h4>
                                        </div>
                                        <div class="avatar_img">
                                            <div class="file-upload btn btn_1 green">
                                                <span>Choose logo</span>
                                                <input type="file" name="image_logo" id="uploadBtnLogo" class="image upload"
                                                       onchange="readURLLogo(this)">
                                            </div>
                                        </div>
                                        <div class="profile-image-preview_logo">
                                            <div class="ct-media--left">
                                                <a>
                                                    <img id="uploadPreviewLogo" src="">
                                                </a>
                                            </div>
                                            <div class="ct-media--content">
                                                <span></span>
                                                <a class="cross" onclick="deleteimgLogo();" style="cursor:pointer;">
                                                    <i class="fa fa-trash-o"></i> Delete
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="company_background">
                                    <div class="col-md-5 col-sm-5 pull-left title_present">
                                        <div class="">
                                            <h4>Choose background image</h4>
                                        </div>
                                        <div class="avatar_img">
                                            <div class="file-upload btn btn_1 green">
                                                <span>Choose image</span>
                                                <input type="file" name="image_bg" id="uploadBtnBg" class="image upload"
                                                       onchange="readURLBg(this)">
                                            </div>
                                        </div>
                                        <div class="profile-image-preview_bg">
                                            <div class="ct-media--left">
                                                <a>
                                                    <img id="uploadPreviewBg" src="">
                                                </a>
                                                </div>
                                            <div class="ct-media--content">
                                                <span></span>
                                                <a class="cross" onclick="deleteimgBg();" style="cursor:pointer;">
                                                    <i class="fa fa-trash-o"></i> Delete
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="bg_control_group">
                                        <label class="control-label" for="foregroundColor">Background Color</label>
                                        <input type="color" name="back_color" class="back_color"
                                               value="#000000">
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-5 pull-left title_present">
                                    <div class="">
                                        <h4>Company Image</h4>
                                    </div>
                                    <div class="avatar_img">
                                        <div class="file-upload btn btn_1 green">
                                            <span>Choose Image</span>
                                            <input type="file" name="image" id="uploadBtn1" class="image upload"
                                                   onchange="readURL(this)">
                                        </div>
                                    </div>
                                    <div class="profile-image-preview">
                                        <div class="ct-media--left">
                                            <a>
                                                <img id="uploadPreview" src="">
                                            </a>
                                        </div>
                                        <div class="ct-media--content">
                                            <span></span>
                                            <a class="cross" onclick="deleteimg();" style="cursor:pointer;">
                                                <i class="fa fa-trash-o"></i> Delete
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-5 pull-left title_present">
                                    <div class="">
                                        <h4>Image Position</h4>
                                    </div>
                                    <input type="hidden" class="img_position_type" name="img_position" value="left">
                                    <div data-type="left" class="position_left img_text_position avtive_position">
                                        <span class="position_left_img">
                                            <i class="icon-picture"></i>
                                        </span>
                                        <span class="position_left_text">
                                            <i class="icon-text-width"></i>
                                        </span>
                                    </div>
                                    <div data-type="right" class="position_right img_text_position">
                                        <span class="position_left_img">
                                            <i class="icon-text-width"></i>
                                        </span>
                                        <span class="position_left_text">
                                            <i class="icon-picture"></i>

                                        </span>
                                    </div>
                                    <div data-type="bottom" class="position_bottom img_text_position">
                                        <span class="position_bottom_text">
                                            <i class="icon-text-width"></i>
                                        </span>
                                        <span class="position_bottom_img">
                                            <i class="icon-picture"></i>
                                        </span>
                                    </div>
                                    <div data-type="top" class="position_top img_text_position">
                                         <span class="position_bottom_text">
                                          <i class="icon-picture"></i>
                                        </span>
                                        <span class="position_bottom_img">
                                              <i class="icon-text-width"></i>

                                        </span>
                                    </div>

                                </div>
                            </div>


                            <hr>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <h4>Upload Gallery</h4>
                                </div>
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <div id="gallerydzone" class="dropzone" dir="ltr"></div>
                                </div>
                            </div>


                            <input type="hidden" name="add_company" value="ok">
                            <button type="submit" class="btn_1 green pull-right">
                                Save
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
