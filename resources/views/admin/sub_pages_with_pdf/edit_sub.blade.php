@extends('admin.layouts.page')
<link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/jquery.switch.css') }}" rel="stylesheet">
@section('content')

    <div class="container">
        <div class="form_box">
            <div class="col-md-8">
                <div class="page_title_box">
                    <h4>{!!  $page_bredcrump !!}</h4>
                </div>
                @if( count($languages) > 1)
                    <ul class="languages_tabs">
                        @foreach($languages as $language)
                            <li data-tab="{{ $language->short }}"
                                class="lang_tab {{ $language->short."_tab" }} @if($language->first == 1) {{"active_tab"}} @endif">
                                <p><span>{{ $language->title }}</span></p>
                            </li>
                        @endforeach
                    </ul>
                @endif
                <div class="about_form">
                    <form class="form-horizontal" method="POST"
                          action="{{ url('/') }}/admin/sub_pages_with_pdf/{{ $parent_id }}/edit/{{ $news_sub_page->id }}"
                          enctype='multipart/form-data'>
                    {{ csrf_field() }}

                        @foreach($languages as $language)
                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="title">{{ $language->title }} Title</label>
                                <input id="about_title" type="text" class="form-control" name="title_{{ $language->short }}" value="{{ $news_sub_page->{"title_".$language->short} }}"
                                       autofocus>
                            </div>
                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="Description">{{ $language->title }} Short Description</label>
                                <textarea class="form-control short_desc" name="short_desc_{{ $language->short }}">{{ $news_sub_page->{"short_desc_".$language->short} }}</textarea>
                            </div>
                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="Description">{{ $language->title }} Description</label>
                                <textarea class="form-control description" name="description_{{ $language->short }}">{{ $news_sub_page->{"description_".$language->short} }}</textarea>
                            </div>

                        @endforeach
                        <div class="form-group show_in_home">
                            <label class="for_class">Show in home</label>
                            <label class="switch-light switch-ios pull-left">
                                <input type="checkbox" name="show_in_home" class="option_hidden" @if($news_sub_page->show_in_home == 1){{ "checked" }} @endif value="1">
                                <span>
							                        <span>Hide</span>
													<span>Show</span>
													</span>
                                <a></a>
                            </label>
                        </div>

                        @if($config_project == 1)
                            <div class="form-group show_in_home">
                                <label class="for_class">Need a donate</label>
                                <label class="switch-light switch-ios pull-left">
                                    <input type="checkbox" name="need_donate" class="option_hidden" @if($news_sub_page->need_donate == 1){{ "checked" }} @endif value="1">
                                    <span>
							                        <span>No</span>
													<span>Yes</span>
													</span>
                                    <a></a>
                                </label>
                            </div>


                            <input type="hidden" name="config_project" value="1">
                            <div class="article_cat author_fields form-group">
                                <label>Category</label>
                                <select class="author-combobox-wrap article_author_name form-control" name="project_category"
                                        id="cat_id" required>
                                    <option value="">--Select category--</option>
                                    @foreach($project_categories as $category)
                                        <option @if($news_sub_page->project_category == $category->id) {{ "selected" }} @endif value="{{ $category->id }}">{{ $category->title }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="data1_field form-group">
                                <label for="Description">Data 1</label>
                                <input type="text" id="datapicker2" class="author-combobox-wrap form-control" name="date1" value="{{ $news_sub_page->date1 }}" autocomplete="off">
                            </div>
                            <div  class="data1_field form-group">
                                <label for="Description">Data 2</label>
                                <input type="text" id="datapicker3" class="author-combobox-wrap form-control" name="date2" value="{{ $news_sub_page->date2 }}" autocomplete="off">
                            </div>
                        @endif




                        <div class="form-group">
                            <label>Slug</label>
                            <input type="text" class="form-control slug_input" name="slug"
                                   value="{{ $news_sub_page->slug }}">

                        </div>
                        <div class="form-group">
                            <label>Add Pdf</label>
                            <div class="news_sub_page_pdf_block">
                                <div class="btn sub_page_with_pdf_add">
                                    <i class="icon-plus"></i>
                                </div>
                                <div class="page_pdf_plus_block">
                                    @if(!empty($news_sub_page_pdfs))
                                        @foreach($news_sub_page_pdfs as $news_sub_page_pdf)

                                            <div class="open_sub_pages_with_block">
                                                @foreach($languages as $key => $language)
                                                <p class="open_sub_pdf_block_title_{{ $news_sub_page_pdf->id }} open_sub_pdf_block_title_{{ $news_sub_page_pdf->id."_".$language->short }} lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif"
                                                   data-id="{{ $news_sub_page_pdf->id }}">{{ $news_sub_page_pdf->{"title_".$language->short} }}</p>
                                                @endforeach
                                                <div data-id="{{ $news_sub_page_pdf->id }}"
                                                     class="page_pdf_append_block_delete">
                                                    <i class="icon-trash"></i>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>

                        </div>
                        <input type="hidden" class="current_page_id" value="{{ $news_sub_page->id }}">
                        <input type="hidden" name="edit_news_sub_page" value="ok">


                        <div class="row">
                            <div class="col-md-12 col-sm-12 pull-left">
                                <label>Upload multiple picts (width 1000px, height 500px)</label>
                            </div>
                            <div class="col-md-12 col-lg-12">
                                <div class="form-group ">

                                    <div class="ct-u-marginBottom30">
                                        <div id="uploadzone" dir="ltr"
                                             class="dropzone  @if(count($projectImages) != '0') {{ $dropzoneDefaultHidden }} @endif">
                                            <div class="dz-default dz-message"><i class="fa fa-camera"></i>
                                                <!--<img src="/../assets/images/fileuploader-dragdrop-icon.png">-->
                                                <div class="btn_1 green"><span>Upload file</span></div>
                                                <h3 class="fileuploader-input-caption"><span>Drop files here or click to upload.</span></h3></div>
                                            @if(count($projectImages) > 0)
                                                @foreach($projectImages as $property_image)
                                                    <div class="dz-preview dz-processing dz-image-preview dz-success dz-complete">
                                                        <div class="dz-image"><img data-dz-thumbnail=""
                                                                                   alt="{{ $property_image->image }}"
                                                                                   src="{{ url('/') }}/public/uploads/gallery/150/{{ $property_image->image }}">
                                                        </div>
                                                        <div class="dz-details">
                                                            <div class="dz-filename">
                                                                <span data-dz-name="">@if(isset($property_image->image)){{ $property_image->image }} @endif</span>
                                                            </div>
                                                            <input type="hidden" data-dz-name1="" name="filename[]"
                                                                   value="{{ $property_image->image }}"
                                                                   class="dz-filename-after">
                                                        </div>
                                                        <div class="dz-progress">
                                                        <span class="dz-upload" data-dz-uploadprogress=""
                                                              style="width: 100%;"></span>
                                                        </div>
                                                        <div class="dz-error-message">
                                                            <span data-dz-errormessage=""></span>
                                                        </div>
                                                        <div class="dz-success-mark">
                                                            <svg width="54px" height="54px" viewBox="0 0 54 54"
                                                                 version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                 xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                                                                <title>Check</title>
                                                                <defs></defs>
                                                                <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                                                   fill-rule="evenodd" sketch:type="MSPage">
                                                                    <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z"
                                                                          id="Oval-2" stroke-opacity="0.198794158"
                                                                          stroke="#747474" fill-opacity="0.816519475"
                                                                          fill="#FFFFFF" sketch:type="MSShapeGroup"></path>
                                                                </g>
                                                            </svg>
                                                        </div>
                                                        <div class="dz-error-mark">
                                                            <svg width="54px" height="54px" viewBox="0 0 54 54"
                                                                 version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                 xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                                                                <title>Error</title>
                                                                <defs></defs>
                                                                <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                                                   fill-rule="evenodd" sketch:type="MSPage">
                                                                    <g id="Check-+-Oval-2" sketch:type="MSLayerGroup"
                                                                       stroke="#747474" stroke-opacity="0.198794158"
                                                                       fill="#FFFFFF" fill-opacity="0.816519475">
                                                                        <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z"
                                                                              id="Oval-2" sketch:type="MSShapeGroup"></path>
                                                                    </g>
                                                                </g>
                                                            </svg>
                                                        </div>
                                                        <a class="dz-remove old_img_remove" data-dz-remove="">Remove
                                                            file</a>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="form-group meta_block">
                            <div class="open_close_meta">
                                <p>Meta <i class="icon-arrow-combo"></i></p>
                            </div>
                            <div class="meta_box">
                                <div class="col-md-6 col-sm-6 pull-left">
                                    @foreach($languages as $language)
                                        <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                            <label>{{ $language->title }} Meta Title:</label>
                                            <input type="text" placeholder='Meta Title' class="form-control"
                                                   value='{{ $news_sub_page->{"meta_title_".$language->short} }}' id='meta_title' name='meta_title_{{ $language->short }}'/>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="col-md-6 col-sm-6 pull-left">
                                    @foreach($languages as $language)
                                        <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                            <label>{{ $language->title }} Meta Keyword:</label>
                                            <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_{{ $language->short }}'
                                                   class="form-control"
                                                   value="{{ $news_sub_page->{"meta_key_".$language->short} }}"/>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="col-md-12 col-sm-12 pull-left">
                                    @foreach($languages as $language)
                                        <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                            <label>{{ $language->title }} Meta Description</label>
                                            <textarea id="meta_desc" class="form-control" name="meta_desc_{{ $language->short }}">{{ $news_sub_page->{"meta_desc_".$language->short} }}</textarea>
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn_1 save_btn green pull-right">
                                Save
                            </button>
                        </div>


                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
<div class="sub_pages_with_pdf_append_popup">
    <div class="sub_pdf_append_block show_block">
        <div id="sub_pdf_append_head" class="sub_pdf_append_head">
            <div class="sub_pdf_append_title">Upload pdf</div>
            <div class="sub_pdf_append_dragh"></div>
            <button type="button" class="sub_with_pdf_append_close" aria-hidden="true"><i class="mce-ico mce-i-remove"></i></button>
        </div>
        @if( count($languages) > 1)
            <ul class="languages_tabs">
                @foreach($languages as $language)
                    <li data-tab="{{ $language->short }}"
                        class="lang_tab {{ $language->short."_tab" }} @if($language->first == 1) {{"active_tab"}} @endif">
                        <p><span>{{ $language->title }}</span></p>
                    </li>
                @endforeach
            </ul>
        @endif
        @foreach($languages as $language)
            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                <label for="title">{{ $language->title }} Title</label>
                <input id="about_title" type="text" class="append_page_with_pdf_file_title append_page_with_pdf_file_title_{{ $language->short }} form-control" name="pdf_title_{{ $language->short }}" value=""
                       autofocus>
            </div>
        @endforeach
        @foreach($languages as $language)
            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                <label for="Description">{{ $language->title }} Short Description</label>
                <textarea class="form-control append_page_with_pdf_file_desc append_page_with_pdf_file_desc_{{ $language->short }} short_desc" name="pdf_desc_{{ $language->short }}"></textarea>
            </div>
        @endforeach
        @php $languages_short = '' @endphp
        @foreach($languages as $language)
            @php $languages_short .= $language->short."," @endphp
            <div class="sub_pdf_append_box form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                <div class="">
                    <label>Upload {{ $language->title }} PDF</label>
                </div>
                <div class="pdf_file_title">
                    <input type="text" name="old_pdf_title_{{ $language->short }}" readonly="" class="append_page_with_pdf_change append_page_with_pdf_change_{{$language->short}} old_pdf_title" value="">
                </div>
                <div class="file-upload btn btn_1 green" style="float:left;">
                    <span>Choose file</span>
                    <input data-lang="{{ $language->short }}" type="file" name="pdf_file_{{ $language->short }}" id="page_with_pdf_file_{{ $language->short }}" class="image upload page_with_pdf_file page_with_pdf_file_{{ $language->short }}">
                </div>
            </div>
        @endforeach
        <input type="hidden" name="lang" value="{{ $languages_short }}" class="site_langs">
        <input type="hidden" class="news_sub_pdf_parent" name="parent_id" value="">
        <div class="sub_pdf_append_footer">
            <button type="button" class="page_with_pdf_append_save">Save</button>
        </div>
    </div>
</div>