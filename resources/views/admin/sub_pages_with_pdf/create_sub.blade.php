@extends('admin.layouts.page')
<link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/jquery.switch.css') }}" rel="stylesheet">
@section('content')

    <div class="container">

        <div class="form_box">
            <div class="page_title_box">
                {!! $page_bredcrump  !!}
            </div>
            @if( count($languages) > 1)
                <ul class="languages_tabs">
                    @foreach($languages as $language)
                        <li data-tab="{{ $language->short }}"
                            class="lang_tab {{ $language->short."_tab" }} @if($language->first == 1) {{"active_tab"}} @endif">
                            <p><span>{{ $language->title }}</span></p>
                        </li>
                    @endforeach
                </ul>
            @endif
            <div class="about_form">
                <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/sub_pages_with_pdf/{{ $parent_id }}/create" enctype='multipart/form-data'>
                {{ csrf_field() }}
                <!--Armenian-->

                    <div class="col-md-8">
                        @foreach($languages as $language)
                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="title">{{ $language->title }} Title</label>
                                <input id="about_title" type="text" class="form-control" name="title_{{ $language->short }}" value=""
                                       autofocus>
                            </div>
                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="Description">{{ $language->title }} Short Description</label>
                                <textarea class="form-control short_desc" name="short_desc_{{ $language->short }}"></textarea>
                            </div>
                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="Description">{{ $language->title }} Description</label>
                                <textarea class="form-control description" name="description_{{ $language->short }}"></textarea>
                            </div>
                        @endforeach
                            <div class="form-group show_in_home">
                                <label class="for_class">Show in home</label>
                                <label class="switch-light switch-ios pull-left">
                                    <input type="checkbox" name="show_in_home" class="option_hidden" value="1">
                                    <span>
							                        <span>Hide</span>
													<span>Show</span>
													</span>
                                    <a></a>
                                </label>
                            </div>

                            @if($config_project == 1)

                                <div class="form-group show_in_home">
                                    <label class="for_class">Need a donate</label>
                                    <label class="switch-light switch-ios pull-left">
                                        <input type="checkbox" name="need_donate" class="option_hidden" value="1">
                                        <span>
							                        <span>No</span>
													<span>Yes</span>
													</span>
                                        <a></a>
                                    </label>
                                </div>


                                <input type="hidden" name="config_project" value="1">
                                <div class="article_cat author_fields form-group">
                                    <label>Category</label>
                                    <select class="author-combobox-wrap article_author_name form-control" name="project_category"
                                            id="cat_id" required>
                                        <option value="">--Select category--</option>
                                        @foreach($project_categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->title }}</option>
                                        @endforeach
                                    </select>
                                </div>


                                <div class="data1_field form-group">
                                    <label for="Description">Data 1</label>
                                    <input type="text" id="datapicker2" class="author-combobox-wrap form-control" name="date1" value="" autocomplete="off">
                                </div>
                                <div  class="data1_field form-group">
                                    <label for="Description">Data 2</label>
                                    <input type="text" id="datapicker3" class="author-combobox-wrap form-control" name="date2" value="" autocomplete="off">
                                </div>
                            @endif
                            
                        <div class="form-group">
                            <label>Add Pdf</label>
                            <div class="news_sub_page_pdf_block">
                                <div class="btn sub_page_with_pdf_add">
                                    <i class="icon-plus"></i>
                                </div>
                                <div class="page_pdf_plus_block">

                                </div>
                            </div>

                        </div>

                            <div class="form-group gallery_box">
                                <label>Upload multiple picts (width 1000px, height 500px)</label>
                                <div id="uploadzone" class="dropzone" dir="ltr"></div>
                            </div>

                        <div class="form-group meta_block">
                            <div class="open_close_meta">
                                <p>Meta <i class="icon-arrow-combo"></i></p>
                            </div>
                            <div class="meta_box">
                                <div class="col-md-6 col-sm-6 pull-left">
                                    @foreach($languages as $language)
                                        <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                            <label>{{ $language->title }} Meta Title:</label>
                                            <input type="text" placeholder='Meta Title' class="form-control"
                                                   value='' id='meta_title' name='meta_title_{{ $language->short }}'/>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="col-md-6 col-sm-6 pull-left">
                                    @foreach($languages as $language)
                                        <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                            <label>{{ $language->title }} Meta Keyword:</label>
                                            <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_{{ $language->short }}'
                                                   class="form-control"
                                                   value=""/>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="col-md-12 col-sm-12 pull-left">
                                    @foreach($languages as $language)
                                        <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                            <label>{{ $language->title }} Meta Description</label>
                                            <textarea id="meta_desc" class="form-control" name="meta_desc_{{ $language->short }}"></textarea>
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                        </div>
                        <input type="hidden" name="add_news_sub_page" value="ok">
                            @if(isset($_GET['sub_id']) && is_numeric($_GET['sub_id']))
                                <input type="hidden" name="folder_parent" value="{{ $_GET['sub_id'] }}" >
                               @endif
                        <div class="col-md-12">
                            <button type="submit" class="btn_1 save_btn green pull-right">
                                Save
                            </button>
                        </div>
                    </div>








                </form>
            </div>
        </div>

    </div>
@endsection

<div class="sub_pages_with_pdf_append_popup">
    <div class="sub_pdf_append_block show_block">
        <div id="sub_pdf_append_head" class="sub_pdf_append_head">
            <div class="sub_pdf_append_title">Upload pdf</div>
            <div class="sub_pdf_append_dragh"></div>
            <button type="button" class="sub_with_pdf_append_close" aria-hidden="true"><i class="mce-ico mce-i-remove"></i></button>
        </div>
        @if( count($languages) > 1)
            <ul class="languages_tabs">
                @foreach($languages as $language)
                    <li data-tab="{{ $language->short }}"
                        class="lang_tab {{ $language->short."_tab" }} @if($language->first == 1) {{"active_tab"}} @endif">
                        <p><span>{{ $language->title }}</span></p>
                    </li>
                @endforeach
            </ul>
        @endif
        @foreach($languages as $language)
            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                <label for="title">{{ $language->title }} Title</label>
                <input id="about_title" type="text" class="append_page_with_pdf_file_title append_page_with_pdf_file_title_{{ $language->short }} form-control" name="pdf_title_{{ $language->short }}" value=""
                       autofocus>
            </div>
        @endforeach
        @foreach($languages as $language)
            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                <label for="Description">{{ $language->title }} Short Description</label>
                <textarea class="form-control append_page_with_pdf_file_desc append_page_with_pdf_file_desc_{{ $language->short }} short_desc" name="pdf_desc_{{ $language->short }}"></textarea>
            </div>
        @endforeach
        @php $languages_short = '' @endphp
        @foreach($languages as $language)
            @php $languages_short .= $language->short."," @endphp
            <div class="sub_pdf_append_box form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                <div class="">
                    <label>Upload {{ $language->title }} PDF</label>
                </div>
                <div class="pdf_file_title">
                    <input type="text" name="old_pdf_title_{{ $language->short }}" readonly="" class="append_page_with_pdf_change append_page_with_pdf_change_{{$language->short}} old_pdf_title" value="">
                </div>
                <div class="file-upload btn btn_1 green" style="float:left;">
                    <span>Choose file</span>
                    <input data-lang="{{ $language->short }}" type="file" name="pdf_file_{{ $language->short }}" id="page_with_pdf_file_{{ $language->short }}" class="image upload page_with_pdf_file page_with_pdf_file_{{ $language->short }}">
                </div>
            </div>
        @endforeach
        <input type="hidden" name="lang" value="{{ $languages_short }}" class="site_langs">
        <input type="hidden" class="news_sub_pdf_parent" name="parent_id" value="">
        <div class="sub_pdf_append_footer">
            <button type="button" class="page_with_pdf_append_save">Save</button>
        </div>
    </div>
</div>
