@extends('admin.layouts.page')

@section('content')

    <div class="container">
              <div class="form_box">
                    <div class="page_title_box">
                        {!! $page_bredcrump  !!}
                    </div>
                    @if( count($languages) > 1)
                        <ul class="languages_tabs">
                            @foreach($languages as $language)
                                <li data-tab="{{ $language->short }}" class="lang_tab {{ $language->short."_tab" }} @if($language->first == 1) {{"active_tab"}} @endif">
                                    <p><span>{{ $language->title }}</span></p>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                    <div class="about_form">
                            <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/page_gallery/add/{{ $parent_id }}" enctype='multipart/form-data'>
                            {{ csrf_field() }}
                            <div class="col-md-8 page_left">

                                @foreach($languages as $language)
                                    <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                        <label for="title">{{ $language->title }} Title</label>
                                        <input id="about_title" type="text" class="form-control" name="title_{{ $language->short }}" value=""
                                               autofocus>
                                    </div>

                                    <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                        <label for="Description">{{ $language->title }} Short Description</label>
                                        <textarea class="form-control short_desc" name="short_desc_{{ $language->short }}"></textarea>
                                    </div>
                                @endforeach

                                <div class="form-group gallery_box">
                                    <label>Upload multiple picts (width 1000px, height 500px)</label>
                                    <div id="uploadzone" class="dropzone" dir="ltr"></div>
                                </div>
                                <div class="col-md-12">
                                    <input type="hidden" name="add_gallery" value="ok">
                                    <button type="submit" class="btn_1 green pull-right">
                                        Save
                                    </button>
                                </div>
                            </div>


                            </form>
                        </div>
                </div>


    </div>
@endsection
