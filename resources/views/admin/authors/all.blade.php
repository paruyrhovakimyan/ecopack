@extends('admin.layouts.page')
<link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/jquery.switch.css') }}" rel="stylesheet">
@section('content')

    <div class="container services_block">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Authors</h3>
                        <a class="btn_1 green pull-right" href="{{ url('/') }}/admin/authors/create">Add Author</a>
                    </div>
                    <div class="about_form">
                        <form class="form-horizontal" method="POST" action="http://foc.loc/admin/about_us">
                            <input type="hidden" name="_token" value="U1AwYoBVfuvUYPcIFGJM9WGm3t1o6xR1K7vLExuD">
                        </form>
                        <section id="section-1" class="content-current">
                            @foreach($authors as $author)
                                <div class="strip_booking strip_project{{ $author->id }}">
                                    <div class="row">
                                        <div class="authors_img_box col-md-2 col-sm-2">
                                            @if($author->image !='')
                                                <img src="{{ url('/') }}/uploads/authors/{{$author->image}}" >
                                            @else
                                                <span class="pe-7s-user"></span>
                                            @endif
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <h3 class="">{{ $author->name }}</h3>
                                        </div>
                                        <div class="col-md-2 col-sm-3">
                                            <ul class="info_booking">
                                                <li><strong>User id</strong> {{ $author->id }}</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <div class="booking_buttons">
                                                <a href="{{ url('/') }}/admin/authors/{{ $author->id }}/edit" class="btn_2">Edit</a>
                                                <a data-id="{{ $author->id }}" class="author_delete btn_delete btn-danger">Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End row -->
                                </div>
                            @endforeach
                        </section>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
