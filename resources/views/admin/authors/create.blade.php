@extends('admin.layouts.page')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3>New Author</h3></div>
                </div>
            </div>
        </div>
        <div class="form_block">
            <ul class="languages_tabs">
                @if($settings->lang_am == 1)
                    <li class="arm_tab @if(config('app.locale') == 'am') {{"active_tab"}} @endif">
                        <p><span>Armenian</span></p>
                    </li>
                @endif
                @if($settings->lang_ru == 1)
                    <li class="rus_tab @if(config('app.locale') == 'ru') {{"active_tab"}} @endif">
                        <p><span>RUSSIAN</span></p>
                    </li>
                @endif
                @if($settings->lang_en == 1)
                    <li class="eng_tab @if(config('app.locale') == 'en') {{"active_tab"}} @endif">
                        <p><span>ENGLISH</span></p>
                    </li>
                @endif
            </ul>
            <div class="form_box">
                <div class="panel panel-default">
                    <div class="about_form">
                        <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/authors" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <!--Armenian-->
                        <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                            <label for="title">Armenian Name</label>
                            <input id="about_title" type="text" class="form-control" name="title" value=""
                                   autofocus>
                        </div>
                        <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                            <label for="Description">Armenian Description</label>
                            <textarea class="form-control description" name="desc"></textarea>
                        </div>
                        <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="row arm_block">
                            <div class="col-md-12 col-sm-12 pull-left">
                                <h4> META HEADERS </h4>
                            </div>
                            <div class="col-md-6 col-sm-6 pull-left">

                                <div class="form-group">
                                    <label>Armenian Meta Title:</label>
                                    <input type="text" placeholder='Meta Title' class="form-control"
                                           value='' id='meta_title' name='meta_title'/>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 pull-left">
                                <div class="form-group">
                                    <label>Armenian Meta Keyword:</label>
                                    <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key'
                                           class="form-control"
                                           value=""/>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 pull-left">
                                <div class="form-group">
                                    <label>Armenian Meta Description</label>
                                    <textarea id="meta_desc" class="form-control" name="meta_desc"></textarea>
                                </div>
                            </div>
                        </div>
                        <!--Armenian-->

                        <!--Russian-->
                        <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block">
                            <label for="title">Russian Name</label>
                            <input id="about_title" type="text" class="form-control" name="title_ru" value="" autofocus>

                        </div>
                        <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block">
                            <label for="Description">Russian Description</label>
                            <textarea class="form-control description" name="desc_ru"></textarea>
                        </div>
                        <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="row rus_block">
                            <div class="col-md-12 col-sm-12 pull-left">
                                <h4> META HEADERS </h4>
                            </div>
                            <div class="col-md-6 col-sm-6 pull-left">

                                <div class="form-group">
                                    <label>Russian Meta Title:</label>
                                    <input type="text" placeholder='Meta Title' class="form-control"
                                           value='' id='meta_title' name='meta_title_ru'/>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 pull-left">
                                <div class="form-group">
                                    <label>Russian Meta Keyword:</label>
                                    <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_ru'
                                           class="form-control"
                                           value=""/>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 pull-left">
                                <div class="form-group">
                                    <label>Russian Meta Description</label>
                                    <textarea id="meta_desc" class="form-control" name="meta_desc_ru"></textarea>
                                </div>
                            </div>
                        </div>
                        <!--Russian-->

                        <!--English-->
                        <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                            <label for="title">English Name</label>
                            <input id="about_title" type="text" class="form-control" name="title_en" value=""
                                   autofocus>
                        </div>
                        <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block {{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="Description">English Description</label>
                            <textarea class="form-control description" name="desc_en"></textarea>
                        </div>
                        <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="row eng_block">
                            <div class="col-md-12 col-sm-12 pull-left">
                                <h4> META HEADERS </h4>
                            </div>
                            <div class="col-md-6 col-sm-6 pull-left">

                                <div class="form-group">
                                    <label>English Meta Title:</label>
                                    <input type="text" placeholder='Meta Title' class="form-control"
                                           value='' id='meta_title' name='meta_title_en'/>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 pull-left">
                                <div class="form-group">
                                    <label>English Meta Keyword:</label>
                                    <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_en'
                                           class="form-control"
                                           value=""/>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 pull-left">
                                <div class="form-group">
                                    <label>English Meta Description</label>
                                    <textarea id="meta_desc" class="form-control" name="meta_desc_en"></textarea>
                                </div>
                            </div>
                        </div>
                        <!--English-->
                         <div class="col-md-4 col-sm-4 pull-left">
                             <div class="form-group">
                                 <label>E-mail</label>
                                 <input type="text" placeholder='Enter E-mail' id='author_mail' name='mail'
                                        class="form-control"
                                        value=""/>
                             </div>
                         </div>
                            <div class="col-md-2 author_fields col-sm-2 pull-left">
                                <div class="form-group">
                                    <label>Gender</label>
                                    <div class="author-tail clearfix">
                                        <div class="author-combobox-wrap">
                                            <select class="form-control" name="gender">
                                                <option value="" selected="">-</option>
                                                <option value="male">Male</option>
                                                <option value="female">Female</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        <div class="col-md-6 col-sm-6  pull-left">
                            <div class="form-group author_fields">
                                <label>Birthday</label>
                                <div class="author-tail clearfix">
                                    <div class="author-date-select">
                                        <div class="author-combobox-wrap author-form-field-alt-date">
                                            <select type="text" name="birth_day" class="form-control" data-type="date">
                                                <option value="" selected="">Date</option>
                                                @for ($i = 1; $i < 32; $i++)
                                                    @if($i < 10)
                                                        <option value="{{ "0".$i }}">{{ "0".$i }}</option>
                                                    @else
                                                        <option value="{{ $i }}">{{ $i }}</option>
                                                    @endif
                                                @endfor
                                            </select></div>
                                        <div class="author-combobox-wrap author-form-field-alt-month">
                                            <select type="text" name="birth_month" class="form-control " data-type="month">

                                                <option value="" selected="">Month</option>
                                                <option value="01">January</option>
                                                <option value="02">February</option>
                                                <option value="03">March</option>
                                                <option value="04">April</option>
                                                <option value="05">May</option>
                                                <option value="06">June</option>
                                                <option value="07">July</option>
                                                <option value="08">August</option>
                                                <option value="09">September</option>
                                                <option value="10">October</option>
                                                <option value="11">November</option>
                                                <option value="12">December</option>
                                            </select></div>
                                        <div class="author-combobox-wrap author-form-field-alt-year">
                                            <select type="text" name="birth_year" class="form-control" data-type="year">
                                                <option value="" selected="">Year</option>
                                                @for ($i = date('Y'); $i >=1898 ; $i--)
                                                    <option value="{{ $i }}">{{ $i }}</option>
                                                @endfor
                                            </select></div>
                                        <input type="hidden" name="birth_date" value="">
                                    </div>
                                </div>
                            </div>
                        </div>

                            <div class="form-group">
                                <div class="company_logo">
                                    <div class="col-md-5 col-sm-5 pull-left title_present">
                                        <div class="choose_img_title">
                                            <h4>Author image <small>(width:250px, height:250px)</small></h4>
                                        </div>
                                        <div class="avatar_img">
                                            <div class="file-upload btn btn_1 green">
                                                <span>Choose image</span>
                                                <input type="file" name="image" id="uploadBtnLogo" class="image upload"
                                                       onchange="readURLLogo(this)">
                                            </div>
                                        </div>
                                        <div class="profile-image-preview_logo">
                                            <div class="ct-media--left">
                                                <a>
                                                    <img id="uploadPreviewLogo" src="">
                                                </a>
                                            </div>
                                            <div class="ct-media--content">
                                                <span></span>
                                                <a class="cross" onclick="deleteimgLogo();" style="cursor:pointer;">
                                                    <i class="fa fa-trash-o"></i> Delete
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <input type="hidden" name="add_author" value="ok">
                            <button type="submit" class="btn_1 green pull-right">
                                Save
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
