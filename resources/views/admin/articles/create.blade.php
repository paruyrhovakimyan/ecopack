@extends('admin.layouts.page')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3>New Article</h3></div>
                </div>
            </div>
        </div>
        <div class="form_block">
            <ul class="languages_tabs">
                @if($settings->lang_am == 1)
                    <li class="arm_tab @if(config('app.locale') == 'am') {{"active_tab"}} @endif">
                        <p><span>Armenian</span></p>
                    </li>
                @endif
                @if($settings->lang_ru == 1)
                    <li class="rus_tab @if(config('app.locale') == 'ru') {{"active_tab"}} @endif">
                        <p><span>RUSSIAN</span></p>
                    </li>
                @endif
                @if($settings->lang_en == 1)
                    <li class="eng_tab @if(config('app.locale') == 'en') {{"active_tab"}} @endif">
                        <p><span>ENGLISH</span></p>
                    </li>
                @endif
            </ul>
            <div class="form_box">
                <div class="panel panel-default">
                    <div class="about_form">
                        <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/articles"
                              enctype='multipart/form-data'>
                        {{ csrf_field() }}

                            <!--Armenian-->
                            <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                                <label for="title">Armenian Title</label>
                                <input id="about_title" type="text" class="form-control" name="title" value=""
                                       autofocus>
                            </div>
                            <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                                <label for="Description">Armenian Short Description</label>
                                <textarea class="form-control short_desc" name="short_desc"></textarea>
                            </div>
                            <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                                <label for="Description">Armenian Description</label>
                                <textarea class="form-control description" name="desc"></textarea>
                            </div>
                            <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="row arm_block">
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <h4> META HEADERS </h4>
                                </div>
                                <div class="col-md-6 col-sm-6 pull-left">

                                    <div class="form-group">
                                        <label>Armenian Meta Title:</label>
                                        <input type="text" placeholder='Meta Title' class="form-control"
                                               value='' id='meta_title' name='meta_title'/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 pull-left">
                                    <div class="form-group">
                                        <label>Armenian Meta Keyword:</label>
                                        <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key'
                                               class="form-control"
                                               value=""/>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <div class="form-group">
                                        <label>Armenian Meta Description</label>
                                        <textarea id="meta_desc" class="form-control" name="meta_desc"></textarea>
                                    </div>
                                </div>
                            </div>
                            <!--Armenian-->



                            <!--Russian-->
                            <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block">
                                <label for="title">Russian Title</label>
                                <input id="about_title" type="text" class="form-control" name="title_ru" value=""
                                       autofocus>
                            </div>
                            <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block">
                                <label for="Description">Russian Short Description</label>
                                <textarea class="form-control short_desc" name="short_desc_ru"></textarea>
                            </div>
                            <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block">
                                <label for="Description">Russian Description</label>
                                <textarea class="form-control description" name="desc_ru"></textarea>
                            </div>
                            <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="row rus_block">
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <h4> META HEADERS </h4>
                                </div>
                                <div class="col-md-6 col-sm-6 pull-left">

                                    <div class="form-group">
                                        <label>Russian Meta Title:</label>
                                        <input type="text" placeholder='Meta Title' class="form-control"
                                               value='' id='meta_title' name='meta_title_ru'/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 pull-left">
                                    <div class="form-group">
                                        <label>Russian Meta Keyword:</label>
                                        <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_ru'
                                               class="form-control"
                                               value=""/>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <div class="form-group">
                                        <label>Russian Meta Description</label>
                                        <textarea id="meta_desc" class="form-control" name="meta_desc_ru"></textarea>
                                    </div>
                                </div>
                            </div>
                            <!--Russian-->

                            <!--English-->
                            <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                <label for="title">English Title</label>
                                <input id="about_title" type="text" class="form-control" name="title_en" value=""
                                       autofocus>
                            </div>
                            <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                <label for="Description">English Short Description</label>
                                <textarea class="form-control short_desc" name="short_desc_en"></textarea>
                            </div>
                            <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                <label for="Description">English Description</label>
                                <textarea class="form-control description" name="desc_en"></textarea>
                            </div>
                            <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="row eng_block">
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <h4> META HEADERS </h4>
                                </div>
                                <div class="col-md-6 col-sm-6 pull-left">

                                    <div class="form-group">
                                        <label>English Meta Title:</label>
                                        <input type="text" placeholder='Meta Title' class="form-control"
                                               value='' id='meta_title' name='meta_title_en'/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 pull-left">
                                    <div class="form-group">
                                        <label>English Meta Keyword:</label>
                                        <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_en'
                                               class="form-control"
                                               value=""/>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <div class="form-group">
                                        <label>English Meta Description</label>
                                        <textarea id="meta_desc" class="form-control" name="meta_desc_en"></textarea>
                                    </div>
                                </div>
                            </div>
                            <!--English-->

                            <div class="article_cat author_fields form-group">
                                <label>Category</label>
                                <select class="author-combobox-wrap article_author_name form-control" name="cat_id"
                                        id="cat_id" required>
                                    <option value="">--Select category--</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->title_am }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <!--Authors-->
                            <div class="form-group add_authors">
                                <div class="">
                                    <h4>Add authors</h4>
                                </div>
                                <div class="btn-primary author_plus">+</div>
                                <div class="article_authors_block">

                                </div>
                            </div>
                            <!--Authors-->
                            <div class="row media_link" style="display: none;">
                                <div class="col-md-7 col-sm-7 pull-left">
                                    <div class="form-group">
                                        <label>Video link</label>
                                        <input type="text" placeholder='Enter video url' id='author_mail'
                                               name='video_link'
                                               class="form-control"
                                               value=""/>
                                    </div>
                                </div>
                            </div>


                            <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="not_media_box arm_block row">
                                <div class="company_logo">
                                    <div class="col-md-5 col-sm-5 pull-left title_present">
                                        <div class="">
                                            <h4>Article image</h4>
                                        </div>
                                        <div class="avatar_img">
                                            <div class="file-upload btn btn_1 green">
                                                <span>Choose image</span>
                                                <input type="file" name="image" id="uploadBtnLogo" class="image upload"
                                                       onchange="readURLLogo(this)">
                                            </div>
                                        </div>
                                        <div class="profile-image-preview_logo">
                                            <div class="ct-media--left">
                                                <a>
                                                    <img id="uploadPreviewLogo" src="">
                                                </a>
                                            </div>
                                            <div class="ct-media--content">
                                                <span></span>
                                                <a class="cross" onclick="deleteimgLogo();" style="cursor:pointer;">
                                                    <i class="fa fa-trash-o"></i> Delete
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="not_media_box row rus_block">
                                <div class="company_logo">
                                    <div class="col-md-5 col-sm-5 pull-left title_present">
                                        <div class="">
                                            <h4>Russian Article image</h4>
                                        </div>
                                        <div class="avatar_img">
                                            <div class="file-upload btn btn_1 green">
                                                <span>Choose image</span>
                                                <input type="file" name="image_ru" id="uploadBtnLogo_ru"
                                                       class="image upload"
                                                       onchange="readURLLogo_ru(this)">
                                            </div>
                                        </div>
                                        <div class="profile-image-preview_logo_ru">
                                            <div class="ct-media--left">
                                                <a>
                                                    <img id="uploadPreviewLogo_ru" src="">
                                                </a>
                                            </div>
                                            <div class="ct-media--content">
                                                <span></span>
                                                <a class="cross" onclick="deleteimgLogoru();" style="cursor:pointer;">
                                                    <i class="fa fa-trash-o"></i> Delete
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="not_media_box row eng_block">
                                <div class="company_logo">
                                    <div class="col-md-5 col-sm-5 pull-left title_present">
                                        <div class="">
                                            <h4>English article image</h4>
                                        </div>
                                        <div class="avatar_img">
                                            <div class="file-upload btn btn_1 green">
                                                <span>Choose image</span>
                                                <input type="file" name="image_en" id="uploadBtnLogo_en"
                                                       class="image upload"
                                                       onchange="readURLLogo_en(this)">
                                            </div>
                                        </div>
                                        <div class="profile-image-preview_logo_en">
                                            <div class="ct-media--left">
                                                <a>
                                                    <img id="uploadPreviewLogo_en" src="">
                                                </a>
                                            </div>
                                            <div class="ct-media--content">
                                                <span></span>
                                                <a class="cross" onclick="deleteimgLogo_en();" style="cursor:pointer;">
                                                    <i class="fa fa-trash-o"></i> Delete
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="not_media_box form-group">
                                <div class="">
                                    <h4>Article pdf</h4>
                                </div>
                                <div class="btn-primary pdf_plus">+</div>
                                <div class="article_pdf_block">

                                </div>
                            </div>

                            <input type="hidden" name="add_article" value="ok">
                            <button type="submit" class="btn_1 green pull-right">
                                Save
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
