@extends('admin.layouts.page')

@if(isset($data['edit_successful']))
    <div class="edit_successful">
        <p>The articles updated</p>
    </div>
@endif

@section('content')

    <div class="container">
        <div class="row edit_category">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span>Edit article: </span><h3>{{ $article->title_am  }}</h3>
                        <a class="btn_1 green pull-right" href="{{ route('articles.index') }}">Back to Articles</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="form_block">
            <ul class="languages_tabs">
                @if($settings->lang_am == 1)
                    <li class="arm_tab @if(config('app.locale') == 'am') {{"active_tab"}} @endif">
                        <p><span>Armenian</span></p>
                    </li>
                @endif
                @if($settings->lang_ru == 1)
                    <li class="rus_tab @if(config('app.locale') == 'ru') {{"active_tab"}} @endif">
                        <p><span>RUSSIAN</span></p>
                    </li>
                @endif
                @if($settings->lang_en == 1)
                    <li class="eng_tab @if(config('app.locale') == 'en') {{"active_tab"}} @endif">
                        <p><span>ENGLISH</span></p>
                    </li>
                @endif
            </ul>
            <div class="form_box">
                <div class="panel panel-default">

                    <div class="about_form">
                        <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/articles/{{ $article->id }}" enctype='multipart/form-data'>
                        {{method_field('PUT')}}
                        {{ csrf_field() }}
                        <!--Armenian-->
                            <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                                <label for="title">Armenian Title</label>
                                <input id="about_title" type="text" class="form-control" name="title" value="{{ $article->title_am }}"
                                       autofocus>
                            </div>
                            <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                                <label for="Description">Armenian Short Description</label>
                                <textarea class="form-control short_desc" name="short_desc">{{ $article->short_desc_am }}</textarea>
                            </div>
                            <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                                <label for="Description">Armenian Description</label>
                                <textarea class="form-control description" name="desc">{{ $article->description_am }}</textarea>
                            </div>
                            <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="row arm_block">
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <h4> META HEADERS </h4>
                                </div>
                                <div class="col-md-6 col-sm-6 pull-left">

                                    <div class="form-group">
                                        <label>Armenian Meta Title:</label>
                                        <input type="text" placeholder='Meta Title' class="form-control"
                                               value='{{ $article->meta_title_am }}' id='meta_title' name='meta_title'/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 pull-left">
                                    <div class="form-group">
                                        <label>Armenian Meta Keyword:</label>
                                        <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key'
                                               class="form-control"
                                               value="{{ $article->meta_key_am }}"/>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <div class="form-group">
                                        <label>Armenian Meta Description</label>
                                        <textarea id="meta_desc" class="form-control" name="meta_desc">{{ $article->meta_desc_am }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <!--Armenian-->

                            <!--Russian-->
                            <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block">
                                <label for="title">Russian Title</label>
                                <input id="about_title" type="text" class="form-control" name="title_ru" value="{{ $article->title_ru }}"
                                       autofocus>

                            </div>
                            <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block">
                                <label for="Description">Russian Short Description</label>
                                <textarea class="form-control short_desc" name="short_desc_ru">{{ $article->short_desc_ru }}</textarea>
                            </div>
                            <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block">
                                <label for="Description">Russian Description</label>
                                <textarea class="form-control description" name="desc_ru">{{ $article->description_ru }}</textarea>
                            </div>
                            <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="row rus_block">
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <h4> META HEADERS </h4>
                                </div>
                                <div class="col-md-6 col-sm-6 pull-left">

                                    <div class="form-group">
                                        <label>Russian Meta Title:</label>
                                        <input type="text" placeholder='Meta Title' class="form-control"
                                               value='{{ $article->meta_title_ru }}' id='meta_title' name='meta_title_ru'/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 pull-left">
                                    <div class="form-group">
                                        <label>Russian Meta Keyword:</label>
                                        <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_ru'
                                               class="form-control"
                                               value="{{ $article->meta_key_ru }}"/>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <div class="form-group">
                                        <label>Russian Meta Description</label>
                                        <textarea id="meta_desc" class="form-control" name="meta_desc_ru">{{ $article->meta_desc_ru }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <!--Russian-->

                            <!--English-->
                            <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                <label for="title">English Title</label>
                                <input id="about_title" type="text" class="form-control" name="title_en" value="{{ $article->title_en }}"
                                       autofocus>
                            </div>
                            <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                <label for="Description">English Short Description</label>
                                <textarea class="form-control short_desc" name="short_desc_en">{{ $article->short_desc_en }}</textarea>
                            </div>
                            <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                <label for="Description">English Description</label>
                                <textarea class="form-control description" name="desc_en">{{ $article->description_en }}</textarea>
                            </div>
                            <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="row eng_block">
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <h4> META HEADERS </h4>
                                </div>
                                <div class="col-md-6 col-sm-6 pull-left">

                                    <div class="form-group">
                                        <label>English Meta Title:</label>
                                        <input type="text" placeholder='Meta Title' class="form-control"
                                               value='{{ $article->meta_title_en }}' id='meta_title' name='meta_title_en'/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 pull-left">
                                    <div class="form-group">
                                        <label>English Meta Keyword:</label>
                                        <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_en'
                                               class="form-control"
                                               value="{{ $article->meta_key_en }}"/>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <div class="form-group">
                                        <label>English Meta Description</label>
                                        <textarea id="meta_desc" class="form-control" name="meta_desc_en">{{ $article->meta_desc_en }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <!--English-->


                            <div class="article_cat author_fields form-group">
                                <label>Category</label>
                                <select class="author-combobox-wrap article_author_name form-control" name="cat_id" id="cat_id" required>
                                    <option value="">--Select category--</option>
                                    @foreach($categories as $category)
                                        <option @if($article->cat_id == $category->id) {{ "selected" }} @endif  value="{{ $category->id }}">{{ $category->title_am }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <!--Authors-->
                                <div class="form-group add_authors">
                                    <div class="">
                                        <h4>Add authors</h4>
                                    </div>
                                    <div class="btn-primary author_plus">+</div>
                                    <div class="article_authors_block">
                                        @foreach($article_authors as $article_author)
                                            <div class="new_author_block author_fields">
                                                <input style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" type="text" placeholder="Arminian author name" class="arm_block author_nik_name form-control" name="author_nik_name[]" value="{{ $article_author->author_name_am }}">
                                                <input style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" type="text" placeholder="Russian author name" class="rus_block author_nik_name form-control" name="author_nik_name_ru[]" value="{{ $article_author->author_name_ru }}">
                                                <input style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" type="text" placeholder="English author name" class="eng_block author_nik_name form-control" name="author_nik_name_en[]" value="{{ $article_author->author_name_en }}">
                                                <select name="author_name[]" class="author-combobox-wrap form-control article_author_name">
                                                    <option value="0" >--Select author--</option>
                                                    <?php
                                                    foreach ($all_authors as $one_author){
                                                    ?>
                                                    <option @if($article_author->author_id == $one_author->id) {{ "selected" }} @endif value="{{$one_author->id }}" >{{$one_author->name_am}}</option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                                <div onclick="remove_from_authors(this)" class="del_author_block"><i class="fa fa-trash-o"></i></div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            <!--Authors-->




                            <div class="row media_link @if($article->cat_id != 1) {{ "not_media" }} @endif" >
                                <div class="col-md-7 col-sm-7 pull-left">
                                    <div class="form-group">
                                        <label>Video link</label>
                                        <input type="text" placeholder='Enter video url' id='author_mail' name='video_link'
                                               class="form-control"
                                               value="{{ $article->video }}"/>
                                    </div>
                                </div>
                            </div>





                            <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="not_media_box arm_block row @if($article->cat_id == 1) {{ "not_media" }} @endif">
                                <div class="company_logo">
                                    <div class="col-md-5 col-sm-5 pull-left title_present">
                                        <div class="">
                                            <h4>Articles image</h4>
                                        </div>
                                        <div class="avatar_img">
                                            <div class="file-upload btn btn_1 green">
                                                <span>Choose image</span>
                                                <input type="file" name="image" id="uploadBtnLogo" class="image upload"
                                                       onchange="readURLEditLogo(this)">
                                            </div>
                                        </div>
                                        <div class="profile-image-preview_logo @if($article->image_am !='') {{ "active" }} @endif">
                                            <div class="ct-media--left">
                                                <a>
                                                    <img id="uploadPreviewLogo" src="/uploads/articles/150/{{ $article->image_am }}">
                                                </a>
                                            </div>
                                            <input type="hidden" name="old_image" class="old_image" value="{{ $article->image_am }}">
                                            <div class="ct-media--content">
                                                <span></span>
                                                <a class="cross" onclick="deleteimgeditLogo();" style="cursor:pointer;">
                                                    <i class="fa fa-trash-o"></i> Delete
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="not_media_box @if($article->cat_id == 1) {{ "not_media" }} @endif row rus_block">
                                <div class="company_logo">
                                    <div class="col-md-5 col-sm-5 pull-left title_present">
                                        <div class="">
                                            <h4>Russian Article image</h4>
                                        </div>
                                        <div class="avatar_img">
                                            <div class="file-upload btn btn_1 green">
                                                <span>Choose image</span>
                                                <input type="file" name="image_ru" id="uploadBtnLogo_ru" class="image upload"
                                                       onchange="readURLLogo_ru(this)">
                                            </div>
                                        </div>
                                        <div class="profile-image-preview_logo_ru @if($article->image_ru !='') {{ "active" }}@endif">
                                            <div class="ct-media--left">
                                                <a>
                                                    <img id="uploadPreviewLogo_ru" src="/uploads/articles/ru/150/{{ $article->image_ru }}">
                                                </a>
                                            </div>
                                            <input type="hidden" name="old_image_ru" class="old_image" value="{{ $article->image_ru }}">
                                            <div class="ct-media--content">
                                                <span></span>
                                                <a class="cross" onclick="deleteimgLogoru();" style="cursor:pointer;">
                                                    <i class="fa fa-trash-o"></i> Delete
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div  style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="not_media_box @if($article->cat_id == 1) {{ "not_media" }} @endif row eng_block">
                                <div class="company_logo">
                                    <div class="col-md-5 col-sm-5 pull-left title_present">
                                        <div class="">
                                            <h4>English article image</h4>
                                        </div>
                                        <div class="avatar_img">
                                            <div class="file-upload btn btn_1 green">
                                                <span>Choose image</span>
                                                <input type="file" name="image_en" id="uploadBtnLogo_en" class="image upload"
                                                       onchange="readURLLogo_en(this)">
                                            </div>
                                        </div>
                                        <div class="profile-image-preview_logo_en @if($article->image_en !='') {{ "active" }} @endif">
                                            <div class="ct-media--left">
                                                <a>
                                                    <img id="uploadPreviewLogo_en" src="/uploads/articles/en/150/{{ $article->image_en }}">
                                                </a>
                                            </div>
                                            <input type="hidden" name="old_image_en" class="old_image" value="{{ $article->image_en }}">
                                            <div class="ct-media--content">
                                                <span></span>
                                                <a class="cross" onclick="deleteimgLogo_en();" style="cursor:pointer;">
                                                    <i class="fa fa-trash-o"></i> Delete
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="not_media_box @if($article->cat_id == 1) {{ "not_media" }} @endif form-group">
                                <div class="">
                                    <h4>Article pdf</h4>
                                </div>
                                <div class="btn-primary pdf_plus">+</div>
                                <div class="article_pdf_block article_edit_pdf_block">
                                    @foreach($article_pdfs as $article_pdf)
                                        <div class="new_author_block author_fields">
                                            <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="isset_pdf arm_block">
                                                @if($article_pdf->pdf_file_am !='')
                                                    <a target="_blank" href="{{ url('/') }}/uploads/articles/pdf/{{ $article_pdf->pdf_file_am }}">{{ $article_pdf->pdf_name_am }}</a>
                                                @endif
                                            </div>
                                            <div class="isset_pdf rus_block" style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif">
                                                @if($article_pdf->pdf_file_en !='')
                                                    <a target="_blank" href="{{ url('/') }}/uploads/articles/pdf_ru/{{ $article_pdf->pdf_file_ru }}">{{ $article_pdf->pdf_name_ru }}</a>
                                                @endif
                                            </div>
                                            <div class="isset_pdf eng_block" style="@if(config('app.locale') != 'en') {{"display: none"}} @endif">
                                                @if($article_pdf->pdf_file_en !='')
                                                    <a target="_blank" href="{{ url('/') }}/uploads/articles/pdf_en/{{ $article_pdf->pdf_file_en }}">{{ $article_pdf->pdf_name_en }}</a>
                                                @endif
                                            </div>


                                            <div class="arm_block" style="@if(config('app.locale') != 'am') {{"display: none"}} @endif">
                                                <input type="file" name="old_pdf[]" class="article_pdf_file" >
                                                <input type="text" placeholder="Arminian Pdf title" class="article_pdf_title form-control" name="old_pdf_title[]" value="{{  $article_pdf->pdf_name_am }}">
                                                <input type="hidden" name="old_pdf_file[]" value="{{  $article_pdf->pdf_file_am }}" >
                                            </div>
                                            <div class="rus_block" style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif">
                                                <input type="file" name="old_pdf_ru[]" class="article_pdf_file" >
                                                <input type="text" placeholder="Russian Pdf title" class="article_pdf_title form-control" name="old_pdf_title_ru[]" value="{{  $article_pdf->pdf_name_ru }}">
                                                <input type="hidden" name="old_pdf_file_ru[]" value="{{  $article_pdf->pdf_file_ru }}" >
                                            </div>
                                            <div class="eng_block" style="@if(config('app.locale') != 'en') {{"display: none"}} @endif">
                                                <input type="file" name="old_pdf_en[]" class="article_pdf_file" >
                                                <input type="text" placeholder="English Pdf title" class="article_pdf_title form-control" name="old_pdf_title_en[]" value="{{  $article_pdf->pdf_name_en }}">
                                                <input type="hidden" name="old_pdf_file_en[]" value="{{  $article_pdf->pdf_file_en }}" >
                                            </div>

                                            <div onclick="remove_from_authors(this)" class="del_author_block"><i class="fa fa-trash-o"></i></div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Slug</label>
                                <input type="text" class="form-control slug_input" name="slug" value="{{ $article->slug }}">
                            </div>

                            <input type="hidden" name="edit_articles" value="ok">
                            <button type="submit" class="btn_1 green pull-right">
                                Save
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
