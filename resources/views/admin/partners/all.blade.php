@extends('admin.layouts.page')
<link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/jquery.switch.css') }}" rel="stylesheet">
@section('content')

    <div class="container services_block">
            <div class="col-md-8 page_show_content">
                    <div class="about_form">
                        <div class="page_title_box partner_title parent_page_title">
                            <h4>Partners</h4>
                            <div class="page_actions_block">
                                <a class="sub_page_add btn_2" href="{{ url('/') }}/admin/partners/create">
                                    <img src="{{ url('/') }}/img/iconAdd.png" alt="Add Partners">
                                    Add
                                </a>
                            </div>
                        </div>
                        <section id="section-1" class="content-current">
                            <form class="checked_form" action="{{ url('/') }}/admin/checked_delete" method="post">
                                {{ csrf_field() }}
                                <div class="select_all_block" style="width: 100%">
                                    <input type="checkbox" class="checked_all" name="checked_all" value="1">
                                    <label>Check all</label>
                                    <button class="checked_delete" type="button" disabled><i class="icon-trash"></i></button>
                                </div>

                            @foreach($partners as $partner)
                                <div class="strip_booking strip_project{{ $partner->id }}">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2">
                                            <div class="check_one">
                                                <input type="checkbox" class="checked_one_input" name="checked_one_input[]" value="{{ $partner->id }}">
                                                <input type="checkbox" class="hidden check_type" name="check_type[]" value="partner" >
                                            </div>
                                            <div class="page_hidden">
                                                @if($partner->hidden == 0)
                                                    <a alt="Show" title="Show" class="visible" data-hidden="1" data-id="{{ $partner->id }}" data-type="partner">
                                                        <i class="icon-eye"></i>
                                                    </a>

                                                @else
                                                    <a alt="Hide" title="Hide" class="visible" data-hidden="0" data-id="{{ $partner->id }}" data-type="partner">
                                                        <i class="icon-eye-off"></i>
                                                    </a>
                                                @endif
                                            </div>

                                        </div>
                                        <div class="project_img_box col-md-2 col-sm-2">
                                            @if($partner->image !='')
                                           <img src="{{ url('/') }}/uploads/partners/150/{{$partner->image}}" >
                                                @else
                                                <div class="date">
                                                    <span class="month">{{ date("M", strtotime($partner->created_at)) }}</span>
                                                    <span class="day"><strong>{{ date("d", strtotime($partner->created_at)) }}</strong>{{ date("D", strtotime($partner->created_at)) }}</span>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <h3 class="">{{ $partner->title }}</h3>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <div class="page_actions_block">
                                                <a href="{{ url('/') }}/admin/partners/{{ $partner->id }}/edit" class="btn_2">
                                                    <i class="icon-edit-3"></i>
                                                    Edit
                                                </a>
                                                <a data-id="{{ $partner->id }}" class="partner_delete btn_delete btn-danger">
                                                    <i class="icon-trash"></i>
                                                    Delete
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End row -->
                                </div>
                            @endforeach
                            </form>
                        </section>

                    </div>
            </div>
    </div>
@endsection
