@extends('admin.layouts.page')

@section('content')
    <div class="container">

            <div class="form_box">
                <div class="page_title_box">
                    <h4>Contact us</h4>
                </div>
                @if( count($languages) > 1)
                    <ul class="languages_tabs">
                        @foreach($languages as $language)
                            <li data-tab="{{ $language->short }}" class="lang_tab {{ $language->short."_tab" }} @if($language->first == 1) {{"active_tab"}} @endif">
                                <p><span>{{ $language->title }}</span></p>
                            </li>
                        @endforeach
                    </ul>
                @endif
                    <div class="about_form">
                        <form action="{{ route('contact_page') }}" method="post">
                            {{ csrf_field() }}
                            <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                @foreach($languages as  $key => $language)
                                    <div class="col-md-8 col-sm-8 lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                        <div class="form_title">
                                            <h3><input type="text" name="title[]"value=" {{ $contact_info_array[$key]->title }} " class="input_hidden">
                                            </h3>
                                        </div>
                                        <div class="step">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="contact_label">
                                                            <input type="text" value=" {{ $contact_info_array[$key]->firstname }} "
                                                                   name="firstname[]" class="input_label">
                                                        </label>
                                                        <input type="text" value=" {{ $contact_info_array[$key]->name_contact }} "
                                                               class="form-control-contact" id="name_contact"
                                                               name="name_contact[]" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label>
                                                            <input type="text" value=" {{ $contact_info_array[$key]->mail }} " name="mail_en"
                                                                   class="input_label">
                                                        </label>
                                                        <input type="text" id="email_contact"
                                                               value=" {{ $contact_info_array[$key]->email_contact }} "
                                                               name="email_contact[]" class="form-control-contact"
                                                               placeholder="">
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End row -->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label><input type="text" value=" {{ $contact_info_array[$key]->message }} "
                                                                      name="message[]" class="input_label"></label>
                                                        <textarea rows="5" id="message_contact" name="message_contact[]"
                                                                  maxlength="1500" class="form-control-contact-text"
                                                                  placeholder=""
                                                                  style="height: 200px; width: 100%;"> {{ $contact_info_array[$key]->message_contact }} </textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row margin-18-10">
                                                <div class="col-md-6">
                                                    <input type="text" value=" {{ $contact_info_array[$key]->send_label }} "
                                                           name="send_label[]" class="btn_1 form-control-contact"
                                                           id="submit-contact">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                    <div class="box_style_1">
                                        <input type="text" value="{{ $contact_info_array[$key]->address_label }}"
                                               name="address_label[]" class="input_right_label">
                                        <ul id="contact-info">
                                            <li><input type="text" value="{{ $contact_info_array[$key]->mail_right }} "
                                                       name="mail_right[]" class="input_right_arrdess">
                                            </li>
                                            <li><textarea name="address[]" class="input_right_arrdess"
                                                          style="width: 100%;"> {{ $contact_info_array[$key]->address }} </textarea>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="box_style_4">
                                        <input type="text" value=" {{ $contact_info_array[$key]->social_media_title }} "
                                               name="social_media_title[]" class="input_right_label">
                                        <ul id="contact-info">
                                            <li>
                                                <label>
                                                    Facebook
                                                </label>
                                                <input type="text" value=" {{ $contact_info_array[$key]->facebook }} "
                                                       name="social_media_fb[]" class="input_right_arrdess"></li>
                                            <li>
                                                <label>
                                                    Youtube
                                                </label>
                                                <input type="text" value=" {{ $contact_info_array[$key]->youtube }} "
                                                       name="social_media_yt[]" class="input_right_arrdess">
                                            </li>
                                            <li>
                                                <label>
                                                    G+
                                                </label>
                                                <input type="text" value=" {{ $contact_info_array[$key]->gplus }} "
                                                       name="social_media_gplus[]" class="input_right_arrdess">
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <input type="hidden" name="save" value="ok">
                            <input class="btn_1 green" type="submit" value="Submit">
                        </form>
                    </div>
            </div>



    </div>
@endsection
