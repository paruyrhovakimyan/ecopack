@extends('admin.layouts.page')

@section('content')

    <div class="container">
        <div class="form_box">
            <div class="about_form">
                <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/admin_users/create">
                {{ csrf_field() }}
                <!--Armenian-->

                    <div class="col-md-8">
                        <div class="page_title_box">
                            <h4>Add Admin</h4>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <label for="title">Name</label>
                                <input id="about_title" type="text" class="form-control" name="title" value=""
                                       autofocus required>
                            </div>
                            <div class="col-md-6">
                                <label for="title">Role</label>
                                <select class="form-control" name="role">
                                    <option value="1">Admin</option>
                                    <option value="2">Super Admin</option>
                                </select>
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <label for="Description">Login</label>
                                <input type="text" class="form-control description" name="login" required>
                            </div>
                            <div class="col-md-6">
                                <label for="Description">Password</label>
                                <input type="text" class="form-control" name="password" required>
                            </div>

                        </div>
                        <input type="hidden" name="add_admin" value="ok">
                        <div class="col-md-12">
                            <button type="submit" class="btn_1 save_btn green pull-right">
                                Save
                            </button>
                        </div>
                    </div>








                </form>
            </div>
        </div>

    </div>
@endsection
