@extends('admin.layouts.page')
<link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/jquery.switch.css') }}" rel="stylesheet">
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3>All Functions</h3></div>
                </div>
            </div>
        </div>
        <div class="form_block">
            <ul class="languages_tabs">
                <li class="arm_tab active_tab">
                    <p><span>Armenian</span></p>
                </li>
                <li class="rus_tab">

                    <p><span>RUSSIAN</span></p>
                </li>
                <li class="eng_tab">
                    <p><span>ENGLISH</span></p>
                </li>
            </ul>
            <div class="form_box">
                <div class="panel panel-default">
                    <div class="about_form">
                        <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/projects">
                            {{ csrf_field() }}
                            <div class="form-group arm_block">
                                <label for="title">Armenian Title</label>
                                <input id="about_title" type="text" class="form-control" name="title" value="" required autofocus>
                            </div>
                            <div class="form-group arm_block">
                                <label for="Description">Armenian Short Description</label>
                                <textarea class="form-control short_desc" name="short_desc"></textarea>
                            </div>
                            <div class="form-group arm_block">
                                <label for="Description">Armenian Description</label>
                                <textarea class="form-control description" name="desc"></textarea>
                            </div>


                            <div style="display: none" class="form-group rus_block">
                                <label for="title">Russian Title</label>
                                <input id="about_title" type="text" class="form-control" name="title_ru" value="" autofocus>
                            </div>
                            <div style="display: none" class="form-group rus_block">
                                <label for="Description">Russian Short Description</label>
                                <textarea class="form-control short_desc" name="short_desc_ru"></textarea>
                            </div>
                            <div style="display: none" class="form-group rus_block">
                                <label for="Description">Russian Description</label>
                                <textarea class="form-control description" name="desc_ru"></textarea>
                            </div>
                            <div style="display: none" class="form-group eng_block">
                                <label for="title">English Title</label>
                                <input id="about_title" type="text" class="form-control" name="title_en" value="" autofocus>
                            </div>
                            <div style="display: none" class="form-group eng_block">
                                <label for="Description">English Short Description</label>
                                <textarea class="form-control short_desc" name="short_desc_en"></textarea>
                            </div>
                            <div style="display: none" class="form-group eng_block">
                                <label for="Description">English Description</label>
                                <textarea class="form-control description" name="desc_en"></textarea>
                            </div>



                            <div class="row">
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <h4> META HEADERS </h4>
                                </div>
                                <div class="col-md-6 col-sm-6 pull-left">

                                    <div class="form-group arm_block">
                                        <label>Armenian Meta Title:</label>
                                        <input type="text" placeholder='Meta Title' class="form-control"
                                               value='' id='meta_title' name='meta_title'/>
                                    </div>
                                    <div style="display: none" class="form-group rus_block">
                                        <label>Russian Meta Title:</label>
                                        <input type="text" placeholder='Meta Title' class="form-control"
                                               value='' id='meta_title' name='meta_title_ru'/>
                                    </div>
                                    <div style="display: none" class="form-group eng_block">
                                        <label>English Meta Title:</label>
                                        <input type="text" placeholder='Meta Title' class="form-control"
                                               value='' id='meta_title' name='meta_title_en'/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 pull-left">
                                    <div class="form-group arm_block">
                                        <label>Armenian Meta Keyword:</label>
                                        <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key' class="form-control"
                                               value=""/>
                                    </div>
                                    <div style="display: none" class="form-group rus_block">
                                        <label>Russian Meta Keyword:</label>
                                        <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_ru' class="form-control"
                                               value=""/>
                                    </div>
                                    <div style="display: none" class="form-group eng_block">
                                        <label>English Meta Keyword:</label>
                                        <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_en' class="form-control"
                                               value=""/>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <div class="form-group arm_block">
                                        <label>Armenian Meta Description</label>
                                        <textarea id="meta_desc" class="form-control" name="meta_desc"></textarea>
                                    </div>
                                    <div style="display: none" class="form-group rus_block">
                                        <label>Russian Meta Description</label>
                                        <textarea id="meta_desc" class="form-control" name="meta_desc_ru"></textarea>
                                    </div>
                                    <div style="display: none" class="form-group eng_block">
                                        <label>English Meta Description</label>
                                        <textarea id="meta_desc" class="form-control" name="meta_desc_en"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group order_block">
                                <label for="service_order">Order</label>
                                <input id="service_order" type="text" class="form-control" name="order" value="1">
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <h4>Upload photo</h4>
                                </div>
                                <div class="col-md-12 col-sm-12 pull-left">
                                    <div id="uploadzone" class="dropzone" dir="ltr"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="company_logo">
                                    <div class="col-md-5 col-sm-5 pull-left title_present">
                                        <div class="">
                                            <h4>Article image</h4>
                                        </div>
                                        <div class="avatar_img">
                                            <div class="file-upload btn btn_1 green">
                                                <span>Choose image</span>
                                                <input type="file" name="image" id="uploadBtnLogo" class="image upload"
                                                       onchange="readURLLogo(this)">
                                            </div>
                                        </div>
                                        <div class="profile-image-preview_logo">
                                            <div class="ct-media--left">
                                                <a>
                                                    <img id="uploadPreviewLogo" src="">
                                                </a>
                                            </div>
                                            <div class="ct-media--content">
                                                <span></span>
                                                <a class="cross" onclick="deleteimgLogo();" style="cursor:pointer;">
                                                    <i class="fa fa-trash-o"></i> Delete
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="">
                                    <h4>Article pdf</h4>
                                </div>
                                <div class="btn-primary pdf_plus">+</div>
                                <div class="article_pdf_block">

                                </div>
                            </div>

                            <input type="hidden" name="add_project" value="ok">
                            <button type="submit" class="btn_1 green pull-right">
                                Save
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
