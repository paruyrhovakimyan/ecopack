@extends('admin.layouts.page')
<link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/jquery.switch.css') }}" rel="stylesheet">
@section('content')

    <div class="container services_block">
        <div class="row">
            <div class="col-md-8 page_show_content">
                    <div class="page_title_box parent_page_title">
                        <h4 style="    padding-top: 7px;">Product Types</h4>
                        <div class="page_actions_block">
                            <a href="{{ url('/') }}/admin/prtype/create" class="sub_page_add btn_2">
                                <i class="icon-docs"></i>
                                Add
                            </a>
                        </div>

                    </div>
                    <div class="child_pages">

                        <section id="section-1" class="content-current">
                            <form class="checked_form" action="{{ url('/') }}/admin/checked_delete" method="post">
                                {{ csrf_field() }}
                                <div class="select_all_block" style="width: 100%">
                                    <input type="checkbox" class="checked_all" name="checked_all" value="1">
                                    <label>Check all</label>
                                    <button class="checked_delete" type="button" disabled><i class="icon-trash"></i></button>
                                </div>
                            @foreach($brands as $brand)
                                <div class="strip_booking">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2">
                                            <div class="check_one">
                                                <input type="checkbox" class="checked_one_input" name="checked_one_input[]" value="{{ $brand->id }}">
                                                <input type="checkbox" class="hidden check_type" name="check_type[]" value="brands" >
                                            </div>
                                            <div class="page_hidden">
                                                @if($brand->hidden == 0)
                                                    <a alt="Show" title="Show" class="visible" data-hidden="1" data-id="{{ $brand->id }}" data-type="prtype">
                                                        <i class="icon-eye"></i>
                                                    </a>

                                                @else
                                                    <a alt="Hide" title="Hide" class="visible" data-hidden="0" data-id="{{ $brand->id }}" data-type="prtype">
                                                        <i class="icon-eye-off"></i>
                                                    </a>
                                                @endif
                                            </div>

                                        </div>
                                        <div class="col-md-8 col-sm-8">
                                            <h3 class="">{{ $brand->{"title_$default_lang"} }}</h3>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <div class="page_actions_block">
                                                <a href="{{ url('/') }}/admin/prtype/{{ $brand->id }}/edit" class="parent_page_edit btn_2">
                                                    <i class="icon-edit-3"></i>
                                                    Edit
                                                </a>
                                                <a data-id="{{ $brand->id }}" class="prcat_delete btn_delete btn-danger">
                                                    <i class="icon-trash"></i>
                                                    Delete
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End row -->


                                </div>
                            @endforeach
                            </form>
                        </section>

                    </div>
            </div>
        </div>
    </div>
@endsection
