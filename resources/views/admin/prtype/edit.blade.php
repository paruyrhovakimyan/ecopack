@extends('admin.layouts.page')

@if(isset($data['edit_successful']))
    <div class="edit_successful">
        <p>The Service updated</p>
    </div>
@endif

@section('content')

    <div class="container">
        <div class="form_box">
            <div class="page_title_box">
                <h4>{{ $brand->{"title_".$default_lang} }}</h4>
            </div>
            @if( count($languages) > 1)
                <ul class="languages_tabs">
                    @foreach($languages as $language)
                        <li data-tab="{{ $language->short }}" class="lang_tab {{ $language->short."_tab" }} @if($language->first == 1) {{"active_tab"}} @endif">
                            <p><span>{{ $language->title }}</span></p>
                        </li>
                    @endforeach
                </ul>
            @endif
                    <div class="about_form">
                        
                        <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/prtype/{{ $brand->id }}" enctype="multipart/form-data">
                        {{method_field('PUT')}}
                        {{ csrf_field() }}
                        <!--Armenian-->
                            <div class="col-md-8 page_left">


                                @foreach($languages as $language)
                                    <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                        <label for="title">{{ $language->title }} Title</label>
                                        <input id="about_title" type="text" class="form-control" name="title_{{ $language->short }}" value="{{ $brand->{"title_".$language->short} }}"
                                               autofocus>
                                    </div>
                                    <div  class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                        <label for="Description">{{ $language->title }} Description</label>
                                        <textarea class="form-control description" name="desc_{{ $language->short }}">{{ $brand->{"description_".$language->short} }}</textarea>
                                    </div>

                                @endforeach
                                <div class="col-md-12">
                                    <input type="hidden" name="edit_category" value="ok">
                                    <button type="submit" class="btn_1 green pull-right">
                                        Save
                                    </button>
                                </div>
                            </div>

                            <!--English-->


                        </form>
                    </div>
            </div>
    </div>
@endsection
