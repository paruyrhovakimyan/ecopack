@extends('admin.layouts.page')

@section('content')

    <div class="container">

        <div class="form_box">
            <div class="page_title_box">
                {!! $page_bredcrump  !!}
            </div>
            <div class="about_form">
                <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/news_sub_page_pdf/{{ $parent_id }}/create" enctype='multipart/form-data'>
                {{ csrf_field() }}
                <!--Armenian-->

                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input id="about_title" type="text" class="form-control" name="title" value=""
                                   autofocus>
                        </div>
                        {{--   <div class="form-group">
                               <label for="Description">Short Description</label>
                               <textarea class="form-control short_desc" name="short_desc_{{ config('app.locale') }}"></textarea>
                           </div>--}}
                        <div  class="form-group">
                            <label for="Description">Description</label>
                            <textarea class="form-control description" name="description"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Add Pdf</label>
                            <div class="news_sub_page_pdf_block">
                                <div class="btn news_sub_page_pdf_add">
                                    <i class="icon-plus"></i>
                                </div>
                                <div class="page_pdf_plus_block">

                                </div>
                            </div>

                        </div>
                        <input type="hidden" name="add_news_sub_page" value="ok">
                        <div class="col-md-12">
                            <button type="submit" class="btn_1 save_btn green pull-right">
                                Save
                            </button>
                        </div>
                    </div>








                </form>
            </div>
        </div>

    </div>
@endsection
