@extends('admin.layouts.page')

@section('content')

    <div class="container">
        <div class="about_form">
            <div class="col-md-8">
                <div class="page_title_box parent_page_title">
                    <h4>{!!  $page_bredcrump !!}</h4>
                </div>
                <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/news_sub_page_pdf/{{ $parent_id }}/edit/{{ $news_sub_page->id }}" enctype='multipart/form-data'>
                {{ csrf_field() }}
                <!--Armenian-->
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input id="about_title" type="text" class="form-control" name="title" value="{{ $news_sub_page->title }}"
                               autofocus>
                    </div>
                    {{--   <div class="form-group">
                           <label for="Description">Short Description</label>
                           <textarea class="form-control short_desc" name="short_desc_{{ config('app.locale') }}"></textarea>
                       </div>--}}
                    <div class="form-group">
                        <label for="Description">Description</label>
                        <textarea class="form-control description" name="description">{{ $news_sub_page->description }}</textarea>
                    </div>


                    <div class="form-group">
                        <label>Add Pdf</label>
                        <div class="news_sub_page_pdf_block">
                            <div class="btn news_sub_page_pdf_add">
                                <i class="icon-plus"></i>
                            </div>
                            <div class="page_pdf_plus_block">
                                @if(!empty($news_sub_page_pdfs))
                                    @foreach($news_sub_page_pdfs as $news_sub_page_pdf)

                                        <div class="open_sub_pdf_block">
                                            <p class="open_sub_pdf_block_title_{{ $news_sub_page_pdf->id }}" data-id="{{ $news_sub_page_pdf->id }}">{{ $news_sub_page_pdf->title }}</p>
                                            <div data-id="{{ $news_sub_page_pdf->id }}" class="pdf_append_block_delete">
                                                <i class="icon-trash"></i>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>

                    </div>
<input type="hidden" class="current_page_id" value="{{ $news_sub_page->id }}">
                    <input type="hidden" name="edit_news_sub_page" value="ok">
                    <div class="col-md-12">
                        <button type="submit" class="btn_1 save_btn green pull-right">
                            Save
                        </button>
                    </div>


                </form>
            </div>
        </div>
    </div>
@endsection
