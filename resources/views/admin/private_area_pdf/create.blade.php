@extends('admin.layouts.page')

@section('content')

    <div class="container">
        <div class="form_box">
            <div class="page_title_box">
                <h4>{{ $title }} | Add Pdf</h4>
            </div>
            <div class="about_form">
                <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/private_area/save_pdf"
                      enctype='multipart/form-data'>
                    {{ csrf_field() }}
                    <div class="col-md-8 page_left">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input id="about_title" type="text" class="form-control" name="title" value=""
                                   autofocus>
                        </div>
                        <div class="form-group">
                            <label for="Description">Short Description</label>
                            <textarea class="form-control short_desc" name="desc"></textarea>
                        </div>
                        <div class="not_media_box form-group">


                            <div class="">
                                <label>Upload PDF</label>
                            </div>
                            <div class="file-upload btn btn_1 green" style="float:left;">
                                <span>Choose file</span>
                                <input type="file" name="pdf_{{ config('app.locale') }}" class="image upload article_pdf_file">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <input type="hidden" name="add_pdf" value="ok">
                            <input type="hidden" name="parent_id" value="{{ $id }}">
                            <button type="submit" class="btn_1 green pull-right">
                                Save
                            </button>
                        </div>
                    </div>


                </form>
            </div>
        </div>
    </div>
@endsection
