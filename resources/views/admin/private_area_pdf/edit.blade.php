@extends('admin.layouts.page')

@section('content')

    <div class="container">
        @if( app('App\Http\Controllers\Admin\SettingsController')->language_count() == 1)
            <div class="form_box">
                <div class="page_title_box">
                    <h4>{{ $parent_title." | ".$private_area_pdf->title }}</h4>
                </div>
                <div class="about_form">
                    <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/private_area/edit_pdf/{{ $private_area_pdf->id }}" enctype='multipart/form-data'>
                        {{ csrf_field() }}
                        <div class="col-md-8 page_left">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input id="about_title" type="text" class="form-control" name="title" value="{{ $private_area_pdf->title }}"
                                       autofocus>
                            </div>
                            <div class="form-group">
                                <label for="Description">Short Description</label>
                                <textarea class="form-control short_desc" name="description">{{ $private_area_pdf->short_desc }}</textarea>
                            </div>
                            <div class="not_media_box form-group">
                                <div class="">
                                    <label>Upload PDF</label>
                                </div>
                                <div class="pdf_file_title">
                                    <input type="text" name="old_pdf_title" readonly class="old_pdf_title" value="{{ $private_area_pdf->file }}">
                                </div>
                                <div class="file-upload btn btn_1 green" style="float:left;">
                                    <span>Choose file</span>
                                    <input type="file" name="pdf_{{ config('app.locale') }}" class="image upload article_pdf_file" >
                                </div>

                            </div>
                            <div class="col-md-12">
                                <input type="hidden" name="edit_pdf" value="ok">
                                <button type="submit" class="btn_1 green pull-right">
                                    Save
                                </button>
                            </div>
                        </div>


                    </form>
                </div>
            </div>
        @else
            <div class="form_block">
                <ul class="languages_tabs">
                    @if($settings->lang_am == 1)
                        <li class="arm_tab @if(config('app.locale') == 'am') {{"active_tab"}} @endif">
                            <p><span>Armenian</span></p>
                        </li>
                    @endif
                    @if($settings->lang_ru == 1)
                        <li class="rus_tab @if(config('app.locale') == 'ru') {{"active_tab"}} @endif">
                            <p><span>RUSSIAN</span></p>
                        </li>
                    @endif
                    @if($settings->lang_en == 1)
                        <li class="eng_tab @if(config('app.locale') == 'en') {{"active_tab"}} @endif">
                            <p><span>ENGLISH</span></p>
                        </li>
                    @endif
                </ul>
                <div class="form_box">
                    <div class="panel panel-default">
                        <div class="about_form">
                            <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/sub_pdf/{{ $parent_id }}/create" enctype='multipart/form-data'>
                            {{ csrf_field() }}

                            <!--Armenian-->
                                <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif"
                                     class="form-group arm_block">
                                    <label for="title">Armenian Title</label>
                                    <input id="about_title" type="text" class="form-control" name="title" value=""
                                           autofocus>
                                </div>
                                <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif"
                                     class="form-group arm_block">
                                    <label for="Description">Armenian Description</label>
                                    <textarea class="form-control description" name="desc"></textarea>
                                </div>

                                <!--Armenian-->

                                <!--Russian-->
                                <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif"
                                     class="form-group rus_block">
                                    <label for="title">Russian Title</label>
                                    <input id="about_title" type="text" class="form-control" name="title_ru" value=""
                                           autofocus>

                                </div>
                                <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif"
                                     class="form-group rus_block">
                                    <label for="Description">Russian Description</label>
                                    <textarea class="form-control description" name="desc_ru"></textarea>
                                </div>
                                <!--Russian-->

                                <!--English-->
                                <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif"
                                     class="form-group eng_block">
                                    <label for="title">English Title</label>
                                    <input id="about_title" type="text" class="form-control" name="title_en" value=""
                                           autofocus>
                                </div>
                                <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif"
                                     class="form-group eng_block {{ $errors->has('description') ? ' has-error' : '' }}">
                                    <label for="Description">English Description</label>
                                    <textarea class="form-control description" name="desc_en"></textarea>
                                </div>
                                <!--English-->

                                <div class="not_media_box form-group">
                                    <div class="">
                                        <h4>Pdf</h4>
                                    </div>
                                    <div class="article_pdf_block">
                                        <div class="new_author_block author_fields">
                                            <div style="<?=(config('app.locale') != 'am') ? "display: none" : ""; ?>" class="arm_block">
                                                <input type="file" name="pdf" class="article_pdf_file" >
                                            </div>
                                            <div style="<?=(config('app.locale') != 'ru') ? "display: none" : ""; ?>" class="rus_block">
                                                <input type="file" name="pdf_ru" class="article_pdf_file" >
                                            </div>
                                            <div style="<?=(config('app.locale') != 'en') ? "display: none" : ""; ?>" class="eng_block">
                                                <input type="file" name="pdf_en" class="article_pdf_file" >
                                            </div>
                                        </div>
                                    </div>
                                </div>





                                <input type="hidden" name="add_pdf" value="ok">
                                <button type="submit" class="btn_1 green pull-right">
                                    Save
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endif


    </div>
@endsection
