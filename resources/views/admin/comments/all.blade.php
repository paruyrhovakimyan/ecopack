@extends('admin.layouts.page')
<link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/jquery.switch.css') }}" rel="stylesheet">
@section('content')

    <div class="container services_block">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Comments</h3>
                    </div>
                    <div class="about_form">
                        <form class="form-horizontal" method="POST" action="{{ route('about_us') }}">
                            {{ csrf_field() }}
                        </form>


                        <section id="section-1" class="content-current">
                            @foreach($comments as $comment)
                                <div class="strip_booking">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2">
                                            <label class="switch-light switch-ios pull-right">
                                                <input data-id="{{ $comment->id }}" type="checkbox" class="comment_hidden comment_hidden{{$comment->id}} option_hidden{{$comment->id}}" @if($comment->approve == 1){{ "checked" }}@endif
                                                value="@if($comment->approve == 1){{ "0" }}@else{{ "1" }}@endif">

                                                <span>
							                        <span>Hide</span>
													<span>Show</span>
													</span>
                                                <a></a>
                                            </label>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <p class="comment_text">{{ $comment->comment_text }}</p>
                                        </div>
                                        <div class="col-md-2 col-sm-3">
                                            <ul class="info_booking">
                                                <li><strong>User name</strong> {{ $comment->user_name }}</li>
                                                <li><strong>Article name</strong> {{ $comment->article_name }}</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <div class="booking_buttons">
                                                <a data-id="{{ $comment->id }}" class="comment_delete btn_delete btn-danger">Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End row -->
                                </div>
                            @endforeach
                        </section>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
