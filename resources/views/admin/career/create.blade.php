@extends('admin.layouts.page')

@section('content')

    <div class="container">
        <div class="form_box">
            <div class="page_title_box">
                <h4><a href="https://printel.mycard.am/admin/partners">Career</a> | Add Career</h4>
            </div>
            @if( count($languages) > 1)
                <ul class="languages_tabs">
                    @foreach($languages as $language)
                        <li data-tab="{{ $language->short }}" class="lang_tab {{ $language->short."_tab" }} @if($language->first == 1) {{"active_tab"}} @endif">
                            <p><span>{{ $language->title }}</span></p>
                        </li>
                    @endforeach
                </ul>
            @endif
            <div class="about_form">
                <div class="col-md-8">

                    <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/career" enctype='multipart/form-data'>

                        {{ csrf_field() }}

                        @foreach($languages as $language)
                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="title">{{ $language->title }} Title</label>
                                <input id="about_title" type="text" class="form-control" name="title_{{ $language->short }}" value=""
                                       autofocus>
                            </div>

                            <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                <label for="Description">{{ $language->title }} Short Description</label>
                                <textarea class="form-control description short_desc" name="description_{{ $language->short }}"></textarea>
                            </div>
                        @endforeach
                        <input type="hidden" name="add_partner" value="ok">
                        <button type="submit" class="btn_1 green pull-right">
                            Save
                        </button>

                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
