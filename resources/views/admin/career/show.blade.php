@extends('admin.layouts.page')


@section('content')
    <div class="container">
        <div class="form_box">
            <div class="page_title_box">
                <h4><a href="https://printel.mycard.am/admin/career">Career</a> | {{ $career->{"title_".$default_lang} }}</h4>
            </div>

            <div class="about_form">
                <div class="col-md-12">
                    <section id="section-1" class="content-current">
                        <form class="checked_form" action="{{ url('/') }}/admin/checked_delete" method="post">
                            {{ csrf_field() }}
                            <div class="select_all_block" style="width: 100%">
                                <input type="checkbox" class="checked_all" name="checked_all" value="1">
                                <label>Check all</label>
                                <button class="checked_delete" type="button" disabled><i class="icon-trash"></i></button>
                            </div>
                            <table class="career_order_table">
                                <tr>
                                    <th></th>
                                    <th>Անուն Ազգանուն</th>
                                    <th>Էլ. փոստ</th>
                                    <th>Նկարագրություն</th>
                                    <th>Կցված ֆայլ</th>
                                    <th></th>
                                </tr>

                            @foreach($careerorders as $careerorder)
                                <tr>
                                    <td>
                                        <div class="check_one">
                                            <input type="checkbox" class="checked_one_input" name="checked_one_input[]" value="{{ $careerorder->id }}">
                                            <input type="checkbox" class="hidden check_type" name="check_type[]" value="careerorder" >
                                        </div>
                                    </td>
                                    <td><p class="">{{ $careerorder->name_surname }}</p></td>
                                    <td><p class="">{{ $careerorder->email }}</p></td>
                                    <td><p class="">{{ $careerorder->description }}</p></td>
                                    <td><a target="_blank" href="{{ url('/') }}/public/uploads/career/{{ $careerorder->order_file }}">Ֆայլ</a></td>
                                    <td>
                                        <a data-id="{{ $careerorder->id }}" class="careerorder_delete btn_delete btn-danger">
                                            <i class="icon-trash"></i>
                                            Delete
                                        </a></td>
                                </tr>

                            @endforeach
                            </table>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection
