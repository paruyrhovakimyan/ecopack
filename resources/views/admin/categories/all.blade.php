@extends('admin.layouts.page')
<link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/jquery.switch.css') }}" rel="stylesheet">
@section('content')

    <div class="container services_block">
        <div class="row">
            <div class="col-md-8 page_show_content">
                    <div class="page_title_box parent_page_title">
                        <h4 style="    padding-top: 7px;">Categories</h4>
                        <div class="page_actions_block">
                            <a href="{{ url('/') }}/admin/categories/create?parent={{ $_REQUEST['parent'] }}" class="sub_page_add btn_2">
                                <i class="icon-docs"></i>
                                Add
                            </a>
                        </div>

                    </div>
                    <div class="child_pages">

                        <section id="section-1" class="content-current">
                            <form class="checked_form" action="{{ url('/') }}/admin/checked_delete" method="post">
                                {{ csrf_field() }}
                                <div class="select_all_block" style="width: 100%">
                                    <input type="checkbox" class="checked_all" name="checked_all" value="1">
                                    <label>Check all</label>
                                    <button class="checked_delete" type="button" disabled><i class="icon-trash"></i></button>
                                </div>
                            @foreach($categories as $category)
                                <div class="strip_booking">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2">
                                            <div class="check_one">
                                                <input type="checkbox" class="checked_one_input" name="checked_one_input[]" value="{{ $category->id }}">
                                                <input type="checkbox" class="hidden check_type" name="check_type[]" value="category" >
                                            </div>
                                            <div class="page_hidden">
                                                @if($category->hidden == 0)
                                                    <a alt="Show" title="Show" class="visible" data-hidden="1" data-id="{{ $category->id }}" data-type="category">
                                                        <i class="icon-eye"></i>
                                                    </a>

                                                @else
                                                    <a alt="Hide" title="Hide" class="visible" data-hidden="0" data-id="{{ $category->id }}" data-type="category">
                                                        <i class="icon-eye-off"></i>
                                                    </a>
                                                @endif
                                            </div>

                                        </div>
                                        <div class="col-md-8 col-sm-8">
                                            <h3 class="">{{ $category->title }}</h3>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <div class="page_actions_block">
                                                <a href="{{ url('/') }}/admin/categories/{{ $category->id }}/edit" class="parent_page_edit btn_2">
                                                    <i class="icon-edit-3"></i>
                                                    Edit
                                                </a>
                                                <a data-id="{{ $category->id }}" class="category_delete btn_delete btn-danger">
                                                    <i class="icon-trash"></i>
                                                    Delete
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End row -->
                                </div>
                            @endforeach
                            </form>
                        </section>

                    </div>
            </div>
        </div>
    </div>
@endsection
