@extends('admin.layouts.page')

@section('content')

    <div class="container">



            <div class="form_box">
                <div class="page_title_box">
                    <h4>Add Category</h4>
                </div>
                @if( count($languages) > 1)
                    <ul class="languages_tabs">
                        @foreach($languages as $language)
                            <li data-tab="{{ $language->short }}"
                                class="lang_tab {{ $language->short."_tab" }} @if($language->first == 1) {{"active_tab"}} @endif">
                                <p><span>{{ $language->title }}</span></p>
                            </li>
                        @endforeach
                    </ul>
                @endif
                <div class="about_form">

                            <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/categories">
                            {{ csrf_field() }}
                                <div class="col-md-8 page_left">

                                    @foreach($languages as $language)
                                        <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                            <label for="title">{{ $language->title }} Title</label>
                                            <input id="about_title" type="text" class="form-control" name="title_{{ $language->short }}" value=""
                                                   autofocus>
                                        </div>

                                        <div  class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                            <label for="Description">{{ $language->title }} Description</label>
                                            <textarea class="form-control description" name="description_{{ $language->short }}"></textarea>
                                        </div>

                                    @endforeach

                                        <div class="form-group meta_block">
                                            <div class="open_close_meta">
                                                <p>Meta <i class="icon-arrow-combo"></i></p>
                                            </div>
                                            <div class="meta_box">
                                                <div class="col-md-6 col-sm-6 pull-left">
                                                    @foreach($languages as $language)
                                                        <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                                            <label>{{ $language->title }} Meta Title:</label>
                                                            <input type="text" placeholder='Meta Title' class="form-control"
                                                                   value='' id='meta_title' name='meta_title_{{ $language->short }}'/>
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <div class="col-md-6 col-sm-6 pull-left">
                                                    @foreach($languages as $language)
                                                        <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                                            <label>{{ $language->title }} Meta Keyword:</label>
                                                            <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_{{ $language->short }}'
                                                                   class="form-control"
                                                                   value=""/>
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <div class="col-md-12 col-sm-12 pull-left">
                                                    @foreach($languages as $language)
                                                        <div class="form-group lang_field lang_field_{{ $language->short }} @if($language->first == 1) {{ "active_field" }} @else {{ "hidden_field" }} @endif">
                                                            <label>{{ $language->title }} Meta Description</label>
                                                            <textarea id="meta_desc" class="form-control" name="meta_desc_{{ $language->short }}"></textarea>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>

                                        </div>
                                    <input type="hidden" name="add_category" value="ok">
                                    <input type="hidden" name="page_parent" value="{{ $_REQUEST['parent'] }}">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn_1 green pull-right">
                                            Save
                                        </button>
                                    </div>
                                </div>




                            </form>
                        </div>
            </div>

    </div>
@endsection
