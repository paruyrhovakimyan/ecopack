@extends('admin.layouts.page')
<link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/jquery.switch.css') }}" rel="stylesheet">
@section('content')

    <div class="container services_block">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>All media</h3>
                        <a class="btn_1 green pull-right" href="{{ url('/') }}/admin/media/create">Add Media</a>
                    </div>
                    <div class="about_form">
                        <form class="form-horizontal" method="POST" action="{{ route('about_us') }}">
                            {{ csrf_field() }}
                        </form>


                        <section id="section-1" class="content-current">
                            @foreach($all_media as $media)
                                <div class="strip_booking">
                                    <div class="row">
                                        <div class="col-md-10 col-sm-10">
                                            <h3 class="">{{ $media->title }}</h3>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <div class="booking_buttons">
                                                <a href="{{ url('/') }}/admin/media/{{ $media->id }}/edit" class="btn_2">Edit</a>
                                                <a data-id="{{ $media->id }}" class="media_delete btn_delete btn-danger">Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End row -->
                                </div>
                            @endforeach
                        </section>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
