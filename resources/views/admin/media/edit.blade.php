@extends('admin.layouts.page')

@if(isset($data['edit_successful']))
    <div class="edit_successful">
        <p>The page updated</p>
    </div>
@endif

@section('content')

    <div class="container">
        <div class="row edit_category">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span>Edit media: </span><h3>{{ $media->title  }}</h3>
                        <a class="btn_1 green pull-right" href="{{ route('media.index') }}">Back to Media list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="form_block">
            <ul class="languages_tabs">
                @if($settings->lang_am == 1)
                    <li class="arm_tab @if(config('app.locale') == 'am') {{"active_tab"}} @endif">
                        <p><span>Armenian</span></p>
                    </li>
                @endif
                @if($settings->lang_ru == 1)
                    <li class="rus_tab @if(config('app.locale') == 'ru') {{"active_tab"}} @endif">
                        <p><span>RUSSIAN</span></p>
                    </li>
                @endif
                @if($settings->lang_en == 1)
                    <li class="eng_tab @if(config('app.locale') == 'en') {{"active_tab"}} @endif">
                        <p><span>ENGLISH</span></p>
                    </li>
                @endif
            </ul>
            <div class="form_box">
                <div class="panel panel-default">

                    <div class="about_form">
                        <form class="form-horizontal" method="POST" action="{{ url('/') }}/admin/media/{{ $media->id }}">
                            {{method_field('PUT')}}
                            {{ csrf_field() }}
                            <!--Armenian-->
                                <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block {{ $errors->has('title') ? ' has-error' : '' }}">
                                    <label for="title">Armenian Title</label>
                                    <input id="about_title" type="text" class="form-control" name="title" value="{{ $media->title }}"
                                           autofocus>
                                </div>
                                <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="form-group arm_block">
                                    <label for="Description">Armenian Description</label>
                                    <textarea class="form-control description" name="desc">{{ $media->description }}</textarea>
                                </div>
                                <div style="@if(config('app.locale') != 'am') {{"display: none"}} @endif" class="row arm_block">
                                    <div class="col-md-12 col-sm-12 pull-left">
                                        <h4> META HEADERS </h4>
                                    </div>
                                    <div class="col-md-6 col-sm-6 pull-left">

                                        <div class="form-group">
                                            <label>Armenian Meta Title:</label>
                                            <input type="text" placeholder='Meta Title' class="form-control"
                                                   value='{{ $media->meta_title }}' id='meta_title' name='meta_title'/>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 pull-left">
                                        <div class="form-group">
                                            <label>Armenian Meta Keyword:</label>
                                            <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key'
                                                   class="form-control"
                                                   value="{{ $media->meta_key }}"/>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 pull-left">
                                        <div class="form-group">
                                            <label>Armenian Meta Description</label>
                                            <textarea id="meta_desc" class="form-control" name="meta_desc">{{ $media->meta_desc }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <!--Armenian-->

                                <!--Russian-->
                                <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block">
                                    <label for="title">Russian Title</label>
                                    <input id="about_title" type="text" class="form-control" name="title_ru" value="{{ $media->title_ru }}"
                                           autofocus>

                                </div>
                                <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="form-group rus_block">
                                    <label for="Description">Russian Description</label>
                                    <textarea class="form-control description" name="desc_ru">{{ $media->description_ru }}</textarea>
                                </div>
                                <div style="@if(config('app.locale') != 'ru') {{"display: none"}} @endif" class="row rus_block">
                                    <div class="col-md-12 col-sm-12 pull-left">
                                        <h4> META HEADERS </h4>
                                    </div>
                                    <div class="col-md-6 col-sm-6 pull-left">

                                        <div class="form-group">
                                            <label>Russian Meta Title:</label>
                                            <input type="text" placeholder='Meta Title' class="form-control"
                                                   value='{{ $media->meta_title_ru }}' id='meta_title' name='meta_title_ru'/>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 pull-left">
                                        <div class="form-group">
                                            <label>Russian Meta Keyword:</label>
                                            <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_ru'
                                                   class="form-control"
                                                   value="{{ $media->meta_key_ru }}"/>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 pull-left">
                                        <div class="form-group">
                                            <label>Russian Meta Description</label>
                                            <textarea id="meta_desc" class="form-control" name="meta_desc_ru">{{ $media->meta_desc_ru }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <!--Russian-->

                                <!--English-->
                                <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                    <label for="title">English Title</label>
                                    <input id="about_title" type="text" class="form-control" name="title_en" value="{{ $media->title_en }}"
                                           autofocus>
                                </div>
                                <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="form-group eng_block">
                                    <label for="Description">English Description</label>
                                    <textarea class="form-control description" name="desc_en">{{ $media->description_en }}</textarea>
                                </div>
                                <div style="@if(config('app.locale') != 'en') {{"display: none"}} @endif" class="row eng_block">
                                    <div class="col-md-12 col-sm-12 pull-left">
                                        <h4> META HEADERS </h4>
                                    </div>
                                    <div class="col-md-6 col-sm-6 pull-left">

                                        <div class="form-group">
                                            <label>English Meta Title:</label>
                                            <input type="text" placeholder='Meta Title' class="form-control"
                                                   value='{{ $media->meta_title_en }}' id='meta_title' name='meta_title_en'/>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 pull-left">
                                        <div class="form-group">
                                            <label>English Meta Keyword:</label>
                                            <input type="text" placeholder='Meta Keyword' id='meta_key' name='meta_key_en'
                                                   class="form-control"
                                                   value="{{ $media->meta_key_en }}"/>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 pull-left">
                                        <div class="form-group">
                                            <label>English Meta Description</label>
                                            <textarea id="meta_desc" class="form-control" name="meta_desc_en">{{ $media->meta_desc_en }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <!--English-->
                    <div class="row">
                        <div class="col-md-7 col-sm-7 pull-left">
                            <div class="form-group">
                                <label>Video link</label>
                                <input type="text" placeholder='Enter video url' id='author_mail' name='video_link'
                                       class="form-control"
                                       value="{{ $media->video }}"/>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5 pull-left">
                            <div class="form-group author_fields">
                                <label>Date</label>
                                <div class="author-tail clearfix">
                                    <div class="author-date-select">
                                        <div class="author-combobox-wrap author-form-field-alt-date">
                                            <select type="text" name="date_day" class="form-control" data-type="date">
                                                <option value="" selected="">Day</option>
                                                @for ($i = 1; $i < 32; $i++)
                                                    @if($i < 10)
                                                        <option @if($media->day == "0".$i) {{ "selected" }} @endif value="{{ "0".$i }}">{{ "0".$i }}</option>
                                                    @else
                                                        <option @if($media->day == $i) {{ "selected" }} @endif value="{{ $i }}">{{ $i }}</option>
                                                    @endif

                                                @endfor
                                            </select></div>
                                        <div class="author-combobox-wrap author-form-field-alt-month">
                                            <select type="text" name="date_month" class="form-control " data-type="month">

                                                <option value="" selected="">Month</option>
                                                <option @if($media->month == "01") {{ "selected" }} @endif value="01">January</option>
                                                <option @if($media->month == "02") {{ "selected" }} @endif value="02">February</option>
                                                <option @if($media->month == "03") {{ "selected" }} @endif value="03">March</option>
                                                <option @if($media->month == "04") {{ "selected" }} @endif value="04">April</option>
                                                <option @if($media->month == "05") {{ "selected" }} @endif value="05">May</option>
                                                <option @if($media->month == "06") {{ "selected" }} @endif value="06">June</option>
                                                <option @if($media->month == "07") {{ "selected" }} @endif value="07">July</option>
                                                <option @if($media->month == "08") {{ "selected" }} @endif value="08">August</option>
                                                <option @if($media->month == "09") {{ "selected" }} @endif value="09">September</option>
                                                <option @if($media->month == "10") {{ "selected" }} @endif value="10">October</option>
                                                <option @if($media->month == "11") {{ "selected" }} @endif value="11">November</option>
                                                <option @if($media->month == "12") {{ "selected" }} @endif value="12">December</option>
                                            </select></div>
                                        <div class="author-combobox-wrap author-form-field-alt-year">
                                            <select type="text" name="date_year" class="form-control" data-type="year">
                                                <option value="" selected="">Year</option>
                                                @for ($i = date('Y'); $i >=1898 ; $i--)
                                                    <option @if($media->year == $i) {{ "selected" }} @endif value="{{ $i }}">{{ $i }}</option>
                                                @endfor
                                            </select></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>




                            <input type="hidden" name="edit_media" value="ok">
                            <button type="submit" class="btn_1 green pull-right">
                                Save
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
