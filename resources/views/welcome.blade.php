<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Deposit as Deposit;
use App\Bank as Bank;

class DepositController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    private function mb_str_split($string)
    {
        # Split at all position not after the start: ^
        # and not before the end: $
        return preg_split('/(?<!^)(?!$)/u', $string);
    }

    private function create_english($title)
    {
        $charlist = $this->mb_str_split($title);
        $array_hayeren = array('.', 'ա', 'բ', 'գ', 'դ', 'ե', 'զ', 'է', 'ը', 'թ', 'ժ', 'ի', 'լ', 'խ', 'ծ', 'կ', 'հ', 'ձ', 'ղ', 'ճ', 'մ', 'յ', 'ն', 'շ', 'ո', 'չ', 'պ', 'ջ', 'ռ', 'ս', 'վ', 'տ', 'ր', 'ց', 'ւ', 'փ', 'ք', 'և', 'օ', 'ֆ', ' ', '  ', '/', '\\', '&', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0');
        $array_english = array('-', 'a', 'b', 'g', 'd', 'e', 'z', 'e', 'y', 't', 'zh', 'i', 'l', 'kh', 'ts', 'k', 'h', 'dz', 'gh', 'ch', 'm', 'y', 'n', 'sh', 'o', 'ch', 'p', 'j', 'r', 's', 'v', 't', 'r', 'c', 'u', 'p', 'q', 'ev', 'o', 'f', '-', '-', '-', '-', '-', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0');
        $new = '';
        if (count($charlist) < 41) {
            $counts = count($charlist);
        } else {
            $counts = 40;
        }
        for ($i = 0; $i < $counts; $i++) {
            $key = array_search(mb_strtolower($charlist[$i], 'UTF-8'), $array_hayeren);
            $new .= $array_english[$key];

        }
        return $new;
    }

    private function create_slug($title, $id_obj)
    {

        $slug = $this->create_english($title);
        $article_isset_slug = DB::table('deposits')->where('slug', '=', $slug)->orderBy('id', 'desc')->count();
        if ($article_isset_slug > 0) {
            $new_slug = $slug . "-" . $id_obj;
        } else {
            $new_slug = $slug;
        }

        return $new_slug;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


        if (isset($request->search_currency)) {
            $current_curr = $request->search_currency;
            $deposits = DB::table('deposit_rating')->where('currency', '=', $request->search_currency)->orderBy('id', 'desc')->get();
        } else {
            $current_curr = 'AMD';
            $deposits = DB::table('deposit_rating')->where('currency', '=', 'AMD')->orderBy('id', 'desc')->get();
        }

        $loan_config = DB::table('loan_config')->where('id', 3)->first();
        return view('admin.deposit.all', compact('current_curr', 'loan_config', 'deposits', 'banks', 'search_cat_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $banks = Bank::all();
        return view('admin.deposit.create', compact('banks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate the form data
        $this->validate($request, [
            'title' => 'required',
            'bank_id' => 'required',
        ]);

        $title = $request->title;

        $bank_id = $request->bank_id;
        $bank_info = DB::table('banks')->where('id', $bank_id)->first();
        $bank_img = url('/') . "/public/uploads/banks/" . $bank_info->image;
        $bank_name = $bank_info->name;
        if ($request->deposit_amd_true == 1) {
            $deposit_amd = $request->deposit_amd;
            $id = DB::table('deposit_rating')->insertGetId(
                [
                    'name' => $request->title,
                    'description' => $request->description,
                    'currency' => "AMD",
                    'bank_img' => $bank_img,
                    'bank_name' => $bank_name,
                    'tokos' => $deposit_amd,
                    'grab' => 0

                ]
            );

            $user_activity_news = '<a target="_blank" href="' . url('/') . '/admin/deposit/' . $id . '/edit">' . $request->title . '</a>';
            DB::table('user_activity')->insert(
                [
                    'user_name' => \Auth::user()->name,
                    'post_name' => $user_activity_news,
                    'type' => 'publish',
                ]
            );


        }
        if ($request->deposit_usd_true == 1) {
            $deposit_usd = $request->deposit_usd;
            $id = DB::table('deposit_rating')->insertGetId(
                [
                    'name' => $request->title,
                    'description' => $request->description,
                    'currency' => "USD",
                    'bank_img' => $bank_img,
                    'bank_name' => $bank_name,
                    'tokos' => $deposit_usd,
                    'grab' => 0

                ]
            );

            $user_activity_news = '<a target="_blank" href="' . url('/') . '/admin/deposit/' . $id . '/edit">' . $request->title . '</a>';
            DB::table('user_activity')->insert(
                [
                    'user_name' => \Auth::user()->name,
                    'post_name' => $user_activity_news,
                    'type' => 'publish',
                ]
            );
        }
        if ($request->deposit_eur_true == 1) {
            $deposit_eur = $request->deposit_eur;
            $id = DB::table('deposit_rating')->insertGetId(
                [
                    'name' => $request->title,
                    'description' => $request->description,
                    'currency' => "EUR",
                    'bank_img' => $bank_img,
                    'bank_name' => $bank_name,
                    'tokos' => $deposit_eur,
                    'grab' => 0

                ]
            );

            $user_activity_news = '<a target="_blank" href="' . url('/') . '/admin/deposit/' . $id . '/edit">' . $request->title . '</a>';
            DB::table('user_activity')->insert(
                [
                    'user_name' => \Auth::user()->name,
                    'post_name' => $user_activity_news,
                    'type' => 'publish',
                ]
            );
        }
        if ($request->deposit_rub_true == 1) {
            $deposit_rub = $request->deposit_rub;
            $id = DB::table('deposit_rating')->insertGetId(
                [
                    'name' => $request->title,
                    'description' => $request->description,
                    'currency' => "RUB",
                    'bank_img' => $bank_img,
                    'bank_name' => $bank_name,
                    'tokos' => $deposit_rub,
                    'grab' => 0

                ]
            );

            $user_activity_news = '<a target="_blank" href="' . url('/') . '/admin/deposit/' . $id . '/edit">' . $request->title . '</a>';
            DB::table('user_activity')->insert(
                [
                    'user_name' => \Auth::user()->name,
                    'post_name' => $user_activity_news,
                    'type' => 'publish',
                ]
            );
        } else {
            $deposit_rub = '0';
        }


        return redirect(route('deposit.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $deposit = DB::table('deposit_rating')->where('id', $id)->first();

        $current_bank = DB::table('banks')->where('name', '=', $deposit->bank_name)->first();
        $current_bank_id = $current_bank->id;
        $banks = Bank::all();
        return view('admin.deposit.edit', compact('deposit', 'banks', 'current_bank_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Validate the form data
        $this->validate($request, [
            'title' => 'required',
            'bank_id' => 'required',
        ]);

        $title = $request->title;


        $bank_id = $request->bank_id;
        $bank_info = DB::table('banks')->where('id', $bank_id)->first();
        $bank_img = url('/') . "/public/uploads/banks/" . $bank_info->image;
        $bank_name = $bank_info->name;
        $deposit = DB::table('deposit_rating')->where('id', $id)->first();
        if ($request->deposit_amd_true == 1) {

            if ($deposit->currency == "AMD") {
                $deposit_amd = $request->deposit_amd;
                DB::table('deposit_rating')
                    ->where('id', $id)
                    ->update(
                        [
                            'name' => $request->title,
                            'description' => $request->description,
                            'currency' => "AMD",
                            'bank_img' => $bank_img,
                            'bank_name' => $bank_name,
                            'tokos' => $deposit_amd,
                            'grab' => 0

                        ]
                    );

                $user_activity_news = '<a target="_blank" href="' . url('/') . '/admin/deposit/' . $id . '/edit">' . $request->title . '</a>';
                DB::table('user_activity')->insert(
                    [
                        'user_name' => \Auth::user()->name,
                        'post_name' => $user_activity_news,
                        'type' => 'update',
                    ]
                );

            } else {
                $deposit_amd = $request->deposit_amd;
                $new_id = DB::table('deposit_rating')->insertGetId(
                    [
                        'name' => $request->title,
                        'description' => $request->description,
                        'currency' => "AMD",
                        'bank_img' => $bank_img,
                        'bank_name' => $bank_name,
                        'tokos' => $deposit_amd,
                        'grab' => 0

                    ]
                );

                $user_activity_news = '<a target="_blank" href="' . url('/') . '/admin/deposit/' . $new_id . '/edit">' . $request->title . '</a>';
                DB::table('user_activity')->insert(
                    [
                        'user_name' => \Auth::user()->name,
                        'post_name' => $user_activity_news,
                        'type' => 'publish',
                    ]
                );

            }
        }
        if ($request->deposit_usd_true == 1) {
            $deposit_usd = $request->deposit_usd;
            if ($deposit->currency == "USD") {

                DB::table('deposit_rating')
                    ->where('id', $id)
                    ->update(
                        [
                            'name' => $request->title,
                            'description' => $request->description,
                            'currency' => "USD",
                            'bank_img' => $bank_img,
                            'bank_name' => $bank_name,
                            'tokos' => $deposit_usd,
                            'grab' => 0

                        ]
                    );

                $user_activity_news = '<a target="_blank" href="' . url('/') . '/admin/deposit/' . $id . '/edit">' . $request->title . '</a>';
                DB::table('user_activity')->insert(
                    [
                        'user_name' => \Auth::user()->name,
                        'post_name' => $user_activity_news,
                        'type' => 'update',
                    ]
                );

            } else {
                $new_id = DB::table('deposit_rating')->insertGetId(
                    [
                        'name' => $request->title,
                        'description' => $request->description,
                        'currency' => "USD",
                        'bank_img' => $bank_img,
                        'bank_name' => $bank_name,
                        'tokos' => $deposit_amd,
                        'grab' => 0

                    ]
                );

                $user_activity_news = '<a target="_blank" href="' . url('/') . '/admin/deposit/' . $new_id . '/edit">' . $request->title . '</a>';
                DB::table('user_activity')->insert(
                    [
                        'user_name' => \Auth::user()->name,
                        'post_name' => $user_activity_news,
                        'type' => 'publish',
                    ]
                );

            }

        } else {
        }
        if ($request->deposit_eur_true == 1) {
            $deposit_eur = $request->deposit_eur;
            if ($deposit->currency == "EUR") {

                DB::table('deposit_rating')
                    ->where('id', $id)
                    ->update(
                        [
                            'name' => $request->title,
                            'description' => $request->description,
                            'currency' => "EUR",
                            'bank_img' => $bank_img,
                            'bank_name' => $bank_name,
                            'tokos' => $deposit_eur,
                            'grab' => 0

                        ]
                    );

                $user_activity_news = '<a target="_blank" href="' . url('/') . '/admin/deposit/' . $id . '/edit">' . $request->title . '</a>';
                DB::table('user_activity')->insert(
                    [
                        'user_name' => \Auth::user()->name,
                        'post_name' => $user_activity_news,
                        'type' => 'update',
                    ]
                );

            } else {
                $new_id = DB::table('deposit_rating')->insertGetId(
                    [
                        'name' => $request->title,
                        'description' => $request->description,
                        'currency' => "EUR",
                        'bank_img' => $bank_img,
                        'bank_name' => $bank_name,
                        'tokos' => $deposit_eur,
                        'grab' => 0

                    ]
                );

                $user_activity_news = '<a target="_blank" href="' . url('/') . '/admin/deposit/' . $new_id . '/edit">' . $request->title . '</a>';
                DB::table('user_activity')->insert(
                    [
                        'user_name' => \Auth::user()->name,
                        'post_name' => $user_activity_news,
                        'type' => 'publish',
                    ]
                );

            }
        }

        if ($request->deposit_rub_true == 1) {
            $deposit_rub = $request->deposit_rub;

            if ($deposit->currency == "RUB") {

                DB::table('deposit_rating')
                    ->where('id', $id)
                    ->update(
                        [
                            'name' => $request->title,
                            'description' => $request->description,
                            'currency' => "RUB",
                            'bank_img' => $bank_img,
                            'bank_name' => $bank_name,
                            'tokos' => $deposit_rub,
                            'grab' => 0

                        ]
                    );

                $user_activity_news = '<a target="_blank" href="' . url('/') . '/admin/deposit/' . $id . '/edit">' . $request->title . '</a>';
                DB::table('user_activity')->insert(
                    [
                        'user_name' => \Auth::user()->name,
                        'post_name' => $user_activity_news,
                        'type' => 'update',
                    ]
                );

            } else {
                $new_id = DB::table('deposit_rating')->insertGetId(
                    [
                        'name' => $request->title,
                        'description' => $request->description,
                        'currency' => "RUB",
                        'bank_img' => $bank_img,
                        'bank_name' => $bank_name,
                        'tokos' => $deposit_rub,
                        'grab' => 0

                    ]
                );

                $user_activity_news = '<a target="_blank" href="' . url('/') . '/admin/deposit/' . $new_id . '/edit">' . $request->title . '</a>';
                DB::table('user_activity')->insert(
                    [
                        'user_name' => \Auth::user()->name,
                        'post_name' => $user_activity_news,
                        'type' => 'publish',
                    ]
                );

            }


        }
        return redirect(route('deposit.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('deposits')->where('id', '=', $id)->delete();
    }

    public function depositConfig(Request $request)
    {
        if ($request->show_in_home == 1) {
            $hidden = 1;
        } else {
            $hidden = 0;
        }
        DB::table('loan_config')->where('id', 3)->update(['hidden' => $hidden,]);
        return redirect(route('deposit.index'));
    }
}
